import 'package:dio/dio.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/auth/model/auth_model.dart';

class SessionTokenResponse {
  SessionToken? sessionToken;
  String? error;

  SessionTokenResponse.error(this.error, [this.sessionToken]);
  SessionTokenResponse.token(this.sessionToken, [this.error]);
}

class AuthRepository {
  static Future<SessionTokenResponse> auth(
      {required LoginPassword credentials, String? firebaseToken}) async {
    final dio = Dio()..options.contentType = Headers.formUrlEncodedContentType;

    final data = {'email': credentials.login, 'password': credentials.password};
    if (firebaseToken != null) {
      data['f_token'] = firebaseToken;
    }

    final response = await dio.post(AuthApi.auth, data: data);

    try {
      return SessionTokenResponse.token(SessionToken.fromJson(response.data));
    } on DioError catch (e) {
      return SessionTokenResponse.error(e.message);
    } catch (e) {
      return SessionTokenResponse.error(e.toString());
    }
  }

  static Future<bool> logout(
      {required SessionToken credentials, String? firebaseToken}) async {
    final dio = Dio()..options.contentType = Headers.formUrlEncodedContentType;

    final data = credentials.toJson();
    if (firebaseToken != null) {
      data['f_token'] = firebaseToken;
    }

    final response = await dio.post(AuthApi.logout, data: data);
    return response.statusCode == 200;
  }
}
