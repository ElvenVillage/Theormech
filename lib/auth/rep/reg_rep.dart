import 'dart:io';

import 'package:dio/dio.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/auth/model/auth_model.dart';
import 'package:teormech/auth/model/reg_model.dart';
import 'package:teormech/auth/rep/auth_rep.dart';

typedef OnSendCallback = void Function(int send, int total);

class RegistrationResponse {
  bool success;
  String? error;

  RegistrationResponse.success([this.success = true]);
  RegistrationResponse.error(this.error, [this.success = false]);
}

class RegistrationRepository {
  static Future<RegistrationResponse> registerUser(
      RegistrationData userData) async {
    final dio = Dio()..options.contentType = Headers.formUrlEncodedContentType;
    try {
      var response = await dio.post(AuthApi.reg, data: {
        'name': userData.firstName.controller.text,
        'surname': userData.surname.controller.text,
        'patronymic': userData.patronymic.controller.text,
        'email': userData.email.controller.text,
        'password': userData.password.controller.text,
        'phone': userData.phone.controller.text,
        'group': userData.groupID.controller.text
      });
      final v = response.data.toString();

      if (v.contains('false')) return RegistrationResponse.error('false');
      return RegistrationResponse.success();
    } on DioError catch (e) {
      return RegistrationResponse.error(e.message);
    }
  }

  static Future<SessionTokenResponse> postreg(
      {required String token,
      required String password,
      String? firebaseToken}) async {
    final dio = Dio()..options.contentType = Headers.formUrlEncodedContentType;
    final data = {'token': token, 'password': password};
    if (firebaseToken != null) {
      data['f_token'] = firebaseToken;
    }

    try {
      final response = await dio.post(AuthApi.postreg, data: data);

      final json = response.data;
      return SessionTokenResponse.token(SessionToken.fromJson(json));
    } on DioError catch (e) {
      return SessionTokenResponse.error(e.message);
    }
  }

  static Future<bool> checkEmailAvailable(String? email) async {
    if (email == null) return true;
    final dio = Dio()..options.contentType = Headers.formUrlEncodedContentType;
    final response = await dio.post(AuthApi.checkEmail, data: {'email': email});
    return (!response.data.toString().contains('false'));
  }

  static Future<Map<String, dynamic>> fetchPreparedData(String token) async {
    final dio = Dio()..options.contentType = Headers.formUrlEncodedContentType;

    final response =
        await dio.post(AuthApi.fetchPreparedData, data: {'token': token});

    return response.data;
    //return jsonDecode(response.data.toString());
  }

  static Future<bool> uploadProfileAvatar(SessionToken token, File avatar,
      OnSendCallback? onProgressCallback) async {
    final formData = FormData.fromMap({
      'photo': await MultipartFile.fromFile(avatar.path),
      'session': token.session,
      'token': token.token
    });

    final dio = Dio()..options.contentType = Headers.formUrlEncodedContentType;
    final response = await dio.post(ProfileApi.uploadAvatar,
        data: formData, onSendProgress: onProgressCallback);

    return response.statusCode == 200;
  }

  static Future<bool> editPassword(
      {required SessionToken token,
      required String password,
      required String oldpassword}) async {
    final dio = Dio()..options.contentType = Headers.formUrlEncodedContentType;
    try {
      var response = await dio.post(ProfileApi.editProfile, data: {
        'session': token.session,
        'token': token.token,
        'key': 'password',
        'val': password,
        'add': oldpassword
      });

      return response.statusCode == 200;
    } on DioError {
      return false;
    }
  }

  static Future<bool> editProfile(
      SessionToken token, String param, String value) async {
    final dio = Dio()..options.contentType = Headers.formUrlEncodedContentType;
    try {
      final response = await dio.post(ProfileApi.editProfile, data: {
        'session': token.session,
        'token': token.token,
        'key': param,
        'val': value
      });

      return response.statusCode == 200;
    } on DioError {
      return false;
    }
  }
}
