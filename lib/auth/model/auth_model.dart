import 'package:teormech/auth/model/storage/auth_storage.dart';

class LoginPassword {
  final String login;
  final String password;

  LoginPassword({required this.login, required this.password});

  LoginPassword.fromJson(Map<String, dynamic> json)
      : login = json['login'],
        password = json['password'];
}

class SessionToken {
  final String session;
  final String token;
  final String id;

  SessionToken({required this.session, required this.token, required this.id});

  SessionToken.fromJson(Map<String, dynamic> json)
      : session = json['session'],
        id = json['id'],
        token = json['token'];

  static Future<SessionToken?> fromSecureStorage(AuthStorage storage) async {
    final session = await storage.read(key: 'session');
    final token = await storage.read(key: 'token');
    final id = await storage.read(key: 'id');

    if ((token != null) && (session != null) && (id != null)) {
      return SessionToken(session: session, token: token, id: id);
    } else
      return null;
  }

  Future<void> save(AuthStorage storage) async {
    await Future.wait([
      storage.write(key: 'session', value: session),
      storage.write(key: 'token', value: token),
      storage.write(key: 'id', value: id)
    ]);
  }

  static Future<void> delete(AuthStorage storage) async {
    await Future.wait([
      storage.delete(key: 'session'),
      storage.delete(key: 'token'),
      storage.delete(key: 'id')
    ]);
  }

  Map<String, String> toJson() {
    return {'session': session, 'token': token};
  }

  SessionToken.empty()
      : id = '-2',
        session = '',
        token = '';
}
