import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:string_validator/string_validator.dart';
import 'package:teormech/auth/model/education.dart';
import 'package:teormech/profile/subscription/model/followers_state.dart';

import 'contacts.dart';

enum ValidationStatus { Initial, Invalid, Valid }

enum RegPage { PrimaryData, SecondaryData, PasswordData }
enum RegStatus { Error, Success }

class RegState {
  bool readOnly;
  bool passwordReadOnly;
  RegPage page;
  FetchingStatus status;
  String? error;
  FetchingStatus registerUploadInfoStatus;
  double? progress;

  RegStatus? regStatus;
  RegState(
      {required this.readOnly,
      this.passwordReadOnly = true,
      this.page = RegPage.PrimaryData,
      this.status = FetchingStatus.Loaded,
      this.registerUploadInfoStatus = FetchingStatus.Loaded,
      this.error,
      this.regStatus,
      this.progress});

  RegState copyWith(
      {bool? readOnly,
      String? token,
      RegPage? page,
      bool? passwordReadOnly,
      FetchingStatus? status,
      FetchingStatus? initFetchStatus,
      FetchingStatus? registerUploadInfoStatus,
      String? error,
      RegStatus? regStatus,
      double? progress}) {
    return RegState(
        readOnly: readOnly ?? this.readOnly,
        passwordReadOnly: passwordReadOnly ?? this.passwordReadOnly,
        status: status ?? this.status,
        page: page ?? this.page,
        registerUploadInfoStatus:
            registerUploadInfoStatus ?? this.registerUploadInfoStatus,
        error: error ?? this.error,
        regStatus: regStatus ?? this.regStatus,
        progress: progress ?? this.progress);
  }
}

abstract class RegistrationField {
  static bool isAlpha(String? data) {
    if (data == null) return true;
    var upper = 'АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЫЬЭЮЯ-QWERTYUIOPASDFGHJKLZXCVBNM ';
    var lower = 'абвгдежзийклмнопрстуфхцчшщыьэюя-qwertyuiopasdfghjklzxcvbnm ';

    for (var c in data.runes) {
      var cc = String.fromCharCode(c);
      if (!lower.contains(cc) && !upper.contains(cc)) return false;
    }
    return true;
  }

  TextEditingController controller;
  ValidationStatus _status = ValidationStatus.Initial;

  RegistrationField(this.controller,
      {this.enableInitial = true,
      this.private = false,
      this.enableInitialPassword = true});

  ValidationStatus validate();

  bool get error => _status == ValidationStatus.Invalid;
  bool get valid => _status == ValidationStatus.Valid;

  bool enableInitial;
  bool private;
  bool enableInitialPassword;
}

class StringRegistrationField extends RegistrationField {
  @override
  ValidationStatus validate() {
    if ((controller.text == '') && enableInitial)
      return ValidationStatus.Initial;
    if ((RegistrationField.isAlpha(controller.text.trim())) &&
        (controller.text != '')) return ValidationStatus.Valid;
    return ValidationStatus.Invalid;
  }

  StringRegistrationField(TextEditingController controller,
      {bool enableInitial = true})
      : super(controller, enableInitial: enableInitial) {
    _status = validate();
  }
}

class GroupIDRegistrationField extends RegistrationField {
  @override
  ValidationStatus validate() {
    if ((controller.text == '') && enableInitial)
      return ValidationStatus.Initial;
    var digits = '0123456789/';
    if (controller.text.length != 13) return ValidationStatus.Invalid;

    for (var char in controller.text.characters) {
      if (!digits.contains(char)) return ValidationStatus.Invalid;
    } // 3630103/90001
    if (controller.text.characters.characterAt(7).string != '/')
      return ValidationStatus.Invalid;

    return ValidationStatus.Valid;
  }

  GroupIDRegistrationField(TextEditingController controller,
      {bool enableInitial = true})
      : super(controller, enableInitial: enableInitial) {
    _status = validate();
  }
}

class EmailRegistrationField extends RegistrationField {
  @override
  ValidationStatus validate() {
    if ((controller.text == '') && enableInitial)
      return ValidationStatus.Initial;
    if (!isEmail(controller.text.toLowerCase()))
      return ValidationStatus.Invalid;

    if (isAvailable != null) {
      if (!isAvailable!) return ValidationStatus.Invalid;
    }
    return ValidationStatus.Valid;
  }

  EmailRegistrationField(TextEditingController controller,
      {this.isAvailable, bool enableInitial = true})
      : super(controller, enableInitial: enableInitial) {
    _status = validate();
  }

  bool? isAvailable = true;
}

class PersonalEmailRegistrationField extends RegistrationField {
  PersonalEmailRegistrationField(TextEditingController controller)
      : super(controller) {
    _status = validate();
  }

  @override
  ValidationStatus validate() {
    if ((controller.text == '') && enableInitial)
      return ValidationStatus.Initial;
    if (!isEmail(controller.text.toLowerCase()))
      return ValidationStatus.Invalid;

    return ValidationStatus.Valid;
  }
}

class PhoneRegistrationField extends RegistrationField {
  @override
  ValidationStatus validate() {
    if ((controller.text == '') && enableInitial)
      return ValidationStatus.Initial;

    if ((controller.text != '') &&
        (controller.text.characters.first == '+') &&
        (isNumeric(controller.text.substring(1))))
      return ValidationStatus.Valid;

    if ((controller.text != '') && (isNumeric(controller.text)))
      return ValidationStatus.Valid;

    return ValidationStatus.Invalid;
  }

  PhoneRegistrationField(TextEditingController controller,
      {bool enableInitial = true})
      : super(controller, enableInitial: enableInitial) {
    _status = validate();
  }
}

class StandartRegistrationField extends RegistrationField {
  @override
  ValidationStatus validate() {
    if ((controller.text == '') && enableInitial)
      return ValidationStatus.Initial;

    return ValidationStatus.Valid;
  }

  StandartRegistrationField(TextEditingController controller)
      : super(controller) {
    _status = validate();
  }
}

class PasswordRegistrationField extends RegistrationField {
  @override
  ValidationStatus validate() {
    if (controller.text == '') {
      if (enableInitialPassword)
        return ValidationStatus.Initial;
      else
        return ValidationStatus.Invalid;
    }

    return ValidationStatus.Valid;
  }

  PasswordRegistrationField(TextEditingController controller)
      : super(controller) {
    _status = validate();
  }
}

class ConfirmPasswordRegistrationField extends RegistrationField {
  PasswordRegistrationField password;
  ConfirmPasswordRegistrationField(
      TextEditingController controller, this.password)
      : super(controller) {
    _status = validate();
  }

  @override
  ValidationStatus validate() {
    if (controller.text != password.controller.text) {
      if (password.enableInitialPassword)
        return ValidationStatus.Initial;
      else
        return ValidationStatus.Invalid;
    }
    return ValidationStatus.Valid;
  }
}

class RegistrationData {
  bool isTeacher;
  bool isStudent;
  bool isEmployee;
  late StringRegistrationField surname;
  late StringRegistrationField firstName;
  late StringRegistrationField patronymic;
  late GroupIDRegistrationField groupID;
  late PhoneRegistrationField phone;
  late EmailRegistrationField email;

  late StandartRegistrationField dateOfBirth;
  late StandartRegistrationField city;
  late StandartRegistrationField about;

  late PasswordRegistrationField password;
  late ConfirmPasswordRegistrationField confirmPassword;
  late PasswordRegistrationField oldPassword;

  late PersonalEmailRegistrationField personalEmail;

  late StandartRegistrationField employeeStatus;
  late StandartRegistrationField teacherStatus;

  late List<Contact> contacts;
  late List<Education> education;

  File? avatar;
  String sex;

  bool get firstPageAccepted {
    return ((surname.valid) &&
        (firstName.valid) &&
        (patronymic.valid) &&
        (phone.valid));
  }

  bool get passwordAccepted => ((password.valid) &&
      (confirmPassword.controller.text == password.controller.text));

  RegistrationData({
    required this.isStudent,
    required this.isTeacher,
    required this.isEmployee,
    required TextEditingController surname,
    required TextEditingController firstName,
    required TextEditingController patronymic,
    required TextEditingController groupID,
    required TextEditingController phone,
    required TextEditingController dateOfBirth,
    required TextEditingController city,
    required TextEditingController email,
    required TextEditingController personalEmail,
    required TextEditingController password,
    required TextEditingController confirmPassword,
    required TextEditingController oldPassword,
    required TextEditingController employeeStatus,
    required TextEditingController teacherStatus,
    List<Contact>? contacts,
    List<Education>? education,
    bool enableInitial = true,
    bool enableInitialPassword = true,
    bool? isAvailable,
    bool enableValidation = true,
    this.sex = '1',
    this.avatar,
    required TextEditingController about,
  }) {
    this.surname =
        StringRegistrationField(surname, enableInitial: enableInitial);
    this.firstName =
        StringRegistrationField(firstName, enableInitial: enableInitial);
    this.patronymic =
        StringRegistrationField(patronymic, enableInitial: enableInitial);
    this.groupID =
        GroupIDRegistrationField(groupID, enableInitial: enableInitial);
    this.phone = PhoneRegistrationField(phone, enableInitial: enableInitial);
    this.email = EmailRegistrationField(
      email,
      isAvailable: isAvailable,
      enableInitial: enableInitial,
    );
    this.oldPassword = PasswordRegistrationField(oldPassword);
    this.password = PasswordRegistrationField(password);
    this.personalEmail = PersonalEmailRegistrationField(
      personalEmail,
    );
    this.teacherStatus = StandartRegistrationField(teacherStatus);
    this.employeeStatus = StandartRegistrationField(employeeStatus);
    this.confirmPassword =
        ConfirmPasswordRegistrationField(confirmPassword, this.password);
    this.contacts = contacts ?? [EmptyContact()];
    this.city = StandartRegistrationField(city);
    this.education = education ?? <Education>[];
    this.dateOfBirth = StandartRegistrationField(dateOfBirth);
    this.about = StandartRegistrationField(about);
  }

  RegistrationData copyWith(
      {bool? enableInitial,
      bool? enableInitialPassword,
      List<Contact>? contacts,
      List<Education>? education,
      bool? isAvailable,
      File? avatar,
      bool? isEmployee,
      bool? isTeacher,
      String? sex,
      bool? isStudent}) {
    return RegistrationData(
        isEmployee: isEmployee ?? this.isEmployee,
        isStudent: isStudent ?? this.isStudent,
        isTeacher: isTeacher ?? this.isTeacher,
        surname: surname.controller,
        firstName: firstName.controller,
        patronymic: patronymic.controller,
        groupID: groupID.controller,
        password: password.controller,
        confirmPassword: confirmPassword.controller,
        phone: phone.controller,
        dateOfBirth: dateOfBirth.controller,
        city: city.controller,
        oldPassword: oldPassword.controller,
        email: email.controller,
        contacts: contacts ?? this.contacts,
        education: education ?? this.education,
        avatar: avatar ?? this.avatar,
        sex: sex ?? this.sex,
        about: about.controller,
        teacherStatus: teacherStatus.controller,
        employeeStatus: employeeStatus.controller,
        personalEmail: personalEmail.controller,
        isAvailable: isAvailable ?? this.email.isAvailable,
        enableInitial: enableInitial ?? true,
        enableInitialPassword: enableInitialPassword ?? true);
  }
}
