import 'dart:convert';

import 'package:flutter/material.dart';

enum EducationLevel {
  School,
  Spec,
  UncompletedHigh,
  High,
  Bachelor,
  Master,
  Candidate,
  Doctor
}

class LightEducation {
  final EducationLevel level;
  final String org;
  final String date;
  final String? spec;
  final String? fac;

  static String levelString(EducationLevel level) {
    switch (level) {
      case EducationLevel.School:
        return '';
      case EducationLevel.Spec:
        return 'Специалист';
      case EducationLevel.UncompletedHigh:
        return '';
      case EducationLevel.High:
        return '';
      case EducationLevel.Bachelor:
        return 'Бакалавр';
      case EducationLevel.Master:
        return 'Магистр';
      case EducationLevel.Candidate:
        return 'К.н.';
      case EducationLevel.Doctor:
        return 'Д.н.';
    }
  }

  LightEducation(
      {required this.level,
      required this.org,
      required this.date,
      this.spec,
      this.fac});

  static List<LightEducation> fromJSON({required String json}) {
    var res = <LightEducation>[];

    try {
      var arr = jsonDecode(json);
      for (var ed in arr) {
        switch (ed[Education.levelKey]) {
          case 'school':
            res.add(LightEducation(
                level: EducationLevel.School,
                org: ed[Education.orgKey],
                date: ed[Education.dateKey]));
            break;
          case 'bach':
            res.add(LightEducation(
                level: EducationLevel.Bachelor,
                date: ed[Education.dateKey],
                org: ed[Education.orgKey],
                fac: ed[Education.facKey],
                spec: ed[Education.specKey]));
            break;
          case 'spec':
            res.add(LightEducation(
                level: EducationLevel.Spec,
                org: ed[Education.orgKey],
                date: ed[Education.dateKey],
                fac: ed[Education.facKey],
                spec: ed[Education.specKey]));
            break;

          case 'mast':
            res.add(LightEducation(
                level: EducationLevel.Master,
                org: ed[Education.orgKey],
                date: ed[Education.dateKey],
                fac: ed[Education.facKey],
                spec: ed[Education.specKey]));
            break;

          case 'cand':
            res.add(LightEducation(
                level: EducationLevel.Candidate,
                org: ed[Education.orgKey],
                date: ed[Education.dateKey],
                fac: ed[Education.facKey],
                spec: ed[Education.specKey]));
            break;

          case 'doct':
            res.add(LightEducation(
                level: EducationLevel.Doctor,
                org: ed[Education.orgKey],
                date: ed[Education.dateKey],
                fac: ed[Education.facKey],
                spec: ed[Education.specKey]));
            break;

          case 'uncomp':
            res.add(LightEducation(
                level: EducationLevel.UncompletedHigh,
                org: ed[Education.orgKey],
                date: ed[Education.dateKey],
                fac: ed[Education.facKey],
                spec: ed[Education.specKey]));
            break;

          case 'high':
            res.add(LightEducation(
                level: EducationLevel.High,
                org: ed[Education.orgKey],
                date: ed[Education.dateKey],
                fac: ed[Education.facKey],
                spec: ed[Education.specKey]));
            break;
        }
      }
      return res;
    } catch (e) {
      return res;
    }
  }
}

class Education {
  //non-null параметры, которые есть у всех типов образования
  final TextEditingController orgController;
  final TextEditingController graduationController;
  final EducationLevel level;

  //доп. параметры для высшего образования
  final TextEditingController? facultyController;
  final TextEditingController? specController;

  static const String levelKey = 'key';
  static const String orgKey = 'org';
  static const String dateKey = 'date';

  static const String facKey = 'fac';
  static const String specKey = 'spec';

  void dispose() {
    orgController.dispose();
    graduationController.dispose();
    facultyController?.dispose();
    specController?.dispose();
  }

  static String toJson(List<Education> list) {
    var a = list.map((e) => e.toMap()).toList();
    return jsonEncode(a);
  }

  Map<String, String> toMap() {
    var a = <String, String>{};

    a[orgKey] = orgController.text.trim();
    a[dateKey] = graduationController.text.trim();

    switch (level) {
      case EducationLevel.School:
        a[levelKey] = 'school';
        break;
      case EducationLevel.Spec:
        a[levelKey] = 'spec';
        a[specKey] = specController!.text.trim();
        a[facKey] = facultyController!.text.trim();
        break;
      case EducationLevel.UncompletedHigh:
        a[levelKey] = 'uncomp';
        a[specKey] = specController!.text.trim();
        a[facKey] = facultyController!.text.trim();
        break;
      case EducationLevel.Bachelor:
        a[levelKey] = 'bach';
        a[specKey] = specController!.text.trim();
        a[facKey] = facultyController!.text.trim();
        break;
      case EducationLevel.Master:
        a[levelKey] = 'mast';
        a[specKey] = specController!.text.trim();
        a[facKey] = facultyController!.text.trim();
        break;
      case EducationLevel.Candidate:
        a[levelKey] = 'cand';
        a[specKey] = specController!.text.trim();
        a[facKey] = facultyController!.text.trim();
        break;
      case EducationLevel.Doctor:
        a[levelKey] = 'doct';
        a[specKey] = specController!.text;
        a[facKey] = facultyController!.text.trim();
        break;
      case EducationLevel.High:
        a[levelKey] = 'high';
        a[specKey] = specController!.text.trim();
        a[facKey] = facultyController!.text.trim();
    }

    return a;
  }

  static List<Education> fromJson({required String json}) {
    var res = <Education>[];
    var array = jsonDecode(json);

    for (var record in array) {
      if (record[levelKey] == 'school') {
        res.add(Education.school(
            orgController: TextEditingController(text: record[orgKey]),
            graduationController:
                TextEditingController(text: record[dateKey])));
      }

      if (record[levelKey] == 'bach') {
        res.add(Education.bachelor(
            orgController: TextEditingController(text: record[orgKey]),
            graduationController: TextEditingController(text: record[dateKey]),
            facultyController: TextEditingController(text: record[facKey]),
            specController: TextEditingController(text: record[specKey])));
      }

      if (record[levelKey] == 'spec') {
        res.add(Education.spec(
            orgController: TextEditingController(text: record[orgKey]),
            graduationController: TextEditingController(text: record[dateKey]),
            facultyController: TextEditingController(text: record[facKey]),
            specController: TextEditingController(text: record[specKey])));
      }

      if (record[levelKey] == 'mast') {
        res.add(Education.master(
            orgController: TextEditingController(text: record[orgKey]),
            graduationController: TextEditingController(text: record[dateKey]),
            facultyController: TextEditingController(text: record[facKey]),
            specController: TextEditingController(text: record[specKey])));
      }

      if (record[levelKey] == 'cand') {
        res.add(Education.candidate(
            orgController: TextEditingController(text: record[orgKey]),
            graduationController: TextEditingController(text: record[dateKey]),
            facultyController: TextEditingController(text: record[facKey]),
            specController: TextEditingController(text: record[specKey])));
      }

      if (record[levelKey] == 'doct') {
        res.add(Education.doctor(
            orgController: TextEditingController(text: record[orgKey]),
            graduationController: TextEditingController(text: record[dateKey]),
            facultyController: TextEditingController(text: record[facKey]),
            specController: TextEditingController(text: record[specKey])));
      }

      if (record[levelKey] == 'uncomp') {
        res.add(Education.uncompleted(
            orgController: TextEditingController(text: record[orgKey]),
            graduationController: TextEditingController(text: record[dateKey]),
            facultyController: TextEditingController(text: record[facKey]),
            specController: TextEditingController(text: record[specKey])));
      }

      if (record[levelKey] == 'high') {
        res.add(Education.high(
            orgController: TextEditingController(text: record[orgKey]),
            graduationController: TextEditingController(text: record[dateKey]),
            facultyController: TextEditingController(text: record[facKey]),
            specController: TextEditingController(text: record[specKey])));
      }
    }

    return res;
  }

  Education(
      {required this.orgController,
      required this.graduationController,
      required this.level,
      this.facultyController,
      this.specController});

  Education.school(
      {required this.orgController, required this.graduationController})
      : level = EducationLevel.School,
        facultyController = null,
        specController = null;

  Education.bachelor(
      {required this.orgController,
      required this.graduationController,
      required this.facultyController,
      required this.specController})
      : level = EducationLevel.Bachelor;

  Education.master(
      {required this.orgController,
      required this.graduationController,
      required this.facultyController,
      required this.specController})
      : level = EducationLevel.Master;

  Education.candidate(
      {required this.orgController,
      required this.graduationController,
      required this.facultyController,
      required this.specController})
      : level = EducationLevel.Candidate;

  Education.doctor(
      {required this.orgController,
      required this.graduationController,
      required this.facultyController,
      required this.specController})
      : level = EducationLevel.Doctor;

  Education.spec(
      {required this.orgController,
      required this.graduationController,
      required this.facultyController,
      required this.specController})
      : level = EducationLevel.Spec;

  Education.uncompleted(
      {required this.orgController,
      required this.graduationController,
      required this.facultyController,
      required this.specController})
      : level = EducationLevel.UncompletedHigh;

  Education.high(
      {required this.orgController,
      required this.graduationController,
      required this.facultyController,
      required this.specController})
      : level = EducationLevel.High;
}
