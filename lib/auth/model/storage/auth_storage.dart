import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hive/hive.dart';

abstract class AuthStorage {
  Future<String?> read({required String key});
  Future<void> write({required String key, required String value});
  Future<void> delete({required String key});
}

class AuthMobileStorage extends AuthStorage {
  final _storage = FlutterSecureStorage();
  @override
  Future<void> delete({required String key}) {
    return _storage.delete(key: key);
  }

  @override
  Future<String?> read({required String key}) {
    return _storage.read(key: key);
  }

  @override
  Future<void> write({required String key, required String value}) {
    return _storage.write(key: key, value: value);
  }
}

class AuthWebStorage extends AuthStorage {
  @override
  Future<void> delete({required String key}) async {
    var box = await Hive.openBox('token');
    box.delete(key);
  }

  @override
  Future<String?> read({required String key}) async {
    var box = await Hive.openBox('token');
    return box.get(key);
  }

  @override
  Future<void> write({required String key, required String value}) async {
    var box = await Hive.openBox('token');
    box.put(key, value);
  }
}
