import 'dart:convert';

import 'package:flutter/material.dart';

enum SocialNetwork { Vk, Telegram, Empty, Email }

extension SocialNetworkToReadable on SocialNetwork {
  String prefixUrl() {
    switch (this) {
      case SocialNetwork.Email:
        return 'mailto:';

      case SocialNetwork.Vk:
        return 'https://vk.com/';

      case SocialNetwork.Telegram:
        return 'https://t.me/';

      case SocialNetwork.Empty:
        return '';
    }
  }
}

class LightContact {
  final SocialNetwork network;
  final String url;
  final String res;

  LightContact({required this.network, required this.url, required this.res});

  String get fullUrl => network.prefixUrl() + this.url;

  static List<LightContact> fromJSON({required String json}) {
    var res = <LightContact>[];
    try {
      var arr = jsonDecode(json);

      for (var contact in arr) {
        var soc = SocialNetwork.Empty;
        var img = 'images/stud.png';
        var url = '';
        switch (contact['soc']) {
          case 'email':
            soc = SocialNetwork.Email;
            img = 'images/icons/mail.png';
            url = contact["text"];
            break;
          case 'VK':
            soc = SocialNetwork.Vk;
            img = 'images/vk.png';
            url = contact["text"];
            break;
          case 'Telegram':
            soc = SocialNetwork.Telegram;
            img = 'images/tg.png';
            url = contact["text"];
            break;
        }
        res.add(LightContact(network: soc, url: url, res: img));
      }
      return res;
    } catch (e) {
      return <LightContact>[];
    }
  }
}

abstract class Contact {
  SocialNetwork socialNetwork;
  final TextEditingController controller;

  Contact({required this.controller, this.socialNetwork = SocialNetwork.Empty});

  String get avatar => '';

  static List<Contact> fromJson({required String json}) {
    var array = jsonDecode(json);
    var res = <Contact>[];

    for (var item in array) {
      if (item['soc'] == 'email')
        res.add(EmailContact(
            controller: TextEditingController(text: item['text'])));

      if (item['soc'] == 'VK')
        res.add(
            VkContact(controller: TextEditingController(text: item['text'])));

      if (item['soc'] == 'Telegram')
        res.add(TelegramContact(
            controller: TextEditingController(text: item['text'])));

      if (item['soc'] == '') res.add(EmptyContact());
    }
    return res;
  }

  static String toJson({required List<Contact> contacts}) {
    var a = <Map<String, String>>[];

    for (var contact in contacts) {
      var soc = '';
      switch (contact.socialNetwork) {
        case SocialNetwork.Empty:
          soc = '';
          break;
        case SocialNetwork.Vk:
          soc = 'VK';
          break;
        case SocialNetwork.Telegram:
          soc = 'Telegram';
          break;

        case SocialNetwork.Email:
          soc = 'email';
          break;
      }
      a.add({'soc': soc, 'text': contact.controller.text.trim()});
    }

    return jsonEncode(a);
  }
}

class VkContact extends Contact {
  @override
  get avatar => 'images/vk.png';

  VkContact({required TextEditingController controller})
      : super(controller: controller, socialNetwork: SocialNetwork.Vk);
}

class EmailContact extends Contact {
  EmailContact({required TextEditingController controller})
      : super(controller: controller, socialNetwork: SocialNetwork.Email);
}

class TelegramContact extends Contact {
  @override
  get avatar => 'images/tg.png';

  TelegramContact({required TextEditingController controller})
      : super(controller: controller, socialNetwork: SocialNetwork.Telegram);
}

class EmptyContact extends Contact {
  @override
  get avatar => 'images/pensil.png';

  EmptyContact() : super(controller: TextEditingController());
}
