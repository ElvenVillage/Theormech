import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/bloc/reg_cubit.dart';
import 'package:teormech/auth/model/reg_model.dart';
import 'package:teormech/generated/l10n.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/clipped_avatar.dart';

class StudentScreenCap extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Center(
            child: Padding(
                padding: EdgeInsets.only(top: 20),
                child: Column(children: [
                  BlocBuilder<RegistrationDataCubit, RegistrationData>(
                      bloc: context.read<RegistrationDataCubit>(),
                      builder: (context, state) {
                        return UserAvatar.fromFile(
                          size: MediaQuery.of(context).size.width / 3,
                          avatarFile: state.avatar,
                          showEditButton: true,
                          onTapCallback: () async {
                            final pickedFile = await UserAvatar.selectFile();

                            if (pickedFile != null)
                              BlocProvider.of<RegistrationDataCubit>(context)
                                  .setAvatar(pickedFile);
                          },
                        );
                      }),
                  SizedBox(
                    height: 10.0,
                  ),
                  BlocBuilder<RegistrationCubit, RegState>(
                      builder: (context, state) {
                    if (state.page != RegPage.PrimaryData)
                      return Column(
                        children: [
                          Text(
                              (state.page == RegPage.SecondaryData)
                                  ? 'Введите дополнительную информацию'
                                  : 'Введите пароль',
                              textAlign: TextAlign.center,
                              style: h1TextStyle.copyWith(
                                color: Colors.white,
                                fontSize: 16,
                              )),
                          Text(
                              '${(state.page == RegPage.SecondaryData) ? "Её" : "Его"} можно будет изменить в настройках профиля',
                              textAlign: TextAlign.center,
                              style: h1TextStyle.copyWith(
                                  color: Colors.white, fontSize: 14))
                        ],
                      );

                    return Column(
                      children: [
                        Text(S.of(context).register_button_main_screen,
                            textAlign: TextAlign.center,
                            style: h1TextStyle.copyWith(
                                color: Colors.white, fontSize: 20)),
                        Text(S.of(context).input_your_data,
                            style: h1TextStyle.copyWith(
                                color: Colors.white, fontSize: 14))
                      ],
                    );
                  }),
                ]))));
  }
}
