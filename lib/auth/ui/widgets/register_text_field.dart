import 'package:flutter/material.dart';
import 'package:teormech/styles/text.dart';

class RegisterTextField extends StatelessWidget {
  final TextEditingController controller;
  final String? errorText;
  final String? hint;
  final bool? readOnly;
  final Widget? suffix;
  final TextInputType? type;
  final int? maxLines;

  RegisterTextField(
      {required this.controller,
      this.errorText,
      this.hint,
      this.type = TextInputType.text,
      this.maxLines,
      this.readOnly = false,
      this.suffix});
  @override
  Widget build(BuildContext context) {
    return TextField(
      style: h1TextStyle.copyWith(fontSize: 16),
      controller: controller,
      enabled: !(readOnly ?? false),
      readOnly: readOnly ?? false,
      maxLines: maxLines,
      keyboardType: type,
      obscureText: type == TextInputType.visiblePassword,
      decoration: InputDecoration(
        isDense: true,
        isCollapsed: (errorText == null),
        contentPadding: const EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
        errorText: (errorText != null) ? "" : null, //errorText,
        errorStyle: TextStyle(height: 0),
        suffixIcon: suffix,

        filled: true,
        hintText: hint,
        hintStyle: TextStyle(fontSize: 14),
        fillColor: Colors.white,
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(32.0),
            borderSide: BorderSide(color: Colors.grey.shade400)),
      ),
    );
  }
}
