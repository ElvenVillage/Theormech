import 'package:flutter/material.dart';
import 'package:teormech/auth/ui/widgets/register_text_field.dart';
import 'package:teormech/styles/text.dart';

class InputContainer extends StatelessWidget {
  final String caption;
  final TextEditingController controller;
  final String? error;
  final String? hint;
  final bool? readOnly;
  final TextInputType type;
  final Widget? suffix;
  final double? rightPadding;
  final Widget? rightSuffix;
  final double middlePadding;
  final int? maxLines;
  final bool leftAlign;

  final Widget? dropdownSearch;
  InputContainer(
      {this.caption = '',
      required this.controller,
      this.error,
      this.hint,
      this.leftAlign = false,
      this.type = TextInputType.text,
      this.readOnly = false,
      this.rightSuffix,
      this.middlePadding = 0,
      this.maxLines,
      this.rightPadding = 0,
      this.dropdownSearch,
      this.suffix});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return IntrinsicHeight(
        child: Container(
      color: Colors.transparent,
      child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: leftAlign ? MainAxisSize.max : MainAxisSize.min,
          children: [
            if (!leftAlign) Spacer(),
            Text(
              caption,
              style: h1TextStyle.copyWith(fontSize: 14),
              softWrap: true,
            ),
            if (middlePadding != 0)
              SizedBox(
                width: middlePadding,
              ),
            Padding(
              padding: const EdgeInsets.only(left: 5),
              child: SizedBox(
                  width: size.width / 2,
                  //height: 20,
                  child: (dropdownSearch == null)
                      ? RegisterTextField(
                          controller: controller,
                          errorText: error,
                          hint: hint,
                          type: type,
                          suffix: suffix,
                          maxLines: type == TextInputType.visiblePassword
                              ? 1
                              : maxLines,
                          readOnly: readOnly)
                      : dropdownSearch),
            ),
            SizedBox(
              width: (rightPadding == 0) ? size.width / 6 : rightPadding,
              child: rightSuffix,
            )
          ]),
    ));
  }
}
