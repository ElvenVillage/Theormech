import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:teormech/auth/ui/widgets/register_text_field.dart';

class ContactField extends StatelessWidget {
  final TextEditingController controller;
  final String? error;
  final String? hint;
  final Widget suffix;
  final bool isLast;
  final bool isFirst;
  final bool isDisposable;
  final bool readOnly;
  final Function? plusCallback;
  final Function? minusCallback;
  ContactField(
      {required this.controller,
      this.error,
      this.hint,
      this.isFirst = false,
      required this.suffix,
      this.isLast = false,
      this.isDisposable = false,
      this.readOnly = false,
      this.plusCallback,
      this.minusCallback});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return IntrinsicHeight(
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
          (!isDisposable)
              ? Expanded(
                  child: (isFirst)
                      ? Text(
                          'Контакты',
                          textAlign: TextAlign.end,
                        )
                      : SizedBox.shrink(),
                )
              : Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      SizedBox(
                        width: 24,
                        child: IconButton(
                          icon: Icon(Icons.remove),
                          padding: const EdgeInsets.only(
                              right: 12, bottom: 8, top: 8),
                          onPressed: () {
                            if (minusCallback != null) minusCallback!();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
          Padding(
            padding: const EdgeInsets.only(left: 5),
            child: SizedBox(
              width: size.width / 2,
              child: IntrinsicHeight(
                  child: Stack(alignment: Alignment.centerRight, children: [
                RegisterTextField(
                  controller: controller,
                  errorText: error,
                  readOnly: readOnly,
                  hint: hint,
                ),
                suffix
              ])),
            ),
          ),
          (!isLast)
              ? SizedBox(
                  width: size.width / 9,
                )
              : SizedBox(
                  width: size.width / 9,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: IconButton(
                      icon: Icon(
                        CupertinoIcons.plus,
                        size: 20,
                      ),
                      onPressed: () {
                        if (plusCallback != null) plusCallback!();
                      },
                    ),
                  ),
                )
        ]));
  }
}
