import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'input_container.dart';

class CalendarInput extends StatelessWidget {
  final TextEditingController controller;
  final Size size;
  final bool? error;
  final Widget? rightSuffix;
  final String caption;

  final DateTime? firstDate;
  final DateTime? lastDate;

  final bool readOnly;

  final DatePickerMode calendarMod;

  CalendarInput(
      {required this.controller,
      required this.size,
      this.rightSuffix,
      required this.firstDate,
      required this.lastDate,
      this.caption = 'Дата рождения',
      required this.readOnly,
      this.calendarMod = DatePickerMode.day,
      this.error});

  @override
  Widget build(BuildContext context) {
    return InputContainer(
        controller: controller,
        caption: caption,
        rightSuffix: rightSuffix,
        rightPadding: size.width / 9,
        error: (error ?? false) ? 'Заполните поле' : null,
        suffix: IconButton(
            padding: EdgeInsets.only(right: 10),
            icon: Icon(CupertinoIcons.calendar),
            iconSize: 29,
            onPressed: () async {
              if (readOnly) return;
              FocusScope.of(context).requestFocus(FocusNode());

              var date = await showDatePicker(
                  context: context,
                  firstDate: firstDate ?? DateTime(2019),
                  lastDate: lastDate ?? DateTime(2030),
                  initialEntryMode: DatePickerEntryMode.input,
                  initialDatePickerMode: calendarMod,
                  initialDate: DateTime.now());
              controller.text = date.toString().substring(0, 10);
            }),
        readOnly: readOnly);
  }
}
