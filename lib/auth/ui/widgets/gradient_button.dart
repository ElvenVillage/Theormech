import 'package:flutter/material.dart';
import 'package:teormech/styles/text.dart';

class GradientIconButton extends StatelessWidget {
  final Color? from;
  final Color? to;
  final Color? color;
  final bool large;

  final VoidCallback onTapCallback;
  final String? text;
  final Icon? icon;

  final ImageIcon? imageIcon;

  GradientIconButton(
      {this.from,
      this.to,
      this.color,
      required this.onTapCallback,
      this.icon,
      this.large = false,
      this.imageIcon,
      this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.0),
          color: color != null ? color : null,
          gradient: color == null
              ? LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [from!, to!])
              : null),
      child: MaterialButton(
        padding: const EdgeInsets.only(left: 10.0),
        onPressed: onTapCallback,
        child: Row(
          children: [
            if (icon != null)
              Padding(
                child: icon,
                padding: EdgeInsets.only(bottom: 2, left: 4),
              )
            else
              imageIcon!,
            if (icon == null || text == null)
              SizedBox(
                width: 5,
              )
            else
              SizedBox(
                width: 10,
              ),
            if (text != null)
              Text(text!,
                  textAlign: TextAlign.left,
                  style: profileTextStyle.copyWith(
                    fontSize: 10,
                  )),
            if (icon == null)
              SizedBox(
                width: 20,
              )
            else
              SizedBox(
                width: 10,
              )
          ],
        ),
      ),
    );
  }
}

class GradientMaterialButton extends StatelessWidget {
  final VoidCallback? onTapCallback;
  final String text;
  final Color? from;
  final Color? to;
  final Color? color;

  final double padding;
  final TextStyle? style;

  GradientMaterialButton(
      {this.onTapCallback,
      required this.text,
      this.from,
      this.to,
      this.color,
      this.padding = 0,
      this.style});
  @override
  Widget build(BuildContext context) {
    return PhysicalModel(
        color: Colors.transparent,
        elevation: 5.0,
        borderRadius: BorderRadius.circular(30.0),
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.0),
              color: color != null ? color : null,
              gradient: from != null
                  ? LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [from!, to!])
                  : null),
          child: MaterialButton(
            minWidth: 150,
            padding: EdgeInsets.fromLTRB(padding, 0, padding, 0),
            onPressed: () {
              if (onTapCallback != null) onTapCallback!();
            },
            child: Text(text,
                textAlign: TextAlign.center,
                style: style ??
                    h1TextStyle.copyWith(
                        color: Colors.white, fontWeight: FontWeight.bold)),
          ),
        ));
  }
}
