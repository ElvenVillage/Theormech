import 'package:flutter/material.dart';
import 'package:teormech/generated/l10n.dart';
import 'package:teormech/styles/text.dart';

class ChoiceScreenCap extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
        child: Center(
            child: Padding(
                padding: EdgeInsets.only(top: 20),
                child: Column(children: [
                  Hero(
                      tag: 'main-logo-splash',
                      child: Image(
                        image: AssetImage('images/logo.png'),
                        width: size.width / 3,
                      )),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    S.of(context).register_button_main_screen,
                    style:
                        h1TextStyle.copyWith(color: Colors.white, fontSize: 24),
                  )
                ]))));
  }
}
