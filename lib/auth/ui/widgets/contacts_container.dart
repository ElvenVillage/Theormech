import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/bloc/reg_cubit.dart';
import 'package:teormech/auth/model/contacts.dart';
import 'package:teormech/auth/model/reg_model.dart';
import 'package:teormech/auth/ui/widgets/contact_field.dart';
import 'package:teormech/widgets/modal_point.dart';

class ContactsContainer extends StatefulWidget {
  final bool readOnly;
  ContactsContainer({required this.readOnly});
  @override
  _ContactsContainerState createState() => _ContactsContainerState();
}

class _ContactsContainerState extends State<ContactsContainer> {
  Map<int, Key> keys = {};

  late RegistrationDataCubit cubit;

  @override
  void initState() {
    cubit = BlocProvider.of<RegistrationDataCubit>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegistrationDataCubit, RegistrationData>(
        bloc: cubit,
        builder: (context, state) {
          Map<int, Contact> filteredContacts = Map.from(state.contacts.asMap());
          filteredContacts.removeWhere((key, value) => value is EmailContact);

          return Column(
              children: filteredContacts
                  .map((int i, c) {
                    var suffix = Padding(
                      padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width / 3),
                      child: TextButton(
                          child: Image(
                            image: AssetImage(c.avatar),
                            height: 24,
                            width: 24,
                          ),
                          onPressed: () {
                            if (!widget.readOnly)
                              showModalBottomSheet(
                                  context: context,
                                  builder: (context) {
                                    return Container(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              3,
                                      child: Column(
                                        children: [
                                          ModalPoint(
                                            image: 'images/vk.png',
                                            text: 'VK',
                                            callback: () {
                                              cubit.updateContact(
                                                  SocialNetwork.Vk, i);
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                          ModalPoint(
                                            image: 'images/tg.png',
                                            text: 'Telegram',
                                            callback: () {
                                              cubit.updateContact(
                                                  SocialNetwork.Telegram, i);
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      ),
                                    );
                                  });
                          }),
                    );

                    if (keys[i] == null) {
                      keys[i] = Key(DateTime.now().toString());
                    }

                    var contactField = ContactField(
                      controller: c.controller,
                      suffix: suffix,
                      isLast: i == filteredContacts.entries.last.key,
                      isFirst: i == 0,
                      readOnly: widget.readOnly,
                      isDisposable: (i != 0),
                      plusCallback: () {
                        if (!widget.readOnly)
                          cubit.appendContact('', SocialNetwork.Empty);
                      },
                      minusCallback: () {
                        if (widget.readOnly) return;
                        cubit.deleteContact(i);
                        keys.removeWhere((key, value) => key == i);
                      },
                    );
                    return MapEntry(
                        i,
                        (i != 0)
                            ? Dismissible(
                                onDismissed: (DismissDirection direction) {
                                  cubit.deleteContact(i);
                                  keys.removeWhere((key, value) => key == i);
                                },
                                direction: DismissDirection.startToEnd,
                                key: keys[i]!,
                                child: contactField,
                              )
                            : contactField);
                  })
                  .values
                  .toList());
        });
  }
}
