import 'package:flutter/material.dart';
import 'package:teormech/profile/chat/ui/chat_list/chat_list_select_bar.dart';

class ChoiceContainerStyle {
  final double elevation;
  final Color selectedColor;
  final FontWeight weight;

  const ChoiceContainerStyle(
      {required this.elevation,
      required this.selectedColor,
      this.weight = FontWeight.bold});

  static ChoiceContainerStyle defaultStyle() => ChoiceContainerStyle(
      elevation: 5, selectedColor: Colors.black, weight: FontWeight.normal);
}

class ChoiceContainer extends StatelessWidget {
  final AssetImage? avatar;
  final String text;
  final bool chosen;
  final VoidCallback onClickCallback;
  final double offset;
  final Widget? widget;

  final ChoiceContainerStyle style;

  ChoiceContainer(
      {this.avatar,
      required this.text,
      required this.chosen,
      required this.onClickCallback,
      this.style = ChatListSelectBar.style,
      this.widget,
      this.offset = 0});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          onClickCallback();
        },
        child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 7),
            child: Container(
                decoration: chosen
                    ? BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(123),
                        border:
                            Border.all(width: 1, color: Colors.grey.shade300))
                    : null,
                padding: (this.offset == 0)
                    ? EdgeInsets.all(10)
                    : EdgeInsets.only(
                        bottom: 10, left: 10 + offset, right: 10, top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    if (avatar != null)
                      Image(
                        image: avatar!,
                        height: 36,
                        width: 36,
                      ),
                    Padding(
                      padding: const EdgeInsets.only(left: 4.0),
                      child: Stack(
                        children: [
                          Padding(
                            padding: widget == null
                                ? EdgeInsets.all(0)
                                : EdgeInsets.only(right: 10),
                            child: Text(
                              text,
                              style: TextStyle(
                                  fontWeight: style.weight,
                                  color: chosen
                                      ? style.selectedColor
                                      : Colors.black),
                            ),
                          ),
                          if (widget != null)
                            Positioned(child: widget!, right: 0, top: 6)
                        ],
                      ),
                    ),
                  ],
                ))));
  }
}
