import 'package:flutter/material.dart';
import 'package:teormech/styles/text.dart';

class LoginTextField extends StatelessWidget {
  final TextEditingController controller;
  final String hint;
  final bool password;
  LoginTextField({
    required this.controller,
    required this.hint,
    required this.password,
  });
  @override
  Widget build(BuildContext context) {
    return TextField(
      obscureText: password,
      style: h1TextStyle,
      controller: controller,
      keyboardType: (password)
          ? TextInputType.visiblePassword
          : TextInputType.emailAddress,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: hint,
          filled: true,
          fillColor: Colors.white,
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
  }
}
