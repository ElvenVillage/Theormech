import 'package:flutter/material.dart';
import 'package:teormech/auth/ui/widgets/gradient_button.dart';
import 'package:teormech/styles/colors.dart';

class NextGroupButtons extends StatelessWidget {
  const NextGroupButtons(
      {Key? key, required this.backCallback, required this.nextCallback})
      : super(key: key);
  final Function backCallback;
  final Function nextCallback;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Spacer(),
        GradientMaterialButton(
          text: 'Назад',
          from: GradientColors.leftColorLeftButton,
          to: GradientColors.rightColorLeftButton,
          onTapCallback: () {
            backCallback();
          },
        ),
        Spacer(),
        GradientMaterialButton(
            text: 'Далее',
            onTapCallback: () {
              nextCallback();
            },
            from: GradientColors.leftColorRightButton,
            to: GradientColors.rightColorRightButton),
        Spacer()
      ],
    );
  }
}
