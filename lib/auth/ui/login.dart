import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/bloc/auth_bloc.dart';
import 'package:teormech/auth/model/auth_model.dart';
import 'package:edge_alert/edge_alert.dart';
import 'package:teormech/auth/ui/forgot/forgot_password_screen.dart';
import 'package:teormech/auth/ui/widgets/gradient_button.dart';
import 'package:teormech/auth/ui/widgets/login_text_field.dart';
import 'package:teormech/generated/l10n.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final loginController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      behavior: HitTestBehavior.translucent,
      child: BlocListener<AuthBloc, AuthState>(
        listener: (context, state) {
          if (state is Authorized) {
            Navigator.of(context, rootNavigator: true)
                .pushReplacementNamed('/profile');
          }
          if (state is Unathorized) {
            EdgeAlert.show(context,
                title: S.of(context).authorization_error,
                icon: Icons.error_outline_rounded,
                backgroundColor: Colors.redAccent,
                duration: EdgeAlert.LENGTH_SHORT,
                description: S.of(context).check_your_credentials_error_message,
                gravity: EdgeAlert.BOTTOM);
          }
        },
        child: Container(
          child: Padding(
            padding: const EdgeInsets.all(36.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Spacer(
                  flex: 2,
                ),
                ConstrainedBox(
                  constraints: const BoxConstraints(maxWidth: 500),
                  child: LoginTextField(
                    controller: loginController,
                    hint: S.of(context).login_hint,
                    password: false,
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                ConstrainedBox(
                  constraints: const BoxConstraints(maxWidth: 500),
                  child: LoginTextField(
                    controller: passwordController,
                    hint: S.of(context).password_hint,
                    password: true,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context, rootNavigator: true).push(
                        MaterialPageRoute(
                            builder: (context) => ForgotPasswordScreen()));
                  },
                  child: Center(
                    child: Text(
                        S.of(context).forgot_password_main_screen_button,
                        style: h1TextStyle.copyWith(
                            color: Colors.white,
                            decoration: TextDecoration.underline)),
                  ),
                ),
                Spacer(
                  flex: 1,
                ),
                Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Container(
                    padding: const EdgeInsets.only(right: 10),
                    height: 40,
                    child: GradientMaterialButton(
                      from: GradientColors.leftColorLeftButton,
                      to: GradientColors.rightColorLeftButton,
                      onTapCallback: () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        Navigator.of(context, rootNavigator: true)
                            .pushNamed('/reg');
                      },
                      text: S.of(context).register_button_main_screen,
                      padding: 20.0,
                    ),
                  ),
                  SizedBox(
                    height: 40,
                    width: 120,
                    child: GradientMaterialButton(
                      from: GradientColors.leftColorRightButton,
                      to: GradientColors.rightColorRightButton,
                      onTapCallback: () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        BlocProvider.of<AuthBloc>(context).add(LoginEvent(
                            credentials: LoginPassword(
                                login: loginController.text,
                                password: passwordController.text)));
                      },
                      text: S.of(context).login_button_main_screen,
                    ),
                  ),
                ]),
                Spacer(
                  flex: 2,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
