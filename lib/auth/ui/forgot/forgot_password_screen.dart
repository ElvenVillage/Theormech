import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/auth/ui/widgets/next_group_buttons.dart';
import 'package:teormech/auth/ui/widgets/register_text_field.dart';
import 'package:teormech/styles/edge_alert_notification.dart';
import 'package:teormech/styles/text.dart';

class ForgotPasswordScreen extends StatefulWidget {
  const ForgotPasswordScreen({Key? key}) : super(key: key);

  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final _controller = TextEditingController();

  Future<bool> _sendToken() async {
    final dio = Dio()..options.contentType = Headers.formUrlEncodedContentType;
    final data = {'email': _controller.text};
    final response = await dio.post(ProfileApi.forgotPassword,
        data: data, options: Options(responseType: ResponseType.plain));
    return (response.data.toString() != 'false' && response.statusCode == 200);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: Image(
              image: AssetImage('images/roles2.png'),
              height: double.infinity,
              fit: BoxFit.cover,
            ),
          ),
          Scaffold(
            backgroundColor: Colors.transparent,
            body: GestureDetector(
              behavior: HitTestBehavior.translucent,
              child: Column(
                children: [
                  Container(
                      child: Stack(children: [
                    Image(
                      image: AssetImage('images/roles.png'),
                      fit: BoxFit.fitWidth,
                    ),
                    SizedBox(
                        height: 200,
                        child: Center(
                          child: Column(
                            children: [
                              Spacer(),
                              Hero(
                                  tag: 'main-logo-splash',
                                  child: Image(
                                    image: AssetImage('images/logo.png'),
                                    height:
                                        MediaQuery.of(context).size.height / 6,
                                  )),
                              Spacer(),
                              Text(
                                'Восстановление пароля',
                                style:
                                    h1TextStyle.copyWith(color: Colors.white),
                              ),
                              Spacer()
                            ],
                          ),
                        )),
                  ])),
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16.0, vertical: 10),
                    child: Column(
                      children: [
                        Text(
                          'Введите адрес корпоративной почты.',
                          style: h2TextStyle,
                        ),
                        Text(
                          'Вам будет необходимо пройти по ссылке, которая придет в письме на указанную Вами почту',
                          style: h2TextStyle,
                          textAlign: TextAlign.center,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 16.0, bottom: 8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                'Эл. почта',
                                style: h2TextStyle.copyWith(fontSize: 14),
                              ),
                              Container(
                                padding: const EdgeInsets.only(left: 8),
                                width: MediaQuery.of(context).size.width * 0.6,
                                child: RegisterTextField(
                                  controller: _controller,
                                  type: TextInputType.emailAddress,
                                ),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                            ],
                          ),
                        ),
                        Spacer(),
                        NextGroupButtons(backCallback: () {
                          Navigator.pop(context);
                        }, nextCallback: () async {
                          final result = await _sendToken();
                          showEdgeAlertNotification(
                              okText: 'Письмо отправлено',
                              errorText: 'Не удалось отправить письмо',
                              context: context,
                              result: result);
                        }),
                        SizedBox(
                          height: 20,
                        )
                      ],
                    ),
                  ))
                ],
              ),
              onTap: () {
                FocusScope.of(context).unfocus();
              },
            ),
          ),
        ],
      ),
    );
  }
}
