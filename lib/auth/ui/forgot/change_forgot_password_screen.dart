import 'dart:convert';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/auth/bloc/auth_bloc.dart';
import 'package:teormech/auth/ui/widgets/next_group_buttons.dart';
import 'package:teormech/auth/ui/widgets/register_text_field.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/clipped_avatar.dart';
import 'package:teormech/styles/edge_alert_notification.dart';
import 'package:teormech/styles/text.dart';

class ChangeForgotPasswordScreen extends StatefulWidget {
  const ChangeForgotPasswordScreen({Key? key, required this.token})
      : super(key: key);

  final String token;

  @override
  _ChageForgotPasswordScreenState createState() =>
      _ChageForgotPasswordScreenState();
}

class _ChageForgotPasswordScreenState
    extends State<ChangeForgotPasswordScreen> {
  Future<String> _fetchUserAvatar() async {
    final dio = Dio()..options.contentType = Headers.formUrlEncodedContentType;
    final data = {'token': widget.token};

    final response = await dio.post(ProfileApi.forgotPasswordInfo,
        data: data, options: Options(responseType: ResponseType.plain));

    final responseData = response.data.toString();
    if (responseData == 'false') {
      Future.delayed(Duration.zero,
          () => context.read<AuthBloc>().add(LogoutEvent(silent: true)));
      return Future.error('error');
    } else
      return jsonDecode(responseData)['photo'];
  }

  Future<bool> _changePassword() async {
    final dio = Dio()..options.contentType = Headers.formUrlEncodedContentType;
    final data = {'token': widget.token, 'password': _confirmPassword.text};

    final response = await dio.post(ProfileApi.changeForgotPassword,
        data: data, options: Options(responseType: ResponseType.plain));

    final responseData = response.data.toString();
    return (responseData != 'false');
  }

  late final Future<String> _userAvatar;

  final _password = TextEditingController();
  final _confirmPassword = TextEditingController();

  bool _isVisiblePassword = false;

  @override
  void initState() {
    _userAvatar = _fetchUserAvatar();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.height / 6;
    return SafeArea(
      child: Stack(
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: Image(
              image: AssetImage('images/roles2.png'),
              height: double.infinity,
              fit: BoxFit.cover,
            ),
          ),
          Scaffold(
            backgroundColor: Colors.transparent,
            body: GestureDetector(
              behavior: HitTestBehavior.translucent,
              child: Column(
                children: [
                  Container(
                      child: Stack(children: [
                    Image(
                      image: AssetImage('images/roles.png'),
                      fit: BoxFit.fitWidth,
                    ),
                    SizedBox(
                        height: 200,
                        child: Center(
                          child: Column(
                            children: [
                              Spacer(),
                              Hero(
                                  tag: 'main-logo-splash',
                                  child: FutureBuilder<String>(
                                      future: _userAvatar,
                                      builder: (context, snapshot) {
                                        if (snapshot.hasData) if (UserAvatarComponentsUtils
                                            .initiailized)
                                          return UserAvatar.fromNetwork(
                                            avatarUrl:
                                                ProfileApi.profileAvatar +
                                                    snapshot.data!,
                                            ignoreFrame: true,
                                            size: width,
                                          );
                                        else
                                          return SizedBox(
                                            width: width,
                                            height: width,
                                          );
                                        if (snapshot.hasError) {
                                          return Container(
                                            child: Text('ban'),
                                          );
                                        } else
                                          return UserAvatar.fromString(
                                            avatarString: 'images/logo.png',
                                            ignoreFrame: true,
                                            online: false,
                                            size: MediaQuery.of(context)
                                                    .size
                                                    .height /
                                                6,
                                          );
                                      })),
                              Spacer(),
                              Text(
                                'Восстановление пароля',
                                style:
                                    h1TextStyle.copyWith(color: Colors.white),
                              ),
                              Spacer()
                            ],
                          ),
                        )),
                  ])),
                  Expanded(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 18.0),
                          child: Text('Восстановление \n пароля',
                              style: h2DrawerGreyText,
                              textAlign: TextAlign.center),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0, bottom: 8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                'Новый пароль',
                                style: h2TextStyle.copyWith(fontSize: 14),
                              ),
                              Container(
                                padding: const EdgeInsets.only(left: 8),
                                width: MediaQuery.of(context).size.width * 0.45,
                                child: RegisterTextField(
                                  maxLines: 1,
                                  controller: _password,
                                  type: _isVisiblePassword
                                      ? null
                                      : TextInputType.visiblePassword,
                                ),
                              ),
                              SizedBox(
                                width: 50,
                                child: IconButton(
                                  icon: ImageIcon(AssetImage(!_isVisiblePassword
                                      ? 'images/icons/visible.png'
                                      : 'images/icons/hide.png')),
                                  onPressed: () {
                                    setState(() {
                                      _isVisiblePassword = !_isVisiblePassword;
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0, bottom: 8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                'Повторите пароль',
                                style: h2TextStyle.copyWith(fontSize: 14),
                              ),
                              Container(
                                padding: const EdgeInsets.only(left: 8),
                                width: MediaQuery.of(context).size.width * 0.45,
                                child: RegisterTextField(
                                  controller: _confirmPassword,
                                  maxLines: 1,
                                  type: _isVisiblePassword
                                      ? null
                                      : TextInputType.visiblePassword,
                                ),
                              ),
                              SizedBox(
                                width: 50,
                              )
                            ],
                          ),
                        ),
                        Spacer(),
                        NextGroupButtons(backCallback: () {
                          Navigator.pushNamedAndRemoveUntil(
                              context, '/', (route) => false);
                          Future.delayed(
                              Duration.zero,
                              () => context
                                  .read<AuthBloc>()
                                  .add(LogoutEvent(silent: true)));
                          ;
                        }, nextCallback: () async {
                          if (_password.text != _confirmPassword.text) {
                            showEdgeAlertNotification(
                                okText: '',
                                errorText: 'Пароли не совпадают',
                                context: context,
                                result: false);
                          } else {
                            final res = await _changePassword();
                            if (res) {
                              Navigator.pushNamedAndRemoveUntil(
                                  context, '/', (route) => false);
                              Future.delayed(
                                  Duration.zero,
                                  () => context
                                      .read<AuthBloc>()
                                      .add(LogoutEvent(silent: true)));
                            } else {
                              showEdgeAlertNotification(
                                  okText: '',
                                  errorText: 'Не удалось изменить пароль',
                                  context: context,
                                  result: false);
                            }
                          }
                        }),
                        SizedBox(
                          height: 20,
                        )
                      ],
                    ),
                  )
                ],
              ),
              onTap: () {
                FocusScope.of(context).unfocus();
              },
            ),
          ),
        ],
      ),
    );
  }
}
