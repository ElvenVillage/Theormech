import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/bloc/auth_bloc.dart';
import 'package:teormech/auth/bloc/reg_cubit.dart';
import 'package:teormech/auth/ui/widgets/student_screen_cap.dart';
import 'regscreens/student_screen.dart';

class RegistrationScreen extends StatelessWidget {
  final String? regToken;
  RegistrationScreen({this.regToken});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(
      builder: (context, authState) {
        return BlocProvider<RegistrationCubit>(
          lazy: false,
          create: (context) => RegistrationCubit(
              authBloc: context.read<AuthBloc>(),
              editProfile: false,
              regToken: regToken),
          child: SafeArea(
              child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomCenter,
                child: Image(
                  image: AssetImage('images/roles2.png'),
                  height: double.infinity,
                  fit: BoxFit.cover,
                ),
              ),
              Scaffold(
                  backgroundColor: Colors.transparent,
                  body: BlocBuilder<AuthBloc, AuthState>(
                      builder: (context, authState) {
                    return BlocProvider<RegistrationDataCubit>(
                      create: (context) => RegistrationDataCubit(
                          regToken: (authState is RegTokenState)
                              ? authState.regToken
                              : null)
                        ..attachListeners(),
                      lazy: false,
                      child: GestureDetector(
                        onTap: () {
                          FocusScope.of(context).requestFocus(FocusNode());
                        },
                        child: Column(
                          children: [
                            Container(
                                child: Stack(children: [
                              Image(
                                image: AssetImage('images/roles.png'),
                                fit: BoxFit.fitWidth,
                              ),
                              SizedBox(height: 200, child: StudentScreenCap()),
                            ])),
                            Expanded(child: StudentScreen())
                          ],
                        ),
                      ),
                    );
                  })),
            ],
          )),
        );
      },
    );
  }
}

class BackButton extends StatelessWidget {
  final Function(BuildContext) handleBackButton;
  BackButton(this.handleBackButton);
  @override
  Widget build(BuildContext context) {
    return Positioned(
      child: IconButton(
        icon: Icon(Icons.arrow_back_rounded),
        onPressed: () {
          context.read<RegistrationCubit>().personalData();
          handleBackButton(context);
        },
        color: Colors.white,
      ),
      left: 7,
      top: 10,
    );
  }
}
