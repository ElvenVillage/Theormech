import 'package:edge_alert/edge_alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/bloc/reg_cubit.dart';
import 'package:teormech/auth/model/reg_model.dart';
import 'package:teormech/widgets/additional_user_info.dart';

class SecondPage extends StatelessWidget {
  final VoidCallback prevPage;
  SecondPage({required this.prevPage});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return BlocConsumer<RegistrationCubit, RegState>(
      listener: (contezt, state) {
        if (state.error != '' && state.error != null)
          EdgeAlert.show(context,
              title: 'Ошибка',
              description: 'Не удалось зарегистрировать пользователя',
              duration: EdgeAlert.LENGTH_SHORT,
              gravity: EdgeAlert.BOTTOM,
              backgroundColor: Colors.red,
              icon: Icons.error);
      },
      builder: (context, state) => SingleChildScrollView(
        child: AdditionalUserInfo(
          size: size,
          editProfile: false,
          prevPage: prevPage,
          regState: state,
          needControl: true,
        ),
      ),
    );
  }
}
