import 'package:edge_alert/edge_alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/bloc/auth_bloc.dart';
import 'package:teormech/auth/bloc/reg_cubit.dart';
import 'package:teormech/auth/model/education.dart';
import 'package:teormech/auth/model/reg_model.dart';
import 'package:teormech/auth/ui/widgets/input_container.dart';
import 'package:teormech/generated/l10n.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/widgets/edit_ok_discard.dart';

class EducationScreen extends StatefulWidget {
  final RegistrationDataCubit cubit;
  final RegistrationCubit regCubit;
  final UserProfileBloc? userCubit;

  final bool appBar;

  EducationScreen(
      {required this.cubit,
      required this.regCubit,
      this.userCubit,
      this.appBar = true});

  @override
  _EducationScreenState createState() => _EducationScreenState();
}

class _EducationScreenState extends State<EducationScreen> {
  var editable = <bool>[];

  String levelCaption(EducationLevel level) {
    switch (level) {
      case EducationLevel.School:
        return 'Среднее образование';
      case EducationLevel.Bachelor:
        return 'Бакалавр';
      case EducationLevel.Master:
        return 'Магистр';
      case EducationLevel.High:
        return 'Высшее образование';
      case EducationLevel.UncompletedHigh:
        return 'Неоконченное высшее';
      case EducationLevel.Candidate:
        return 'Кандидат наук';
      case EducationLevel.Spec:
        return 'Специалист';
      case EducationLevel.Doctor:
        return 'Доктор наук';
      // default:
      //   return 'Дополнительное';
    }
  }

  void initState() {
    editable = widget.cubit.state.education.map((e) => false).toList();
    super.initState();
  }

  Future<void> confirm(BuildContext context) async {
    try {
      var token = (context.read<AuthBloc>().state as Authorized).token;
      var a = await widget.regCubit
          .uploadAdditionalData(token, data: widget.cubit.state);
      //widget.userCubit?.update();
      if (a)
        EdgeAlert.show(context,
            backgroundColor: Colors.teal,
            icon: Icons.verified,
            title: 'Успех',
            description: 'Данные профиля обновлены',
            duration: 1,
            gravity: EdgeAlert.BOTTOM);
      else
        EdgeAlert.show(context,
            icon: Icons.error,
            title: 'Ошибка',
            description: 'Не удалось изменить данные',
            duration: 1,
            gravity: EdgeAlert.BOTTOM,
            backgroundColor: Colors.red);
    } catch (e) {}
  }

  List<Widget> _educationsList(
      Size size, RegistrationData state, RegState regState) {
    var a = state.education
        .asMap()
        .map((int id, Education education) => MapEntry(
            id,
            Container(
              child: Column(
                children: [
                  Row(
                    children: [
                      IconButton(
                        icon: Icon(Icons.remove),
                        onPressed: () async {
                          if (editable[id]) {
                            widget.cubit.deleteEducation(id);
                            editable.removeAt(id);
                            await confirm(context);
                            widget.userCubit?.fetchFromServer(
                                loadFromHive: false, forceReload: true);
                          }
                        },
                      ),
                      Text(levelCaption(education.level),
                          style: h2TextStyle.copyWith(
                              fontWeight: FontWeight.bold,
                              color: Colors.black)),
                      OkDiscard(okCallback: () async {
                        setState(() {
                          editable[id] = false;
                        });
                        await confirm(context);
                        widget.userCubit?.fetchFromServer(
                            loadFromHive: false, forceReload: true);
                        return true;
                      }, editCallback: () async {
                        setState(() {
                          editable[id] = true;
                        });
                        return true;
                      }, cancelCallback: () async {
                        setState(() {
                          editable[id] = false;
                        });

                        return true;
                      })
                    ],
                  ),
                  if (editable[id])
                    InputContainer(
                      controller: TextEditingController(),
                      //readOnly: true,
                      caption: 'Уровень',
                      rightPadding: size.width / 9,
                      middlePadding: 48,
                      suffix: Listener(
                        onPointerDown: (_) =>
                            FocusScope.of(context).requestFocus(FocusNode()),
                        child: Padding(
                            padding: EdgeInsets.only(right: 20),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<EducationLevel>(
                                onChanged: (level) {
                                  widget.cubit.updateEducation(
                                      id: id,
                                      level: level ?? EducationLevel.Spec);
                                },
                                value: state.education[id].level,
                                style: profileTextStyle.copyWith(
                                    fontSize: 12, color: Colors.black),
                                items: [
                                  DropdownMenuItem(
                                      child: Text('Среднее'),
                                      value: EducationLevel.School),
                                  DropdownMenuItem(
                                      child: Text('Среднее специальное'),
                                      value: EducationLevel.Spec),
                                  DropdownMenuItem(
                                      child: Text('Неоконченное высшее'),
                                      value: EducationLevel.UncompletedHigh),
                                  DropdownMenuItem(
                                    child: Text('Высшее'),
                                    value: EducationLevel.High,
                                  ),
                                  DropdownMenuItem(
                                    child: Text('Бакалавр'),
                                    value: EducationLevel.Bachelor,
                                  ),
                                  DropdownMenuItem(
                                      child: Text('Магистр'),
                                      value: EducationLevel.Master),
                                  DropdownMenuItem(
                                      child: Text('Кандидат наук'),
                                      value: EducationLevel.Candidate),
                                  DropdownMenuItem(
                                      child: Text('Доктор наук'),
                                      value: EducationLevel.Doctor)
                                ],
                              ),
                            )),
                      ),
                    ),
                  SizedBox(
                    height: 10,
                  ),
                  InputContainer(
                    controller: education.orgController,
                    caption: 'Учебное \nзаведение',
                    middlePadding: 32,
                    rightPadding: size.width / 9,
                    readOnly: !editable[id],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  if (education.facultyController != null)
                    InputContainer(
                      controller: education.facultyController!,
                      caption: 'Факультет',
                      middlePadding: 32,
                      rightPadding: size.width / 9,
                      readOnly: !editable[id],
                    ),
                  if (education.facultyController != null)
                    SizedBox(
                      height: 10,
                    ),
                  if (education.specController != null)
                    InputContainer(
                      controller: education.specController!,
                      rightPadding: size.width / 9,
                      caption: 'Специализация',
                      middlePadding: 2,
                      readOnly: !editable[id],
                    ),
                  if (education.specController != null)
                    SizedBox(
                      height: 10,
                    ),
                  InputContainer(
                    controller: education.graduationController,
                    readOnly: !editable[id],
                    type: TextInputType.phone,
                    middlePadding: 20,
                    rightPadding: size.width / 9,
                    caption: 'Год выпуска',
                  ),
                ],
              ),
            )))
        .values
        .toList();
    a.add(Container(
        child: Column(
      children: [
        Row(
          children: [
            SizedBox(
              width: 48,
            ),
            Text('Дополнительное образование',
                style: h2TextStyle.copyWith(
                    fontWeight: FontWeight.bold, color: Colors.black)),
            IconButton(
                icon: Icon(Icons.add),
                onPressed: () {
                  setState(() {
                    editable.add(false);
                  });
                  widget.cubit.appendEducation();
                })
          ],
        )
      ],
    )));
    return a;
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
        child: Scaffold(
      backgroundColor: (widget.appBar) ? Colors.transparent : Colors.white,
      appBar: (widget.appBar)
          ? ProfileAppBar(
              title: Padding(
                  padding: EdgeInsets.only(right: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(S.of(context).from_profile_to_edit_profile,
                          style: h1TextStyle.copyWith(
                              color: Colors.white, fontSize: 18)),
                    ],
                  )),
            )
          : AppBar(
              toolbarHeight: 64,
              backgroundColor: GradientColors.backColor,
              title: Padding(
                  padding: EdgeInsets.only(right: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(S.of(context).from_profile_to_edit_profile,
                          style: h1TextStyle.copyWith(
                              color: Colors.white, fontSize: 18)),
                    ],
                  )),
            ),
      body: SingleChildScrollView(
        child: BlocBuilder<RegistrationDataCubit, RegistrationData>(
            bloc: widget.cubit,
            builder: (context, state) {
              return BlocBuilder<RegistrationCubit, RegState>(
                  bloc: widget.regCubit,
                  builder: (context, regState) {
                    return Column(
                        children: _educationsList(size, state, regState));
                  });
            }),
      ),
    ));
  }
}
