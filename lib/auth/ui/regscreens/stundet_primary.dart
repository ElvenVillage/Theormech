import 'package:edge_alert/edge_alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/widgets/primary_user_info.dart';
import '../../bloc/reg_cubit.dart';
import '../../model/reg_model.dart';

class FirstPage extends StatefulWidget {
  final Function nextPage;

  FirstPage({required this.nextPage});
  @override
  _FirstPageState createState() => _FirstPageState(nextPage);
}

class _FirstPageState extends State<FirstPage> {
  final Function nextPage;
  _FirstPageState(this.nextPage);
  bool emailErrorShowed = false;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<RegistrationDataCubit, RegistrationData>(
      listener: (context, state) {
        if ((state.email.isAvailable != null) &&
            (!state.email.isAvailable!) &&
            (!emailErrorShowed)) {
          EdgeAlert.show(context,
              title: 'Ошибка',
              duration: EdgeAlert.LENGTH_SHORT,
              icon: Icons.error_outline_rounded,
              backgroundColor: Colors.redAccent,
              description: 'Данный e-mail уже зарегистрирован',
              gravity: EdgeAlert.BOTTOM);
          emailErrorShowed = true;
        }
      },
      bloc: context.read<RegistrationDataCubit>(),
      builder: (context, state) {
        return ListView(children: [
          PrimaryUserInfo(
            nextPage: nextPage,
            editProfile: false,
            state: state,
            needControl: true,
          ),
          SizedBox(
            height: 50,
          )
        ]);
      },
    );
  }
}
