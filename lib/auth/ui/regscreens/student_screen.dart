import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/bloc/reg_cubit.dart';
import 'package:teormech/auth/model/reg_model.dart';
import 'package:teormech/auth/ui/regscreens/student_password.dart';

import 'student_secondary.dart';
import 'stundet_primary.dart';

class StudentScreen extends StatefulWidget {
  @override
  _StudentScreenState createState() => _StudentScreenState();
}

class _StudentScreenState extends State<StudentScreen> {
  PageController _controller = PageController(initialPage: 0);

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void nextPage(PageController controller) {
    _controller.nextPage(
        duration: Duration(milliseconds: 300), curve: Curves.linear);
  }

  void prevPage(PageController controller) {
    _controller.previousPage(
        duration: Duration(milliseconds: 300), curve: Curves.linear);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if ((_controller.page ?? 0) > 0) {
          prevPage(_controller);
          return false;
        }
        return true;
      },
      child: BlocBuilder<RegistrationDataCubit, RegistrationData>(
        builder: (context, state) {
          return PageView.builder(
            itemCount: (state.firstPageAccepted)
                ? (state.passwordAccepted)
                    ? 3
                    : 2
                : 1,
            itemBuilder: (context, idx) {
              if (idx == 0)
                return FirstPage(
                  nextPage: () {
                    nextPage(_controller);
                  },
                );
              if (idx == 2)
                return SecondPage(
                  prevPage: () {
                    prevPage(_controller);
                  },
                );
              else
                return PasswordPage(
                  nextPage: () {
                    nextPage(_controller);
                  },
                  prevPage: () {
                    prevPage(_controller);
                  },
                );
            },
            physics: NeverScrollableScrollPhysics(),
            onPageChanged: (int a) {
              if (a == 0) context.read<RegistrationCubit>().personalData();

              if (a == 1) context.read<RegistrationCubit>().passwordData();

              if (a == 2) context.read<RegistrationCubit>().additionalData();
            },
            controller: _controller,
          );
        },
      ),
    );
  }
}
