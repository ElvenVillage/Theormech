import 'package:edge_alert/edge_alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/widgets/password_user_info.dart';
import '../../bloc/reg_cubit.dart';
import '../../model/reg_model.dart';

class PasswordPage extends StatefulWidget {
  final Function nextPage;
  final Function prevPage;

  PasswordPage({required this.nextPage, required this.prevPage});
  @override
  _PasswordPageState createState() => _PasswordPageState();
}

class _PasswordPageState extends State<PasswordPage> {
  _PasswordPageState();
  bool emailErrorShowed = false;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<RegistrationDataCubit, RegistrationData>(
      listener: (context, state) {
        if (!emailErrorShowed && state.password.error) {
          EdgeAlert.show(context,
              title: 'Ошибка',
              duration: EdgeAlert.LENGTH_SHORT,
              icon: Icons.error_outline_rounded,
              backgroundColor: Colors.redAccent,
              description: 'Введите корректный пароль',
              gravity: EdgeAlert.BOTTOM);
          emailErrorShowed = true;
        }
      },
      bloc: context.read<RegistrationDataCubit>(),
      builder: (context, state) {
        return ListView(children: [
          PasswordUserInfo(
            state: state,
            needControl: true,
            prevPage: widget.prevPage,
            nextPage: widget.nextPage,
          ),
          SizedBox(
            height: 50,
          )
        ]);
      },
    );
  }
}
