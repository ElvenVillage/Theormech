import 'dart:async';
import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/bloc/auth_bloc.dart';
import 'package:teormech/auth/model/auth_model.dart';
import 'package:teormech/auth/model/contacts.dart';
import 'package:teormech/auth/model/education.dart';
import 'package:teormech/auth/model/reg_model.dart';
import 'package:teormech/auth/rep/reg_rep.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/subscription/model/followers_state.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';

class RegistrationDataCubit extends Cubit<RegistrationData> {
  RegistrationDataCubit({String? regToken, bool editProfile = false})
      : super(RegistrationData(
            isEmployee: false,
            isStudent: false,
            isTeacher: false,
            surname: TextEditingController(),
            firstName: TextEditingController(),
            patronymic: TextEditingController(),
            email: TextEditingController(),
            about: TextEditingController(),
            oldPassword: TextEditingController(),
            phone: TextEditingController(),
            city: TextEditingController(),
            password: TextEditingController(),
            groupID: TextEditingController(),
            employeeStatus: TextEditingController(),
            teacherStatus: TextEditingController(),
            confirmPassword: TextEditingController(),
            dateOfBirth: TextEditingController(),
            personalEmail: TextEditingController())) {
    if (regToken != null) {
      fetchPreparedData(regToken);
      setValidation(false);
    }
    if (editProfile) setValidation(false);
  }

  bool wasInitialized = false;
  bool enableValidation = true;
  bool needSubsSync = true;

  StreamSubscription? _subscription;

  void subscribe(UserProfileBloc profileBloc) {
    _subscription = profileBloc.stream.listen((data) {
      if (!needSubsSync) return;

      state.isEmployee = data.profile.isEmployee;
      state.isStudent = data.profile.isStudent;
      state.isTeacher = data.profile.isTeacher;
      state.firstName.controller.text = data.profile.name;
      state.surname.controller.text = data.profile.surname;
      state.patronymic.controller.text = data.profile.patronymic;
      state.phone.controller.text = data.profile.phone ?? '';
      state.groupID.controller.text = data.profile.group ?? '';
      state.email.controller.text = data.profile.email;
      state.dateOfBirth.controller.text = data.profile.birth ?? '';
      state.city.controller.text = data.profile.hometown ?? '';
      state.employeeStatus.controller.text = data.profile.employeeStatus ?? '';
      state.teacherStatus.controller.text = data.profile.teacherStatus ?? '';
      state.sex = data.profile.sex;

      state.about.controller.text = data.profile.about ?? '';

      var contacts =
          Contact.fromJson(json: data.profile.contacts ?? '[{"soc":""}]');
      if ((contacts.every((contact) => contact is EmailContact)) ||
          (contacts.isEmpty)) contacts.add(EmptyContact());
      state.contacts = contacts;

      state.personalEmail.controller = state.contacts
          .firstWhere((element) => element is EmailContact)
          .controller;

      state.education =
          Education.fromJson(json: data.profile.education ?? '[]');

      needSubsSync = false;
    });
    profileBloc.update();
  }

  void setValidation(bool cond) {
    enableValidation = cond;
  }

  void updateSex(String? sex) {
    emit(state.copyWith(sex: sex));
  }

  void setAvatar(File avatar) {
    emit(state.copyWith(avatar: avatar));
  }

  void attachListeners() {
    if (!wasInitialized) {
      state.surname.controller.addListener(() {
        emit(state.copyWith(enableInitial: state.surname.enableInitial));
      });
      state.firstName.controller.addListener(() {
        emit(state.copyWith(enableInitial: state.surname.enableInitial));
      });
      state.patronymic.controller.addListener(() {
        emit(state.copyWith(enableInitial: state.surname.enableInitial));
      });
      state.groupID.controller.addListener(() {
        emit(state.copyWith(enableInitial: state.surname.enableInitial));
      });
      state.email.controller.addListener(() async {
        if (!enableValidation) return;
        var value = await RegistrationRepository.checkEmailAvailable(
            state.email.controller.text);

        emit(state.copyWith(
            enableInitial: state.surname.enableInitial, isAvailable: value));
      });
      state.phone.controller.addListener(() {
        emit(state.copyWith(enableInitial: state.surname.enableInitial));
      });

      state.teacherStatus.controller.addListener(() {
        emit(state.copyWith(enableInitial: state.surname.enableInitial));
      });

      state.employeeStatus.controller.addListener(() {
        emit(state.copyWith(enableInitial: state.surname.enableInitial));
      });

      state.dateOfBirth.controller.addListener(() {
        emit(state.copyWith(enableInitial: state.surname.enableInitial));
      });

      state.password.controller.addListener(() {
        emit(state.copyWith(enableInitialPassword: false));
      });

      state.confirmPassword.controller.addListener(() {
        emit(state.copyWith(enableInitialPassword: false));
      });

      state.personalEmail.controller.addListener(() {
        emit(state.copyWith(enableInitial: state.surname.enableInitial));
      });
    }
    wasInitialized = true;
  }

  void notifyListeners() {
    emit(state.copyWith(enableInitial: false));
  }

  void deleteContact(int id) {
    var nState = state.copyWith();
    nState.contacts.removeAt(id);

    emit(nState);
  }

  void updateContact(SocialNetwork network, int id) {
    var contacts = state.contacts;

    if (contacts[id].socialNetwork != network) {
      switch (network) {
        case SocialNetwork.Telegram:
          {
            contacts[id] = TelegramContact(controller: contacts[id].controller);
            break;
          }
        case SocialNetwork.Vk:
          {
            contacts[id] = VkContact(controller: contacts[id].controller);
            break;
          }

        case SocialNetwork.Empty:
          {
            contacts[id] = EmptyContact();
            break;
          }
        case SocialNetwork.Email:
      }
    }

    emit(state.copyWith(contacts: contacts));
  }

  void appendContact(String contact, SocialNetwork network) {
    var contacts = state.contacts;

    if (network == SocialNetwork.Vk)
      contacts.add(VkContact(controller: TextEditingController()));

    if (network == SocialNetwork.Telegram)
      contacts.add(TelegramContact(controller: TextEditingController()));

    if ((network == SocialNetwork.Empty) &&
        (state.contacts.last.socialNetwork != SocialNetwork.Empty))
      contacts.add(EmptyContact());

    emit(state.copyWith(contacts: contacts));
  }

  void deleteEducation(int id) {
    var nState = state.copyWith();
    nState.education.removeAt(id);
    emit(nState);
  }

  void appendEducation() {
    var education = state.education;

    education.add(Education.school(
            orgController: TextEditingController(),
            graduationController: TextEditingController())
        //facultyController: TextEditingController(),
        // specController: TextEditingController())
        );
    emit(state.copyWith(education: education));
  }

  void updateEducation({required int id, required EducationLevel level}) {
    var education = state.education;

    if (education[id].level == level) return;

    var orgController = TextEditingController();
    var graduationController = TextEditingController();

    var facultyController =
        (level == EducationLevel.School) ? null : TextEditingController();
    var specController =
        (level == EducationLevel.School) ? null : TextEditingController();

    education[id] = Education(
        orgController: orgController,
        graduationController: graduationController,
        facultyController: facultyController,
        specController: specController,
        level: level);
  }

  Future<void> fetchPreparedData(String token) async {
    var json = await RegistrationRepository.fetchPreparedData(token);

    if (json['isStudent'] == '1') {
      emit(state.copyWith(isStudent: true));
      state.groupID.controller.text = json['group'];
    }

    if (json['isTeacher'] == '1') {
      emit(state.copyWith(isTeacher: true));
      state.teacherStatus.controller.text = json['teacher_status'];
    }

    if (json['isEmployee'] == '1') {
      emit(state.copyWith(isEmployee: true));
      state.employeeStatus.controller.text = json['employee_status'];
    }

    state.firstName.controller.text = json['name'];
    state.surname.controller.text = json['surname'];
    state.patronymic.controller.text = json['patronymic'];
    state.email.controller.text = json['email'];
  }

  @override
  Future<void> close() async {
    _subscription?.cancel();

    state.surname.controller.dispose();
    state.firstName.controller.dispose();
    state.patronymic.controller.dispose();
    state.about.controller.dispose();
    state.city.controller.dispose();
    state.dateOfBirth.controller.dispose();
    state.email.controller.dispose();
    state.groupID.controller.dispose();
    state.password.controller.dispose();
    state.employeeStatus.controller.dispose();
    state.teacherStatus.controller.dispose();
    state.contacts.forEach((element) {
      element.controller.dispose();
    });
    state.education.forEach((element) {
      element.dispose();
    });

    super.close();
  }
}

class RegistrationCubit extends Cubit<RegState> {
  RegistrationCubit(
      {required bool editProfile, this.regToken, required this.authBloc})
      : super(RegState(readOnly: true)) {
    if (regToken != null) {
      fetchPreparedData(regToken!);
    } else {
      readOnly(editProfile);
    }
  }

  RegistrationData? data;
  final AuthBloc authBloc;
  String? regToken;

  void fetchPreparedData(String token) async {
    try {
      await RegistrationRepository.fetchPreparedData(token);

      emit(RegState(readOnly: true, status: FetchingStatus.InProgress));
    } catch (e) {
      emit(RegState(
          readOnly: true,
          error: 'Пользователь уже зарегистрирован',
          regStatus: RegStatus.Error));
    }
  }

  void endLoading() {
    emit(state.copyWith(initFetchStatus: FetchingStatus.Loaded));
  }

  Future<bool?> uploadAvatar(SessionToken token, {required File avatar}) async {
    var a = await RegistrationRepository.uploadProfileAvatar(token, avatar,
        (send, total) {
      emit(state.copyWith(progress: send / total));
    });
    return a;
  }

  Future<bool> updatePassword(SessionToken token,
      {required String newPassword,
      required String newPasswordConfirmation,
      required String oldPassword}) async {
    if (newPassword.isNotEmpty && newPasswordConfirmation.isNotEmpty) {
      emit(state.copyWith(registerUploadInfoStatus: FetchingStatus.InProgress));
      var a = await RegistrationRepository.editPassword(
          token: token, oldpassword: oldPassword, password: newPassword);
      emit(state.copyWith(registerUploadInfoStatus: FetchingStatus.Loaded));
      return a;
    } else
      return false;
  }

  Future<bool> uploadAdditionalData(SessionToken token,
      {RegistrationData? data, UserProfileState? profileState}) async {
    bool needToUploadAvatar = false;
    if (regToken != null) {
      emit(state.copyWith(registerUploadInfoStatus: FetchingStatus.InProgress));
      needToUploadAvatar = true;
    } else {
      emit(state.copyWith(status: FetchingStatus.InProgress));
    }
    if ((this.data == null) && (data != null)) this.data = data;
    if (data != null) {
      List<Contact> contacts = List.from(data.contacts);
      if (!(contacts.any((element) => element is EmailContact)))
        contacts.add(EmailContact(controller: data.personalEmail.controller));

      var a = await Future.wait([
        if ((profileState == null) ||
            (data.about.controller.text != profileState.profile.about))
          RegistrationRepository.editProfile(
              token, 'about', data.about.controller.text.trim()),
        if ((profileState == null) ||
            (data.city.controller.text != profileState.profile.hometown))
          RegistrationRepository.editProfile(
              token, 'hometown', data.city.controller.text.trim()),
        if ((profileState == null) ||
            (data.dateOfBirth.controller.text != profileState.profile.birth))
          RegistrationRepository.editProfile(
              token, 'birth', data.dateOfBirth.controller.text),
        RegistrationRepository.editProfile(
            token, 'study', Education.toJson(data.education)),
        if ((profileState == null) ||
            (data.phone.controller.text != profileState.profile.phone))
          RegistrationRepository.editProfile(
              token, 'phone', data.phone.controller.text),
        RegistrationRepository.editProfile(
            token, 'contacts', Contact.toJson(contacts: contacts)),
        RegistrationRepository.editProfile(token, 'sex', data.sex),
        if ((data.isEmployee) &&
            ((profileState == null) ||
                (data.employeeStatus.controller.text !=
                    profileState.profile.employeeStatus)))
          RegistrationRepository.editProfile(
              token, 'employee_status', data.employeeStatus.controller.text),
        if ((data.isTeacher) &&
            ((profileState == null) ||
                (data.teacherStatus.controller.text !=
                    profileState.profile.teacherStatus)))
          RegistrationRepository.editProfile(
              token, 'teacher_status', data.teacherStatus.controller.text)
      ]);

      if (needToUploadAvatar && data.avatar != null)
        a.add(await uploadAvatar(token, avatar: data.avatar!) ?? false);

      if (!a.contains(false)) {
        emit(state.copyWith(
            status: FetchingStatus.Loaded,
            regStatus: RegStatus.Success,
            registerUploadInfoStatus: FetchingStatus.Loaded));
        return true;
      } else {
        emit(state.copyWith(
            status: FetchingStatus.Loaded,
            regStatus: RegStatus.Error,
            registerUploadInfoStatus: FetchingStatus.Loaded));
        return false;
      }
    }
    return false;
  }

  void readOnly(bool readOnly) {
    emit(state.copyWith(readOnly: readOnly));
  }

  void passwordReadOnly(bool readOnly) {
    emit(state.copyWith(passwordReadOnly: readOnly));
  }

  void personalData() {
    emit(state.copyWith(page: RegPage.PrimaryData));
  }

  void additionalData() {
    emit(state.copyWith(page: RegPage.SecondaryData));
  }

  void passwordData() {
    emit(state.copyWith(page: RegPage.PasswordData));
  }

  void register(RegistrationData data) async {
    this.data = data;

    if (regToken == null) {
      //обычная регистрация
      var status = await RegistrationRepository.registerUser(data);

      if (status.success) {
        authBloc.add(LoginEvent(
            credentials: LoginPassword(
                login: data.email.controller.text,
                password: data.password.controller.text)));

        emit(state.copyWith(regStatus: RegStatus.Success));
      } else
        emit(state.copyWith(regStatus: RegStatus.Error));
    } else {
      //пост-регистрация
      final fcmToken = await FirebaseMessaging.instance.getToken();
      final response = await RegistrationRepository.postreg(
          token: regToken!,
          password: data.password.controller.text,
          firebaseToken: fcmToken);

      if (response.sessionToken != null) {
        authBloc.add(LoginWithExistedSession(
            token: response.sessionToken!,
            callback: () async {
              final addData = await uploadAdditionalData(response.sessionToken!,
                  data: data);
              if (addData) emit(state.copyWith(regStatus: RegStatus.Success));
            }));
      } else
        print(response.error);
    }
  }
}
