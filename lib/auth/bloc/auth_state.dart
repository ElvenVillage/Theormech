part of 'auth_bloc.dart';

abstract class AuthState {}

class Unathorized extends AuthState {}

class Initial extends AuthState {}

class Authorized extends AuthState {
  final SessionToken token;
  final bool isHeadman;
  final bool isTeacher;
  final Token? navigationToken;
  final RemoteMessage? initialMessage;

  Authorized(
      {required this.token,
      this.isHeadman = false,
      this.navigationToken,
      this.initialMessage,
      this.isTeacher = false});

  Authorized clearMetadata() {
    return Authorized(token: token, isHeadman: isHeadman, isTeacher: isTeacher);
  }
}

class RegTokenState extends AuthState {
  final String regToken;
  RegTokenState(this.regToken);
}

class ForgotPasswordState extends AuthState {
  final String token;
  ForgotPasswordState(this.token);
}
