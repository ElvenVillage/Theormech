import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:teormech/auth/model/storage/auth_storage.dart';
import 'package:teormech/auth/model/auth_model.dart';
import 'package:teormech/auth/rep/auth_rep.dart';
import 'package:teormech/profile/profile/rep/user_profile_rep.dart';
part 'auth_events.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  late final AuthStorage _storage;

  AuthBloc() : super(Initial()) {
    _initFCM();

    if (!kIsWeb) {
      _storage = AuthMobileStorage();
      _initDynamicLinks();
    } else {
      _storage = AuthWebStorage();
      Future.delayed(Duration.zero, () => add(LoadEvent()));
    }

    on<LoginEvent>(_login);
    on<LogoutEvent>(_logout);
    on<LoadEvent>(_load);
    on<HeadmanEvent>(_headmanHandler);
    on<TeacherEvent>(_teacherHandler);
    on<LoginWithExistedSession>(_loginWithSession);
    on<ClearMetadataEvent>(_clearMetadata);
  }

  void _clearMetadata(ClearMetadataEvent event, Emitter<AuthState> emit) {
    if (state is Authorized) {
      emit((state as Authorized).clearMetadata());
    }
  }

  void _initializeRepositories(SessionToken token) {
    UserProfileRepository(credentials: token);
  }

  void _login(LoginEvent event, Emitter<AuthState> emit) async {
    final firebaseToken =
        (!kIsWeb) ? await FirebaseMessaging.instance.getToken() : null;
    final token = await AuthRepository.auth(
        credentials: event.credentials, firebaseToken: firebaseToken);

    if (token.sessionToken == null) {
      emit(Unathorized());
    } else {
      await token.sessionToken?.save(_storage);
      _initializeRepositories(token.sessionToken!);
      emit(Authorized(token: token.sessionToken!));
    }
  }

  Future<void> _logoutHandler() async {
    if (state is Authorized) {
      final firebaseToken =
          (!kIsWeb) ? await FirebaseMessaging.instance.getToken() : null;
      try {
        await SessionToken.delete(_storage);
        await AuthRepository.logout(
            credentials: (state as Authorized).token,
            firebaseToken: firebaseToken);
        await FirebaseMessaging.instance.deleteToken();
        await FirebaseMessaging.instance.getToken();
      } catch (_) {
        return;
      }
    }
  }

  Future<void> _logout(LogoutEvent event, Emitter<AuthState> emit) async {
    if (event.silent)
      emit(Unathorized());
    else {
      try {
        await _logoutHandler();
        emit(Unathorized());
        add(LoadEvent());
      } catch (_) {}
    }
  }

  void _load(LoadEvent event, Emitter<AuthState> emit) async {
    if (event.token != null) {
      switch (event.token!.type) {
        case TokenType.RegistrationToken:
          await _logoutHandler();
          emit(RegTokenState(event.token!.token));
          break;
        case TokenType.JoinChatToken:
        case TokenType.EventToken:
        case TokenType.QRCodeToken:
          if (state is Authorized) {
            final nState = state as Authorized;
            _initializeRepositories(nState.token);

            emit(Authorized(
                token: nState.token,
                isHeadman: nState.isHeadman,
                isTeacher: nState.isTeacher,
                navigationToken: event.token!));
          } else {
            final token = await SessionToken.fromSecureStorage(_storage);
            if (token == null) {
              await Future.delayed(Duration(seconds: 3));
              emit(Unathorized());
            } else {
              _initializeRepositories(token);
              emit(Authorized(token: token, navigationToken: event.token!));
            }
          }
          break;
        case TokenType.ForgotPasswordToken:
          await _logoutHandler();
          emit(ForgotPasswordState(event.token!.token));
          break;
      }
    } else if (event.message != null) {
      if (state is Authorized) {
        final nState = state as Authorized;
        _initializeRepositories(nState.token);
        emit(Authorized(
            token: nState.token,
            initialMessage: event.message,
            isHeadman: nState.isHeadman,
            isTeacher: nState.isTeacher,
            navigationToken: nState.navigationToken));
      } else {
        final token = await SessionToken.fromSecureStorage(_storage);
        if (token != null) {
          _initializeRepositories(token);
          emit(Authorized(
            token: token,
            initialMessage: event.message,
          ));
        }
      }
    } else {
      final token = await SessionToken.fromSecureStorage(_storage);

      if (token == null) {
        await Future.delayed(Duration(seconds: 3));
        emit(Unathorized());
      } else {
        _initializeRepositories(token);
        await Future.delayed(Duration(seconds: 1));
        emit(Authorized(token: token));
      }
    }
  }

  void _headmanHandler(HeadmanEvent event, Emitter<AuthState> emit) async {
    emit(Authorized(
        token: (state as Authorized).token,
        isHeadman: event.isHeadman,
        isTeacher: (state as Authorized).isTeacher,
        initialMessage: (state as Authorized).initialMessage,
        navigationToken: (state as Authorized).navigationToken));
  }

  void _teacherHandler(TeacherEvent event, Emitter<AuthState> emit) async {
    emit(Authorized(
        token: (state as Authorized).token,
        isHeadman: (state as Authorized).isHeadman,
        initialMessage: (state as Authorized).initialMessage,
        isTeacher: true,
        navigationToken: (state as Authorized).navigationToken));
  }

  void _loginWithSession(
      LoginWithExistedSession event, Emitter<AuthState> emit) async {
    await event.token.save(_storage);
    await event.callback();
    emit(Authorized(token: event.token));
  }

  Future<void> _initFCM() async {
    await Firebase.initializeApp();

    final msgs = FirebaseMessaging.instance;
    await msgs.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );
  }

  Future<bool> _handleDynamicLinkTap(
      PendingDynamicLinkData? dynamicLink, bool initial) async {
    final deepLink = dynamicLink?.link;

    if (deepLink != null) {
      final segments = deepLink.pathSegments;
      if (segments.contains('forgot_password')) {
        add(LoadEvent(token: Token.restorePasswordToken(segments.last)));
      }
      if (segments.contains('prereg')) {
        add(LoadEvent(token: Token.regToken(segments.last)));
      }
      if (segments.contains('join_group_chat')) {
        add(LoadEvent(
          token: Token.joinToken(segments.last, initial),
        ));
      }
      if (segments.contains('event')) {
        add(LoadEvent(token: Token.goToEventToken(segments.last, initial)));
      }
      if (segments.contains('checkin')) {
        add(LoadEvent(token: Token.qrCodeToken(segments.last, initial)));
      }
    }
    return (deepLink != null);
  }

  void _initDynamicLinks() async {
    FirebaseDynamicLinks.instance.onLink
        .listen((a) => _handleDynamicLinkTap(a, false))
        .onError((e) {
      print(e.message);
    });

    final data = await FirebaseDynamicLinks.instance.getInitialLink();
    if (data?.link != null) {
      await _handleDynamicLinkTap(data, true);
    } else {
      final initialMessage =
          await FirebaseMessaging.instance.getInitialMessage();
      if (initialMessage != null) {
        add(LoadEvent(message: initialMessage));
      } else {
        add(LoadEvent());
      }
    }
  }
}
