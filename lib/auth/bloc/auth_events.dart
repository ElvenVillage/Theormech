part of 'auth_bloc.dart';

abstract class AuthEvent {}

class LoginEvent extends AuthEvent {
  final LoginPassword credentials;

  LoginEvent({required this.credentials});
}

class LogoutEvent extends AuthEvent {
  final bool silent;
  LogoutEvent({this.silent = false});
}

enum TokenType {
  RegistrationToken,
  JoinChatToken,
  ForgotPasswordToken,
  EventToken,
  QRCodeToken
}

class Token {
  final String token;
  final TokenType type;
  bool initial = false;

  Token.regToken(this.token) : type = TokenType.RegistrationToken;
  Token.joinToken(this.token, this.initial) : type = TokenType.JoinChatToken;
  Token.restorePasswordToken(this.token) : type = TokenType.ForgotPasswordToken;
  Token.goToEventToken(this.token, this.initial) : type = TokenType.EventToken;
  Token.qrCodeToken(this.token, this.initial) : type = TokenType.QRCodeToken;
}

class LoadEvent extends AuthEvent {
  Token? token;
  RemoteMessage? message;
  LoadEvent({this.token, this.message});
}

class HeadmanEvent extends AuthEvent {
  final bool isHeadman;
  HeadmanEvent(this.isHeadman);
}

class TeacherEvent extends AuthEvent {}

typedef PostregCallback = Future<void> Function();

class LoginWithExistedSession extends AuthEvent {
  final SessionToken token;
  final PostregCallback callback;
  LoginWithExistedSession({required this.token, required this.callback});
}

class ClearMetadataEvent extends AuthEvent {}
