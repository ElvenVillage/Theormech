import 'package:path_provider/path_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:hive/hive.dart';
import 'package:teormech/auth/bloc/auth_bloc.dart';
import 'package:teormech/auth/ui/reg.dart';
import 'package:teormech/profile/about/bloc/cache_manager_bloc.dart';
import 'package:teormech/profile/authorized_navigator.dart';
import 'package:teormech/profile/chat/model/chat_model.dart';
import 'package:teormech/profile/chat/model/message_model.dart';
import 'package:teormech/profile/scheduler/model/schedule_model.dart';
import 'package:teormech/profile/profile/model/portfolio_state.dart';
import 'package:teormech/profile/subscription/model/followers_state.dart';
import 'package:teormech/profile/profile/model/user_profile.dart';
import 'package:teormech/rep/native_downloader.dart';
import 'package:teormech/splash/splash.dart';

import 'auth/ui/forgot/change_forgot_password_screen.dart';
import 'generated/l10n.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  if (!kIsWeb) {
    var appDir = await getApplicationDocumentsDirectory();
    Hive.init(appDir.path);
  }

  await Firebase.initializeApp();

  Hive.registerAdapter(FollowerAdapter());
  Hive.registerAdapter(FollowersAdapter());
  Hive.registerAdapter(UserProfileAdapter());
  Hive.registerAdapter(MessageAdapter());
  Hive.registerAdapter(ChatAdapter());
  Hive.registerAdapter(ProfessionalLevelAdapter());
  Hive.registerAdapter(MessagesListAdapter());
  Hive.registerAdapter(PortfolioTagAdapter());
  Hive.registerAdapter(PortfolioTagListAdapter());
  Hive.registerAdapter(PortfolioStateAdapter());
  Hive.registerAdapter(PortfolioAchievementAdapter());
  Hive.registerAdapter(CalendarEventTypeAdapter());
  Hive.registerAdapter(CalendarEventAdapter());
  Hive.registerAdapter(CalendarEventListAdapter());

  await SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  if (!kIsWeb)
    FlutterDownloader.initialize(debug: true).then((_) =>
        FlutterDownloader.registerCallback(NativeDownloader.downloadCallback));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static final _authKey = GlobalKey();
  static final _splashKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AuthBloc>(
          create: (context) => AuthBloc(),
          lazy: false,
        ),
        BlocProvider<CacheManagerBloc>(create: (context) => CacheManagerBloc())
      ],
      child: Builder(builder: (context) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(fontFamily: 'Roboto'),
          localizationsDelegates: [
            S.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: S.delegate.supportedLocales,
          onGenerateRoute: (settings) {
            if (settings.name == '/profile') {
              return PageRouteBuilder(
                  transitionDuration: Duration.zero,
                  pageBuilder: (context, a, b) =>
                      BlocConsumer<AuthBloc, AuthState>(
                        listener: (context, st) {
                          if (st is Unathorized)
                            Navigator.pushNamedAndRemoveUntil(
                                context, '/', (route) => false);
                        },
                        builder: (context, state) {
                          if (state is Authorized) {
                            if (state.navigationToken?.initial ??
                                false || state.initialMessage != null)
                              return IndexedStack(
                                children: [
                                  SplashScreen(key: _splashKey),
                                  AuthorizedPage(key: _authKey),
                                ],
                              );
                            else {
                              return AuthorizedPage(key: _authKey);
                            }
                          } else {
                            return const SizedBox();
                          }
                        },
                      ));
            }

            if (settings.name == '/reg')
              return MaterialPageRoute(
                  builder: (context) => BlocListener<AuthBloc, AuthState>(
                      listener: (ctx, st) {
                        if (st is Authorized)
                          Navigator.pushNamedAndRemoveUntil(
                              context, '/profile', (route) => false);
                        if (st is Unathorized)
                          Navigator.pushNamedAndRemoveUntil(
                              context, '/login', (route) => false);
                      },
                      child: RegistrationScreen(
                        regToken: settings.arguments as String?,
                      )));

            if (settings.name == '/forgot')
              return MaterialPageRoute(
                builder: (context) => ChangeForgotPasswordScreen(
                    token: settings.arguments as String),
              );

            return PageRouteBuilder(
                transitionDuration: Duration.zero,
                pageBuilder: (context, a, b) => Builder(
                      builder: (context) => BlocListener<AuthBloc, AuthState>(
                        listener: (context, state) {
                          if (state is Authorized) {
                            Navigator.pushNamedAndRemoveUntil(
                                context, '/profile', (route) => false);
                          }
                          if (state is RegTokenState)
                            Navigator.pushNamedAndRemoveUntil(
                                context, '/reg', ModalRoute.withName('/login'),
                                arguments: state.regToken);

                          if (state is ForgotPasswordState) {
                            Navigator.pushNamed(context, '/forgot',
                                arguments: state.token);
                          }
                        },
                        child: SplashScreen(key: _splashKey),
                      ),
                    ));
          },
        );
      }),
    );
  }
}
