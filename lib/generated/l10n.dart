// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `About`
  String get about_screen_title {
    return Intl.message(
      'About',
      name: 'about_screen_title',
      desc: '',
      args: [],
    );
  }

  /// `Error`
  String get authorization_error {
    return Intl.message(
      'Error',
      name: 'authorization_error',
      desc: '',
      args: [],
    );
  }

  /// `Please check your credentials`
  String get check_your_credentials_error_message {
    return Intl.message(
      'Please check your credentials',
      name: 'check_your_credentials_error_message',
      desc: '',
      args: [],
    );
  }

  /// `Email`
  String get login_hint {
    return Intl.message(
      'Email',
      name: 'login_hint',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get password_hint {
    return Intl.message(
      'Password',
      name: 'password_hint',
      desc: '',
      args: [],
    );
  }

  /// `Forgot your password?`
  String get forgot_password_main_screen_button {
    return Intl.message(
      'Forgot your password?',
      name: 'forgot_password_main_screen_button',
      desc: '',
      args: [],
    );
  }

  /// `Register`
  String get register_button_main_screen {
    return Intl.message(
      'Register',
      name: 'register_button_main_screen',
      desc: '',
      args: [],
    );
  }

  /// `Login`
  String get login_button_main_screen {
    return Intl.message(
      'Login',
      name: 'login_button_main_screen',
      desc: '',
      args: [],
    );
  }

  /// `FOSS Licenses`
  String get foss_licenses {
    return Intl.message(
      'FOSS Licenses',
      name: 'foss_licenses',
      desc: '',
      args: [],
    );
  }

  /// `Cache`
  String get cache {
    return Intl.message(
      'Cache',
      name: 'cache',
      desc: '',
      args: [],
    );
  }

  /// `Clear`
  String get clear_cache {
    return Intl.message(
      'Clear',
      name: 'clear_cache',
      desc: '',
      args: [],
    );
  }

  /// `Clear`
  String get clear_data {
    return Intl.message(
      'Clear',
      name: 'clear_data',
      desc: '',
      args: [],
    );
  }

  /// `Copy Firebase Token`
  String get copy_fcm_token {
    return Intl.message(
      'Copy Firebase Token',
      name: 'copy_fcm_token',
      desc: '',
      args: [],
    );
  }

  /// `Token was copied!`
  String get copy_fcm_token_success {
    return Intl.message(
      'Token was copied!',
      name: 'copy_fcm_token_success',
      desc: '',
      args: [],
    );
  }

  /// `Profile`
  String get profile {
    return Intl.message(
      'Profile',
      name: 'profile',
      desc: '',
      args: [],
    );
  }

  /// `Portfolio`
  String get portfolio {
    return Intl.message(
      'Portfolio',
      name: 'portfolio',
      desc: '',
      args: [],
    );
  }

  /// `Subscriptions`
  String get subscriptions {
    return Intl.message(
      'Subscriptions',
      name: 'subscriptions',
      desc: '',
      args: [],
    );
  }

  /// `Chats`
  String get chats {
    return Intl.message(
      'Chats',
      name: 'chats',
      desc: '',
      args: [],
    );
  }

  /// `Newsfeed`
  String get newsfeed {
    return Intl.message(
      'Newsfeed',
      name: 'newsfeed',
      desc: '',
      args: [],
    );
  }

  /// `Projects`
  String get projects {
    return Intl.message(
      'Projects',
      name: 'projects',
      desc: '',
      args: [],
    );
  }

  /// `Calendar`
  String get schedule {
    return Intl.message(
      'Calendar',
      name: 'schedule',
      desc: '',
      args: [],
    );
  }

  /// `Events`
  String get events {
    return Intl.message(
      'Events',
      name: 'events',
      desc: '',
      args: [],
    );
  }

  /// `Jobs`
  String get jobs {
    return Intl.message(
      'Jobs',
      name: 'jobs',
      desc: '',
      args: [],
    );
  }

  /// `Support`
  String get support {
    return Intl.message(
      'Support',
      name: 'support',
      desc: '',
      args: [],
    );
  }

  /// `Logout`
  String get logout {
    return Intl.message(
      'Logout',
      name: 'logout',
      desc: '',
      args: [],
    );
  }

  /// `Student `
  String get student_of_a_group {
    return Intl.message(
      'Student ',
      name: 'student_of_a_group',
      desc: '',
      args: [],
    );
  }

  /// `Online`
  String get online {
    return Intl.message(
      'Online',
      name: 'online',
      desc: '',
      args: [],
    );
  }

  /// `To dialog`
  String get from_profile_to_dialog {
    return Intl.message(
      'To dialog',
      name: 'from_profile_to_dialog',
      desc: '',
      args: [],
    );
  }

  /// `Edit profile`
  String get from_profile_to_edit_profile {
    return Intl.message(
      'Edit profile',
      name: 'from_profile_to_edit_profile',
      desc: '',
      args: [],
    );
  }

  /// `Unfollow`
  String get unfollow_from_profile {
    return Intl.message(
      'Unfollow',
      name: 'unfollow_from_profile',
      desc: '',
      args: [],
    );
  }

  /// `Follow`
  String get follow_from_profile {
    return Intl.message(
      'Follow',
      name: 'follow_from_profile',
      desc: '',
      args: [],
    );
  }

  /// `Following`
  String get user_follows_count {
    return Intl.message(
      'Following',
      name: 'user_follows_count',
      desc: '',
      args: [],
    );
  }

  /// `Followers`
  String get user_followers_count {
    return Intl.message(
      'Followers',
      name: 'user_followers_count',
      desc: '',
      args: [],
    );
  }

  /// `Likes`
  String get likes_count {
    return Intl.message(
      'Likes',
      name: 'likes_count',
      desc: '',
      args: [],
    );
  }

  /// `About`
  String get user_profile_interests {
    return Intl.message(
      'About',
      name: 'user_profile_interests',
      desc: '',
      args: [],
    );
  }

  /// `Education`
  String get user_profile_education {
    return Intl.message(
      'Education',
      name: 'user_profile_education',
      desc: '',
      args: [],
    );
  }

  /// `City`
  String get user_profile_hometown {
    return Intl.message(
      'City',
      name: 'user_profile_hometown',
      desc: '',
      args: [],
    );
  }

  /// `Birth`
  String get user_profile_birthdate {
    return Intl.message(
      'Birth',
      name: 'user_profile_birthdate',
      desc: '',
      args: [],
    );
  }

  /// `Phone`
  String get user_profile_phone_number {
    return Intl.message(
      'Phone',
      name: 'user_profile_phone_number',
      desc: '',
      args: [],
    );
  }

  /// `Email`
  String get user_profile_email {
    return Intl.message(
      'Email',
      name: 'user_profile_email',
      desc: '',
      args: [],
    );
  }

  /// `Check your network connectivity`
  String get check_your_network_connection {
    return Intl.message(
      'Check your network connectivity',
      name: 'check_your_network_connection',
      desc: '',
      args: [],
    );
  }

  /// `Users`
  String get all_users_list {
    return Intl.message(
      'Users',
      name: 'all_users_list',
      desc: '',
      args: [],
    );
  }

  /// `Recommendations`
  String get recommendations {
    return Intl.message(
      'Recommendations',
      name: 'recommendations',
      desc: '',
      args: [],
    );
  }

  /// `Подписчики`
  String get followings_list {
    return Intl.message(
      'Подписчики',
      name: 'followings_list',
      desc: '',
      args: [],
    );
  }

  /// `Share`
  String get respost_message {
    return Intl.message(
      'Share',
      name: 'respost_message',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get search {
    return Intl.message(
      'Search',
      name: 'search',
      desc: '',
      args: [],
    );
  }

  /// `Gender`
  String get select_gender {
    return Intl.message(
      'Gender',
      name: 'select_gender',
      desc: '',
      args: [],
    );
  }

  /// `Female`
  String get female {
    return Intl.message(
      'Female',
      name: 'female',
      desc: '',
      args: [],
    );
  }

  /// `Male`
  String get male {
    return Intl.message(
      'Male',
      name: 'male',
      desc: '',
      args: [],
    );
  }

  /// `No data found`
  String get no_data_found {
    return Intl.message(
      'No data found',
      name: 'no_data_found',
      desc: '',
      args: [],
    );
  }

  /// `Work experience`
  String get work_experience {
    return Intl.message(
      'Work experience',
      name: 'work_experience',
      desc: '',
      args: [],
    );
  }

  /// `Language skills`
  String get language_skills {
    return Intl.message(
      'Language skills',
      name: 'language_skills',
      desc: '',
      args: [],
    );
  }

  /// `Professional skills`
  String get professional_skills {
    return Intl.message(
      'Professional skills',
      name: 'professional_skills',
      desc: '',
      args: [],
    );
  }

  /// `Specializations`
  String get specializations {
    return Intl.message(
      'Specializations',
      name: 'specializations',
      desc: '',
      args: [],
    );
  }

  /// `Add publication`
  String get add_publication {
    return Intl.message(
      'Add publication',
      name: 'add_publication',
      desc: '',
      args: [],
    );
  }

  /// `Add achievement`
  String get add_achievement {
    return Intl.message(
      'Add achievement',
      name: 'add_achievement',
      desc: '',
      args: [],
    );
  }

  /// `Title`
  String get achievement_title {
    return Intl.message(
      'Title',
      name: 'achievement_title',
      desc: '',
      args: [],
    );
  }

  /// `Link`
  String get achievement_link {
    return Intl.message(
      'Link',
      name: 'achievement_link',
      desc: '',
      args: [],
    );
  }

  /// `Add work experience`
  String get add_work_experience {
    return Intl.message(
      'Add work experience',
      name: 'add_work_experience',
      desc: '',
      args: [],
    );
  }

  /// `Show demo variant`
  String get show_demo_resume {
    return Intl.message(
      'Show demo variant',
      name: 'show_demo_resume',
      desc: '',
      args: [],
    );
  }

  /// `Add language`
  String get add_language_skill {
    return Intl.message(
      'Add language',
      name: 'add_language_skill',
      desc: '',
      args: [],
    );
  }

  /// `Please describe your language skills`
  String get enter_language_skills {
    return Intl.message(
      'Please describe your language skills',
      name: 'enter_language_skills',
      desc: '',
      args: [],
    );
  }

  /// `Skills and specializations`
  String get specializations_and_skills {
    return Intl.message(
      'Skills and specializations',
      name: 'specializations_and_skills',
      desc: '',
      args: [],
    );
  }

  /// `Tag`
  String get input_tag {
    return Intl.message(
      'Tag',
      name: 'input_tag',
      desc: '',
      args: [],
    );
  }

  /// `Seems that nobody uses this tag. Are you sure?`
  String get tag_is_not_created_yet {
    return Intl.message(
      'Seems that nobody uses this tag. Are you sure?',
      name: 'tag_is_not_created_yet',
      desc: '',
      args: [],
    );
  }

  /// `Chat avatar`
  String get chat_photo {
    return Intl.message(
      'Chat avatar',
      name: 'chat_photo',
      desc: '',
      args: [],
    );
  }

  /// `Select new avatar`
  String get select_new_photo {
    return Intl.message(
      'Select new avatar',
      name: 'select_new_photo',
      desc: '',
      args: [],
    );
  }

  /// `Make photo`
  String get make_new_photo {
    return Intl.message(
      'Make photo',
      name: 'make_new_photo',
      desc: '',
      args: [],
    );
  }

  /// `Title`
  String get chat_title {
    return Intl.message(
      'Title',
      name: 'chat_title',
      desc: '',
      args: [],
    );
  }

  /// `Add`
  String get add_chat_members {
    return Intl.message(
      'Add',
      name: 'add_chat_members',
      desc: '',
      args: [],
    );
  }

  /// `Chat:`
  String get all_chat_members {
    return Intl.message(
      'Chat:',
      name: 'all_chat_members',
      desc: '',
      args: [],
    );
  }

  /// `Enter your data`
  String get input_your_data {
    return Intl.message(
      'Enter your data',
      name: 'input_your_data',
      desc: '',
      args: [],
    );
  }

  /// `Date`
  String get date_of_publication {
    return Intl.message(
      'Date',
      name: 'date_of_publication',
      desc: '',
      args: [],
    );
  }

  /// `Date`
  String get date_of_achievement {
    return Intl.message(
      'Date',
      name: 'date_of_achievement',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'ru'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
