// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ru locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "about_screen_title":
            MessageLookupByLibrary.simpleMessage("О приложении"),
        "achievement_link": MessageLookupByLibrary.simpleMessage("Ссылка"),
        "achievement_title": MessageLookupByLibrary.simpleMessage("Название"),
        "add_achievement":
            MessageLookupByLibrary.simpleMessage("Добавить достижение"),
        "add_chat_members":
            MessageLookupByLibrary.simpleMessage("Добавить участников"),
        "add_language_skill":
            MessageLookupByLibrary.simpleMessage("Добавить язык"),
        "add_publication":
            MessageLookupByLibrary.simpleMessage("Добавить публикацию"),
        "add_work_experience":
            MessageLookupByLibrary.simpleMessage("Добавить опыт работы"),
        "all_chat_members":
            MessageLookupByLibrary.simpleMessage("Все участники:"),
        "all_users_list":
            MessageLookupByLibrary.simpleMessage("Все пользователи"),
        "authorization_error": MessageLookupByLibrary.simpleMessage("Ошибка"),
        "cache": MessageLookupByLibrary.simpleMessage("Кэш"),
        "chat_photo": MessageLookupByLibrary.simpleMessage("Фотография чата"),
        "chat_title": MessageLookupByLibrary.simpleMessage("Название чата"),
        "chats": MessageLookupByLibrary.simpleMessage("Чаты"),
        "check_your_credentials_error_message":
            MessageLookupByLibrary.simpleMessage(
                "Проверьте ваши учетные данные"),
        "check_your_network_connection": MessageLookupByLibrary.simpleMessage(
            "Проверьте сетевое подключение"),
        "clear_cache": MessageLookupByLibrary.simpleMessage("Очистить"),
        "clear_data": MessageLookupByLibrary.simpleMessage("Очистить"),
        "copy_fcm_token":
            MessageLookupByLibrary.simpleMessage("Скопировать FCM-токен"),
        "copy_fcm_token_success":
            MessageLookupByLibrary.simpleMessage("Токен скопирован!"),
        "date_of_achievement":
            MessageLookupByLibrary.simpleMessage("Дата получения"),
        "date_of_publication":
            MessageLookupByLibrary.simpleMessage("Дата публикации"),
        "enter_language_skills": MessageLookupByLibrary.simpleMessage(
            "Укажите язык и уровень владения"),
        "events": MessageLookupByLibrary.simpleMessage("Мероприятия"),
        "female": MessageLookupByLibrary.simpleMessage("женский"),
        "follow_from_profile":
            MessageLookupByLibrary.simpleMessage("Подписаться"),
        "followings_list": MessageLookupByLibrary.simpleMessage("Подписчики"),
        "forgot_password_main_screen_button":
            MessageLookupByLibrary.simpleMessage("Забыли пароль?"),
        "foss_licenses": MessageLookupByLibrary.simpleMessage("Лицензии FOSS"),
        "from_profile_to_dialog":
            MessageLookupByLibrary.simpleMessage("К диалогу"),
        "from_profile_to_edit_profile":
            MessageLookupByLibrary.simpleMessage("Редактировать профиль"),
        "input_tag": MessageLookupByLibrary.simpleMessage("Введите тег"),
        "input_your_data":
            MessageLookupByLibrary.simpleMessage("Введите свои личные данные"),
        "jobs": MessageLookupByLibrary.simpleMessage("Вакансии"),
        "language_skills": MessageLookupByLibrary.simpleMessage("Знание языка"),
        "likes_count": MessageLookupByLibrary.simpleMessage("Лайков"),
        "login_button_main_screen":
            MessageLookupByLibrary.simpleMessage("Вход"),
        "login_hint": MessageLookupByLibrary.simpleMessage("Логин"),
        "logout": MessageLookupByLibrary.simpleMessage("Выйти"),
        "make_new_photo": MessageLookupByLibrary.simpleMessage("Сделать новое"),
        "male": MessageLookupByLibrary.simpleMessage("мужской"),
        "newsfeed": MessageLookupByLibrary.simpleMessage("Новости"),
        "no_data_found":
            MessageLookupByLibrary.simpleMessage("Ничего не найдено"),
        "online": MessageLookupByLibrary.simpleMessage("В сети"),
        "password_hint": MessageLookupByLibrary.simpleMessage("Пароль"),
        "portfolio": MessageLookupByLibrary.simpleMessage("Портфолио"),
        "professional_skills":
            MessageLookupByLibrary.simpleMessage("Профессиональные навыки"),
        "profile": MessageLookupByLibrary.simpleMessage("Профиль"),
        "projects": MessageLookupByLibrary.simpleMessage("Проекты"),
        "recommendations": MessageLookupByLibrary.simpleMessage("Рекомендации"),
        "register_button_main_screen":
            MessageLookupByLibrary.simpleMessage("Регистрация"),
        "respost_message": MessageLookupByLibrary.simpleMessage("Переслать"),
        "schedule": MessageLookupByLibrary.simpleMessage("Расписание"),
        "search": MessageLookupByLibrary.simpleMessage("Поиск"),
        "select_gender": MessageLookupByLibrary.simpleMessage("Пол"),
        "select_new_photo":
            MessageLookupByLibrary.simpleMessage("Выбрать новое"),
        "show_demo_resume":
            MessageLookupByLibrary.simpleMessage("Посмотреть шаблон"),
        "specializations":
            MessageLookupByLibrary.simpleMessage("Специализации"),
        "specializations_and_skills":
            MessageLookupByLibrary.simpleMessage("Специализации и навыки"),
        "student_of_a_group":
            MessageLookupByLibrary.simpleMessage("Студент гр.: "),
        "subscriptions": MessageLookupByLibrary.simpleMessage("Подписки"),
        "support": MessageLookupByLibrary.simpleMessage("Задать вопрос"),
        "tag_is_not_created_yet": MessageLookupByLibrary.simpleMessage(
            "Если такого тега нет, его можно добавить."),
        "unfollow_from_profile":
            MessageLookupByLibrary.simpleMessage("Отписаться"),
        "user_followers_count":
            MessageLookupByLibrary.simpleMessage("Подписчиков"),
        "user_follows_count": MessageLookupByLibrary.simpleMessage("Подписки"),
        "user_profile_birthdate":
            MessageLookupByLibrary.simpleMessage("Дата рождения"),
        "user_profile_education":
            MessageLookupByLibrary.simpleMessage("Образование"),
        "user_profile_email": MessageLookupByLibrary.simpleMessage("Email"),
        "user_profile_hometown":
            MessageLookupByLibrary.simpleMessage("Родной город"),
        "user_profile_interests":
            MessageLookupByLibrary.simpleMessage("Интересы"),
        "user_profile_phone_number":
            MessageLookupByLibrary.simpleMessage("Номер телефона"),
        "work_experience": MessageLookupByLibrary.simpleMessage("Опыт работы")
      };
}
