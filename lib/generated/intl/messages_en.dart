// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "about_screen_title": MessageLookupByLibrary.simpleMessage("About"),
        "achievement_link": MessageLookupByLibrary.simpleMessage("Link"),
        "achievement_title": MessageLookupByLibrary.simpleMessage("Title"),
        "add_achievement":
            MessageLookupByLibrary.simpleMessage("Add achievement"),
        "add_chat_members": MessageLookupByLibrary.simpleMessage("Add"),
        "add_language_skill":
            MessageLookupByLibrary.simpleMessage("Add language"),
        "add_publication":
            MessageLookupByLibrary.simpleMessage("Add publication"),
        "add_work_experience":
            MessageLookupByLibrary.simpleMessage("Add work experience"),
        "all_chat_members": MessageLookupByLibrary.simpleMessage("Chat:"),
        "all_users_list": MessageLookupByLibrary.simpleMessage("Users"),
        "authorization_error": MessageLookupByLibrary.simpleMessage("Error"),
        "cache": MessageLookupByLibrary.simpleMessage("Cache"),
        "chat_photo": MessageLookupByLibrary.simpleMessage("Chat avatar"),
        "chat_title": MessageLookupByLibrary.simpleMessage("Title"),
        "chats": MessageLookupByLibrary.simpleMessage("Chats"),
        "check_your_credentials_error_message":
            MessageLookupByLibrary.simpleMessage(
                "Please check your credentials"),
        "check_your_network_connection": MessageLookupByLibrary.simpleMessage(
            "Check your network connectivity"),
        "clear_cache": MessageLookupByLibrary.simpleMessage("Clear"),
        "clear_data": MessageLookupByLibrary.simpleMessage("Clear"),
        "copy_fcm_token":
            MessageLookupByLibrary.simpleMessage("Copy Firebase Token"),
        "copy_fcm_token_success":
            MessageLookupByLibrary.simpleMessage("Token was copied!"),
        "date_of_achievement": MessageLookupByLibrary.simpleMessage("Date"),
        "date_of_publication": MessageLookupByLibrary.simpleMessage("Date"),
        "enter_language_skills": MessageLookupByLibrary.simpleMessage(
            "Please describe your language skills"),
        "events": MessageLookupByLibrary.simpleMessage("Events"),
        "female": MessageLookupByLibrary.simpleMessage("Female"),
        "follow_from_profile": MessageLookupByLibrary.simpleMessage("Follow"),
        "followings_list": MessageLookupByLibrary.simpleMessage("Подписчики"),
        "forgot_password_main_screen_button":
            MessageLookupByLibrary.simpleMessage("Forgot your password?"),
        "foss_licenses": MessageLookupByLibrary.simpleMessage("FOSS Licenses"),
        "from_profile_to_dialog":
            MessageLookupByLibrary.simpleMessage("To dialog"),
        "from_profile_to_edit_profile":
            MessageLookupByLibrary.simpleMessage("Edit profile"),
        "input_tag": MessageLookupByLibrary.simpleMessage("Tag"),
        "input_your_data":
            MessageLookupByLibrary.simpleMessage("Enter your data"),
        "jobs": MessageLookupByLibrary.simpleMessage("Jobs"),
        "language_skills":
            MessageLookupByLibrary.simpleMessage("Language skills"),
        "likes_count": MessageLookupByLibrary.simpleMessage("Likes"),
        "login_button_main_screen":
            MessageLookupByLibrary.simpleMessage("Login"),
        "login_hint": MessageLookupByLibrary.simpleMessage("Email"),
        "logout": MessageLookupByLibrary.simpleMessage("Logout"),
        "make_new_photo": MessageLookupByLibrary.simpleMessage("Make photo"),
        "male": MessageLookupByLibrary.simpleMessage("Male"),
        "newsfeed": MessageLookupByLibrary.simpleMessage("Newsfeed"),
        "no_data_found": MessageLookupByLibrary.simpleMessage("No data found"),
        "online": MessageLookupByLibrary.simpleMessage("Online"),
        "password_hint": MessageLookupByLibrary.simpleMessage("Password"),
        "portfolio": MessageLookupByLibrary.simpleMessage("Portfolio"),
        "professional_skills":
            MessageLookupByLibrary.simpleMessage("Professional skills"),
        "profile": MessageLookupByLibrary.simpleMessage("Profile"),
        "projects": MessageLookupByLibrary.simpleMessage("Projects"),
        "recommendations":
            MessageLookupByLibrary.simpleMessage("Recommendations"),
        "register_button_main_screen":
            MessageLookupByLibrary.simpleMessage("Register"),
        "respost_message": MessageLookupByLibrary.simpleMessage("Share"),
        "schedule": MessageLookupByLibrary.simpleMessage("Calendar"),
        "search": MessageLookupByLibrary.simpleMessage("Search"),
        "select_gender": MessageLookupByLibrary.simpleMessage("Gender"),
        "select_new_photo":
            MessageLookupByLibrary.simpleMessage("Select new avatar"),
        "show_demo_resume":
            MessageLookupByLibrary.simpleMessage("Show demo variant"),
        "specializations":
            MessageLookupByLibrary.simpleMessage("Specializations"),
        "specializations_and_skills":
            MessageLookupByLibrary.simpleMessage("Skills and specializations"),
        "student_of_a_group": MessageLookupByLibrary.simpleMessage("Student "),
        "subscriptions": MessageLookupByLibrary.simpleMessage("Subscriptions"),
        "support": MessageLookupByLibrary.simpleMessage("Support"),
        "tag_is_not_created_yet": MessageLookupByLibrary.simpleMessage(
            "Seems that nobody uses this tag. Are you sure?"),
        "unfollow_from_profile":
            MessageLookupByLibrary.simpleMessage("Unfollow"),
        "user_followers_count":
            MessageLookupByLibrary.simpleMessage("Followers"),
        "user_follows_count": MessageLookupByLibrary.simpleMessage("Following"),
        "user_profile_birthdate": MessageLookupByLibrary.simpleMessage("Birth"),
        "user_profile_education":
            MessageLookupByLibrary.simpleMessage("Education"),
        "user_profile_email": MessageLookupByLibrary.simpleMessage("Email"),
        "user_profile_hometown": MessageLookupByLibrary.simpleMessage("City"),
        "user_profile_interests": MessageLookupByLibrary.simpleMessage("About"),
        "user_profile_phone_number":
            MessageLookupByLibrary.simpleMessage("Phone"),
        "work_experience":
            MessageLookupByLibrary.simpleMessage("Work experience")
      };
}
