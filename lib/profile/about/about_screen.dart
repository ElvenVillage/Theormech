import 'package:edge_alert/edge_alert.dart';
import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/services.dart';
import 'package:package_info/package_info.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/generated/l10n.dart';
import 'package:teormech/profile/about/bloc/cache_manager_bloc.dart';
import 'package:teormech/profile/about/foss_licenses_screen.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/styles/text.dart';

class AboutScreen extends StatefulWidget {
  const AboutScreen({Key? key}) : super(key: key);

  @override
  _AboutScreenState createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {
  final _future = PackageInfo.fromPlatform();

  @override
  void initState() {
    context.read<CacheManagerBloc>().init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ProfileAppBar(
        title: Text(
          S.of(context).about_screen_title,
          style: appBarTextStyle,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 20, left: 20),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Row(
            children: [
              SizedBox(
                  height: 50,
                  width: 50,
                  child: Image(
                    image: AssetImage('images/logo.png'),
                  )),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'HSTM Live',
                      style: appBarTextStyle.copyWith(color: Colors.black),
                    ),
                    FutureBuilder<PackageInfo>(
                        future: _future,
                        builder: (context, packageState) {
                          if (packageState.connectionState ==
                              ConnectionState.done) {
                            return Text(
                                'version ${packageState.data?.version}+${packageState.data?.buildNumber}');
                          } else
                            return Text('version');
                        }),
                  ],
                ),
              ),
            ],
          ),
          Divider(),
          SizedBox(
            width: double.infinity,
            height: 45,
            child: InkWell(
                child: Row(
                  children: [
                    Text(S.of(context).foss_licenses),
                  ],
                ),
                onTap: () {
                  context.read<BottomBarBloc>().go(OssLicensesPage.route());
                }),
          ),
          Divider(),
          BlocBuilder<CacheManagerBloc, CacheManagerState>(
            builder: (context, state) => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text('${S.of(context).cache} ${state.cacheMbs}'),
                    MaterialButton(
                        child: Text(S.of(context).clear_cache),
                        onPressed: () {
                          context.read<CacheManagerBloc>().clearCache();
                        })
                  ],
                ),
                Row(
                  children: [
                    Text('Данные: ${state.dataMbs}'),
                    MaterialButton(
                        child: Text(S.of(context).clear_data),
                        onPressed: () {
                          context.read<CacheManagerBloc>().clearData();
                        })
                  ],
                ),
                Divider(),
              ],
            ),
          ),
          SizedBox(
            width: double.infinity,
            height: 45,
            child: InkWell(
                child: Row(
                  children: [Text(S.of(context).copy_fcm_token)],
                ),
                onTap: () async {
                  final token = await FirebaseMessaging.instance.getToken();
                  if (token != null) {
                    Clipboard.setData(ClipboardData(text: token));
                    EdgeAlert.show(context,
                        backgroundColor: Colors.teal,
                        icon: Icons.close,
                        title: S.of(context).copy_fcm_token_success,
                        description: token,
                        duration: 1,
                        gravity: EdgeAlert.BOTTOM);
                  }
                }),
          ),
        ]),
      ),
    );
  }
}
