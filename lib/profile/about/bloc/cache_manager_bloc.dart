import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:teormech/profile/chat/model/chat_model.dart';
import 'package:teormech/profile/chat/model/message_model.dart';
import 'package:teormech/profile/profile/model/portfolio_state.dart';
import 'package:teormech/profile/profile/model/user_profile.dart';
import 'package:teormech/profile/profile/rep/user_portfolio_rep.dart';
import 'package:teormech/profile/profile/rep/user_profile_rep.dart';
import 'package:teormech/profile/scheduler/model/schedule_model.dart';
import 'package:teormech/profile/subscription/model/followers_state.dart';

class CacheManagerState {
  final int cache;
  final int data;
  CacheManagerState({required this.cache, required this.data});

  CacheManagerState copyWith({int? cache, int? data}) {
    return CacheManagerState(
        cache: cache ?? this.cache, data: data ?? this.data);
  }

  CacheManagerState.empty()
      : cache = 0,
        data = 0;

  String _format(int data) {
    final sizeInKb = data / 1024;
    final sizeInMb = sizeInKb / 1024;
    if (sizeInMb < 1)
      return sizeInKb.toStringAsFixed(1) + ' КБ';
    else
      return sizeInMb.toStringAsFixed(1) + ' МБ';
  }

  String get cacheMbs => _format(cache);
  String get dataMbs => _format(data);
}

class CacheManagerBloc extends Cubit<CacheManagerState> {
  CacheManagerBloc() : super(CacheManagerState.empty());

  int _dirStatSync(String dirPath) {
    int totalSize = 0;
    var dir = Directory(dirPath);
    try {
      if (dir.existsSync()) {
        dir
            .listSync(recursive: true, followLinks: false)
            .forEach((FileSystemEntity entity) {
          if (entity is File) {
            totalSize += entity.lengthSync();
          }
        });
      }
    } catch (e) {
      print(e.toString());
    }

    return totalSize;
  }

  void clearCache() async {
    await DefaultCacheManager().emptyCache();
    init();
  }

  void clearAll() async {
    UserProfileRepository().clearMem();
    UserPortfolioRepository().clearMem();
    await DefaultCacheManager().emptyCache();
    Hive.deleteFromDisk();
  }

  Future<void> clearData({bool needToInit = true}) async {
    final _dataDir = await getApplicationDocumentsDirectory();
    for (var i in _dataDir.listSync()) {
      if (i.path.endsWith('hive')) {
        final boxName = i.uri.pathSegments.last.replaceAll('.hive', '');

        Box box;
        if (boxName == 'profiles')
          box = await Hive.openBox<UserProfile>(boxName);
        else if (boxName == 'chats')
          box = await Hive.openBox<Chat>(boxName);
        else if (boxName == 'spec_messages')
          box = await Hive.openBox<MessagesList>(boxName);
        else if (boxName == 'userportfoliobox')
          box = await Hive.openBox<PortfolioState>(boxName);
        else if (boxName == 'months_events')
          box = await Hive.openBox<CalendarEventList>(boxName);
        else if (boxName == 'edit_portfolio')
          box = await Hive.openBox<PortfolioState>(boxName);
        else if (boxName == 'cached_inputs')
          box = await Hive.openBox<String>(boxName);
        else if (boxName == 'followers')
          box = await Hive.openBox<Followers>(boxName);
        else
          box = await Hive.openBox(boxName);
        await box.deleteFromDisk();
      }
    }
    if (needToInit) init();
  }

  void init() async {
    final _tempDir = await getTemporaryDirectory();
    final size = _dirStatSync(_tempDir.path);
    final _dataDir = await getApplicationDocumentsDirectory();
    final dataSize = _dirStatSync(_dataDir.path);
    emit(state.copyWith(cache: size, data: dataSize));
  }
}
