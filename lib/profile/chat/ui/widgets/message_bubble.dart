import 'dart:convert';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:open_file/open_file.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/chat/model/message_model.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';
import 'package:teormech/profile/newsfeed/rep/newsfeed_repository.dart';
import 'package:teormech/profile/newsfeed/ui/pages/events/event_screen.dart';
import 'package:teormech/profile/newsfeed/ui/pages/jobs/job_screen.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';
import 'package:teormech/rep/downloader.dart';
import 'package:teormech/rep/native_downloader.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';
import 'package:url_launcher/url_launcher.dart';
import 'chat_gallery.dart';

class VacancyRepostMessageBubble extends StatefulWidget {
  const VacancyRepostMessageBubble({Key? key, required this.id})
      : super(key: key);
  final String id;

  @override
  State<VacancyRepostMessageBubble> createState() =>
      _VacancyRepostMessageBubbleState();
}

class _VacancyRepostMessageBubbleState extends State<VacancyRepostMessageBubble>
    with AutomaticKeepAliveClientMixin {
  late final Future<JobModel> job;
  @override
  void initState() {
    job = NewsfeedRepository().getVacancy(widget.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return FutureBuilder(
        future: job,
        builder: ((context, AsyncSnapshot<JobModel> snapshot) {
          if (snapshot.hasData) {
            return InkWell(
              onTap: () {
                context
                    .read<BottomBarBloc>()
                    .go(JobScreen.route(snapshot.data!));
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text(
                          'Вакансия: ${snapshot.data?.position}',
                          style: tStyle.copyWith(
                              decoration: TextDecoration.underline),
                        ),
                        Spacer(),
                        ImageIcon(AssetImage('images/icons/pereyti.png'),
                            color: Colors.white)
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text('${snapshot.data?.company}',
                          style: TextStyle(color: Colors.white)),
                    ),
                  ],
                ),
              ),
            );
          } else
            return const SizedBox.shrink();
        }));
  }

  @override
  bool get wantKeepAlive => true;
}

class EventRepostMessageBubble extends StatefulWidget {
  const EventRepostMessageBubble({Key? key, required this.id})
      : super(key: key);
  final int id;

  @override
  State<EventRepostMessageBubble> createState() =>
      _EventRepostMessageBubbleState();
}

class _EventRepostMessageBubbleState extends State<EventRepostMessageBubble>
    with AutomaticKeepAliveClientMixin {
  late final Future<EventModel?> event;
  @override
  void initState() {
    event = NewsfeedRepository().fetchEvent(id: widget.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return FutureBuilder(
        future: event,
        builder: (context, AsyncSnapshot<EventModel?> state) {
          if (state.hasData) {
            final type = state.data?.type;
            final title = state.data?.title;
            final logo = state.data?.logo;

            return InkWell(
              onTap: () {
                context.read<BottomBarBloc>().go(
                    EventScreen.route(eventId: widget.id),
                    hideBottombar: true);
              },
              child: Flexible(
                child: Row(
                  children: [
                    if (logo != null)
                      SizedBox(
                          width: 50,
                          height: 50,
                          child: CachedNetworkImage(
                            imageUrl: EventsApi.eventPhoto + logo,
                            fit: BoxFit.cover,
                          )),
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(title ?? 'Мероприятие',
                                style: tStyle.copyWith(
                                    decoration: TextDecoration.none,
                                    fontWeight: FontWeight.bold)),
                            Text(type ?? 'событие',
                                style: tStyle.copyWith(
                                    decoration: TextDecoration.none))
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            );
          } else {
            return const SizedBox.shrink();
          }
        });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  bool get wantKeepAlive => true;
}

class MessageBubble extends StatelessWidget {
  const MessageBubble(
      {Key? key,
      required this.message,
      required this.selected,
      required this.isMyMessage})
      : super(key: key);

  final Message message;
  final bool selected;
  final bool isMyMessage;

  Widget _newsfeedRepostBubble(String message, double width) {
    dynamic msgObject;
    try {
      msgObject = jsonDecode(message.replaceAll("'", '"'));
    } catch (_) {
      msgObject =
          jsonDecode(message.replaceAll('type', "'type'").replaceAll("'", '"'));
    }
    Widget? child;

    if (msgObject['type'] == 'event') {
      child = EventRepostMessageBubble(
        id: msgObject['id'],
        key: ValueKey(msgObject['id']),
      );
    }
    if (msgObject['type'] == 'vacancy') {
      child = VacancyRepostMessageBubble(
        id: msgObject['id'].toString(),
        key: ValueKey(msgObject['id']),
      );
    }

    return SizedBox(
      height: 70,
      width: width,
      child: child,
    ); //TODO
  }

  Widget _bigMessageBubbleContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _messageText(),
            if (message.photos.isNotEmpty) BubbleGallery(message: message),
            if (message.attachments.isNotEmpty)
              AttachmentsGallery(
                message: message,
              ),
          ],
        ),
        Padding(
          padding: EdgeInsets.only(top: 2),
          child: Text(
            message.isEdited
                ? 'Ред. в ${message.modificationDateFormatted}'
                : message.dateFormatted,
            style: h2DrawerGreyText.copyWith(
                fontSize: 12,
                color: Colors.white.withAlpha(0xAA),
                fontWeight: FontWeight.bold),
          ),
        ),
      ],
    );
  }

  Widget _messageText() {
    return Linkify(
      text: message.text,
      textAlign: TextAlign.start,
      onOpen: (link) => launch(link.url),
      style: h2DrawerGreyText.copyWith(color: Colors.white),
      linkStyle: h2DrawerGreyText.copyWith(color: Colors.white),
    );
  }

  Widget _smallMessageBubbleContent() {
    return Row(
      children: [
        _messageText(),
        Padding(
          padding: const EdgeInsets.only(top: 12, left: 5),
          child: Text(
            message.isEdited
                ? 'Ред.  в ${message.modificationDateFormatted}'
                : message.dateFormatted,
            style: h2DrawerGreyText.copyWith(
                fontSize: 12,
                color: Colors.white.withAlpha(0xAA),
                fontWeight: FontWeight.bold),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return BlocBuilder<UserProfileBloc, UserProfileState>(
        builder: (context, userProfile) {
      final isNewsfeedRepost = message.type == Message.NEWSFEED_REPOST;
      final isBigMessage = message.text.length > 12 ||
          message.photos.isNotEmpty ||
          message.attachments.isNotEmpty;
      return Container(
        key: ValueKey(message.id),
        child: isNewsfeedRepost
            ? _newsfeedRepostBubble(message.text, width * 0.6)
            : (isBigMessage)
                ? _bigMessageBubbleContent()
                : _smallMessageBubbleContent(),
        padding: const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: userProfile.profile.isAdmin
                    ? [
                        Color(0xFFed8261).withAlpha(selected ? 100 : 210),
                        Color(0xf75676).withAlpha(selected ? 100 : 210)
                      ]
                    : !isMyMessage
                        ? [
                            GradientColors.leftColorRightButton
                                .withAlpha(selected ? 100 : 210),
                            GradientColors.leftColorLeftButton
                                .withAlpha(selected ? 100 : 210),
                          ]
                        : [
                            Color(0xFF3356D6).withAlpha(selected ? 100 : 210),
                            Color(0xFF26A5E2).withAlpha(selected ? 100 : 210)
                          ]),
            border: Border.all(width: 1, color: Colors.transparent),
            borderRadius: BorderRadius.circular(19)),
      );
    });
  }
}

class BubbleGallery extends StatelessWidget {
  const BubbleGallery({Key? key, required this.message}) : super(key: key);
  final Message message;

  @override
  Widget build(BuildContext context) {
    final photos = message.photos;
    if (photos.length == 1) {
      return Padding(
        padding: const EdgeInsets.only(top: 10),
        child:
            ImageInMessageBubble(photo: photos.first, idx: 0, message: message),
      );
    }

    return Container(
        padding: const EdgeInsets.only(top: 10),
        constraints: BoxConstraints(
            maxHeight: MediaQuery.of(context).size.height * 0.8,
            maxWidth: MediaQuery.of(context).size.width * 0.6),
        child: StaggeredGridView.countBuilder(
            crossAxisCount: 6,
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: photos.length < 5 ? photos.length : 5,
            itemBuilder: (context, idx) => idx != 4
                ? Container(
                    child: ImageInMessageBubble(
                        photo: photos[idx], idx: idx, message: message))
                : Material(
                    child: InkWell(
                      onTap: () {
                        context.read<BottomBarBloc>().go(
                            MaterialPageRoute(
                                builder: (context) => ChatGallery(
                                      files: message.photos,
                                      mode: PhotoGallery.Chat,
                                      idx: idx,
                                      isFromChat: true,
                                    )),
                            hideBottombar: true);
                      },
                      child: Container(
                        color: Colors.grey.withAlpha(100),
                        child: Center(
                            child: Text(
                          '+${photos.length - 4}',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        )),
                      ),
                    ),
                  ),
            staggeredTileBuilder: (idx) {
              final length = photos.length;
              if (length == 2 || length == 4) {
                return StaggeredTile.count(3, 3);
              }
              if (length == 3) {
                if (idx == 0)
                  return StaggeredTile.count(6, 6);
                else
                  return StaggeredTile.count(3, 3);
              }
              if (length > 4) {
                if (idx < 2)
                  return StaggeredTile.count(3, 3);
                else
                  return StaggeredTile.count(2, 2);
              }
              return null;
            }));
  }
}

class ImageInMessageBubble extends StatelessWidget {
  const ImageInMessageBubble(
      {Key? key, required this.photo, required this.idx, required this.message})
      : super(key: key);
  final int idx;
  final String photo;

  final Message message;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final needToLoadPlaceholder = photo.startsWith('https://');
    return ConstrainedBox(
      constraints: BoxConstraints(
          maxHeight: size.height * 0.6, maxWidth: size.width * 0.6),
      child: GestureDetector(
        onTap: () {
          context.read<BottomBarBloc>().go(
              MaterialPageRoute(
                  builder: (context) => ChatGallery(
                        files: message.photos,
                        idx: idx,
                        mode: PhotoGallery.Chat,
                        isFromChat: true,
                      )),
              hideBottombar: true);
        },
        child: CachedNetworkImage(
          imageUrl:
              needToLoadPlaceholder ? photo : ChatApi.chatPhotosMax + photo,
          imageBuilder: (context, prov) => ClipRect(
            clipBehavior: Clip.hardEdge,
            child: FittedBox(
              fit: BoxFit.cover,
              child: Image(
                image: prov,
              ),
            ),
          ),
          placeholder: (context, url) {
            return SizedBox(
              height: 100,
              width: 100,
              child: Center(
                child: CachedNetworkImage(
                  imageUrl: needToLoadPlaceholder
                      ? photo
                      : ChatApi.chatPhotosMin + photo,
                  imageBuilder: (context, prov) =>
                      Image(fit: BoxFit.cover, image: prov),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

class AttachmentsGalleryRow extends StatefulWidget {
  const AttachmentsGalleryRow(
      {Key? key,
      required this.attachment,
      required this.realName,
      this.isInGallery = false})
      : super(key: key);
  final String attachment;
  final String realName;
  final bool isInGallery;

  @override
  _AttachmentsGalleryRowState createState() => _AttachmentsGalleryRowState();
}

class _AttachmentsGalleryRowState extends State<AttachmentsGalleryRow> {
  double _progress = 0;

  Map<String, String> _taskIds = {};

  final _port = ReceivePort();

  late final String url;

  void _initIds() async {
    final list = await FlutterDownloader.loadTasksWithRawQuery(
        query: 'SELECT * FROM task WHERE status!=3');

    list?.forEach((task) {
      _taskIds[task.url] = task.taskId;
    });
  }

  @override
  void initState() {
    super.initState();

    url = ChatApi.chatFiles + widget.attachment;

    IsolateNameServer.registerPortWithName(
        _port.sendPort, Downloader.DOWNLOAD_SEND_PORT);

    _initIds();

    _port.listen((message) {
      setState(() {
        if (message[0] == _taskIds[url]) {
          _progress = message[2] / 100;
          print(_progress);
        }
      });
    });
  }

  @override
  void dispose() {
    IsolateNameServer.removePortNameMapping(Downloader.DOWNLOAD_SEND_PORT);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(bottom: 5),
      constraints: BoxConstraints(
          maxWidth: MediaQuery.of(context).size.width *
              (widget.isInGallery ? 1 : 0.6)),
      child: InkWell(
        onTap: () async {
          if (_progress > 0 && _progress < 0.99) return;
          bool isAllowed = false;
          while (!isAllowed)
            isAllowed = await NativeDownloader.checkPermission();

          final lPath = await NativeDownloader.getDownloadDirectory();
          final dPath = (lPath?.path ?? '') + '/' + widget.realName;
          final fileExists = File(dPath).existsSync();

          if (fileExists) {
            OpenFile.open(dPath);
          } else {
            final taskId = await FlutterDownloader.enqueue(
                url: url,
                savedDir: lPath!.path,
                saveInPublicStorage: true,
                showNotification: true,
                openFileFromNotification: true,
                fileName: widget.realName);
            if (taskId != null) {
              _taskIds[url] = taskId;
            }
          }
        },
        child: Row(
          children: [
            if (_progress > 0 && _progress < 0.99)
              SizedBox(
                child: CircularProgressIndicator(value: _progress),
                height: 24,
                width: 24,
              )
            else
              Icon(
                Icons.file_copy,
                color: Colors.white,
              ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Text(
                  widget.realName,
                  style: TextStyle(color: Colors.white),
                  maxLines: widget.isInGallery ? null : 2,
                  overflow: widget.isInGallery ? null : TextOverflow.ellipsis,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class AttachmentsGallery extends StatefulWidget {
  const AttachmentsGallery({Key? key, required this.message}) : super(key: key);
  final Message message;

  @override
  _AttachmentsGalleryState createState() => _AttachmentsGalleryState();
}

class _AttachmentsGalleryState extends State<AttachmentsGallery> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        children: widget.message.attachments
            .map((e) => AttachmentsGalleryRow(
                  attachment: e['file'],
                  realName: e['real_name'],
                ))
            .toList());
  }
}
