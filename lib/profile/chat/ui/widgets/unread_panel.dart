import 'package:flutter/material.dart';
import 'package:teormech/styles/colors.dart';

class UnreadPanel extends StatelessWidget {
  const UnreadPanel(
      {Key? key,
      required this.countOfUnread,
      required this.readAllMessagesCallback,
      required this.jumpToLastUnread})
      : super(key: key);

  final int countOfUnread;
  final VoidCallback readAllMessagesCallback;
  final VoidCallback jumpToLastUnread;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: GestureDetector(
        onTap: jumpToLastUnread,
        child: Container(
          height: 40,
          width: double.infinity,
          color: GradientColors.backColor,
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Text('${countOfUnread.toString()} непрочитанных',
                    style: TextStyle(
                      color: Colors.white,
                    )),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: IconButton(
                    icon: Icon(Icons.mark_chat_read, color: Colors.white),
                    onPressed: readAllMessagesCallback),
              )
            ],
          ),
        ),
      ),
    );
  }
}
