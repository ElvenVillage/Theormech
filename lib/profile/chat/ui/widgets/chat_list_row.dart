import 'dart:convert';
import 'package:teormech/profile/chat/model/message_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/chat/model/chat_model.dart';
import 'package:teormech/profile/chat/ui/pages/chat_screen.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/clipped_avatar.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/empty_avatar.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';

class ChatListRow extends StatelessWidget {
  final String? profileAvatar;
  final Chat chat;
  final bool? online;
  final String? nestedProfileAvatar;
  final bool isGroupChat;
  final String? onlineString;

  ChatListRow({
    required this.profileAvatar,
    required this.chat,
    this.isGroupChat = false,
    this.nestedProfileAvatar,
    this.online,
    this.onlineString,
  });

  String _parseServiceMessage(String body) {
    final a = jsonDecode(body.replaceAll("'", '"'));
    switch (a['event']) {
      case 'invite':
        return 'Пользователь приглашен';
      case 'join':
        return 'Пользователь присоединился';
      case 'leave':
        return 'Пользователь покинул чат';
      case 'remove':
        return 'Пользователь удален';
      default:
        return 'Сообщение удалено';
    }
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width > 500
        ? 500
        : MediaQuery.of(context).size.width;

    var avatar = ((profileAvatar != '') && (profileAvatar != null))
        ? UserAvatar.fromNetwork(
            size: width / 8,
            key: Key(chat.body + profileAvatar!),
            online: online ?? false,
            ignoreFrame: isGroupChat,
            avatarUrl: (isGroupChat
                    ? GroupChatsApi.groupChatLogoMin
                    : ProfileApi.profileAvatar) +
                profileAvatar!)
        : EmptyAvatar(
            size: width / 8,
            online: online ?? false,
            ignoreOnline: chat.isGroupChat,
            groupChat: chat.isGroupChat,
          );
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 500),
      child: InkWell(
        key: Key(chat.messageDate),
        child: Container(
          padding: EdgeInsets.only(left: 16, top: 4, bottom: 4),
          decoration: chat.unreadMessages <= 0
              ? null
              : BoxDecoration(
                  gradient: LinearGradient(colors: [
                  Colors.blue.withAlpha(50),
                  Colors.white.withAlpha(50)
                ])),
          child: Row(
            children: [
              avatar,
              Container(
                padding: const EdgeInsets.only(left: 10),
                width: width / 1.8,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                        isGroupChat
                            ? (chat.name.isNotEmpty)
                                ? chat.name
                                : 'Без названия'
                            : chat.surname + ' ' + chat.name,
                        style: profileTextStyle.copyWith(
                            fontWeight: FontWeight.w500,
                            fontSize: 14,
                            color: Colors.black)),
                    Padding(
                        padding: EdgeInsets.only(top: 3),
                        child: Row(
                          children: [
                            if (nestedProfileAvatar != null)
                              Padding(
                                padding: const EdgeInsets.only(right: 5.0),
                                child: UserAvatar.fromNetwork(
                                    size: width / 16,
                                    online: false,
                                    ignoreFrame: true,
                                    avatarUrl: ProfileApi.profileAvatar +
                                        nestedProfileAvatar!),
                              ),
                            Expanded(
                              child: Text(
                                chat.body.trim().isEmpty
                                    ? 'Пересланное сообщение'
                                    : chat.type == Message.NEWSFEED_REPOST
                                        ? _parseNewsfeedRepostMessage(chat.body)
                                        : chat.type == Message.PLAIN
                                            ? chat.body
                                            : _parseServiceMessage(chat.body),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ],
                        ))
                  ],
                ),
              ),
              Expanded(
                  child: Column(children: [
                Text(chat.formattedMessage),
                if (chat.unreadMessages != 0)
                  Container(
                    width: chat.unreadMessages > 0 ? 17 : 10,
                    height: chat.unreadMessages > 0 ? 17 : 10,
                    padding: const EdgeInsets.all(0),
                    margin: EdgeInsets.only(top: 10),
                    child: Center(
                      child: Text(
                        chat.unreadMessages != -1
                            ? chat.unreadMessages.toString()
                            : '',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 10),
                      ),
                    ),
                    decoration: BoxDecoration(
                        color: chat.unreadMessages != -1
                            ? GradientColors.backColor
                            : Color(0xFF848687),
                        //border: Border.all(width: 1),
                        borderRadius: BorderRadius.circular(36)),
                  ),
              ]))
            ],
          ),
        ),
        onTap: () {
          context
              .read<BottomBarBloc>()
              .go(ChatScreenWrapper.route(chat: chat), hideBottombar: true);

          FocusScope.of(context).requestFocus(FocusNode());
        },
      ),
    );
  }

  String _parseNewsfeedRepostMessage(String body) {
    dynamic message;
    try {
      message = jsonDecode(body.replaceAll("'", '"'));
    } catch (_) {
      message =
          jsonDecode(body.replaceAll('type', "'type'").replaceAll("'", '"'));
    }
    if (message['type'] == 'vacancy') {
      return 'Вакансия';
    }
    if (message['type'] == 'project') {} //TODO Project Repost
    if (message['type'] == 'event') {
      return 'Мероприятие';
    }
    return 'Новость';
  }
}
