import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/profile/chat/bloc/chat_list_bloc.dart';
import 'package:teormech/profile/chat/model/chat_list_state.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/clipped_avatar.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/empty_avatar.dart';

class RepostMessages extends StatelessWidget {
  const RepostMessages({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChatListBloc, ChatListState>(
      builder: (context, state) {
        final size = MediaQuery.of(context).size;

        return Align(
          alignment: Alignment.bottomCenter,
          child: Container(
              width: double.infinity,
              height: 70,
              color: Colors.grey.shade400,
              child: ListView.builder(
                itemBuilder: (context, idx) {
                  final message = state.messagesForForwarding[idx];

                  return Card(
                      child: SizedBox(
                    height: 40,
                    width: size.width / 2.5,
                    child: Stack(
                      children: [
                        Positioned.fill(
                          child: Row(
                            children: [
                              UserAvatarInsideRepostedBar(message.uid,
                                  size: size.width / 5),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 4.0),
                                  child: Text(
                                    message.text,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: null,
                                    softWrap: true,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Positioned(
                          right: 0,
                          top: -10,
                          child: IconButton(
                            icon: Icon(Icons.cancel_outlined),
                            onPressed: () {
                              context
                                  .read<ChatListBloc>()
                                  .removeMessageFromForwarding(
                                      message: message);
                            },
                          ),
                        ),
                      ],
                    ),
                  ));
                },
                scrollDirection: Axis.horizontal,
                itemCount: state.messagesForForwarding.length,
              )),
        );
      },
    );
  }
}

class UserAvatarInsideRepostedBar extends StatelessWidget {
  const UserAvatarInsideRepostedBar(this._uid, {Key? key, required this.size})
      : super(key: key);
  final String _uid;
  final double size;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<UserProfileBloc>(
      create: (context) =>
          UserProfileBloc.fromLid(lid: _uid, loadFromHive: false),
      child: Padding(
        padding: const EdgeInsets.only(left: 3.0),
        child: Builder(
          builder: (context) {
            return BlocBuilder<UserProfileBloc, UserProfileState>(
              builder: (context, state) {
                if (state.profile.avatar != '')
                  return UserAvatar.fromNetwork(
                      size: size / 2,
                      avatarUrl:
                          ProfileApi.profileAvatar + state.profile.avatar!);
                else
                  return EmptyAvatar(
                    size: size / 2,
                    online: false,
                    ignoreOnline: true,
                  );
              },
            );
          },
        ),
      ),
    );
  }
}
