import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/profile/chat/model/message_model.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/clipped_avatar.dart';
import 'message_bubble.dart';

class RepostMessageEnvironment extends StatelessWidget {
  const RepostMessageEnvironment({
    Key? key,
    required this.message,
    required this.userCallback,
    required this.isMyMessage,
    required this.selected,
  }) : super(key: key);

  final List<Message> message;
  final Function userCallback;
  final bool isMyMessage;
  final bool selected;

  Widget _userNameInRepostSequence(Message msg) {
    return Padding(
      padding: const EdgeInsets.only(top: 5.0),
      child: BlocProvider(
        create: (context) => UserProfileBloc.fromLid(
          lid: msg.forward,
          loadFromHive: false,
        ),
        child: Builder(builder: (context) {
          return BlocBuilder<UserProfileBloc, UserProfileState>(
              builder: (context, forwardUserState) {
            return Row(
              children: [
                if (forwardUserState.profile.avatar != '' &&
                    forwardUserState.profile.avatar != null)
                  UserAvatar.fromNetwork(
                      size: 25,
                      online: forwardUserState.online,
                      avatarUrl: ProfileApi.profileAvatar +
                          forwardUserState.profile.avatar!)
                else
                  UserAvatar.fromString(
                      size: 25,
                      online: forwardUserState.online,
                      avatarString: 'images/logostud.png'),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Text(
                    '${forwardUserState.profile.name} ' +
                        '${forwardUserState.profile.surname}:',
                  ),
                )
              ],
            );
          });
        }),
      ),
    );
  }

  Widget _messageBubbleInRepostSequence(Message msg) {
    final bubble = MessageBubble(
      isMyMessage: isMyMessage,
      key: ValueKey(msg.id),
      message: msg,
      selected: selected,
    );
    return Padding(
        padding: const EdgeInsets.only(top: 5),
        child: (msg.text.length > 30)
            ? bubble
            : Row(
                children: [bubble],
              ));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border(left: BorderSide(width: 2, color: Color(0x990b239a)))),
      child: Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child: Builder(builder: (context) {
          final children = <Widget>[];

          children.add(_userNameInRepostSequence(message.last));
          children.add(_messageBubbleInRepostSequence(message.last));

          for (var i = message.length - 2; i >= 0; i--) {
            if (message[i].forward != message[i + 1].forward)
              children.add(_userNameInRepostSequence(message[i]));
            children.add(_messageBubbleInRepostSequence(message[i]));
          }
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: children,
          );
        }),
      ),
    );
  }
}
