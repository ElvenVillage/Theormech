import 'dart:convert';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:teormech/profile/chat/model/message_model.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';

enum ServiceMessageType { loading, invite, remove, join, leave }

class ServiceMessageModel {
  final ServiceMessageType msg;
  final String acting;
  final String? passive;

  ServiceMessageModel({required this.msg, required this.acting, this.passive});

  factory ServiceMessageModel.fromJson(Map<String, dynamic> json) {
    final event = json['event'];
    switch (event) {
      case 'invite':
        return ServiceMessageModel(
            msg: ServiceMessageType.invite,
            acting: json['acting_id'],
            passive: json['passive_id']);
      case 'join':
        return ServiceMessageModel(
            msg: ServiceMessageType.join, acting: json['acting_id']);
      case 'remove':
        return ServiceMessageModel(
            msg: ServiceMessageType.remove,
            acting: json['acting_id'],
            passive: json['passive_id']);
      case 'leave':
        return ServiceMessageModel(
            msg: ServiceMessageType.leave, acting: json['acting_id']);
      default:
        return ServiceMessageModel(
            msg: ServiceMessageType.loading, acting: '0');
    }
  }
}

class ServiceMessage extends StatefulWidget {
  const ServiceMessage({required this.message, this.key}) : super(key: key);
  final Key? key;
  final Message message;

  @override
  _ServiceMessageState createState() => _ServiceMessageState();
}

class _ServiceMessageState extends State<ServiceMessage> {
  late final UserProfileBloc _invitorBloc;
  late final UserProfileBloc _invitedBloc;
  late final ServiceMessageModel model;

  @override
  void initState() {
    final json = jsonDecode(widget.message.text);
    setState(() {
      model = ServiceMessageModel.fromJson(json);
    });

    _invitorBloc =
        UserProfileBloc.fromLid(lid: model.acting, loadFromHive: false);

    _invitedBloc = UserProfileBloc.fromLid(
        lid: model.passive ?? model.acting, loadFromHive: false);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<UserProfileBloc>.value(
      key: Key(widget.message.text),
      value: _invitedBloc,
      child: Builder(builder: (context) {
        return BlocBuilder<UserProfileBloc, UserProfileState>(
            builder: (context, invitedState) {
          return BlocProvider<UserProfileBloc>.value(
            value: _invitorBloc,
            child: Builder(builder: (context) {
              return Container(
                margin: EdgeInsets.only(
                  top: 10,
                ),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(123),
                ),
                height: 32,
                alignment: Alignment.center,
                child: BlocBuilder<UserProfileBloc, UserProfileState>(
                    builder: (context, invitorState) {
                  var baseText = '';
                  switch (model.msg) {
                    case ServiceMessageType.loading:
                      baseText = '';
                      break;
                    case ServiceMessageType.invite:
                      if (invitorState.profile.sex == '1') {
                        baseText =
                            '${invitorState.profile.surname} пригласил ${invitedState.profile.surname}';
                      } else {
                        baseText =
                            '${invitorState.profile.surname} пригласила ${invitedState.profile.surname}';
                      }
                      break;
                    case ServiceMessageType.remove:
                      if (invitorState.profile.sex == '1') {
                        baseText =
                            '${invitorState.profile.surname} удалил ${invitedState.profile.surname}';
                      } else {
                        baseText =
                            '${invitorState.profile.surname} удалила ${invitedState.profile.surname}';
                      }
                      break;
                    case ServiceMessageType.join:
                      if (invitorState.profile.sex == '1') {
                        baseText =
                            '${invitorState.profile.surname} присоединился к чату';
                      } else {
                        baseText =
                            '${invitorState.profile.surname} присоединилась к чату';
                      }

                      break;
                    case ServiceMessageType.leave:
                      if (invitorState.profile.sex == '1') {
                        baseText = 'покинул чат';
                      } else {
                        baseText = 'покинула чат';
                      }
                      break;
                  }
                  return Text(
                    baseText,
                    key: Key(baseText),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                  );
                }),
              );
            }),
          );
        });
      }),
    );
  }
}
