import 'dart:math';

import 'package:flutter/material.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/chat/model/message_model.dart';
import 'package:teormech/profile/chat/ui/widgets/message_widget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/profile/profile/ui/profile_wrapper.dart';

class MessageRow extends StatelessWidget {
  const MessageRow(
      {Key? key,
      this.needToShowSpacer = false,
      this.bottomSpacer,
      // this.reversedSpacers = false,
      required this.message,
      required this.selected,
      required this.isInGroupChatSecondary,
      this.isInGroupChat = false,
      this.onTapCallback,
      required this.selectionMode,
      required this.myId,
      this.isFirstUnread = false,
      required this.selectionLongTapCallback,
      required this.selectionTapCallback})
      : super(key: key);
  //final bool reversedSpacers;
  final bool needToShowSpacer;
  final String? bottomSpacer;
  final bool isFirstUnread;
  final List<Message> message;
  final String myId;
  final bool selected;
  final bool selectionMode;
  final bool isInGroupChat;
  final VoidCallback? onTapCallback;
  final bool isInGroupChatSecondary;

  final VoidCallback selectionLongTapCallback;
  final VoidCallback selectionTapCallback;

  bool get _isMyMessage => message.first.uid == myId;

  Widget _messageWidget(BuildContext context) {
    return MessageWidget(
      key: ValueKey(message.first.id),
      isMyMessage: _isMyMessage,
      message: message,
      isInGroupChatSecondary: isInGroupChatSecondary,
      isInGroupChat: isInGroupChat,
      userCallback: () {
        context.read<BottomBarBloc>().go(PageRouteBuilder(
            pageBuilder: (context, f1, f2) => ProfileScreenWrapper.fromLid(
                  lid: message.first.forward != '' &&
                          message.first.forward != '0'
                      ? message.first.forward
                      : message.first.uid,
                )));
      },
      selected: selected,
    );
  }

  Widget _spacer(String text, double width) {
    return Container(
      margin: EdgeInsets.only(
        top: 10,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(123),
      ),
      height: 32,
      width: width * 0.3,
      alignment: Alignment.center,
      child: Text(
        text,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.grey,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var sideFlex = 2;
    var messageFlex = 1;

    final a = (message.map((e) => e.text.length)).reduce(max);
    if (a > 30) {
      sideFlex = 1;
      messageFlex = 2;
    }

    final messageWidget = _messageWidget(context);
    final width = MediaQuery.of(context).size.width;

    final topSpacers = <Widget>[
      if (needToShowSpacer) _spacer(message.first.dateDayFormatted, width),
      if (isFirstUnread)
        Row(
          children: [
            Expanded(
                child: Divider(
              color: Colors.grey,
              indent: 10,
              height: 2,
            )),
            Container(
              margin: const EdgeInsets.only(bottom: 16.0),
              child: _spacer('Новые сообщения', width * 1.4),
            ),
            Expanded(
                child: Divider(
              indent: 10,
              color: Colors.grey,
              height: 2,
            ))
          ],
        ),
    ];

    final bottomSpacers = <Widget>[
      if (bottomSpacer != null)
        Padding(
          padding: const EdgeInsets.only(bottom: 15.0),
          child: _spacer(bottomSpacer!, width * 2),
        )
    ];

    final body = GestureDetector(
      behavior: HitTestBehavior.translucent,
      onLongPress: () {
        selectionLongTapCallback();
      },
      onTap: () {
        if (onTapCallback != null) onTapCallback!();
        selectionTapCallback();
      },
      child: Padding(
        padding: EdgeInsets.only(top: isFirstUnread ? 10 : 3, bottom: 7),
        child: Row(
          children: [
            if (message.first.uid == myId)
              Spacer(
                flex: sideFlex,
              ),
            if (messageFlex != 1)
              Flexible(flex: messageFlex, child: messageWidget)
            else
              messageWidget,
            if (message.first.uid != myId)
              Spacer(
                flex: sideFlex,
              )
          ],
        ),
      ),
    );

    // if (!reversedSpacers) {
    return Column(
      children: [...topSpacers, body, ...bottomSpacers],
    );
    // } else {
    //    return Column(
    //      children: [...bottomSpacers, body, ...topSpacers],
    //    );
    //  }
  }
}
