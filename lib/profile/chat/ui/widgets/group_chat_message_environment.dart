import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/profile/chat/model/message_model.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/clipped_avatar.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/empty_avatar.dart';

class GroupChatMessageEnvironment extends StatelessWidget {
  const GroupChatMessageEnvironment(
      {Key? key,
      required this.message,
      required this.userCallback,
      required this.selected,
      required this.isInGroupChatSecondary,
      required this.bubble})
      : super(key: key);

  final Message message;
  final Function userCallback;
  final bool selected;
  final Widget bubble;
  final bool isInGroupChatSecondary;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserProfileBloc, UserProfileState>(
        builder: (context, userState) {
      return Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (!isInGroupChatSecondary)
              GestureDetector(
                onTap: () {
                  userCallback();
                },
                child: Row(
                  children: [
                    if (userState.profile.avatar != '' &&
                        userState.profile.avatar != null)
                      UserAvatar.fromNetwork(
                          size: 25,
                          avatarUrl: ProfileApi.profileAvatar +
                              userState.profile.avatar!)
                    else
                      EmptyAvatar(
                        size: 25,
                        ignoreOnline: true,
                        groupChat: false,
                        online: false,
                      ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text(
                        '${userState.profile.name} ' +
                            '${userState.profile.surname}:',
                      ),
                    )
                  ],
                ),
              ),
            Padding(
                padding: const EdgeInsets.only(
                  left: 35,
                ),
                child: bubble),
          ],
        ),
      );
    });
  }
}
