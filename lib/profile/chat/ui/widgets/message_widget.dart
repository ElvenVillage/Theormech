import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/profile/chat/model/message_model.dart';
import 'package:teormech/profile/chat/ui/widgets/group_chat_message_environment.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'message_bubble.dart';
import 'repost_message_environment.dart';

class MessageWidget extends StatelessWidget {
  const MessageWidget(
      {Key? key,
      required this.message,
      required this.userCallback,
      required this.selected,
      required this.isInGroupChatSecondary,
      this.isInGroupChat = false,
      required this.isMyMessage})
      : super(key: key);

  final List<Message> message;

  final bool selected;
  final bool isInGroupChatSecondary;
  final bool isMyMessage;
  final Function userCallback;
  final bool isInGroupChat;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<UserProfileBloc>(
        create: (context) => UserProfileBloc.fromLid(
              lid: message.first.uid,
              loadFromHive: false,
            ),
        child: (!isInGroupChat)
            ? (message.first.forward != '0')
                ? RepostMessageEnvironment(
                    message: message,
                    userCallback: userCallback,
                    isMyMessage: isMyMessage,
                    selected: selected)
                : MessageBubble(
                    key: ValueKey(message.first.id),
                    message: message.first,
                    selected: selected,
                    isMyMessage: isMyMessage,
                  )
            : (message.first.forward == '0')
                ? GroupChatMessageEnvironment(
                    message: message.first,
                    isInGroupChatSecondary: isInGroupChatSecondary,
                    userCallback: userCallback,
                    selected: selected,
                    bubble: MessageBubble(
                        message: message.first,
                        key: ValueKey(message.first.id),
                        selected: selected,
                        isMyMessage: isMyMessage))
                : GroupChatMessageEnvironment(
                    message: message.first,
                    isInGroupChatSecondary: false,
                    userCallback: userCallback,
                    selected: selected,
                    bubble: RepostMessageEnvironment(
                        message: message,
                        userCallback: userCallback,
                        isMyMessage: isMyMessage,
                        selected: selected)));
  }
}
