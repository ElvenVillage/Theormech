import 'dart:io';
import 'dart:math';

import 'package:dio/dio.dart';
import 'package:edge_alert/edge_alert.dart';
import 'package:flutter/services.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:path_provider/path_provider.dart';
import 'package:photo_view/photo_view.dart';

import 'package:teormech/api_constants.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_list_bloc.dart';

enum PhotoGallery { Chat, Post }

class ChatGallery extends StatefulWidget {
  final List<dynamic> files;
  final PhotoGallery mode;
  final int idx;
  final bool isFromChat;
  ChatGallery(
      {required this.files,
      required this.idx,
      required this.isFromChat,
      required this.mode});

  @override
  _ChatGalleryState createState() => _ChatGalleryState(idx);
}

class _ChatGalleryState extends State<ChatGallery> {
  late final PageController _pageController;
  bool _enableScrolling = true;

  late final List<Key> _keys;

  bool loading = false;
  int _index = 0;

  _ChatGalleryState(int initialIndex) {
    _pageController = PageController(initialPage: initialIndex);
    _index = initialIndex;
    _pageController.addListener(() {
      setState(() {
        _index = _pageController.page?.round() ?? initialIndex;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _keys = [for (var i = 0; i < widget.files.length; i++) Key(i.toString())];
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp
    ]);
  }

  @override
  void dispose() {
    _pageController.dispose();
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String downloadPath;

    switch (widget.mode) {
      case PhotoGallery.Chat:
        downloadPath = ChatApi.chatPhotosMax + widget.files[_index];
        break;
      case PhotoGallery.Post:
        downloadPath = widget.files[_index];
    }
    return WillPopScope(
      onWillPop: () async {
        context.read<BottomBarBloc>().pop();
        return true;
      },
      child: Scaffold(
          backgroundColor: Colors.black,
          appBar: AppBar(
            backgroundColor: Colors.black,
            title: Text('Вложение'),
            actions: [
              SizedBox(
                width: 40,
                height: 40,
                child: IconButton(
                  icon: ImageIcon(
                    AssetImage('images/icons/selection_bar/share.png'),
                  ),
                  onPressed: () async {
                    final rnd = new Random();

                    Directory tempDir = await getTemporaryDirectory();
                    String tempPath = tempDir.path;

                    final tempFilePath =
                        '$tempPath' + (rnd.nextInt(100)).toString() + '.png';

                    final dio = Dio();

                    await dio.download(downloadPath, tempFilePath);

                    context
                        .read<ChatListBloc>()
                        .addPhotosForForwarding(files: [File(tempFilePath)]);

                    var max = 3;
                    if (widget.isFromChat) max = 2;
                    for (var i = 0; i < max; i++)
                      context.read<BottomBarBloc>().pop();
                    context.read<BottomBarBloc>().setTab(BottomBars.Chat);
                  },
                  iconSize: 38,
                  color: Colors.white,
                ),
              ),
              if (!loading)
                SizedBox(
                  width: 40,
                  height: 40,
                  child: IconButton(
                      icon: Icon(Icons.file_download),
                      color: Colors.white,
                      onPressed: () async {
                        setState(() {
                          loading = true;
                        });
                        final result = await GallerySaver.saveImage(
                            downloadPath,
                            albumName: 'ВШТМ Live');
                        var error = false;
                        if (result ?? false)
                          setState(() {
                            loading = false;
                          });
                        else
                          error = true;
                        EdgeAlert.show(context,
                            title: error ? 'Ошибка' : 'Сохранено',
                            icon: error
                                ? Icons.error_outline_rounded
                                : Icons.verified,
                            backgroundColor: error ? Colors.red : Colors.green,
                            duration: EdgeAlert.LENGTH_SHORT,
                            description: error ? 'Ошибка' : 'Сохранено',
                            gravity: EdgeAlert.BOTTOM);
                      }),
                )
              else
                SizedBox(
                  height: 40,
                  width: 40,
                  child: Center(
                    child: CircularProgressIndicator(
                      backgroundColor: Colors.transparent,
                    ),
                  ),
                ),
            ],
          ),
          body: PageView.builder(
            physics: _enableScrolling ? null : NeverScrollableScrollPhysics(),
            controller: _pageController,
            itemCount: widget.files.length,
            itemBuilder: (context, index) {
              final url = widget.mode == PhotoGallery.Chat
                  ? ChatApi.chatPhotosMax + widget.files[index]
                  : widget.files[index];
              return Dismissible(
                  direction: DismissDirection.vertical,
                  onDismissed: (direction) {
                    context.read<BottomBarBloc>().pop();
                  },
                  key: _keys[index],
                  child: CachedNetworkImage(
                    imageUrl: url,
                    imageBuilder: (context, provider) => PhotoView(
                      imageProvider: provider,
                      minScale: PhotoViewComputedScale.contained,
                      scaleStateChangedCallback: (scaleState) {
                        if (scaleState.index == 0) {
                          setState(() {
                            _enableScrolling = true;
                          });
                        } else
                          setState(() {
                            _enableScrolling = false;
                          });
                      },
                    ),
                  ));
            },
          )),
    );
  }
}
