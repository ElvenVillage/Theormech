import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/profile/chat/model/chat_model.dart';
import 'package:teormech/profile/chat/ui/pages/chats_screen.dart';
import 'package:teormech/profile/chat/ui/widgets/chat_list_row.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';

class GroupChatsList extends StatelessWidget {
  const GroupChatsList(
      {Key? key, required this.widget, required this.groupChats})
      : super(key: key);

  final ChatsScreen widget;
  final List<Chat> groupChats;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, idx) {
        final chat = groupChats[idx];

        return BlocProvider(
          key: Key(chat.messageDate),
          create: (context) =>
              UserProfileBloc.fromLid(lid: chat.senderId, loadFromHive: false),
          child: Builder(builder: (context) {
            return BlocBuilder<UserProfileBloc, UserProfileState>(
                builder: (context, userState) {
              return ChatListRow(
                profileAvatar: chat.photo,
                chat: chat,
                nestedProfileAvatar: userState.profile.avatar != ''
                    ? userState.profile.avatar
                    : null,
                isGroupChat: true,
              );
            });
          }),
        );
      },
      itemCount: groupChats.length,
    );
  }
}
