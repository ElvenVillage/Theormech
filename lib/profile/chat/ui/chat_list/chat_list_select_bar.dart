import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:teormech/auth/ui/widgets/choice_container.dart';
import 'package:teormech/profile/chat/bloc/chat_list_page_cubit.dart';

class Redmark extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 6,
      height: 6,
      decoration: BoxDecoration(
          color: Colors.pinkAccent, borderRadius: BorderRadius.circular(12)),
    );
  }
}

class ChatListSelectBar {
  static const style = ChoiceContainerStyle(
    elevation: 5,
    selectedColor: Colors.black,
    weight: FontWeight.normal,
  );

  static SliverAppBar chatBar({
    required BuildContext context,
    required ChatListPage status,
    required bool haveUnreadChats,
    required bool haveUnreadGroupChats,
    required bool haveUnreadMailings,
    required PageController controller,
    required ScrollController scrollController,
  }) {
    return SliverAppBar(
      automaticallyImplyLeading: false,
      backgroundColor: Colors.white,
      pinned: true,
      flexibleSpace: Padding(
        padding: const EdgeInsets.only(left: 4, right: 4, top: 10),
        child: SingleChildScrollView(
          controller: scrollController,
          scrollDirection: Axis.horizontal,
          child: Row(children: [
            ChoiceContainer(
              text: 'Чаты',
              chosen: status == ChatListPage.Chats,
              style: style,
              widget: haveUnreadChats ? Redmark() : null,
              onClickCallback: () {
                context
                    .read<ChatListPageCubit>()
                    .selectPage(ChatListPage.Chats);
              },
            ),
            ChoiceContainer(
              text: 'Групповые чаты',
              chosen: status == ChatListPage.Groups,
              style: style,
              widget: haveUnreadGroupChats ? Redmark() : null,
              onClickCallback: () {
                context
                    .read<ChatListPageCubit>()
                    .selectPage(ChatListPage.Groups);
              },
            ),
            ChoiceContainer(
              text: 'Рассылка',
              chosen: status == ChatListPage.Important,
              style: style,
              onClickCallback: () {
                context
                    .read<ChatListPageCubit>()
                    .selectPage(ChatListPage.Important);
              },
              widget: haveUnreadMailings ? Redmark() : null,
            ),
            ChoiceContainer(
              text: 'Избранное',
              chosen: status == ChatListPage.Saved,
              style: style,
              onClickCallback: () {
                context
                    .read<ChatListPageCubit>()
                    .selectPage(ChatListPage.Saved);
              },
            ),
          ]),
        ),
      ),
    );
  }
}
