import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/profile/bloc/online_status.dart';
import 'package:teormech/profile/chat/model/chat_list_state.dart';
import 'package:teormech/profile/chat/model/chat_model.dart';
import 'package:teormech/profile/chat/rep/chat_rep.dart';
import 'package:teormech/profile/chat/ui/widgets/chat_list_row.dart';
import 'package:teormech/profile/chat/ui/pages/chats_screen.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';

class PersonalChatsList extends StatelessWidget {
  const PersonalChatsList({
    Key? key,
    required this.widget,
    required this.state,
  }) : super(key: key);

  final ChatsScreen widget;
  final ChatListState state;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemBuilder: (context, idx) {
          final chat = state.chats[idx];

          return BlocProvider(
            create: (context) {
              return UserProfileBloc.fromLid(
                  lid: chat.senderId, loadFromHive: false);
            },
            child: PersonalChatListEntry(
              chat: chat,
            ),
          );
        },
        itemCount: state.chats.length);
  }
}

class PersonalChatListEntry extends StatelessWidget {
  const PersonalChatListEntry({
    Key? key,
    required this.chat,
  }) : super(key: key);

  final Chat chat;
  @override
  Widget build(BuildContext context) {
    final now = DateTime.now();
    final onlineString = OnlineStatus.onlineString(
        lastOnline: DateTime.tryParse(chat.lastOnline ?? '1970-01-01') ??
            DateTime.fromMillisecondsSinceEpoch(0),
        sex: chat.gender,
        now: now);

    return BlocBuilder<UserProfileBloc, UserProfileState>(
        builder: (context, userState) {
      return ChatListRow(
        onlineString: userState.onlineString,
        online: onlineString == 'В сети',
        profileAvatar: chat.photo,
        chat: chat,
        nestedProfileAvatar: userState.profile.avatar != '' &&
                userState.profile.lid == ChatRepository().credentials.id
            ? userState.profile.avatar
            : null,
      );
    });
  }
}
