import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:teormech/profile/chat/bloc/saved_messages_bloc.dart';
import 'package:teormech/profile/chat/model/message_model.dart';
import 'package:teormech/profile/chat/ui/widgets/message_row.dart';
import 'package:teormech/profile/chat/ui/widgets/unread_panel.dart';

class SavedChatList extends StatelessWidget {
  const SavedChatList({Key? key, required this.myId, required this.isMailings})
      : super(key: key);
  final String myId;
  final bool isMailings;

  @override
  Widget build(BuildContext context) {
    if (!isMailings)
      return BlocBuilder<SavedMessagesBloc, SavedMessagesState>(
          builder: (context, savedMessagesList) {
        final messages = savedMessagesList.messages.reversedMessages;
        return _messagesList(messages, savedMessagesList.selected,
            savedMessagesList.selectionMode);
      });
    else
      return BlocBuilder<SavedMessagesBloc, SavedMessagesState>(
          builder: (context, state) {
        final messages = state.mailings.reversedMessages;
        return Stack(
          children: [
            _messagesList(messages, state.selectedMailings, state.selectionMode,
                firstUnreadIdx: state.firstUnreadIdx,
                listener: state.positionsListener,
                controller: state.scrollController),
            if (state.unreadMailings > 0)
              UnreadPanel(
                  countOfUnread: state.unreadMailings,
                  readAllMessagesCallback:
                      context.read<SavedMessagesBloc>().readMailings,
                  jumpToLastUnread:
                      context.read<SavedMessagesBloc>().jumpToUnread),
          ],
        );
      });
  }

  Widget _messagesList(
      List<Message> messages, List<int> selected, bool selectionMode,
      {int? firstUnreadIdx,
      ItemScrollController? controller,
      ItemPositionsListener? listener}) {
    return Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: ScrollablePositionedList.builder(
          itemCount: messages.length,
          itemPositionsListener: listener,
          itemScrollController: controller,
          itemBuilder: (context, idx) {
            final message = messages[idx];
            final prevMessage =
                (idx == messages.length - 1) ? message : messages[idx + 1];
            final isFirstUnread = idx == (firstUnreadIdx ?? -1);
            final needToShowSpacer =
                message.differ(prevMessage) || prevMessage.id == message.id;
            return SavedMessageEntry(
              msg: message,
              isMailing: isMailings,
              isFirstUnread: isFirstUnread,
              needToShowSpacer: needToShowSpacer,
              myId: myId,
              selectionMode: selectionMode,
              selected: selected.contains(message.id),
            );
          }),
    );
  }
}

class SavedMessageEntry extends StatelessWidget {
  const SavedMessageEntry(
      {Key? key,
      required this.msg,
      required this.myId,
      required this.selected,
      required this.selectionMode,
      required this.isMailing,
      required this.needToShowSpacer,
      required this.isFirstUnread})
      : super(key: key);
  final Message msg;
  final String myId;
  final bool selected;
  final bool selectionMode;
  final bool needToShowSpacer;
  final bool isMailing;
  final bool isFirstUnread;

  @override
  Widget build(BuildContext context) {
    return MessageRow(
        needToShowSpacer: needToShowSpacer,
        message: [msg],
        isInGroupChatSecondary: false,
        //reversedSpacers: true,
        isInGroupChat: msg.uid != myId,
        isFirstUnread: isFirstUnread,
        selectionLongTapCallback: () {
          if (!isMailing)
            context.read<SavedMessagesBloc>().selectMessage(msg.id);
        },
        selectionTapCallback: () {
          if (selectionMode)
            context.read<SavedMessagesBloc>().selectMessage(msg.id);
        },
        selected: selected,
        selectionMode: selectionMode,
        myId: myId);
  }
}
