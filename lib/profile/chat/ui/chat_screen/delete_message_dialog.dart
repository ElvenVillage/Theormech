import 'package:flutter/material.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/widgets/multilne_text_field.dart';

typedef TextCallback = void Function(String text);

class DeleteDialog extends StatefulWidget {
  const DeleteDialog(
      {Key? key,
      this.yesCaption = 'Да',
      this.noCaption = 'Нет',
      this.enableMultilineInput = false,
      required this.isDeleting,
      required this.text,
      required this.yesCallback,
      required this.noCallback})
      : super(key: key);

  final bool isDeleting;
  final String text;

  final String yesCaption;
  final String noCaption;

  final TextCallback yesCallback;
  final TextCallback noCallback;

  final bool enableMultilineInput;

  @override
  State<DeleteDialog> createState() => _DeleteDialogState();
}

class _DeleteDialogState extends State<DeleteDialog> {
  final _controller = TextEditingController();
  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Offstage(
        offstage: widget.isDeleting,
        child: Container(
          width: MediaQuery.of(context).size.width * 0.7,
          height: widget.enableMultilineInput ? 300 : 150,
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(16)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Text(
                  widget.text,
                  style: h2TextStyle,
                  textAlign: widget.enableMultilineInput
                      ? TextAlign.left
                      : TextAlign.center,
                ),
              ),
              if (widget.enableMultilineInput)
                Padding(
                  padding:
                      const EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Причина исключения',
                        textAlign: TextAlign.left,
                        style: h2DrawerGreyText,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0),
                        child: MultilineTextField(),
                      ),
                    ],
                  ),
                ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  MaterialButton(
                      onPressed: () {
                        widget.yesCallback(_controller.text);
                      },
                      child: Text(
                        widget.yesCaption,
                        style: h2TextStyle.copyWith(color: Colors.pink),
                      )),
                  MaterialButton(
                      onPressed: () {
                        widget.noCallback(_controller.text);
                      },
                      child: Text(
                        widget.noCaption,
                        style: h2TextStyle.copyWith(color: Colors.pink),
                      )),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
