import 'package:edge_alert/edge_alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/bloc/auth_bloc.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_events.dart';
import 'package:teormech/profile/chat/model/chats_state.dart';
import 'package:teormech/profile/chat/ui/pages/chat_attachments_screen.dart';
import 'package:teormech/profile/chat/ui/pages/create_group_chat_screen.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/clipped_avatar.dart';
import 'package:teormech/profile/profile/ui/profile_wrapper.dart';
import 'package:teormech/styles/edge_alert_notification.dart';

class ChatOptionTopBar extends StatelessWidget {
  final Animation<double> opacity;
  final Animation<Offset> offset;

  final ChatBloc chatBloc;

  final Function closeCallback;

  const ChatOptionTopBar({
    Key? key,
    required this.opacity,
    required this.closeCallback,
    required this.offset,
    required this.chatBloc,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final isTeacher = (context.read<AuthBloc>().state as Authorized).isTeacher;
    return FadeTransition(
      opacity: opacity,
      child: SlideTransition(
        position: offset,
        child: BlocBuilder<ChatBloc, ChatState>(builder: (context, chatState) {
          return Container(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 10),
              alignment: Alignment.topCenter,
              height: chatState.chat.isGroupChat
                  ? chatState.isAdmin
                      ? 320
                      : 280
                  : isTeacher
                      ? 280
                      : 240,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(22),
                  color: Colors.white,
                  boxShadow: [BoxShadow(blurRadius: 5, color: Colors.grey)]),
              child: Material(
                color: Colors.transparent,
                child: Column(crossAxisAlignment: CrossAxisAlignment.center,
                    //mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ChatOption(
                          icon: ImageIcon(
                            AssetImage('images/icons/chat_options/profile.png'),
                            color: Colors.pinkAccent,
                          ),
                          callback: () {
                            closeCallback();
                            if (chatState.chat.isGroupChat) {
                              context.read<BottomBarBloc>().go(PageRouteBuilder(
                                  pageBuilder: (context, f1, f2) =>
                                      BlocProvider<ChatBloc>.value(
                                        value: chatBloc,
                                        child: CreateGroupChatScreen(
                                          chat: chatBloc,
                                          isCreatingChat: false,
                                        ),
                                      )));
                            } else
                              context.read<BottomBarBloc>().go(PageRouteBuilder(
                                    pageBuilder: (context, f1, f2) =>
                                        ProfileScreenWrapper.fromLid(
                                      lid: chatState.chat.id,
                                    ),
                                  ));
                          },
                          caption: chatState.chat.isGroupChat
                              ? 'Показать участников'
                              : 'Профиль'),
                      if (chatState.chat.isGroupChat && chatState.isAdmin)
                        ChatOption(
                            icon: ImageIcon(
                              AssetImage('images/icons/gallery.png'),
                              color: Colors.pinkAccent,
                            ),
                            callback: () async {
                              final file = await UserAvatar.selectFile();
                              if (file != null)
                                context
                                    .read<ChatBloc>()
                                    .add(UploadGroupChatPhotoEvent(file: file));
                            },
                            caption: 'Фотография чата'),
                      if (chatState.chat.isGroupChat)
                        ChatOption(
                          icon: ImageIcon(AssetImage('images/icons/share.png'),
                              color: Colors.pinkAccent),
                          callback: () async {
                            closeCallback();
                            final result = await chatBloc.shareLink();
                            showEdgeAlertNotification(
                                okText: 'Ссылка скопирована',
                                errorText: 'Не удалось создать ссылку',
                                context: context,
                                result: result);
                          },
                          caption: 'Поделиться ссылкой',
                        ),
                      ChatOption(
                        icon: ImageIcon(
                            AssetImage(
                                'images/icons/chat_options/attachments.png'),
                            color: Colors.pinkAccent),
                        callback: () {
                          closeCallback();
                          context.read<BottomBarBloc>().go(
                              MaterialPageRoute(
                                  builder: (context) => BlocProvider.value(
                                      value: chatBloc,
                                      child: ChatAttachmentsScreen())),
                              hideBottombar: true);
                        },
                        caption: 'Показать вложения',
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.6,
                        decoration: BoxDecoration(
                            border: Border(
                                top: BorderSide(
                                    width: 1, color: Colors.grey.shade400))),
                      ),
                      ChatOption(
                        icon: ImageIcon(
                            AssetImage(
                                'images/icons/chat_options/notifications.png'),
                            color: Colors.pinkAccent),
                        callback: () {
                          closeCallback();
                        },
                        caption: 'Отключить уведомления',
                      ),
                      ChatOption(
                        icon: ImageIcon(
                            AssetImage('images/icons/chat_options/forbid.png'),
                            color: Colors.pinkAccent),
                        callback: () {
                          closeCallback();
                          if (chatState.chat.isGroupChat) {
                            chatBloc.leaveChat();
                          } else {
                            chatBloc.add(BanUserEvent());
                            if (!chatState.ban.isUserBanned) {
                              EdgeAlert.show(context,
                                  backgroundColor: Colors.teal,
                                  icon: Icons.close,
                                  title: 'Пользователь заблокирован',
                                  description:
                                      'Пользователь не может отправлять вам сообщения',
                                  duration: 1,
                                  gravity: EdgeAlert.BOTTOM);
                            } else {
                              EdgeAlert.show(context,
                                  backgroundColor: Colors.teal,
                                  icon: Icons.verified,
                                  title: 'Пользователь разблокирован',
                                  description:
                                      'Пользователь вновь может отправлять вам сообщения',
                                  duration: 1,
                                  gravity: EdgeAlert.BOTTOM);
                            }
                          }
                        },
                        caption: chatState.chat.isGroupChat
                            ? 'Выйти из беседы'
                            : chatState.ban.isUserBanned
                                ? 'Разрешить отправку'
                                : 'Запретить отправку',
                      ),
                      if (isTeacher && !chatState.chat.isGroupChat)
                        ChatOption(
                            icon: ImageIcon(
                                AssetImage(
                                    'images/icons/chat_options/razrotpr.png'),
                                color: Colors.pinkAccent),
                            callback: () {
                              chatBloc.toggleChatRestrictions();
                            },
                            caption: chatState.ban.canWriteToMe
                                ? 'Установить ограничения'
                                : 'Снять ограничения для студента')
                    ]),
              ));
        }),
      ),
    );
  }
}

class ChatOption extends StatelessWidget {
  final ImageIcon icon;
  final String caption;
  final Function callback;
  const ChatOption({
    required this.icon,
    required this.callback,
    required this.caption,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      child: SizedBox(
          width: double.infinity,
          height: 50,
          child: Row(
            children: [
              icon,
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Text(caption),
              ),
            ],
          )),
      onPressed: () {
        callback();
      },
      padding: EdgeInsets.only(left: 20),
    );
  }
}
