import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:teormech/api_constants.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/clipped_avatar.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/empty_avatar.dart';
import 'package:teormech/profile/profile/ui/profile_wrapper.dart';

class ChatAppBar extends StatelessWidget {
  const ChatAppBar({
    Key? key,
    required this.onlineStatus,
    required this.tStyle,
    required this.constraints,
    required this.photo,
    required this.name,
    required this.surname,
    this.userId,
    this.groupChat = false,
  }) : super(key: key);

  final String onlineStatus;
  final TextStyle tStyle;
  final String? userId;
  final String? photo;
  final String name;
  final String surname;
  final BoxConstraints constraints;
  final bool groupChat;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      key: Key(photo ?? ''),
      onTap: () {
        if (userId != null)
          context.read<BottomBarBloc>().go(PageRouteBuilder(
                pageBuilder: (context, f1, f2) =>
                    ProfileScreenWrapper.fromLid(lid: userId!),
              ));
      },
      child: Row(
        children: [
          if ((photo != null) && (photo != ""))
            UserAvatar.fromNetwork(
              size: constraints.maxHeight * 0.7,
              avatarUrl: (groupChat
                      ? GroupChatsApi.groupChatLogoMin
                      : ProfileApi.profileAvatar) +
                  photo!,
              ignoreFrame: groupChat,
              online: onlineStatus == 'В сети',
            )
          else
            EmptyAvatar(
                size: constraints.maxHeight * 0.7,
                online: onlineStatus == 'В сети',
                ignoreOnline: groupChat,
                groupChat: groupChat),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  groupChat ? name : surname + ' ' + name,
                  style: tStyle,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                if (!groupChat)
                  Text(
                    onlineStatus,
                    style: TextStyle(color: Colors.white),
                  )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
