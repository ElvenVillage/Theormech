import 'package:edge_alert/edge_alert.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_events.dart';
import 'package:teormech/profile/chat/bloc/chat_list_bloc.dart';
import 'package:teormech/profile/chat/bloc/saved_messages_bloc.dart';
import 'package:teormech/profile/chat/model/message_model.dart';

class SelectionChatBar extends StatelessWidget {
  final Animation<double> opacity;
  final Animation<Offset> offset;
  final int selectedCount;
  final Function closeCallback;
  final List<Message>? messages;
  final PageController? pageController;

  final bool isForSaved;
  final bool selectedNotMyMessagesInPlainChat;

  SelectionChatBar(this.selectedCount,
      {required this.closeCallback,
      required this.offset,
      this.isForSaved = false,
      this.selectedNotMyMessagesInPlainChat = false,
      this.pageController,
      this.messages,
      required this.opacity});
  @override
  Widget build(BuildContext context) {
    return FadeTransition(
      opacity: opacity,
      child: SlideTransition(
        position: offset,
        child: PhysicalModel(
          color: Colors.white,
          elevation: 8,
          child: SizedBox(
            width: double.infinity,
            height: 50,
            child: Padding(
              padding: const EdgeInsets.only(left: 5),
              child: Row(
                children: [
                  IconButton(
                      icon: Icon(
                        Icons.close,
                        color: Colors.pinkAccent,
                      ),
                      onPressed: () => closeCallback()),
                  Text('Выбрано сообщений: $selectedCount'),
                  SelectionMenuButton(
                    icon: isForSaved
                        ? 'images/icons/selection_bar/unsave.png'
                        : 'images/icons/selection_bar/save.png',
                    onPressed: () {
                      if (isForSaved)
                        context.read<SavedMessagesBloc>().unsaveMessages();
                      else
                        context.read<ChatBloc>().add(SaveMessagesEvent());
                    },
                  ),
                  SelectionMenuButton(
                    icon: 'images/icons/selection_bar/share.png',
                    onPressed: () {
                      if (isForSaved && !selectedNotMyMessagesInPlainChat) {
                        context.read<ChatListBloc>().addMessagesForForwarding(
                            messages: messages!,
                            chatId: '',
                            isFromGroupChat: false,
                            fromSaves: true);
                        context
                            .read<SavedMessagesBloc>()
                            .disselectAllMessages();
                        pageController?.jumpToPage(0);
                      } else {
                        context.read<ChatBloc>().repostMessage();
                      }
                      if (!isForSaved || selectedNotMyMessagesInPlainChat)
                        context.read<BottomBarBloc>().pop();
                    },
                  ),
                  if (!isForSaved && !selectedNotMyMessagesInPlainChat)
                    SelectionMenuButton(
                      morePadding: true,
                      icon: 'images/icons/selection_bar/pensil.png',
                      onPressed: () {
                        if (!isForSaved) {
                          context.read<ChatBloc>().add(EditMessageEvent());
                        }
                      },
                    ),
                  SelectionMenuButton(
                    icon: 'images/icons/selection_bar/copy.png',
                    onPressed: () {
                      if (isForSaved && !selectedNotMyMessagesInPlainChat)
                        context.read<SavedMessagesBloc>().saveMessage();
                      else
                        context.read<ChatBloc>().saveMessage();
                      EdgeAlert.show(context,
                          backgroundColor: Colors.teal,
                          icon: Icons.copy_sharp,
                          title: 'Скопировано',
                          description: 'Текст сообщения скопирован',
                          duration: 1,
                          gravity: EdgeAlert.BOTTOM);
                    },
                  ),
                  if (!isForSaved && !selectedNotMyMessagesInPlainChat)
                    SelectionMenuButton(
                      icon: 'images/icons/selection_bar/delete.png',
                      onPressed: () {
                        if (!isForSaved)
                          context
                              .read<ChatBloc>()
                              .add(DeleteMessagesEvent(isDeleting: false));
                      },
                    ),
                  SizedBox(
                    width: 10,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class SelectionMenuButton extends StatelessWidget {
  const SelectionMenuButton(
      {Key? key,
      required this.onPressed,
      required this.icon,
      this.morePadding = false})
      : super(key: key);

  final VoidCallback onPressed;
  final bool morePadding;
  final String icon;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Material(
        color: Colors.white,
        child: InkWell(
          child: Padding(
            padding: morePadding
                ? EdgeInsets.only(left: 7, right: 6)
                : EdgeInsets.only(left: 4.0, right: 3),
            child: ImageIcon(
              AssetImage(icon),
              color: Colors.pinkAccent,
            ),
          ),
          onTap: onPressed,
        ),
      ),
    );
  }
}
