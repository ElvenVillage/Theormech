import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_list_bloc.dart';
import 'package:teormech/profile/chat/model/chat_list_state.dart';

class RepostMessages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        context.read<ChatListBloc>().forwardMessages();
      },
      child: Card(
        margin: const EdgeInsets.only(left: 4, top: 4, right: 4),
        elevation: 8,
        child: Container(
            width: size.width * 0.9,
            height: 40,
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border(
                    left: BorderSide(color: Colors.pinkAccent, width: 5))),
            child: BlocBuilder<ChatListBloc, ChatListState>(
              builder: (context, state) {
                var text = ' пересланных сообщений';
                final length = state.messagesForForwarding.length +
                    state.photosForForwarding.length +
                    state.newsfeedObjectsForForwarding.length;
                if (length == 1) text = ' пересланное сообщение';
                if (length > 1 && length < 5) text = ' пересланных сообщения';

                return Row(
                  children: [
                    Text(
                      '    X  ' + length.toString() + text,
                      style: TextStyle(
                          color: Colors.pinkAccent.shade100,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                );
              },
            )),
      ),
    );
  }
}
