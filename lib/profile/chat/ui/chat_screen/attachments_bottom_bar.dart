import 'dart:io';
import 'dart:typed_data';

import 'package:edge_alert/edge_alert.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:teormech/profile/chat/bloc/typing_bloc.dart';
import 'package:teormech/profile/chat/model/typing_bloc_state.dart';
import 'package:open_file/open_file.dart';
import 'package:teormech/styles/colors.dart';

class AttachmentsBottomBar extends StatelessWidget {
  final Animation<double> opacity;
  final Animation<Offset> offset;
  final ImagePicker imagePicker;

  const AttachmentsBottomBar(
      {Key? key,
      required this.opacity,
      required this.offset,
      required this.imagePicker})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return FadeTransition(
        opacity: opacity,
        child: SlideTransition(
          position: offset,
          child: PhysicalModel(
            elevation: 8,
            color: Colors.white,
            child: SizedBox(
              width: size.width,
              height: 70,
              child: BlocBuilder<TypingBloc, TypingBlocState>(
                  builder: (context, state) {
                return (state.attachedImages.isEmpty &&
                        state.attachedFiles.isEmpty &&
                        state.webImages.isEmpty)
                    ? Row(children: [
                        Spacer(),
                        AttachImageButton(
                          imagePicker: imagePicker,
                          initial: true,
                        ),
                        Spacer(),
                        AttachFileButton(
                          initial: true,
                        ),
                        Spacer()
                      ])
                    : Row(
                        children: [
                          SizedBox(
                            width: size.width * 0.5,
                            child: ReorderableListView(
                                onReorder:
                                    context.read<TypingBloc>().reorderPhotos,
                                scrollDirection: Axis.horizontal,
                                children: [
                                  ...(kIsWeb
                                          ? state.webImages
                                          : state.attachedImages)
                                      .asMap()
                                      .map((idx, picked) => MapEntry(
                                          idx,
                                          AttachedPhoto(
                                            idx: idx,
                                            photo: kIsWeb
                                                ? null
                                                : state.attachedImages[idx],
                                            webPhoto: kIsWeb
                                                ? state.webImages[idx]
                                                : null,
                                            key: Key(idx.toString()),
                                          )))
                                      .values
                                      .toList(),
                                  ...state.attachedFiles
                                      .asMap()
                                      .map((idx, picked) => MapEntry(
                                          idx,
                                          FileAttachment(
                                            idx: idx,
                                            file: state.attachedFiles[idx],
                                            key: Key((idx + 1000).toString()),
                                          )))
                                      .values
                                      .toList(),
                                ]),
                          ),
                          AttachImageButton(imagePicker: imagePicker),
                          AttachFileButton()
                        ],
                      );
              }),
            ),
          ),
        ));
  }
}

class AttachedPhoto extends StatelessWidget {
  const AttachedPhoto({Key? key, this.photo, required this.idx, this.webPhoto})
      : super(key: key);
  final int idx;
  final File? photo;
  final Uint8List? webPhoto;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Stack(
        children: [
          Container(
            padding: const EdgeInsets.all(8.0),
            width: 80,
            height: 80,
            child: kIsWeb
                ? Image.memory(webPhoto!, height: 60, width: 60)
                : Image(image: FileImage(photo!), height: 60, width: 60),
          ),
          Positioned(
              right: 0,
              top: 0,
              child: IconButton(
                  onPressed: () {
                    context.read<TypingBloc>().removeImage(idx: idx);
                  },
                  icon: Icon(
                    Icons.disabled_by_default_outlined,
                    color: Colors.pinkAccent,
                  )))
        ],
      ),
    );
  }
}

class FileAttachment extends StatefulWidget {
  const FileAttachment({Key? key, required this.idx, required this.file})
      : super(key: key);
  final int idx;
  final File file;

  @override
  _FileAttachmentState createState() => _FileAttachmentState();
}

class _FileAttachmentState extends State<FileAttachment> {
  late final Future<String> _fileSize;

  Future<String> _computeFileSize() async {
    final size = await widget.file.length();
    final sizeInKb = size / 1024;
    final sizeInMb = sizeInKb / 1024;
    if (sizeInMb < 1)
      return sizeInKb.toStringAsFixed(1) + ' КБ';
    else
      return sizeInMb.toStringAsFixed(1) + ' МБ';
  }

  @override
  void initState() {
    _fileSize = _computeFileSize();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        GestureDetector(
          onTap: () {
            OpenFile.open(widget.file.path);
          },
          child: ConstrainedBox(
            constraints: BoxConstraints(maxWidth: 170),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 8),
              child: Container(
                height: 50,
                padding: const EdgeInsets.only(left: 8, right: 8),
                decoration: BoxDecoration(
                    color: GradientColors.documentsBackground,
                    borderRadius: BorderRadius.circular(12)),
                child: Center(
                  child: FutureBuilder(
                    future: _fileSize,
                    builder: (context, AsyncSnapshot<String> snapshot) {
                      return Row(
                        children: [
                          Flexible(
                            flex: 5,
                            child: Text(
                              widget.file.uri.pathSegments.last,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          Text(
                            snapshot.data ?? '',
                            overflow: TextOverflow.clip,
                          ),
                        ],
                      );
                    },
                    initialData: '',
                  ),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          right: 10,
          top: 10,
          child: GestureDetector(
            child: Icon(
              Icons.disabled_by_default_outlined,
              color: Colors.pinkAccent,
            ),
            onTap: () {
              context.read<TypingBloc>().removeAttachment(idx: widget.idx);
            },
          ),
        ),
      ],
    );
  }
}

class AttachFileButton extends StatelessWidget {
  const AttachFileButton({
    Key? key,
    this.initial = false,
  }) : super(key: key);

  final bool initial;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        decoration: BoxDecoration(
            border:
                Border(left: BorderSide(color: Colors.grey.withAlpha(100)))),
        height: double.infinity,
        width: initial
            ? MediaQuery.of(context).size.width * 0.45
            : MediaQuery.of(context).size.width * 0.2,
        child: MaterialButton(
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 10.0),
                child: Icon(
                  Icons.file_present,
                  size: 30,
                  color: Colors.pinkAccent,
                ),
              ),
              if (initial)
                Text('Документ',
                    style: TextStyle(
                        color: Colors.pinkAccent,
                        fontWeight: FontWeight.bold,
                        fontSize: 16)),
              if (initial) Spacer(),
            ],
          ),
          onPressed: () async {
            try {
              final pickedFiles =
                  await FilePicker.platform.pickFiles(allowMultiple: true);
              if (pickedFiles != null) {
                final files = pickedFiles.paths
                    .map((path) => path != null ? File(path) : null)
                    .toList();

                context
                    .read<TypingBloc>()
                    .appendFiles(files.whereType<File>().toList());
              }
            } on PlatformException {
              EdgeAlert.show(context,
                  title: 'Ошибка',
                  description: 'Установите разрешения',
                  duration: EdgeAlert.LENGTH_LONG,
                  gravity: EdgeAlert.BOTTOM,
                  backgroundColor: Colors.red,
                  icon: Icons.error);
            }
          },
        ),
      ),
    );
  }
}

class AttachImageButton extends StatelessWidget {
  const AttachImageButton({
    Key? key,
    required this.imagePicker,
    this.initial = false,
  }) : super(key: key);

  final ImagePicker imagePicker;
  final bool initial;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: MaterialButton(
        child: SizedBox(
          height: double.infinity,
          width: initial ? MediaQuery.of(context).size.width * 0.45 : 30,
          child: Row(
            children: [
              if (initial) Spacer(),
              Padding(
                padding: const EdgeInsets.only(right: 10.0),
                child: Icon(
                  Icons.photo,
                  color: Colors.pinkAccent,
                  size: 30,
                ),
              ),
              if (initial)
                Text(
                  'Изображение',
                  style: TextStyle(
                      color: Colors.pinkAccent,
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                )
            ],
          ),
        ),
        onPressed: () async {
          if (kIsWeb) {
            final pickedFiles =
                await FilePicker.platform.pickFiles(allowMultiple: true);
            if (pickedFiles != null) {
              final files = pickedFiles.files;

              context
                  .read<TypingBloc>()
                  .appendWebImages(files.map((e) => e.bytes!).toList());
            }
          } else {
            try {
              final pickedImages = await this.imagePicker.getMultiImage();

              final files = pickedImages?.map((e) => File(e.path)).toList();
              context.read<TypingBloc>().appendImages(files ?? []);
            } on PlatformException {
              EdgeAlert.show(context,
                  title: 'Ошибка',
                  description: 'Установите разрешения',
                  duration: EdgeAlert.LENGTH_LONG,
                  gravity: EdgeAlert.BOTTOM,
                  backgroundColor: Colors.red,
                  icon: Icons.error);
            }
          }
        },
      ),
    );
  }
}
