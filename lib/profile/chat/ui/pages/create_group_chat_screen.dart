import 'dart:io';
import 'package:edge_alert/edge_alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/generated/l10n.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_events.dart';
import 'package:teormech/profile/chat/bloc/chat_list_bloc.dart';
import 'package:teormech/profile/chat/model/chats_state.dart';
import 'package:teormech/profile/chat/rep/group_chat_rep.dart';
import 'package:teormech/profile/chat/ui/pages/chat_screen.dart';
import 'package:teormech/profile/chat/ui/chat_screen/delete_message_dialog.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/clipped_avatar.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/empty_avatar.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/file_avatar.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/network_avatar.dart';
import 'package:teormech/profile/subscription/bloc/followers_bloc.dart';
import 'package:teormech/profile/subscription/model/followers_state.dart';
import 'package:teormech/profile/subscription/ui/widgets/profile_row.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/widgets/loading_progress_indicator.dart';

class CreateGroupChatScreen extends StatefulWidget {
  const CreateGroupChatScreen(
      {Key? key, this.chat, required this.isCreatingChat})
      : super(key: key);

  final ChatBloc? chat;
  final bool isCreatingChat;

  @override
  _CreateGroupChatScreenState createState() => _CreateGroupChatScreenState();
}

class _CreateGroupChatScreenState extends State<CreateGroupChatScreen> {
  File? _pickedFile;
  final _controller = TextEditingController();

  List<String> _users = [];
  List<String> _admins = [];
  bool _inProgress = false;
  bool _isUsersExpanded = false;
  bool _amIAdmin = false;
  bool _isDeleting = false;

  List<String> _usersToAdd = [];
  String? _userToDelete;
  String? _userNameToDelete;

  @override
  void initState() {
    context.read<FollowersCubit>().updateSearches(query: '');

    if (widget.chat != null) _loadChatParticipants();
    super.initState();
  }

  Future<void> _loadChatParticipants() async {
    _controller.text = widget.chat!.state.chat.name;
    final users = await GroupChatsRepository()
        .getChatMembers(chatId: widget.chat!.state.chat.id);
    final admins = await GroupChatsRepository()
        .getChatMembers(chatId: widget.chat!.state.chat.id, admins: true);

    setState(() {
      _users = users.map((e) => e['id'] as String).toList();
      for (var admin in admins) _admins.add(admin);
      _amIAdmin = _admins.contains(GroupChatsRepository().credentials.id);
    });
  }

  Future<void> _updateGroupChatAvatar() async {
    await GroupChatsRepository().uploadAvatar(
        avatar: _pickedFile!, cid: widget.chat?.state.chat.id ?? '1');
    widget.chat?.chatListBloc.load(
        loadFromHive: false,
        loadFromNetwork: true,
        loadUnreadCount: false,
        init: true);
  }

  void _createChat() async {
    setState(() {
      _inProgress = true;
    });
    if (_controller.text.trim().isNotEmpty) {
      final a = await GroupChatsRepository().createChat(
        name: _controller.text.trim(),
      );
      if (a != 'false') {
        if (_pickedFile != null)
          await GroupChatsRepository()
              .uploadAvatar(avatar: _pickedFile!, cid: a);
        for (var user in _users) {
          await GroupChatsRepository().inviteUser(cid: a, uid: user);
        }

        context.read<ChatListBloc>().load(
            loadFromHive: false,
            loadFromNetwork: true,
            init: true,
            updateOnlyGroupChats: true,
            callback: (loadedChats) {
              final chats =
                  loadedChats.where((c) => c.isGroupChat && c.id == a);
              if (chats.isNotEmpty) {
                setState(() {
                  _inProgress = false;
                });
                context.read<BottomBarBloc>().pop();
                context.read<BottomBarBloc>().go(
                    ChatScreenWrapper.route(chat: chats.first),
                    hideBottombar: true);
                return true;
              } else {
                return false;
              }
            },
            loadUnreadCount: false);
      } else {
        EdgeAlert.show(context,
            icon: Icons.error,
            title: 'Ошибка',
            description: 'Не удалось создать чат',
            duration: 1,
            gravity: EdgeAlert.BOTTOM,
            backgroundColor: Colors.red);
      }
    } else {
      EdgeAlert.show(context,
          icon: Icons.error,
          title: 'Ошибка',
          description: 'Введите корректное имя',
          duration: 1,
          gravity: EdgeAlert.BOTTOM,
          backgroundColor: Colors.red);
    }
  }

  Future<void> _syncChat() async {
    for (var i in _usersToAdd) {
      await GroupChatsRepository()
          .inviteUser(cid: widget.chat!.state.chat.id, uid: i);
    }
    if (_pickedFile != null) await _updateGroupChatAvatar();
    widget.chat?.add(LoadMessagesEvent(initial: true));
    context.read<BottomBarBloc>().pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ProfileAppBar(
          title: Text(widget.isCreatingChat ? 'Создание чата' : 'Участники',
              style: TextStyle(fontSize: 18, color: Colors.white)),
          rightmostButton: IconButton(
              icon: Icon(
                Icons.arrow_forward,
                color: Colors.white,
              ),
              onPressed: () async {
                if (widget.chat == null)
                  _createChat();
                else
                  _syncChat();
              })),
      body: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Stack(
          children: [
            Container(
              padding: const EdgeInsets.only(top: 10),
              color: Colors.white,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      height: 30,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0, top: 8),
                        child: Text(S.of(context).chat_photo,
                            style:
                                const TextStyle(fontWeight: FontWeight.bold)),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      height: 150,
                      child: Row(
                        children: [
                          if (widget.chat != null)
                            BlocBuilder<ChatBloc, ChatState>(
                                builder: (context, chatState) {
                              return SizedBox(
                                  child: _pickedFile != null
                                      ? FileAvatar(
                                          file: _pickedFile!, size: 120)
                                      : (chatState.chat.photo == '' ||
                                              chatState.chat.photo == null)
                                          ? EmptyAvatar(
                                              ignoreOnline: true,
                                              blank: true,
                                              size: 120,
                                              online: true,
                                            )
                                          : NetworkAvatar(
                                              avatarString: GroupChatsApi
                                                      .groupChatLogoMax +
                                                  chatState.chat.photo!,
                                              online: false,
                                              key: Key(chatState.chat.photo!),
                                              showPlaceholder: true,
                                              ignoreOnline: true,
                                              size: 120,
                                            ),
                                  height: 130,
                                  width: 130);
                            })
                          else
                            SizedBox(
                                child: _pickedFile != null
                                    ? FileAvatar(file: _pickedFile!, size: 120)
                                    : EmptyAvatar(
                                        ignoreOnline: true,
                                        blank: true,
                                        size: 120,
                                        online: true,
                                      )),
                          SizedBox(
                            width: 20,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: Material(
                              color: Colors.white,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: InkWell(
                                      child: Text(
                                          S.of(context).select_new_photo,
                                          style: const TextStyle(
                                              color: Colors.black)),
                                      onTap: () async {
                                        if (_amIAdmin ||
                                            widget.isCreatingChat) {
                                          final file =
                                              await UserAvatar.selectFile();
                                          setState(() {
                                            _pickedFile = file;
                                          });
                                        }
                                      },
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: InkWell(
                                      child: Text(S.of(context).make_new_photo,
                                          style: const TextStyle(
                                              color: Colors.black)),
                                      onTap: () async {
                                        if (_amIAdmin ||
                                            widget.isCreatingChat) {
                                          final file =
                                              await UserAvatar.selectFile(
                                                  fromCamera: true);
                                          setState(() {
                                            _pickedFile = file;
                                          });
                                        }
                                      },
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: Text(S.of(context).chat_title),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Center(
                            child: SizedBox(
                              width: MediaQuery.of(context).size.width * 0.8,
                              child: TextField(
                                controller: _controller,
                                readOnly: widget.chat != null,
                                decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: const EdgeInsets.fromLTRB(
                                      15.0, 10.0, 15.0, 10.0),
                                  filled: true,
                                  hintStyle: TextStyle(fontSize: 14),
                                  fillColor: Colors.white,
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(32.0),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    BlocBuilder<FollowersCubit, FollowersState>(
                        builder: (context, state) {
                      final width = MediaQuery.of(context).size.width;
                      return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            MaterialButton(
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.add_circle_outline,
                                    color: Colors.pinkAccent,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Text(S.of(context).add_chat_members),
                                  ),
                                ],
                              ),
                              onPressed: () {
                                setState(() {
                                  _isUsersExpanded = !_isUsersExpanded;
                                });
                              },
                            ),
                            Center(
                              child: Container(
                                  width: width * 0.7,
                                  padding: const EdgeInsets.only(bottom: 8),
                                  decoration: BoxDecoration(
                                      border: Border(
                                          top:
                                              BorderSide(color: Colors.grey)))),
                            ),
                            AnimatedContainer(
                                duration: const Duration(milliseconds: 400),
                                height: _isUsersExpanded
                                    ? state.followings
                                            .where((follower) =>
                                                !_users.contains(follower.id))
                                            .length *
                                        width /
                                        6
                                    : 0,
                                child: ClipRect(
                                  child: Column(
                                    children: state.followings
                                        .where((follower) =>
                                            !_users.contains(follower.id))
                                        .map(
                                          (user) => Container(
                                            decoration: (_users
                                                    .contains(user.id))
                                                ? BoxDecoration(
                                                    gradient:
                                                        LinearGradient(colors: [
                                                    Colors.blueAccent
                                                        .withAlpha(30),
                                                    Colors.white.withAlpha(30)
                                                  ]))
                                                : null,
                                            child: ProfileRow(
                                              name: user.name,
                                              surname: user.surname,
                                              disableCallback: () {
                                                if (!_usersToAdd
                                                    .contains(user.id))
                                                  setState(() {
                                                    _usersToAdd.add(user.id);
                                                    _users.add(user.id);
                                                  });
                                              },
                                              profileAvatar: user.avatar,
                                              userId: user.id,
                                              online: user.online,
                                              caption: user.onlineString,
                                              status: FetchingStatus.Loaded,
                                            ),
                                          ),
                                        )
                                        .toList(),
                                  ),
                                )),
                          ]);
                    }),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 16, vertical: 8),
                          child: Text(S.of(context).all_chat_members),
                        ),
                        Column(
                          children: _users
                              .where((element) => _users.contains(element))
                              .map(
                                (user) => Container(
                                  child: BlocProvider<UserProfileBloc>(
                                    create: (context) =>
                                        UserProfileBloc.fromLid(
                                            lid: user, loadFromHive: false),
                                    child: Builder(builder: (context) {
                                      return BlocBuilder<UserProfileBloc,
                                              UserProfileState>(
                                          builder: (context, user) {
                                        return ProfileRow(
                                          isAdmin: _admins
                                              .contains(user.profile.lid),
                                          name: user.profile.name,
                                          surname: user.profile.surname,
                                          disableCallback: widget.isCreatingChat
                                              ? () {
                                                  setState(() {
                                                    _users.removeWhere(
                                                        (element) =>
                                                            element ==
                                                            user.profile.lid);
                                                    _usersToAdd.removeWhere(
                                                        (element) =>
                                                            element ==
                                                            user.profile.lid);
                                                  });
                                                }
                                              : null,
                                          rightSuffix: _amIAdmin &&
                                                  !widget.isCreatingChat &&
                                                  !_admins.contains(
                                                      user.profile.lid)
                                              ? IconButton(
                                                  icon: ImageIcon(AssetImage(
                                                      'images/icons/selection_bar/cross.png')),
                                                  onPressed: () {
                                                    setState(() {
                                                      _isDeleting = true;
                                                      _userToDelete =
                                                          user.profile.lid;
                                                      _userNameToDelete =
                                                          user.profile.surname +
                                                              ' ' +
                                                              user.profile.name;
                                                    });
                                                  },
                                                  color: Colors.pinkAccent,
                                                )
                                              : null,
                                          profileAvatar: user.profile.avatar,
                                          userId: user.profile.lid,
                                          online: user.online,
                                          caption: user.onlineString,
                                          status: FetchingStatus.Loaded,
                                        );
                                      });
                                    }),
                                  ),
                                ),
                              )
                              .toList(),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Positioned.fill(
              child: Offstage(
                offstage: !_isDeleting,
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      _isDeleting = false;
                      _userToDelete = null;
                      _userNameToDelete = null;
                    });
                  },
                  child: Container(
                    color: Colors.grey.withAlpha(200),
                  ),
                ),
              ),
            ),
            DeleteDialog(
              isDeleting: !_isDeleting,
              text: 'Исключить пользователя ${_userNameToDelete ?? "Ошибка"}',
              yesCallback: (_) async {
                await GroupChatsRepository().adminRemover(
                    cid: widget.chat?.state.chat.id ?? '0',
                    uid: _userToDelete ?? '0');
                _isDeleting = false;
                _userToDelete = null;
                _userNameToDelete = null;
                widget.chat?.add(LoadMessagesEvent(
                  initial: true,
                ));
              },
              noCallback: (_) {
                setState(() {
                  _userToDelete = null;
                  _userNameToDelete = null;
                  _isDeleting = false;
                });
              },
            ),
            Positioned.fill(
                child: LoadingProgressIndicator(
              listenToChatBloc: false,
              needToShow: _inProgress,
            )),
          ],
        ),
      ),
    );
  }
}
