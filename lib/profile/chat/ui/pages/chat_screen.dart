import 'package:edge_alert/edge_alert.dart';
import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:teormech/auth/bloc/auth_bloc.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_events.dart';
import 'package:teormech/profile/chat/bloc/chat_list_bloc.dart';
import 'package:teormech/profile/bloc/online_status.dart';
import 'package:teormech/profile/chat/bloc/saved_messages_bloc.dart';
import 'package:teormech/profile/chat/bloc/typing_bloc.dart';
import 'package:teormech/profile/chat/model/chat_list_state.dart';
import 'package:teormech/profile/chat/model/chat_model.dart';
import 'package:teormech/profile/chat/model/chats_state.dart';
import 'package:teormech/profile/chat/model/typing_bloc_state.dart';
import 'package:teormech/profile/chat/ui/chat_screen/delete_message_dialog.dart';
import 'package:teormech/profile/chat/ui/widgets/message_row.dart';
import 'package:teormech/profile/chat/ui/widgets/service_message.dart';
import 'package:teormech/profile/chat/ui/widgets/unread_panel.dart';
import 'package:teormech/profile/profile/rep/user_profile_rep.dart';
import 'package:teormech/profile/subscription/model/followers_state.dart';
import 'package:teormech/profile/chat/ui/chat_screen/attachments_bottom_bar.dart';
import 'package:teormech/profile/chat/ui/chat_screen/chat_app_bar.dart';
import 'package:teormech/profile/chat/ui/chat_screen/chat_options_top_bar.dart';
import 'package:teormech/profile/chat/ui/chat_screen/repost_messages.dart';
import 'package:teormech/profile/chat/ui/chat_screen/selection_bar.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/styles/transitions/slide_fade.dart';
import 'package:teormech/widgets/loading_progress_indicator.dart';

class ChatScreenWrapper extends StatelessWidget {
  late final Chat? chat;
  late final Follower? user;
  ChatScreenWrapper({this.chat, this.user});

  static PageRouteBuilder route({Chat? chat, Follower? user}) {
    return PageRouteBuilder(
        transitionsBuilder: slideTransition,
        reverseTransitionDuration: Duration.zero,
        pageBuilder: (context, f1, f2) => ChatScreenWrapper(
              chat: chat,
              user: user,
            ));
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ChatBloc>(create: (context) {
          Chat? newChat;
          if (user != null) {
            newChat = Chat(
                id: user!.id,
                type: '1',
                isGroupChat: false,
                body: '',
                gender: user!.sex,
                lastOnline: user!.lastOnline,
                name: user!.name,
                surname: user!.surname,
                photo: user!.avatar,
                messageDate: '',
                senderId: '');
          }
          return ChatBloc(
              ChatState.empty(
                      (context.read<AuthBloc>().state as Authorized).token.id)
                  .copyWith(
                      chat: (user == null) ? chat : newChat,
                      id: (user == null) ? chat!.id : user!.id),
              savedMessagesBloc: context.read<SavedMessagesBloc>(),
              bottomBarBloc: context.read<BottomBarBloc>(),
              authBloc: context.read<AuthBloc>(),
              chatListBloc: context.read<ChatListBloc>())
            ..add(
              LoadMessagesEvent(silent: false, initial: true),
            );
        }),
      ],
      child: Builder(builder: (context) {
        return BlocProvider<TypingBloc>(
          create: (context) => TypingBloc(
              chatBloc: context.read<ChatBloc>(), chatId: chat?.id ?? '0'),
          child: ChatScreen(),
        );
      }),
    );
  }
}

class ChatScreen extends StatefulWidget {
  ChatScreen();

  static const tStyle =
      TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold);

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> with TickerProviderStateMixin {
  late final _controller =
      AnimationController(vsync: this, duration: Duration(milliseconds: 500));
  late final _animationOffset =
      Tween<Offset>(begin: Offset(0, -0.5), end: Offset.zero)
          .animate(CurvedAnimation(parent: _controller, curve: Curves.easeIn));
  late final Animation<double> _animationOpacity =
      Tween<double>(begin: 0, end: 1).animate(_controller);

  late final _attachmentsAnimController = AnimationController(
      vsync: this, duration: const Duration(milliseconds: 500));
  late final _attachmentsOffset =
      Tween<Offset>(begin: const Offset(0, 0.5), end: Offset.zero)
          .animate(_attachmentsAnimController);
  late final _attachmentsOpacity =
      Tween<double>(begin: 0, end: 1).animate(_attachmentsAnimController);

  late final _chatOptionsTopBarController = AnimationController(
      vsync: this, duration: const Duration(milliseconds: 500));
  late final _chatOptionsTopBarOffset =
      Tween<Offset>(begin: const Offset(0, -1.0), end: const Offset(0, 0.1))
          .animate(_chatOptionsTopBarController);
  late final _chatOptionsTopBarOpacity =
      Tween<double>(begin: 0, end: 1).animate(_chatOptionsTopBarController);

  late final String _myID;
  int? _lastIdx;

  final _imagePicker = ImagePicker();
  final _textFieldFocusNode = FocusNode();

  var _optionsMenuShowed = false;
  var _needToShowEmojiPicker = false;
  var _needToShowAttachments = false;
  var _selectionMenuShowed = false;

  late final String _nextMonday;

  @override
  void initState() {
    _myID = UserProfileRepository().credentials.id;

    var monday = 1;
    var now = new DateTime.now();

    while (now.weekday != monday) {
      now = now.add(new Duration(days: 1));
    }

    _nextMonday = (now.day.toString().length == 1 ? '0' : '') +
        now.day.toString() +
        '.' +
        (now.month.toString().length == 1 ? '0' : '') +
        now.month.toString();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    _attachmentsAnimController.dispose();
    _chatOptionsTopBarController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            resizeToAvoidBottomInset: true,
            appBar: ProfileAppBar(
              leftAlignedTitle: true,
              rightmostButton: IconButton(
                onPressed: () {
                  _optionsMenuShowed = true;
                  _chatOptionsTopBarController.forward();
                },
                icon: Icon(
                  Icons.more_vert,
                  color: Colors.white,
                  size: 30,
                ),
              ),
              title: LayoutBuilder(builder: (context, constraints) {
                return BlocBuilder<ChatBloc, ChatState>(
                    builder: (context, chatState) {
                  var onlineStatus = OnlineStatus.onlineString(
                      lastOnline: (chatState.chat.lastOnline != null)
                          ? DateTime.tryParse(chatState.chat.lastOnline!)
                          : DateTime.fromMillisecondsSinceEpoch(0),
                      sex: chatState.chat.gender,
                      now: DateTime.now());
                  return BlocBuilder<ChatListBloc, ChatListState>(
                    builder: (context, chatListState) {
                      Chat? chat;
                      try {
                        if (chatState.chat.isGroupChat)
                          chat = chatListState.groupChats.firstWhere(
                              (chat) => chat.id == chatState.chat.id);
                        else
                          chat = chatListState.chats.firstWhere(
                              (chat) => chat.id == chatState.chat.id);
                      } catch (e) {
                        chat = null;
                      }
                      return ChatAppBar(
                        onlineStatus: onlineStatus,
                        tStyle: ChatScreen.tStyle,
                        groupChat: chatState.chat.isGroupChat,
                        userId: chatState.chat.isGroupChat
                            ? null
                            : chatState.chat.id,
                        constraints: constraints,
                        surname: chat?.surname ?? chatState.chat.surname,
                        name: chat?.name ?? chatState.chat.name,
                        photo: chat?.photo ?? chatState.chat.photo,
                      );
                    },
                  );
                });
              }),
            ),
            body: Stack(
              children: [
                OverflowBox(
                  minHeight: 600,
                  child: Image(
                    image: AssetImage('images/roles2.png'),
                    height: double.infinity,
                    fit: BoxFit.fill,
                  ),
                ),
                Column(
                  children: [
                    Expanded(
                        flex: 9,
                        child: GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () {
                            if (_optionsMenuShowed) {
                              setState(() {
                                _optionsMenuShowed = false;
                              });
                              _chatOptionsTopBarController.reverse();
                            }
                          },
                          child: BlocConsumer<ChatBloc, ChatState>(
                              listenWhen: (chat1, chat2) {
                            if (chat1.messages.messages.isEmpty &&
                                chat2.messages.messages.isNotEmpty)
                              _lastIdx ??= chat2.messages.messages.last.id;

                            return chat1.selectionMode != chat2.selectionMode ||
                                chat1.success != chat2.success ||
                                chat1.isDeleting != chat2.isDeleting;
                          }, listener: (context, chatState) {
                            if (chatState.selectionMode)
                              _controller.forward();
                            else
                              _controller.reverse();

                            if (chatState.success == CustomStatus.Success) {
                              EdgeAlert.show(context,
                                  backgroundColor: Colors.teal,
                                  icon: Icons.save,
                                  title: 'В избранном',
                                  description: 'Сообщение сохранено',
                                  duration: 1,
                                  gravity: EdgeAlert.BOTTOM);
                            }

                            if (chatState.success == CustomStatus.Error) {
                              EdgeAlert.show(context,
                                  backgroundColor: Colors.red,
                                  icon: Icons.error,
                                  title: 'Ошибка',
                                  description: 'Сетевая ошибка',
                                  duration: 1,
                                  gravity: EdgeAlert.BOTTOM);
                            }
                          }, builder: (context, chatState) {
                            return Stack(
                              children: [
                                Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: ScrollablePositionedList.builder(
                                      physics: const ClampingScrollPhysics(),
                                      itemScrollController:
                                          chatState.scrollController,
                                      itemCount: chatState.messageRows.length,
                                      itemPositionsListener:
                                          chatState.positionsListener,
                                      reverse: true,
                                      itemBuilder: (context, idx) {
                                        final message =
                                            chatState.messageRows[idx];

                                        if (message.first.type == '0')
                                          return ServiceMessage(
                                            message: message.first,
                                            key: Key(message.first.text),
                                          );

                                        final prevMessage = (idx ==
                                                chatState.messageRows.length -
                                                    1)
                                            ? message
                                            : chatState.messageRows[idx + 1];

                                        final needToShowSpacer = message.first
                                                .differ(prevMessage.first) ||
                                            message.elementAt(0).id ==
                                                prevMessage.elementAt(0).id;
                                        final isGroupChat =
                                            chatState.chat.isGroupChat &&
                                                message.first.uid != _myID;

                                        String? bottomSpacer = null;

                                        final _cond = message
                                            .map((e) => e.id)
                                            .contains(_lastIdx);

                                        if (chatState.ban.canWrite &&
                                            _cond &&
                                            chatState.ban.limit != -1)
                                          bottomSpacer =
                                              'Вам доступно еще ${chatState.ban.limit} сообщения(-й)';

                                        if (!chatState.ban.canWrite && _cond)
                                          bottomSpacer =
                                              'Отправить сообщение можно будет ${_nextMonday}';

                                        if (chatState.ban.amIBanned && idx == 0)
                                          bottomSpacer =
                                              'Пользователь запретил Вам отправлять ему сообщения';

                                        final firstUnread =
                                            idx == chatState.firstUnreadIdx;

                                        final isInGroupChatSecondary =
                                            prevMessage.first.uid ==
                                                    message.first.uid &&
                                                prevMessage.first.type == '1' &&
                                                !needToShowSpacer;

                                        return MessageRow(
                                            key: ValueKey(message.first.id),
                                            needToShowSpacer: needToShowSpacer,
                                            isInGroupChatSecondary:
                                                isInGroupChatSecondary,
                                            isFirstUnread: firstUnread,
                                            bottomSpacer: bottomSpacer,
                                            message: message,
                                            isInGroupChat: isGroupChat,
                                            onTapCallback: () {
                                              if (_optionsMenuShowed)
                                                _chatOptionsTopBarController
                                                    .reverse();
                                            },
                                            selected: chatState.selectedIds
                                                .contains(message.first.id),
                                            selectionMode:
                                                chatState.selectionMode,
                                            selectionLongTapCallback: () {
                                              setState(() {
                                                _selectionMenuShowed = true;
                                              });
                                              for (var msg in message)
                                                context.read<ChatBloc>().add(
                                                    SelectMessageEvent(
                                                        idx: msg.id,
                                                        isMyMessage:
                                                            msg.uid == _myID));
                                            },
                                            selectionTapCallback: () {
                                              if (chatState.selectionMode) {
                                                for (var msg in message) {
                                                  context.read<ChatBloc>().add(
                                                      SelectMessageEvent(
                                                          idx: msg.id,
                                                          isMyMessage:
                                                              msg.uid ==
                                                                  _myID));
                                                  setState(() {
                                                    _selectionMenuShowed =
                                                        chatState
                                                                .selectedCount !=
                                                            0;
                                                  });
                                                }
                                              }
                                            },
                                            myId: _myID);
                                      },
                                    )),
                                if (chatState.chat.unreadMessages > 0 &&
                                    !_selectionMenuShowed)
                                  UnreadPanel(
                                      countOfUnread:
                                          chatState.chat.unreadMessages,
                                      readAllMessagesCallback: () {
                                        context
                                            .read<ChatBloc>()
                                            .add(ReadAllMessagesEvent());
                                      },
                                      jumpToLastUnread: () {
                                        context.read<ChatBloc>().jumpToUnread();
                                      }),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 30),
                                  child: ChatOptionTopBar(
                                    closeCallback: () {
                                      setState(() {
                                        _optionsMenuShowed = false;
                                      });
                                      _chatOptionsTopBarController.reverse();
                                    },
                                    chatBloc: context.read<ChatBloc>(),
                                    offset: _chatOptionsTopBarOffset,
                                    opacity: _chatOptionsTopBarOpacity,
                                  ),
                                ),
                                BlocBuilder<ChatListBloc, ChatListState>(
                                    builder: (context, listState) {
                                  if (listState
                                          .messagesForForwarding.isNotEmpty ||
                                      listState
                                          .photosForForwarding.isNotEmpty ||
                                      listState.newsfeedObjectsForForwarding
                                          .isNotEmpty)
                                    return Align(
                                      alignment: Alignment.bottomCenter,
                                      child: RepostMessages(),
                                    );
                                  else
                                    return SizedBox.shrink();
                                }),
                                SelectionChatBar(chatState.selectedCount,
                                    opacity: _animationOpacity,
                                    selectedNotMyMessagesInPlainChat: chatState
                                        .selected
                                        .where((e) => !e.isMyMessage)
                                        .isNotEmpty,
                                    closeCallback: () => context
                                        .read<ChatBloc>()
                                        .add(DisselectAllMessages()),
                                    offset: _animationOffset),
                                Align(
                                  alignment: Alignment.bottomCenter,
                                  child: Offstage(
                                    offstage: !_needToShowAttachments,
                                    child: AttachmentsBottomBar(
                                      opacity: _attachmentsOpacity,
                                      offset: _attachmentsOffset,
                                      imagePicker: _imagePicker,
                                    ),
                                  ),
                                ),
                                Positioned.fill(
                                  child: Offstage(
                                    offstage: !chatState.isDeleting,
                                    child: GestureDetector(
                                      onTap: () {
                                        context
                                            .read<ChatBloc>()
                                            .add(DisselectAllMessages());
                                      },
                                      child: Container(
                                        color: Colors.grey.withAlpha(200),
                                      ),
                                    ),
                                  ),
                                ),
                                DeleteDialog(
                                  isDeleting: !chatState.isDeleting,
                                  text:
                                      'Вы действительно хотите \n удалить ${chatState.selectedCount} сообщение(-й)',
                                  yesCallback: (_) {
                                    context.read<ChatBloc>().add(
                                        DeleteMessagesEvent(isDeleting: true));
                                  },
                                  noCallback: (_) {
                                    context
                                        .read<ChatBloc>()
                                        .add(DisselectAllMessages());
                                  },
                                ),
                                LoadingProgressIndicator(
                                  needToShow: (chatState.loadProgress ?? 2) <
                                          1 ||
                                      chatState.success == CustomStatus.Loading,
                                  listenToChatBloc: true,
                                ),
                              ],
                            );
                          }),
                        )),
                    Container(
                        color: GradientColors.backColor,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 10),
                        child: BlocConsumer<TypingBloc, TypingBlocState>(
                            listenWhen: (state1, state2) =>
                                state1.isBottomBarRequired !=
                                    state2.isBottomBarRequired ||
                                state1.editing != state2.editing,
                            listener: (context, typingState) {
                              if (!typingState.editing) {
                                if (typingState.isBottomBarRequired) {
                                  _attachmentsAnimController.forward();
                                  setState(() {
                                    _needToShowAttachments = true;
                                  });
                                } else {
                                  _attachmentsAnimController.reverse();
                                  setState(() {
                                    _needToShowAttachments = false;
                                  });
                                }
                              }
                            },
                            builder: (context, typingState) {
                              return Stack(
                                children: [
                                  TextField(
                                    controller: typingState.controller,
                                    focusNode: _textFieldFocusNode,
                                    maxLines: null,
                                    decoration: InputDecoration(
                                        filled: true,
                                        hintText: 'Сообщение',
                                        hintStyle: h2DrawerGreyText,
                                        fillColor: Colors.white,
                                        contentPadding: EdgeInsets.only(
                                            left: 40.0, right: 70, top: 25),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(12))),
                                  ),
                                  IconButton(
                                    icon: ImageIcon(
                                        AssetImage('images/icons/skr.png')),
                                    onPressed: () async {
                                      _textFieldFocusNode.unfocus();
                                      context
                                          .read<TypingBloc>()
                                          .enableAttachMode();
                                      await Future.delayed(
                                          Duration(milliseconds: 500));
                                      _textFieldFocusNode.canRequestFocus =
                                          true;
                                    },
                                  ),
                                  Positioned(
                                    right: 30,
                                    child: IconButton(
                                        icon: Icon(
                                          Icons.emoji_emotions_outlined,
                                          color: Colors.blue,
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            _needToShowEmojiPicker =
                                                !_needToShowEmojiPicker;
                                          });
                                        }),
                                  ),
                                  Positioned(
                                    right: 0,
                                    child: IconButton(
                                      icon: Icon(Icons.arrow_forward_rounded),
                                      onPressed: () {
                                        context.read<TypingBloc>().send();
                                      },
                                    ),
                                  ),
                                ],
                              );
                            })),
                    if (_needToShowEmojiPicker)
                      Flexible(
                        flex: 3,
                        child: EmojiPicker(
                            onEmojiSelected: (category, emoji) {
                              final _controller =
                                  context.read<TypingBloc>().state.controller;
                              _controller
                                ..text += emoji.emoji
                                ..selection = TextSelection.fromPosition(
                                    TextPosition(
                                        offset: _controller.text.length));
                            },
                            config: const Config(
                                columns: 7,
                                emojiSizeMax: 32.0,
                                verticalSpacing: 0,
                                horizontalSpacing: 0,
                                initCategory: Category.RECENT,
                                bgColor: Color(0xFFF2F2F2),
                                indicatorColor: Colors.blue,
                                iconColor: Colors.grey,
                                iconColorSelected: Colors.blue,
                                progressIndicatorColor: Colors.blue,
                                backspaceColor: Colors.blue,
                                showRecentsTab: true,
                                recentsLimit: 28,
                                noRecentsText: 'No Recents',
                                noRecentsStyle: TextStyle(
                                    fontSize: 20, color: Colors.black26),
                                categoryIcons: CategoryIcons(),
                                buttonMode: ButtonMode.MATERIAL)),
                      ),
                  ],
                ),
              ],
            )));
  }
}
