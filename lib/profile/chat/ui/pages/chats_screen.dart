import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:teormech/auth/bloc/auth_bloc.dart';
import 'package:teormech/generated/l10n.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_list_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_list_page_cubit.dart';
import 'package:teormech/profile/chat/bloc/saved_messages_bloc.dart';
import 'package:teormech/profile/chat/bloc/typing_bloc.dart';
import 'package:teormech/profile/chat/model/chat_list_state.dart';
import 'package:teormech/profile/chat/ui/chat_list/chat_list_select_bar.dart';
import 'package:teormech/profile/chat/ui/chat_list/personal_chat_list.dart';
import 'package:teormech/profile/chat/ui/pages/create_group_chat_screen.dart';
import 'package:teormech/profile/chat/ui/chat_list/group_chats_list.dart';
import 'package:teormech/profile/chat/ui/chat_list/saved_chat_list.dart';
import 'package:teormech/profile/chat/ui/chat_screen/attachments_bottom_bar.dart';
import 'package:teormech/profile/chat/ui/chat_screen/selection_bar.dart';
import 'package:teormech/profile/profile/rep/user_profile_rep.dart';
import 'package:teormech/profile/subscription/model/followers_state.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/profile/ui/widgets/select_app_bar.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/widgets/loading_progress_indicator.dart';

class ChatsScreen extends StatefulWidget {
  ChatsScreen();

  @override
  _ChatsScreenState createState() => _ChatsScreenState();
}

enum ChatsListPage { Chats, GroupChats, Favourites, Marked }

class _ChatsScreenState extends State<ChatsScreen>
    with TickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  final _filterTextController = TextEditingController();

  final _imagePicker = ImagePicker();

  var _searchBoxExpanded = false;
  var _headmanBoxExpanded = false;

  Timer? _debounce;
  void _onSearchChange(String query, Function(String query) onChange) {
    if (_debounce?.isActive ?? false) _debounce?.cancel();
    _debounce = Timer(const Duration(milliseconds: 700), () {
      onChange(query);
      _debounce?.cancel();
    });
  }

  late final String _myId;

  late final _controller =
      AnimationController(vsync: this, duration: Duration(milliseconds: 500));
  late final _animationOffset =
      Tween<Offset>(begin: Offset(0, -0.5), end: Offset.zero)
          .animate(CurvedAnimation(parent: _controller, curve: Curves.easeIn));
  late final Animation<double> _animationOpacity =
      Tween<double>(begin: 0, end: 1).animate(_controller);

  late final _attachmentsAnimController = AnimationController(
      vsync: this, duration: const Duration(milliseconds: 500));
  late final _attachmentsOffset =
      Tween<Offset>(begin: const Offset(0, 0.5), end: Offset.zero)
          .animate(_attachmentsAnimController);
  late final _attachmentsOpacity =
      Tween<double>(begin: 0, end: 1).animate(_attachmentsAnimController);

  @override
  void dispose() {
    _debounce?.cancel();
    _filterTextController.dispose();

    _controller.dispose();
    _attachmentsAnimController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _myId = UserProfileRepository().credentials.id;

    _filterTextController.addListener(() {
      _onSearchChange(_filterTextController.text,
          (query) => context.read<ChatListBloc>().query(query));
    });

    context
        .read<ChatListPageCubit>()
        .addPageControllerListener((pageController) {
      final pageIdx = pageController.page?.floor() ?? 0;
      final chatPageCubit = context.read<ChatListPageCubit>();
      final prevPage = chatPageCubit.state.page.index;

      if (prevPage != pageIdx) {
        setState(() {
          _searchBoxExpanded = false;
        });
        _filterTextController.clear();
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SafeArea(
      child: BlocBuilder<ChatListBloc, ChatListState>(
        builder: (context, state) {
          return Scaffold(
            backgroundColor: Colors.transparent,
            appBar: ProfileAppBar(
                overrideLeftButton: (state.messagesForForwarding.isNotEmpty ||
                        state.photosForForwarding.isNotEmpty ||
                        state.newsfeedObjectsForForwarding.isNotEmpty)
                    ? IconButton(
                        alignment: Alignment.centerLeft,
                        icon: Icon(
                          Icons.close_rounded,
                          size: 32,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          context.read<ChatListBloc>().forwardMessages();
                        },
                      )
                    : null,
                title: Text(
                  (state.messagesForForwarding.isEmpty ||
                          state.photosForForwarding.isEmpty)
                      ? S.of(context).chats
                      : S.of(context).respost_message,
                  style: appBarTextStyle,
                ),
                rightmostButton: IconButton(
                    icon: Icon(Icons.add_outlined),
                    iconSize: 20,
                    color: Colors.white,
                    onPressed: () {
                      context.read<BottomBarBloc>().go(MaterialPageRoute(
                          builder: (context) => CreateGroupChatScreen(
                                isCreatingChat: true,
                              )));
                    }),
                rightButton: BlocBuilder<ChatListPageCubit, ChatListPageState>(
                  builder: (context, chatListPageState) {
                    return BlocBuilder<AuthBloc, AuthState>(
                      builder: (context, authState) {
                        final isHeadman = (authState as Authorized).isHeadman;
                        final isPersonal =
                            chatListPageState.page == ChatListPage.Chats;
                        final isMailings =
                            chatListPageState.page == ChatListPage.Important;
                        return IconButton(
                          icon: ImageIcon(AssetImage(isPersonal
                              ? 'images/icons/chat_options/search.png'
                              : 'images/icons/comments.png')),
                          onPressed: () {
                            if (isMailings && isHeadman) {
                              setState(() {
                                _headmanBoxExpanded = !_headmanBoxExpanded;
                              });
                            }

                            if (isPersonal) {
                              setState(() {
                                _searchBoxExpanded = !_searchBoxExpanded;
                              });
                            }
                          },
                          color: (isPersonal || (isMailings && isHeadman))
                              ? Colors.white
                              : Colors.transparent,
                          iconSize: 20,
                        );
                      },
                    );
                  },
                )),
            body: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: BlocBuilder<ChatListPageCubit, ChatListPageState>(
                builder: (context, chatPageState) {
                  return BlocProvider<TypingBloc>(
                    create: (_) => TypingBloc(chatId: 'headman'),
                    child: Stack(
                      children: [
                        Container(
                            child: BlocConsumer<SavedMessagesBloc,
                                    SavedMessagesState>(
                                listenWhen: (stateA, stateB) {
                          return stateA.selectionMode != stateB.selectionMode ||
                              stateA.attachmentsBarRequired !=
                                  stateB.attachmentsBarRequired;
                        }, listener: (context, savedState) {
                          if (savedState.selectionMode)
                            _controller.forward();
                          else
                            _controller.reverse();

                          if (savedState.attachmentsBarRequired)
                            _attachmentsAnimController.forward();
                          else
                            _attachmentsAnimController.reverse();
                        }, builder: (context, savedMessagesState) {
                          return NestedScrollView(
                            headerSliverBuilder: (context, innerBoxIsScrolled) {
                              final isMailings =
                                  chatPageState.page == ChatListPage.Important;
                              final isPersonal =
                                  chatPageState.page == ChatListPage.Chats;
                              return [
                                if (isMailings && _headmanBoxExpanded)
                                  SliverToBoxAdapter(
                                    child: HeadmanAppBar(
                                        controller:
                                            savedMessagesState.controller,
                                        sendCallback: () {
                                          final typingBlocState =
                                              context.read<TypingBloc>().state;

                                          context
                                              .read<SavedMessagesBloc>()
                                              .sendMailing(
                                                  attachments: typingBlocState
                                                      .attachedFiles,
                                                  photos: typingBlocState
                                                      .attachedImages);

                                          FocusScope.of(context).unfocus();
                                          if (savedMessagesState
                                              .attachmentsBarRequired)
                                            context
                                                .read<SavedMessagesBloc>()
                                                .toggleAttachments();
                                        },
                                        attachFilesCallback: () {
                                          context
                                              .read<SavedMessagesBloc>()
                                              .toggleAttachments();
                                        }),
                                  ),
                                if (isPersonal && _searchBoxExpanded)
                                  SliverToBoxAdapter(
                                    child: Container(
                                      padding: const EdgeInsets.only(
                                          left: 20, right: 20, top: 10),
                                      child: TextField(
                                        controller: _filterTextController,
                                        decoration: InputDecoration(
                                            contentPadding:
                                                const EdgeInsets.fromLTRB(
                                                    20.0, 10, 20.0, 10.0),
                                            hintText: S.of(context).search,
                                            hintStyle: h2DrawerGreyText,
                                            suffixIcon: Icon(Icons.search),
                                            enabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.grey),
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        32.0)),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        32.0))),
                                      ),
                                    ),
                                  ),
                                ChatListSelectBar.chatBar(
                                  haveUnreadChats:
                                      state.countOfUnreadMessagesInChats != 0,
                                  haveUnreadGroupChats:
                                      state.countOfUnreadMessagesInGroupChats !=
                                          0,
                                  context: context,
                                  scrollController:
                                      chatPageState.scrollController,
                                  status: chatPageState.page,
                                  controller: chatPageState.controller,
                                  haveUnreadMailings:
                                      savedMessagesState.unreadMailings != 0,
                                ),
                                if (savedMessagesState.selected.isNotEmpty)
                                  SliverAppBar(
                                    pinned: true,
                                    backgroundColor: Colors.white,
                                    flexibleSpace: SelectionChatBar(
                                      savedMessagesState.selected.length,
                                      isForSaved: true,
                                      pageController: chatPageState.controller,
                                      messages: savedMessagesState
                                          .messages.messages
                                          .where((element) => savedMessagesState
                                              .selected
                                              .contains(element.id))
                                          .toList(),
                                      closeCallback: () {
                                        context
                                            .read<SavedMessagesBloc>()
                                            .disselectAllMessages();
                                      },
                                      offset: _animationOffset,
                                      opacity: _animationOpacity,
                                    ),
                                  ),
                              ];
                            },
                            body: Container(
                              decoration: BoxDecoration(
                                  border: Border(
                                      top: BorderSide(
                                          color: Colors.grey.shade300))),
                              child: PageView(
                                controller: chatPageState.controller,
                                physics: const NeverScrollableScrollPhysics(),
                                children: [
                                  PersonalChatsList(
                                    widget: widget,
                                    state: state,
                                  ),
                                  GroupChatsList(
                                    widget: widget,
                                    groupChats: state.groupChats,
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                        border: Border(
                                            top: BorderSide(
                                                width: 1,
                                                color: Colors.grey.shade300))),
                                    child: SavedChatList(
                                      isMailings: true,
                                      myId: _myId,
                                    ),
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                        border: Border(
                                            top: BorderSide(
                                                width: 1,
                                                color: Colors.grey.shade300))),
                                    child: SavedChatList(
                                      isMailings: false,
                                      myId: _myId,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        })),
                        Positioned.fill(
                            child: LoadingProgressIndicator(
                                listenToChatBloc: false,
                                needToShow:
                                    state.status == FetchingStatus.InProgress)),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Offstage(
                            offstage: chatPageState != ChatListPage.Important,
                            child: AttachmentsBottomBar(
                              opacity: _attachmentsOpacity,
                              offset: _attachmentsOffset,
                              imagePicker: _imagePicker,
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
          );
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
