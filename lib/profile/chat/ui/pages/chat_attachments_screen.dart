import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/auth/ui/widgets/choice_container.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/bloc/online_status.dart';
import 'package:teormech/profile/chat/bloc/chat_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_events.dart';
import 'package:teormech/profile/chat/model/chats_state.dart';
import 'package:teormech/profile/chat/ui/chat_screen/chat_app_bar.dart';
import 'package:teormech/profile/chat/ui/widgets/chat_gallery.dart';
import 'package:teormech/profile/chat/ui/widgets/message_bubble.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/profile/ui/widgets/select_app_bar.dart';

import 'chat_screen.dart';

class ChatAttachmentsScreen extends StatefulWidget {
  const ChatAttachmentsScreen({Key? key}) : super(key: key);

  @override
  _ChatAttachmentsScreenState createState() => _ChatAttachmentsScreenState();
}

class _ChatAttachmentsScreenState extends State<ChatAttachmentsScreen> {
  final PageController _controller = PageController();
  late final ChatBloc _chatBloc;

  bool _documentsSelected = false;

  @override
  void initState() {
    _chatBloc = context.read<ChatBloc>();
    _chatBloc.add(LoadPhotosEvent());
    _controller.addListener(_pageControllerListener);
    super.initState();
  }

  void _pageControllerListener() {
    final idx = _controller.page?.round() ?? 0;
    if (idx == 1) {
      _chatBloc.add(LoadPhotosEvent(loadAttachments: true));
      setState(() {
        _documentsSelected = true;
      });
    }
    if (idx == 0) {
      setState(() {
        _documentsSelected = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChatBloc, ChatState>(builder: (context, chatState) {
      return Scaffold(
        appBar: ProfileAppBar(
          leftAlignedTitle: true,
          title: LayoutBuilder(builder: (context, constraints) {
            final onlineStatus = OnlineStatus.onlineString(
                lastOnline: (chatState.chat.lastOnline != null)
                    ? DateTime.tryParse(chatState.chat.lastOnline!)
                    : DateTime.fromMillisecondsSinceEpoch(0),
                sex: chatState.chat.gender,
                now: DateTime.now());
            return ChatAppBar(
              groupChat: chatState.chat.isGroupChat,
              onlineStatus: onlineStatus,
              tStyle: ChatScreen.tStyle,
              userId: chatState.chat.isGroupChat ? null : chatState.chat.id,
              constraints: constraints,
              name: chatState.chat.name,
              surname: chatState.chat.surname,
              photo: chatState.chat.photo,
            );
          }),
        ),
        body: NestedScrollView(
          headerSliverBuilder: (context, wasScrolled) => [
            SliverAppBar(
              automaticallyImplyLeading: false,
              backgroundColor: Colors.white,
              pinned: true,
              flexibleSpace: Padding(
                padding: const EdgeInsets.only(left: 4, right: 4, top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ChoiceContainer(
                        text: 'Изображения',
                        chosen: !_documentsSelected,
                        onClickCallback: () {
                          _controller.jumpToPage(0);
                        },
                        style: SelectAppBar.style),
                    ChoiceContainer(
                        text: 'Документы',
                        chosen: _documentsSelected,
                        onClickCallback: () {
                          _controller.jumpToPage(1);
                        },
                        style: SelectAppBar.style),
                  ],
                ),
              ),
            )
          ],
          body: PageView(
            controller: _controller,
            children: [
              Container(
                child: StaggeredGridView.countBuilder(
                  crossAxisCount: 3,
                  itemBuilder: (context, idx) {
                    return ClipRect(
                      clipBehavior: Clip.hardEdge,
                      child: GestureDetector(
                        onTap: () {
                          context.read<BottomBarBloc>().go(
                              MaterialPageRoute(
                                  builder: (context) => ChatGallery(
                                        files: chatState.chat.photos,
                                        idx: idx,
                                        mode: PhotoGallery.Chat,
                                        isFromChat: false,
                                      )),
                              hideBottombar: true);
                        },
                        child: FittedBox(
                          fit: BoxFit.cover,
                          child: CachedNetworkImage(
                            imageUrl: ChatApi.chatPhotosMax +
                                chatState.chat.photos[idx],
                            placeholder: (context, url) => CachedNetworkImage(
                                imageUrl: ChatApi.chatPhotosMin +
                                    chatState.chat.photos[idx]),
                          ),
                        ),
                      ),
                    );
                  },
                  staggeredTileBuilder: (idx) => StaggeredTile.count(1, 1),
                  itemCount: chatState.chat.photos.length,
                ),
              ),
              Container(
                child: ListView.builder(
                  itemBuilder: (context, idx) => Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 15, vertical: 2),
                    child: Container(
                      padding: const EdgeInsets.only(
                          left: 10, right: 10, top: 5, bottom: 5),
                      decoration: BoxDecoration(
                          color: Color(0xFFcccccc).withAlpha(210),
                          border:
                              Border.all(width: 1, color: Colors.transparent),
                          borderRadius: BorderRadius.circular(8)),
                      child: AttachmentsGalleryRow(
                        attachment: chatState.chat.attachments[idx]['file'],
                        realName: chatState.chat.attachments[idx]['real_name'],
                        isInGallery: true,
                      ),
                    ),
                  ),
                  itemCount: chatState.chat.attachments.length,
                ),
              )
            ],
          ),
        ),
      );
    });
  }
}
