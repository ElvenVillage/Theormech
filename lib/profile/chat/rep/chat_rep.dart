import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:dio/dio.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/auth/model/auth_model.dart';
import 'package:teormech/profile/chat/model/chat_model.dart';
import 'package:teormech/profile/chat/model/message_model.dart';
import 'package:teormech/profile/profile/rep/user_profile_rep.dart';

class Ban {
  bool amIBanned;
  bool isUserBanned;

  String _limit;
  String? _myLimit;

  set updateMyLimit(String limit) => _myLimit = limit;

  int _parse(String? _limit) {
    if (_limit == null) return -1;
    if (_limit == 'false') return 0;
    if (_limit == 'true') return -1;
    final a = int.tryParse(_limit.replaceAll('"', ''));

    if (a == null)
      return 0;
    else {
      if (a <= 0) return 0;
    }
    return a;
  }

  int _parseMyLimit(String? _limit) {
    if (_limit == null) return 0;
    if (_limit == 'false' || _limit == 'true') return 0;
    final a = int.tryParse(_limit.replaceAll('"', ''));
    if (a == null) return 0;
    return a;
  }

  bool get canWrite => _parse(_limit) > 0 || _parse(_limit) == -1;
  bool get canWriteToMe => _parseMyLimit(_myLimit) == 0;

  int get limit => _parse(_limit);
  int get myLimit => _parse(_myLimit);

  Ban(
      {required this.amIBanned,
      required this.isUserBanned,
      required String limit,
      String? myLimit})
      : _limit = limit,
        _myLimit = myLimit;

  factory Ban.fromJson(Map<String, dynamic> json) {
    return Ban(
        amIBanned: json['i_am_bad'],
        isUserBanned: json['bad_user'],
        limit: json['limit'].toString());
  }
}

class ChatRepository {
  late final Dio _dio;
  SessionToken credentials;

  static ChatRepository? _instance;

  ChatRepository._internal(this.credentials) {
    _dio = Dio()..options.contentType = Headers.formUrlEncodedContentType;
  }

  factory ChatRepository() {
    if (_instance == null) {
      _instance = ChatRepository._internal(UserProfileRepository().credentials);
    }
    return _instance!..credentials = UserProfileRepository().credentials;
  }

  Future<bool> setCustomLimits(
      {required String uid, required String limits}) async {
    final data = credentials.toJson();
    data['uid'] = uid;
    data['limit'] = limits.toString();

    try {
      final response = await _dio.post(ProfileApi.setCustomLimits,
          data: data, options: Options(responseType: ResponseType.plain));
      return !response.data.toString().contains('false');
    } on DioError {
      return false;
    }
  }

  Future<bool> sendMailing(
      {required String body,
      List<File>? photos,
      List<File>? attachments}) async {
    final Map<String, dynamic> data = {
      'session': credentials.session,
      'token': credentials.token
    };
    final dio = Dio();
    if ((photos?.isEmpty ?? true) && (attachments?.isEmpty ?? true))
      dio.options.contentType = Headers.formUrlEncodedContentType;
    data['body'] = body;
    data['key'] = 'group';

    if (photos != null)
      for (var i = 0; i < photos.length; i++)
        data['photo_$i'] = await MultipartFile.fromFile(photos[i].path);

    if (attachments != null)
      for (var i = 0; i < attachments.length; i++)
        data['file_$i'] = await MultipartFile.fromFile(attachments[i].path);

    final response = await dio.post(ChatApi.sendMailing,
        data: FormData.fromMap(data),
        options: Options(responseType: ResponseType.plain));
    return response.statusCode == 200;
  }

  Future<String> fetchMessageAttachments(
      {required String block, bool fetchPhotos = false}) async {
    final data = credentials.toJson();
    data['block'] = block;
    final response = await _dio.post(
        fetchPhotos ? ChatApi.showAlbum : ChatApi.showAttachments,
        data: data,
        options: Options(responseType: ResponseType.plain));
    return response.data.toString();
  }

  Future<List<Message>> _tryParseMessagesJson(
      {required List<dynamic> json, required SessionToken credentials}) async {
    final messages = <Message>[];

    for (var messageJson in json) {
      var message = Message(
          text: messageJson['body'],
          id: int.tryParse(messageJson['id'] ?? '0') ?? 0,
          isRead: messageJson['isRead'] != '0',
          uid: messageJson['uid'],
          type: messageJson['type'],
          isEdited:
              messageJson['isEdited'] != '0' && messageJson['isEdited'] != null,
          modificationTime: messageJson['modification_time'] ?? '',
          forward: messageJson['forward'],
          sended: true,
          sendTime: messageJson['create_time']);
      final photosToken = messageJson['photos'];
      if (photosToken != null && photosToken != '') {
        final photosResponse = await _dio.post(ChatApi.showAlbum,
            data: {
              'session': credentials.session,
              'token': credentials.token,
              'block': photosToken
            },
            options: Options(responseType: ResponseType.plain));
        message = message.copyWith(photosJson: photosResponse.data.toString());
      }

      final attachmentsToken = messageJson['attachments'];
      if (attachmentsToken != null && attachmentsToken != '') {
        final filesResponse = await _dio.post(ChatApi.showAttachments,
            data: {
              'session': credentials.session,
              'token': credentials.token,
              'block': attachmentsToken
            },
            options: Options(responseType: ResponseType.plain));
        message =
            message.copyWith(attachmentsJson: filesResponse.data.toString());
      }

      messages.add(message);
    }
    messages.sort((a, b) => a.id - b.id);
    return messages;
  }

  Future<Ban> showBan({required String uid, bool isTeacher = false}) async {
    final data = credentials.toJson();

    data['uid'] = uid;
    final options = Options(responseType: ResponseType.plain);
    final rawResponse =
        await _dio.post(ChatApi.showBan, data: data, options: options);
    String? teacherLimits;
    if (isTeacher)
      teacherLimits =
          (await _dio.post(ChatApi.getMyLimits, data: data, options: options))
              .data
              .toString();
    try {
      final response = jsonDecode(rawResponse.data.toString());
      final ban = Ban.fromJson(response);
      if (teacherLimits != null) ban.updateMyLimit = teacherLimits;

      return ban;
    } catch (E) {
      return Ban(amIBanned: false, isUserBanned: false, limit: 'true');
    }
  }

  Future<bool> banUser({required String uid, required bool unban}) async {
    final data = credentials.toJson();
    data['uid'] = uid;

    if (unban) {
      data['unban'] = 'true';
    }
    final response = await _dio.post(ChatApi.banUser, data: data);
    return response.statusCode == 200;
  }

  Future<List<Chat>> fetchChats({String? query}) async {
    final searchMode = query != null;
    final a = <Chat>[];

    final data = credentials.toJson();
    if (searchMode) data['query'] = query!;

    final response = await _dio
        .post(searchMode ? ChatApi.searchChats : ChatApi.showChats, data: data);

    if (response.data.toString() == 'false') return a;

    List<dynamic> j = response.data;

    for (var chatJson in j) {
      final chat = Chat.fromJson(chatJson);
      a.add(chat);
    }
    return a;
  }

  Future<List<Message>> fetchMessages(
      {required String id, bool groupChat = false, String? lid}) async {
    final data = credentials.toJson();
    data['uid'] = id;

    if (groupChat) data['cid'] = id;
    if (lid != null) data['lid'] = lid;

    final response = await _dio.post(
        groupChat ? GroupChatsApi.chatMessages : ChatApi.chatMessages,
        data: data);
    try {
      return await _tryParseMessagesJson(
          json: response.data, credentials: credentials);
    } catch (e) {
      return [];
    }
  }

  Future<bool> _sendPlainTextMessage(
      String body, String uid, bool groupChat) async {
    final data = credentials.toJson();
    data['uid'] = uid;
    data['body'] = body;

    if (groupChat) data['cid'] = uid;
    final response = await _dio.post(
        groupChat ? GroupChatsApi.sendChatMessage : ChatApi.createMessage,
        data: data,
        options: Options(responseType: ResponseType.plain));

    return response.statusCode == 200;
  }

  Future<bool> deleteMessage(
      {required String mid,
      required bool isGroupChat,
      required String uid}) async {
    final data = credentials.toJson();
    data['mid'] = mid;

    if (isGroupChat)
      data['cid'] = uid;
    else
      data['uid'] = uid;
    final response = await _dio.post(ChatApi.deleteMessage,
        data: data, options: Options(responseType: ResponseType.plain));

    return response.statusCode == 200;
  }

  Future<bool> _sendMessageWithAttachments(
      {required String body,
      required String uid,
      List<File>? photos,
      List<File>? attachments,
      Function? callback,
      List<Uint8List>? webPhotos,
      required bool groupChat}) async {
    final dio = Dio();
    Map<String, dynamic> formMap = {
      'session': credentials.session,
      'token': credentials.token,
      'uid': uid,
      'body': body,
    };
    if (groupChat) formMap['cid'] = uid;

    if (photos != null) {
      for (var i = 0; i < photos.length; i++) {
        formMap['photo_$i'] = await MultipartFile.fromFile(photos[i].path);
      }
    }

    if (webPhotos != null) {
      for (var i = 0; i < webPhotos.length; i++) {
        formMap['photo_$i'] = await MultipartFile.fromBytes(webPhotos[i]);
      }
    }

    if (attachments != null) {
      for (var i = 0; i < attachments.length; i++) {
        formMap['file_$i'] = await MultipartFile.fromFile(attachments[i].path);
      }
    }
    final response = await dio.post(
        groupChat ? GroupChatsApi.sendChatMessage : ChatApi.createMessage,
        data: FormData.fromMap(formMap), onSendProgress: (a, b) {
      if (callback != null) callback(a, b);
    });
    return response.statusCode == 200;
  }

  Future<bool> sendMessage(
      {required String body,
      required String uid,
      Function? callback,
      bool groupChat = false,
      List<File>? photos,
      List<Uint8List>? webPhotos,
      List<File>? attachments}) async {
    if (((photos == null) || (photos.isEmpty)) &&
        ((attachments == null) || (attachments.isEmpty))) {
      return await _sendPlainTextMessage(body, uid, groupChat);
    } else {
      return await _sendMessageWithAttachments(
          body: body,
          uid: uid,
          photos: photos,
          attachments: attachments,
          callback: callback,
          groupChat: groupChat,
          webPhotos: webPhotos);
    }
  }

  Future<bool> editMessage(
      {required String chatId,
      required String messageId,
      required bool isFromGroupChat,
      required String body}) async {
    try {
      final data = credentials.toJson();
      data['mid'] = messageId;
      data['body'] = body;

      if (isFromGroupChat)
        data['cid'] = chatId;
      else
        data['uid'] = chatId;
      final response = await _dio.post(
          isFromGroupChat
              ? GroupChatsApi.editGroupChatMessage
              : ChatApi.editMessage,
          data: data,
          options: Options(responseType: ResponseType.plain));
      return response.statusCode == 200;
    } catch (e) {
      return false;
    }
  }

  Future<bool> forwardMessage(
      {required String sourceChatId,
      required String targetChatId,
      required String messageId,
      required bool isFromGroupChat,
      required bool fromSaves,
      required bool isToGroupChat}) async {
    final data = credentials.toJson();

    data['mid'] = messageId;
    if (fromSaves) data['my_saves'] = 'true';
    if (isFromGroupChat)
      data['cfid'] = sourceChatId;
    else
      data['uid'] = sourceChatId;
    if (isToGroupChat)
      data['cid'] = targetChatId;
    else
      data['usid'] = targetChatId;

    final response = await _dio.post(ChatApi.forwardMessage,
        data: data, options: Options(responseType: ResponseType.plain));
    if (response.data != 'false') return true;
    return false;
  }

  Future<bool> readChatMessages(
      {String? chatId,
      bool isGroupChat = false,
      bool isMailing = false}) async {
    final data = credentials.toJson();
    if (!isMailing) {
      if (isGroupChat)
        data['cid'] = chatId!;
      else
        data['uid'] = chatId!;
    }
    final response = await _dio.post(
        isMailing
            ? ChatApi.readWarnings
            : isGroupChat
                ? GroupChatsApi.readGroupChat
                : ChatApi.readChatMessages,
        data: data,
        options: Options(responseType: ResponseType.plain));

    return response.statusCode == 200;
  }

  Future<String> countUnreadMessages(
      {String? chatId,
      bool isMailings = false,
      bool isGroupChat = false}) async {
    final data = credentials.toJson();
    if (!isMailings) {
      if (isGroupChat)
        data['cid'] = chatId!;
      else
        data['uid'] = chatId!;
    }
    try {
      final response = await _dio.post(
          isMailings
              ? ChatApi.countWarnings
              : isGroupChat
                  ? GroupChatsApi.countGroupChatUnread
                  : ChatApi.chatUnreadMessages,
          data: data,
          options: Options(responseType: ResponseType.plain));
      return response.data.toString();
    } catch (e) {
      return '0';
    }
  }

  Future<List<Message>> fetchSavedMessages({
    String? lid,
    bool fetchMailings = false,
  }) async {
    final data = credentials.toJson();
    if (lid != null) data['lid'] = lid;
    try {
      final response = await _dio
          .post(fetchMailings ? ChatApi.mailings : ChatApi.mySaves, data: data);

      final body = response.data;

      if (body == 'false') return [];

      return await _tryParseMessagesJson(json: body, credentials: credentials);
    } catch (e) {
      return [];
    }
  }

  Future<bool> saveMessage(
      {required String uid,
      required bool isGromGroupChat,
      required String mid}) async {
    final data = credentials.toJson();
    data['mid'] = mid;

    if (isGromGroupChat)
      data['cid'] = uid;
    else
      data['uid'] = uid;

    try {
      final response = await _dio.post(
          isGromGroupChat
              ? GroupChatsApi.saveGroupMessage
              : ChatApi.saveMessage,
          data: data);
      return response.statusCode == 200;
    } catch (e) {
      return false;
    }
  }

  Future<bool> unsaveMessage({required String mid}) async {
    final data = credentials.toJson();
    data['mid'] = mid;

    try {
      final response = await _dio.post(ChatApi.unsaveMessage, data: data);
      return response.statusCode == 200;
    } catch (e) {
      return false;
    }
  }

  Future<String> fetchChatAttachments(
      {required String uid,
      bool fetchPhotos = true,
      required bool isGroupChat}) async {
    final data = credentials.toJson();
    if (isGroupChat)
      data['cid'] = uid;
    else
      data['uid'] = uid;
    try {
      final response = await _dio.post(
          fetchPhotos ? ChatApi.allChatPhotos : ChatApi.allChatAttachmets,
          data: data,
          options: Options(responseType: ResponseType.plain));

      return response.data.toString();
    } catch (e) {
      return 'false';
    }
  }

  Future<String> fetchChatLink({required String cid}) async {
    final data = credentials.toJson();
    data['cid'] = cid;

    try {
      final linkResponse = await _dio.post(ChatApi.getChatLink,
          data: data, options: Options(responseType: ResponseType.plain));
      return linkResponse.data.toString();
    } on DioError {
      return '';
    }
  }

  Future<String> joinChatByLink({required String token}) async {
    final data = credentials.toJson();
    data['link'] = token;

    try {
      final response = await _dio.post(ChatApi.joinChatFromLink,
          data: data, options: Options(responseType: ResponseType.plain));
      return response.data.toString();
    } on DioError {
      return 'false';
    }
  }

  Future<bool> uploadGroupChatPhoto(
      {required File file,
      required ProgressCallback onReceiveProgress,
      required String cid}) async {
    final dio = Dio();
    final Map<String, dynamic> data = {
      'token': credentials.token,
      'session': credentials.session,
      'logo': MultipartFile.fromFile(file.path),
      'cid': cid
    };

    final dataReq = FormData.fromMap(data);
    final response = await dio.post(GroupChatsApi.uploadGroupChatPhoto,
        data: dataReq, onReceiveProgress: onReceiveProgress);
    return response.data.toString() != false;
  }
}
