import 'dart:io';
import 'package:dio/dio.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/auth/model/auth_model.dart';
import 'package:teormech/profile/chat/model/chat_model.dart';
import 'package:teormech/profile/profile/rep/user_profile_rep.dart';

class GroupChatsRepository {
  late final Dio _dio;
  SessionToken credentials;
  static GroupChatsRepository? _instance;

  GroupChatsRepository._internal(this.credentials) {
    _dio = Dio()..options.contentType = Headers.formUrlEncodedContentType;
  }
  factory GroupChatsRepository() {
    if (_instance == null) {
      _instance =
          GroupChatsRepository._internal(UserProfileRepository().credentials);
    }
    return _instance!..credentials = UserProfileRepository().credentials;
  }

  Future<List<Chat>> fetchChats() async {
    final a = <Chat>[];

    try {
      final response = await _dio.post(GroupChatsApi.showGroupChats,
          data: {'session': credentials.session, 'token': credentials.token});

      List<dynamic> j = response.data;

      for (var chatJson in j) {
        final chat = Chat.groupChatFromJson(chatJson);
        a.add(chat);
      }

      return a;
    } catch (e) {
      return [];
    }
  }

  Future<List<dynamic>> getChatMembers(
      {required String chatId, bool admins = false}) async {
    try {
      final response = await _dio.post(
          admins
              ? GroupChatsApi.groupChatAdmins
              : GroupChatsApi.groupChatMember,
          data: {
            'session': credentials.session,
            'token': credentials.token,
            'cid': chatId
          });

      return response.data;
    } catch (e) {
      return [];
    }
  }

  Future<bool> adminRemover({required String cid, required String uid}) async {
    try {
      final data = credentials.toJson();
      data['cid'] = cid;
      data['uid'] = uid;
      final response = await _dio.post(GroupChatsApi.adminRemover,
          data: data, options: Options(responseType: ResponseType.plain));
      return response.statusCode == 200;
    } on DioError {
      return false;
    }
  }

  Future<String> createChat({required String name}) async {
    final data = credentials.toJson();
    data['name'] = name;

    try {
      final response = await _dio.post(GroupChatsApi.createChat,
          data: data, options: Options(responseType: ResponseType.plain));
      return response.data.toString();
    } catch (e) {
      return 'false';
    }
  }

  Future<bool> uploadAvatar({required File avatar, required String cid}) async {
    final dio = Dio();
    Map<String, dynamic> formMap = {
      'session': credentials.session,
      'token': credentials.token,
      'cid': cid
    };

    formMap['logo'] = await MultipartFile.fromFile(avatar.path);

    try {
      final response = await dio.post(GroupChatsApi.uploadGroupChatPhoto,
          data: FormData.fromMap(formMap),
          options: Options(responseType: ResponseType.plain));
      return response.data.toString() != 'false';
    } on DioError {
      return false;
    }
  }

  Future<bool> inviteUser({required String cid, required String uid}) async {
    try {
      final response = await _dio.post(GroupChatsApi.addToGroupChat, data: {
        'session': credentials.session,
        'token': credentials.token,
        'uid': uid,
        'cid': cid
      });

      return response.statusCode == 200;
    } catch (e) {
      return false;
    }
  }

  Future<bool> leaveChat({
    required String cid,
  }) async {
    try {
      final response = await _dio.post(GroupChatsApi.leaveGroupChat,
          data: {
            'session': credentials.session,
            'token': credentials.token,
            'cid': cid
          },
          options: Options(responseType: ResponseType.plain));

      return response.statusCode == 200;
    } catch (e) {
      return false;
    }
  }
}
