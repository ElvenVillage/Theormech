import 'dart:io';

import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';
import 'package:teormech/profile/subscription/model/followers_state.dart';

import 'chat_model.dart';
import 'message_model.dart';

class ChatListState {
  final List<Chat> chatList;

  List<Chat> get groupChats =>
      chatList.where((element) => element.isGroupChat).toList();

  List<Chat> get chats =>
      chatList.where((element) => !element.isGroupChat).toList();

  final List<Message> messagesForForwarding;
  final List<NewsfeedObject> newsfeedObjectsForForwarding;
  final List<File> photosForForwarding;

  final FetchingStatus status;

  int get countOfUnreadMessagesInGroupChats => groupChats.fold(
      0,
      (previousValue, element) =>
          previousValue + ((element.unreadMessages != 0) ? 1 : 0));

  int get countOfUnreadMessagesInChats => chats.fold(
      0,
      (previousValue, element) =>
          previousValue + ((element.unreadMessages > 0) ? 1 : 0));

  int get countOfUnreadMessages => chatList.fold(
      0,
      (previousValue, element) =>
          previousValue + ((element.unreadMessages > 0) ? 1 : 0));

  ChatListState(
      {required this.chatList,
      required this.messagesForForwarding,
      required this.photosForForwarding,
      required this.newsfeedObjectsForForwarding,
      required this.status});
  ChatListState.empty()
      : chatList = const [],
        status = FetchingStatus.Initial,
        photosForForwarding = const [],
        newsfeedObjectsForForwarding = const [],
        messagesForForwarding = const [];

  ChatListState copyWith({
    List<Chat>? chats,
    FetchingStatus? status,
    List<NewsfeedObject>? newsfeedObjectsForForwarding,
    List<Message>? messagesForForwarding,
    List<File>? photosForForwarding,
  }) {
    return ChatListState(
        chatList: chats ?? this.chatList,
        status: status ?? this.status,
        newsfeedObjectsForForwarding:
            newsfeedObjectsForForwarding ?? this.newsfeedObjectsForForwarding,
        photosForForwarding: photosForForwarding ?? this.photosForForwarding,
        messagesForForwarding:
            messagesForForwarding ?? this.messagesForForwarding);
  }
}
