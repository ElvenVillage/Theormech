import 'dart:convert';

import 'package:hive/hive.dart';

import 'message_model.dart';

part 'chat_model.g.dart';

@HiveType(typeId: 3)
class Chat extends HiveObject {
  @HiveField(0)
  final String id;
  @HiveField(1)
  final String? photo;
  @HiveField(2)
  final String name;
  @HiveField(3)
  final String surname;
  @HiveField(4)
  final String messageDate;
  @HiveField(5)
  final String senderId;
  @HiveField(6)
  final String body;
  @HiveField(7)
  final String? lastOnline;
  @HiveField(8)
  final String gender;
  @HiveField(9)
  int unreadMessages = 0;

  @HiveField(10)
  String? photosJson;
  List<dynamic> get photos =>
      photosJson == '' || photosJson == null || photosJson == 'false'
          ? []
          : jsonDecode(photosJson!).map((photo) => photo['file']).toList();

  @HiveField(11)
  String? attachmentsJson;
  List<dynamic> get attachments => attachmentsJson == '' ||
          attachmentsJson == null ||
          attachmentsJson == 'false'
      ? []
      : jsonDecode(attachmentsJson!)
          .map((photo) =>
              {'file': photo['file'], 'real_name': photo['real_name']})
          .toList();

  @HiveField(12)
  final bool isGroupChat;

  @HiveField(13)
  final String type;

  bool wasRead;

  DateTime get _onlineDate => DateTime.parse(
      (lastOnline != '' && lastOnline != null) ? lastOnline! : '1970-01-01');
  String get formattedOnline =>
      Message.formatDate(_onlineDate.hour) +
      ':' +
      Message.formatDate(_onlineDate.minute);

  DateTime? _parsedMessageDate;
  DateTime get messageDateParsed =>
      _parsedMessageDate ??= DateTime.parse(messageDate);
  String get formattedMessage {
    final now = DateTime.now();
    if (now.difference(messageDateParsed).inDays < 1)
      return Message.formatDate(messageDateParsed.hour) +
          ':' +
          Message.formatDate(messageDateParsed.minute);
    else
      return messageDateParsed.day.toString() +
          ' ' +
          Message.formatMonth(messageDateParsed.month);
  }

  Chat(
      {required this.id,
      required this.photo,
      required this.name,
      required this.surname,
      required this.messageDate,
      required this.senderId,
      required this.lastOnline,
      required this.gender,
      required this.isGroupChat,
      required this.type,
      this.attachmentsJson,
      this.photosJson,
      this.unreadMessages = 0,
      this.wasRead = true,
      required this.body});

  factory Chat.fromJson(Map<String, dynamic> chatJson) {
    return Chat(
        id: chatJson['id'],
        photo: chatJson['photo'],
        gender: chatJson['sex'],
        name: chatJson['name'],
        surname: chatJson['surname'],
        messageDate: chatJson['message_date'],
        senderId: chatJson['sender_id'],
        isGroupChat: false,
        lastOnline: chatJson['last_online'],
        wasRead: (chatJson['isRead'] ?? '1') == '1',
        type: chatJson['message_type'],
        body: chatJson['body']);
  }

  factory Chat.groupChatFromJson(Map<String, dynamic> groupChatJson) {
    return Chat(
        id: groupChatJson['id'],
        surname: '',
        name: groupChatJson['name'],
        photo: groupChatJson['photo'],
        isGroupChat: true,
        body: groupChatJson['last_message_text'],
        type: groupChatJson['last_message_type'],
        messageDate: groupChatJson['last_message'],
        lastOnline: '',
        gender: '0',
        senderId: groupChatJson['last_message_sender']);
  }
}
