import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/material.dart';

class TypingBlocState {
  final TextEditingController controller;
  final List<File> attachedImages;
  final List<File> attachedFiles;

  final List<Uint8List> webImages;
  final List<Uint8List> webFiles;

  final bool isBottomBarRequired;
  final bool editing;

  TypingBlocState(
      {required this.controller,
      required this.attachedImages,
      required this.attachedFiles,
      required this.webFiles,
      required this.webImages,
      this.editing = false,
      this.isBottomBarRequired = false});

  TypingBlocState copyWith(
      {List<File>? attachedImages,
      bool? isBottomBarRequired,
      bool? editing,
      List<Uint8List>? webImages,
      List<Uint8List>? webFiles,
      List<File>? attachedFiles}) {
    return TypingBlocState(
        controller: this.controller,
        editing: editing ?? this.editing,
        isBottomBarRequired: isBottomBarRequired ?? this.isBottomBarRequired,
        attachedFiles: attachedFiles ?? this.attachedFiles,
        webFiles: webFiles ?? this.webFiles,
        webImages: webImages ?? this.webImages,
        attachedImages: attachedImages ?? this.attachedImages);
  }
}
