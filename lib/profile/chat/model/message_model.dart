import 'dart:convert';

import 'package:hive/hive.dart';
part 'message_model.g.dart';

@HiveType(typeId: 4)
class Message {
  static String formatDate(int val) {
    if (val < 10)
      return '0' + val.toString();
    else
      return val.toString();
  }

  static const NEWSFEED_REPOST = '4';
  static const PLAIN = '1';

  @HiveField(0)
  final int id;
  @HiveField(1)
  final String uid;
  @HiveField(2)
  final String text;
  @HiveField(3)
  final String sendTime;
  @HiveField(4)
  String photosJson;
  @HiveField(5)
  bool isEdited;
  @HiveField(6)
  final String? modificationTime;
  @HiveField(7)
  final String forward;
  @HiveField(8)
  final String attachmentsJson;
  @HiveField(9)
  final String type;

  List<dynamic>? _photos;

  bool isRead;

  final bool sended;

  String? chatId;
  bool? isFromGroupChat;
  bool? isFromSaves;

  DateTime? get sendDateTime => DateTime.tryParse(sendTime);
  DateTime? get _modificationTime => DateTime.tryParse(modificationTime ?? '');

  bool differ(Message msg) =>
      sendDateTime?.day != msg.sendDateTime?.day ||
      sendDateTime?.month != msg.sendDateTime?.month;

  String get dateDayFormatted {
    if (sendDateTime == null)
      return '';
    else {
      final now = DateTime.now();
      final sendDateTime = this.sendDateTime!;
      final diff = now.day - sendDateTime.day;
      final diffMonths = now.month - sendDateTime.month;
      if (diff < 1 && diffMonths == 0) return 'Сегодня';
      if (diff >= 1 && diff < 2) return 'Вчера ';

      return sendDateTime.day.toString() +
          ' ' +
          formatMonth(sendDateTime.month);
    }
  }

  String get dateFormatted {
    if (sendDateTime == null)
      return '';
    else {
      final sendDateTime = this.sendDateTime!;
      return formatDate(sendDateTime.hour) +
          ':' +
          formatDate(sendDateTime.minute);
    }
  }

  String get modificationDateFormatted {
    if (_modificationTime == null)
      return '';
    else {
      final sendDateTime = this._modificationTime!;
      final diff = DateTime.now().difference(sendDateTime).inDays;
      if (diff < 1)
        return formatDate(sendDateTime.hour) +
            ':' +
            formatDate(sendDateTime.minute);
      else
        return formatDate(sendDateTime.day) +
            '.' +
            formatDate(sendDateTime.month);
    }
  }

  List<dynamic> get photos {
    if (_photos != null) return _photos!;
    if (photosJson == '' || photosJson == 'false') return [];
    final a = jsonDecode(photosJson);
    return _photos ??= a.map((e) => e['file']).toList();
  }

  List<dynamic> get attachments => (attachmentsJson == 'false')
      ? []
      : jsonDecode(attachmentsJson)
          .map((attachment) => {
                'file': attachment['file'],
                'real_name': attachment['real_name'] ?? attachment['file']
              })
          .toList();

  Message.empty()
      : id = 0,
        attachmentsJson = '[]',
        photosJson = '[]',
        sendTime = '',
        type = '1',
        sended = true,
        text = '',
        isEdited = false,
        isRead = true,
        forward = '0',
        modificationTime = '',
        uid = '';

  Message copyWith(
      {bool? selected,
      bool? sended,
      String? photosJson,
      bool? isRead,
      String? attachmentsJson}) {
    return Message(
        id: this.id,
        type: this.type,
        text: this.text,
        attachmentsJson: attachmentsJson ?? this.attachmentsJson,
        isEdited: this.isEdited,
        modificationTime: this.modificationTime,
        uid: this.uid,
        isRead: isRead ?? this.isRead,
        photosJson: photosJson ?? this.photosJson,
        sendTime: this.sendTime,
        forward: this.forward,
        sended: sended ?? this.sended);
  }

  Message(
      {required this.text,
      required this.id,
      this.photosJson = '[]',
      this.attachmentsJson = '[]',
      required this.uid,
      required this.isEdited,
      required this.type,
      this.isFromGroupChat,
      required this.forward,
      required this.modificationTime,
      required this.sendTime,
      this.isFromSaves,
      this.isRead = true,
      this.sended = true});

  static String formatMonth(int month) {
    switch (month) {
      case 1:
        return 'января';
      case 2:
        return 'февраля';
      case 3:
        return 'марта';
      case 4:
        return 'апреля';
      case 5:
        return 'мая';
      case 6:
        return 'июня';
      case 7:
        return 'июля';
      case 8:
        return 'августа';
      case 9:
        return 'сентября';
      case 10:
        return 'октября';
      case 11:
        return 'ноября';
      case 12:
        return 'декабря';
    }
    return '';
  }
}

@HiveType(typeId: 5)
class MessagesList {
  @HiveField(0)
  List<Message> messages;
  MessagesList(this.messages);
  List<Message> get reversedMessages => messages.reversed.toList();

  void sync(List<Message> part, bool last) {
    if (part.isEmpty) return;
    if (messages.isEmpty) messages = part;
    final partIds = part.map((msg) => msg.id);
    final msgs = messages.where((element) => !partIds.contains(element.id));

    final rightPart = last ? double.maxFinite : part.last.id;
    final leftPart = part.first.id;

    messages = [...msgs, ...part]
      ..sort((a, b) => a.id - b.id)
      ..removeWhere((msg) {
        return msg.id >= leftPart &&
            msg.id <= rightPart &&
            !partIds.contains(msg.id);
      });
  }
}
