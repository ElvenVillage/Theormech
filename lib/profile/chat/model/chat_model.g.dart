// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ChatAdapter extends TypeAdapter<Chat> {
  @override
  final int typeId = 3;

  @override
  Chat read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Chat(
      id: fields[0] as String,
      photo: fields[1] as String?,
      name: fields[2] as String,
      surname: fields[3] as String,
      messageDate: fields[4] as String,
      senderId: fields[5] as String,
      lastOnline: fields[7] as String?,
      gender: fields[8] as String,
      isGroupChat: fields[12] as bool,
      type: fields[13] as String,
      attachmentsJson: fields[11] as String?,
      photosJson: fields[10] as String?,
      unreadMessages: fields[9] as int,
      body: fields[6] as String,
    );
  }

  @override
  void write(BinaryWriter writer, Chat obj) {
    writer
      ..writeByte(14)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.photo)
      ..writeByte(2)
      ..write(obj.name)
      ..writeByte(3)
      ..write(obj.surname)
      ..writeByte(4)
      ..write(obj.messageDate)
      ..writeByte(5)
      ..write(obj.senderId)
      ..writeByte(6)
      ..write(obj.body)
      ..writeByte(7)
      ..write(obj.lastOnline)
      ..writeByte(8)
      ..write(obj.gender)
      ..writeByte(9)
      ..write(obj.unreadMessages)
      ..writeByte(10)
      ..write(obj.photosJson)
      ..writeByte(11)
      ..write(obj.attachmentsJson)
      ..writeByte(12)
      ..write(obj.isGroupChat)
      ..writeByte(13)
      ..write(obj.type);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ChatAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
