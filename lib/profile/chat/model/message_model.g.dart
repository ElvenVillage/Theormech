// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MessageAdapter extends TypeAdapter<Message> {
  @override
  final int typeId = 4;

  @override
  Message read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Message(
      text: fields[2] as String,
      id: fields[0] as int,
      photosJson: fields[4] as String,
      attachmentsJson: fields[8] as String,
      uid: fields[1] as String,
      isEdited: fields[5] as bool,
      type: fields[9] as String,
      forward: fields[7] as String,
      modificationTime: fields[6] as String?,
      sendTime: fields[3] as String,
    );
  }

  @override
  void write(BinaryWriter writer, Message obj) {
    writer
      ..writeByte(10)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.uid)
      ..writeByte(2)
      ..write(obj.text)
      ..writeByte(3)
      ..write(obj.sendTime)
      ..writeByte(4)
      ..write(obj.photosJson)
      ..writeByte(5)
      ..write(obj.isEdited)
      ..writeByte(6)
      ..write(obj.modificationTime)
      ..writeByte(7)
      ..write(obj.forward)
      ..writeByte(8)
      ..write(obj.attachmentsJson)
      ..writeByte(9)
      ..write(obj.type);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MessageAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class MessagesListAdapter extends TypeAdapter<MessagesList> {
  @override
  final int typeId = 5;

  @override
  MessagesList read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MessagesList(
      (fields[0] as List).cast<Message>(),
    );
  }

  @override
  void write(BinaryWriter writer, MessagesList obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.messages);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MessagesListAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
