import 'dart:io';

import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:teormech/profile/chat/bloc/chat_events.dart';
import 'package:teormech/profile/chat/rep/chat_rep.dart';

import 'chat_model.dart';
import 'message_model.dart';

enum CustomStatus { Success, Initial, Error, Loading }

class ChatState {
  late MessagesList messages;
  List<Message>? _reversedMessages;
  List<Message> get reversedMessages {
    if (_reversedMessages == null ||
        _reversedMessages?.length != messages.messages.length)
      return _reversedMessages ??= messages.messages.reversed.toList();
    return _reversedMessages!;
  }

  late final Chat chat;
  late final ItemScrollController scrollController;
  late final ItemPositionsListener positionsListener;
  late final bool selectionMode;
  late final List<SelectMessageEvent> selected;
  List<int> get selectedIds => selected.map((e) => e.idx).toList();
  final String myId;

  final bool isAdmin;

  bool isEditing;
  bool isDeleting;
  CustomStatus success;

  final Ban ban;

  List<List<Message>>? _messageRows;

  bool _deserveMessageToBeShown(Message msg) =>
      msg.text.trim().isNotEmpty ||
      msg.attachments.isNotEmpty ||
      msg.photos.isNotEmpty;

  List<List<Message>> get messageRows {
    //Геттер группирует сообщения в блоки
    final reversedMessages = this.reversedMessages;
    var start = 0;
    while (start < reversedMessages.length) {
      final firstMessage = reversedMessages[start];
      if (_deserveMessageToBeShown(firstMessage)) {
        _messageRows = [
          [firstMessage]
        ];
        break;
      }
      start++;
    }

    for (var i = start + 1; i < reversedMessages.length; i++) {
      final a = reversedMessages[i];
      final b = reversedMessages[i - 1];

      if (!_deserveMessageToBeShown(a)) {
        continue; //Пустое сообщение разрывает цепочку
      }

      if ((a.uid != b.uid) ||
          (b.forward == '0') ||
          (a.forward == '0') ||
          (a.type == '0')) {
        _messageRows!.add([a]);
        continue;
      }

      _messageRows?.last.add(a);
    }
    return _messageRows ?? [];
  }

  final double? loadProgress;

  int get selectedCount => selected.length;
  int? _firstUnreadIdx;
  int get firstUnreadIdx {
    final messageRows = this.messageRows;
    if (_firstUnreadIdx != null) return _firstUnreadIdx!;
    for (var i = messageRows.length - 1; i >= 0; i--) {
      if (!messageRows[i].first.isRead && messageRows[i].first.uid != myId) {
        _firstUnreadIdx = i;
        return _firstUnreadIdx!;
      }
    }
    return -1;
  }

  int get latestId {
    var idx = 0;
    for (var i in messages.messages) {
      var iInt = i.id;
      if (iInt > idx) idx = iInt;
    }
    return idx;
  }

  ChatState(
      {required this.chat,
      required this.isDeleting,
      required this.myId,
      required this.messages,
      required this.isAdmin,
      this.selectionMode = false,
      required this.ban,
      required this.selected,
      this.isEditing = false,
      this.success = CustomStatus.Initial,
      required this.scrollController,
      required this.loadProgress,
      required this.positionsListener});

  ChatState copyWith(
      {String? id,
      MessagesList? messages,
      bool? selectionMode,
      Chat? chat,
      bool? isEditing,
      List<File>? attachedImages,
      Ban? ban,
      CustomStatus? success,
      double? loadProgress,
      bool? isDeleting,
      bool? isAdmin,
      List<SelectMessageEvent>? selected}) {
    return ChatState(
      chat: chat ?? this.chat,
      ban: ban ?? this.ban,
      messages: messages ?? this.messages,
      myId: this.myId,
      success: success ?? this.success,
      loadProgress: loadProgress ?? this.loadProgress,
      selected: selected ?? this.selected,
      scrollController: this.scrollController,
      positionsListener: this.positionsListener,
      isEditing: isEditing ?? this.isEditing,
      isDeleting: isDeleting ?? this.isDeleting,
      isAdmin: isAdmin ?? this.isAdmin,
      selectionMode: selectionMode ?? this.selectionMode,
    );
  }

  ChatState.empty(this.myId)
      : isEditing = false,
        isAdmin = false,
        success = CustomStatus.Initial,
        loadProgress = null,
        ban = Ban(amIBanned: false, isUserBanned: false, limit: 'true'),
        isDeleting = false {
    messages = MessagesList([]);
    this.selectionMode = false;

    this.selected = [];

    this.scrollController = ItemScrollController();
    this.positionsListener = ItemPositionsListener.create();

    chat = Chat(
        body: '',
        isGroupChat: false,
        gender: '1',
        type: '1',
        id: '',
        messageDate: '',
        name: '',
        photo: '',
        senderId: '',
        lastOnline: '',
        surname: '');
  }
}
