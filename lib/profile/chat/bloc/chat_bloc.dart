import 'dart:async';
import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:teormech/auth/bloc/auth_bloc.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_list_bloc.dart';
import 'package:teormech/profile/chat/bloc/saved_messages_bloc.dart';
import 'package:teormech/profile/chat/model/chats_state.dart';
import 'package:teormech/profile/chat/model/message_model.dart';
import 'package:teormech/profile/chat/rep/chat_rep.dart';
import 'package:teormech/profile/chat/rep/group_chat_rep.dart';
import 'package:flutter/foundation.dart';
import 'package:teormech/profile/newsfeed/rep/newsfeed_repository.dart';
import 'chat_events.dart';

class ChatBloc extends Bloc<ChatEvent, ChatState> {
  ChatBloc(
    ChatState state, {
    required this.chatListBloc,
    required this.bottomBarBloc,
    required this.authBloc,
    required this.savedMessagesBloc,
  }) : super(state) {
    on<ProgressEvent>(_onProgress);
    on<SaveMessagesEvent>(_onSaveMessage);
    on<LoadMessagesEvent>(_onLoadMessages);
    on<LoadPhotosEvent>(_onLoadPhotos);
    on<UploadGroupChatPhotoEvent>(_onUploadGroupChatPhoto);
    on<ReadAllMessagesEvent>(_onReadAllMessages);
    on<SendMessageEvent>(_onSendMessage);
    on<DisselectAllMessages>(_onDisselectAllMessages);
    on<SelectMessageEvent>(_onSelectMessage);
    on<EditMessageEvent>(_onEditMessage);
    on<EditMessageTextEvent>(_onEditMessageText);
    on<DeleteMessagesEvent>(_onDeleteMessage);
    on<BanUserEvent>(_onBanUser);

    init();
    _initPushNotifications();
  }

  final ChatListBloc chatListBloc;
  final SavedMessagesBloc savedMessagesBloc;
  final BottomBarBloc bottomBarBloc;
  final AuthBloc authBloc;
  Box<MessagesList>? _box;

  late final Timer updateTimer;

  bool _readyToMarkRead = false;
  bool _wasFullFetched = false;
  bool _readyToSend = true;

  int? rightId;
  StreamSubscription<RemoteMessage>? _pushSubscription;

  void init() {
    updateTimer = Timer.periodic(
        kIsWeb ? Duration(seconds: 5) : Duration(minutes: 2), (timer) {
      add(LoadMessagesEvent(silent: true, prev: false, initial: false));
    });
    state.positionsListener.itemPositions.addListener(_positionsListener);
  }

  void _initPushNotifications() {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      final data = message.data;
      if ((data['cid'] == state.chat.id && state.chat.isGroupChat) ||
          (data['uid'] == state.chat.id && !state.chat.isGroupChat)) {
        add(LoadMessagesEvent(initial: false, silent: true));
      }
    });
  }

  void _positionsListener() {
    final itemPositions = state.positionsListener.itemPositions.value;

    final max = itemPositions
        .where((ItemPosition position) => position.itemTrailingEdge > 0)
        .reduce((ItemPosition min, ItemPosition position) =>
            position.itemTrailingEdge < min.itemTrailingEdge ? position : min)
        .index;

    final min = itemPositions
        .where((ItemPosition position) => position.itemLeadingEdge < 1)
        .reduce((ItemPosition max, ItemPosition position) =>
            position.itemLeadingEdge > max.itemLeadingEdge ? position : max)
        .index;

    if (state.reversedMessages.length > 90) {
      rightId = state.reversedMessages[min].id;
      if (state.reversedMessages.length - min < 10 &&
          !(state.success == CustomStatus.Loading) &&
          !_wasFullFetched) {
        add(LoadMessagesEvent(silent: true, prev: true, initial: false));
      }
    }

    if (max < 2 && _readyToMarkRead) {
      _readyToMarkRead = false;
      add(ReadAllMessagesEvent());
    }
  }

  @override
  Future<void> close() async {
    updateTimer.cancel();
    state.positionsListener.itemPositions.removeListener(_positionsListener);
    _pushSubscription?.cancel();
    return super.close();
  }

  Message _mockMessage(int countOfImages, String text) {
    final now = DateTime.now().toString();
    return Message(
        id: state.latestId + 1,
        type: '1',
        text: text
            .replaceAll(RegExp(r'(?:[\t ]*(?:\r?\n|\r))+'), '\n')
            .replaceAll(RegExp(' {2,}'), ' '),
        isEdited: false,
        forward: '0',
        photosJson: jsonEncode([
          for (var i = 0; i < (countOfImages); i++)
            {'file': 'https://bucket-t5.s3.eu-north-1.amazonaws.com/zagl.gif'}
        ]),
        modificationTime: '',
        sended: false,
        uid: ChatRepository().credentials.id,
        sendTime: now);
  }

  void _onProgress(ProgressEvent event, Emitter<ChatState> emit) {
    emit(state.copyWith(
        loadProgress: event.progress, success: CustomStatus.Initial));
  }

  Future<void> _onSaveMessage(
      SaveMessagesEvent event, Emitter<ChatState> emit) async {
    final result = <bool>[];
    for (var selectedId in state.selected)
      result.add(await ChatRepository().saveMessage(
          uid: state.chat.id,
          isGromGroupChat: state.chat.isGroupChat,
          mid: selectedId.idx.toString()));
    emit(state.copyWith(
        selected: const [],
        selectionMode: false,
        success: result.contains(false)
            ? CustomStatus.Error
            : CustomStatus.Success));
    await savedMessagesBloc.init(needToCheckHive: false);
    emit(state.copyWith(success: CustomStatus.Initial));
  }

  void _onUploadGroupChatPhoto(
      UploadGroupChatPhotoEvent event, Emitter<ChatState> emit) async {
    if (state.chat.isGroupChat) {
      emit(state.copyWith(loadProgress: 0.1));
      await ChatRepository().uploadGroupChatPhoto(
          file: event.file,
          onReceiveProgress: (int a, int b) {
            add(ProgressEvent(a / b));
          },
          cid: state.chat.id);
      emit(state.copyWith(success: CustomStatus.Initial));
      chatListBloc.load(loadFromHive: false);
    }
  }

  Future<void> _onSendMessage(
      SendMessageEvent event, Emitter<ChatState> emit) async {
    if (!_readyToSend) return;

    final now = DateTime.now().toString();

    final forwardedNewsfeedObjects =
        chatListBloc.state.newsfeedObjectsForForwarding;
    if (forwardedNewsfeedObjects.isNotEmpty) {
      _readyToSend = false;

      forwardedNewsfeedObjects.forEach((forwardedNewsfeedObject) async {
        await NewsfeedRepository().sendFromNewsfeed(
            sendedObject: forwardedNewsfeedObject,
            cid: state.chat.isGroupChat ? state.chat.id : null,
            uid: state.chat.isGroupChat ? null : state.chat.id);
      });
    }

    final forwardedMessages = chatListBloc.state.messagesForForwarding;
    if (forwardedMessages.isNotEmpty) _readyToSend = false;

    var nMessages = state.messages.messages
      ..addAll(forwardedMessages.map((e) => Message(
          id: state.latestId,
          text: e.text,
          type: '1',
          isEdited: e.isEdited,
          forward: e.uid,
          photosJson: e.photosJson,
          modificationTime: e.modificationTime,
          sendTime: now,
          uid: ChatRepository().credentials.id,
          sended: false)));

    emit(state.copyWith(messages: MessagesList(nMessages)));

    final forwardingResults = <bool>[];
    for (var forwardingMsg in forwardedMessages) {
      forwardingResults.add(await ChatRepository().forwardMessage(
          fromSaves: forwardingMsg.isFromSaves ?? false,
          sourceChatId: forwardingMsg.chatId ?? state.chat.id,
          targetChatId: state.chat.id,
          messageId: forwardingMsg.id.toString(),
          isFromGroupChat: forwardingMsg.isFromGroupChat ?? false,
          isToGroupChat: state.chat.isGroupChat));
    }
    if (forwardingResults.contains(false)) {
      emit(state.copyWith(success: CustomStatus.Error));
      Future.delayed(Duration(seconds: 1))
          .then((_) => emit(state.copyWith(success: CustomStatus.Initial)));
    }

    final sharedPhotos = chatListBloc.state.photosForForwarding;

    if (sharedPhotos.isNotEmpty) {
      _readyToSend = false;
      nMessages.add(_mockMessage(sharedPhotos.length, ' '));
      emit(state.copyWith(
          messages: MessagesList(nMessages), success: CustomStatus.Loading));
      final sharingRes = await ChatRepository().sendMessage(
          body: ' ',
          groupChat: state.chat.isGroupChat,
          uid: state.chat.id,
          callback: (int a, int b) {
            add(ProgressEvent(a / b));
          },
          photos: sharedPhotos);
      if (!sharingRes)
        emit(state.copyWith(success: CustomStatus.Error));
      else
        chatListBloc.forwardMessages();
      await Future.delayed(Duration(seconds: 1))
          .then((_) => emit(state.copyWith(success: CustomStatus.Initial)));
    }

    final msgContainsLoadingContent = (event.photos?.isNotEmpty ?? false) ||
        (event.attachments?.isNotEmpty ?? false) ||
        (event.webPhotos?.isNotEmpty ?? false);

    if (msgContainsLoadingContent ||
        event.msg.isNotEmpty ||
        forwardedMessages.isNotEmpty ||
        forwardedNewsfeedObjects.isNotEmpty) {
      _readyToSend = false;
      nMessages.add(_mockMessage(event.photos?.length ?? 0, event.msg));
      emit(state.copyWith(
          messages: MessagesList(nMessages),
          success: CustomStatus.Initial,
          isEditing: false,
          loadProgress: msgContainsLoadingContent ? 0.1 : null));

      final res = await ChatRepository().sendMessage(
          body: event.msg == ''
              ? ' '
              : event.msg
                  .replaceAll(new RegExp(r'(?:[\t ]*(?:\r?\n|\r))+'), '\n')
                  .replaceAll(RegExp(' {2,}'), ' '),
          uid: state.chat.id,
          photos: event.photos,
          webPhotos: event.webPhotos,
          groupChat: state.chat.isGroupChat,
          attachments: event.attachments,
          callback: (int a, int b) {
            add(ProgressEvent(a / b));
          });
      _readyToSend = true;

      if (!res) {
        emit(state.copyWith(success: CustomStatus.Error));
        await Future.delayed(Duration(seconds: 1))
            .then((_) => emit(state.copyWith(success: CustomStatus.Initial)));
      }

      emit(state.copyWith(loadProgress: null, success: CustomStatus.Initial));
      chatListBloc.load(loadUnreadCount: false, loadFromHive: false);
      add(LoadMessagesEvent(silent: true, initial: false));
      add(ReadAllMessagesEvent());
      chatListBloc.forwardMessages();
    }
  }

  Future<void> _onLoadMessages(
      LoadMessagesEvent event, Emitter<ChatState> emit) async {
    final isTeacher = (authBloc.state as Authorized).isTeacher;

    if (!event.silent) {
      emit(state.copyWith(success: CustomStatus.Loading));
      if (_box == null && (state.chat.id != '')) {
        _box = await Hive.openBox(
            state.chat.isGroupChat ? 'group' + state.chat.id : state.chat.id);
      }

      if (state.messages.messages.isEmpty) {
        var messagesFromHive = _box?.get(state.chat.id);
        emit(state.copyWith(messages: messagesFromHive));
      }
    }

    final messages = await ChatRepository().fetchMessages(
        lid: rightId == null || !event.prev ? null : (rightId).toString(),
        id: state.chat.id,
        groupChat: state.chat.isGroupChat);
    Ban? ban;
    if (!state.chat.isGroupChat) {
      ban = await ChatRepository()
          .showBan(uid: state.chat.id, isTeacher: isTeacher);
    }
    if (messages.isNotEmpty) {
      final startLength = state.messages.messages.length;
      final mList = state.messages..sync(messages, !event.prev);
      final endLength = mList.messages.length;
      if (endLength < startLength) {
        print('bug');
      }

      _box?.put(state.chat.id, mList);
      emit(state.copyWith(
          messages: mList, success: CustomStatus.Initial, ban: ban));
    } else {
      emit(state.copyWith(success: CustomStatus.Initial));
      _wasFullFetched = true;
    }

    if (event.initial) {
      final idx = state.firstUnreadIdx;
      if (idx != -1)
        WidgetsBinding.instance?.addPostFrameCallback((value) async {
          state.scrollController.jumpTo(index: idx);
          await Future.delayed(Duration(seconds: 1));
          _readyToMarkRead = true;
        });
      if (state.chat.isGroupChat) {
        final admins = await GroupChatsRepository()
            .getChatMembers(chatId: state.chat.id, admins: true);
        final isAdmin = admins.contains(ChatRepository().credentials.id);
        emit(state.copyWith(isAdmin: isAdmin));
      }
    }
  }

  void _onDisselectAllMessages(
      DisselectAllMessages event, Emitter<ChatState> emit) {
    emit(state.copyWith(
        selected: [],
        selectionMode: false,
        isEditing: false,
        isDeleting: false));
  }

  void _onReadAllMessages(
      ReadAllMessagesEvent event, Emitter<ChatState> emit) async {
    var nChat = state.messages;
    state.chat.unreadMessages = 0;
    nChat.messages.forEach((msg) {
      msg.isRead = true;
    });

    await ChatRepository().readChatMessages(
        chatId: state.chat.id, isGroupChat: state.chat.isGroupChat);

    chatListBloc.readChat(state.chat);
    emit(state.copyWith(messages: nChat));
  }

  void _onSelectMessage(SelectMessageEvent event, Emitter<ChatState> emit) {
    if (state.selectedIds.contains(event.idx)) {
      final nSelected = state.selected..removeWhere((e) => e.idx == event.idx);

      emit(state.copyWith(
          selected: nSelected,
          selectionMode: nSelected.isNotEmpty,
          isEditing: false));
    } else {
      emit(state
          .copyWith(selected: [...state.selected, event], selectionMode: true));
    }
  }

  void _onEditMessage(EditMessageEvent event, Emitter<ChatState> emit) {
    final editableMessage = state.messages.messages
        .firstWhere((msg) => msg.id == state.selected.first.idx);

    if (editableMessage.uid == ChatRepository().credentials.id &&
        editableMessage.forward == '0') emit(state.copyWith(isEditing: true));
  }

  void _onEditMessageText(
      EditMessageTextEvent event, Emitter<ChatState> emit) async {
    await ChatRepository().editMessage(
        isFromGroupChat: state.chat.isGroupChat,
        chatId: state.chat.id,
        messageId: state.messages.messages
            .firstWhere((msg) => msg.id == state.selected.first.idx)
            .id
            .toString(),
        body: event.body);

    add(LoadMessagesEvent(silent: false, initial: false));
  }

  void repostMessage() {
    chatListBloc.addMessagesForForwarding(
        messages: state.selectedIds
            .map((id) =>
                state.messages.messages.firstWhere((msg) => msg.id == id))
            .toList(),
        chatId: state.chat.id,
        fromSaves: false,
        isFromGroupChat: state.chat.isGroupChat);
    bottomBarBloc.setTab(BottomBars.Chat);
  }

  void jumpToUnread() {
    if (state.firstUnreadIdx != -1) {
      state.scrollController.scrollTo(
          index: state.firstUnreadIdx, duration: Duration(seconds: 1));
    }
  }

  void _onDeleteMessage(DeleteMessagesEvent event, Emitter<ChatState> emit) {
    if (event.isDeleting) {
      Future.wait(state.selectedIds.map((e) async {
        await ChatRepository().deleteMessage(
            mid: e.toString(),
            uid: state.chat.id,
            isGroupChat: state.chat.isGroupChat);
      }));
      emit(state.copyWith(
          isDeleting: false, selected: [], selectionMode: false));
      add(LoadMessagesEvent(initial: false));
      chatListBloc.load(
          loadFromHive: false,
          updateOnlyGroupChats: state.chat.isGroupChat,
          updateOnlyPersonalChats: !state.chat.isGroupChat);
    } else {
      emit(state.copyWith(isDeleting: true));
    }
  }

  Future<void> _onLoadPhotos(
      LoadPhotosEvent event, Emitter<ChatState> emit) async {
    final photosJson = await ChatRepository().fetchChatAttachments(
        uid: state.chat.id,
        fetchPhotos: !event.loadAttachments,
        isGroupChat: state.chat.isGroupChat);
    if (event.loadAttachments) {
      final nChat = state.chat..attachmentsJson = photosJson;
      emit(state.copyWith(chat: nChat));
    } else {
      final nChat = state.chat..photosJson = photosJson;
      emit(state.copyWith(chat: nChat));
    }
  }

  Future<void> _onBanUser(BanUserEvent event, Emitter<ChatState> emit) async {
    final result = await ChatRepository()
        .banUser(uid: state.chat.id, unban: state.ban.isUserBanned);
    if (result)
      emit(state.copyWith(
          ban: state.ban..isUserBanned = !state.ban.isUserBanned));
  }

  Future<void> leaveChat() async {
    if (state.chat.isGroupChat) {
      final res = await GroupChatsRepository().leaveChat(cid: state.chat.id);
      if (res) {
        chatListBloc.load(
            loadFromHive: false,
            loadFromNetwork: true,
            updateOnlyGroupChats: true,
            loadUnreadCount: false);
        bottomBarBloc.setTab(BottomBars.Chat);
      }
    }
  }

  Future<void> toggleChatRestrictions() async {
    if (!state.chat.isGroupChat) {
      final limits = state.ban.canWriteToMe ? 'DELETE' : '0';
      await ChatRepository()
          .setCustomLimits(uid: state.chat.id, limits: limits);
      add(LoadMessagesEvent(initial: false));
    }
  }

  void saveMessage() {
    final message = state.messages.messages
        .firstWhere((element) => element.id == state.selected.first.idx);
    Clipboard.setData(ClipboardData(text: message.text));
  }

  Future<bool> shareLink() async {
    if (state.chat.isGroupChat) {
      final link = await ChatRepository().fetchChatLink(cid: state.chat.id);
      if (link.isNotEmpty) {
        Clipboard.setData(ClipboardData(text: link));
        return true;
      }
    }
    return false;
  }
}
