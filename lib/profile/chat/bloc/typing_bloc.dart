import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:teormech/profile/chat/bloc/saved_messages_bloc.dart';
import 'package:teormech/profile/chat/model/chats_state.dart';
import 'package:teormech/profile/chat/model/typing_bloc_state.dart';

import 'chat_bloc.dart';
import 'chat_events.dart';

class TypingBloc extends Cubit<TypingBlocState> {
  ChatBloc? chatBloc;
  SavedMessagesBloc? savedMessagesBloc;
  final String chatId;

  Box<String>? _cachedInputsBox;
  late final Timer _timer;
  bool wasTrasfered = false;

  late final StreamSubscription<ChatState>? _subscription;

  TypingBloc({this.chatBloc, required this.chatId, this.savedMessagesBloc})
      : super(TypingBlocState(
            controller: TextEditingController(),
            attachedImages: [],
            webFiles: [],
            webImages: [],
            attachedFiles: [])) {
    init();
    _timer = Timer.periodic(Duration(seconds: 5), (timer) {
      if (_cachedInputsBox?.isOpen ?? false) {
        _cachedInputsBox?.put(chatId, state.controller.text);
      }
    });
  }

  void init() async {
    _cachedInputsBox = await Hive.openBox('cached_inputs');
    final cachedText = _cachedInputsBox!.get(chatId);

    state.controller.text = cachedText ?? '';

    _subscription = chatBloc?.stream.listen((event) {
      if (event.isEditing && !wasTrasfered) {
        state.controller.text = event.messages.messages
            .where((element) => element.id == event.selected.first.idx)
            .first
            .text;
        wasTrasfered = true;
      }
      if (!event.isEditing && wasTrasfered) wasTrasfered = false;
      //emit(state.copyWith(editing: event.isEditing));
    });
  }

  void reorderPhotos(int oldIndex, int newIndex) {
    final nState = state.copyWith();
    if (oldIndex < newIndex) {
      newIndex -= 1;
    }
    final item = nState.attachedImages.removeAt(oldIndex);
    nState.attachedImages.insert(newIndex, item);

    emit(nState);
  }

  @override
  Future<void> close() async {
    _cachedInputsBox?.put(chatId, state.controller.text);
    _cachedInputsBox?.close();
    _subscription?.cancel();
    _timer.cancel();
    state.controller.dispose();
    return super.close();
  }

  void send() {
    _cachedInputsBox?.put(chatId, '');

    if (chatBloc != null) {
      if (chatBloc!.state.isEditing) {
        chatBloc!.add(EditMessageTextEvent(body: state.controller.text));
        chatBloc!.add(DisselectAllMessages());
        state.controller.text = '';
        emit(state.copyWith(attachedImages: [], isBottomBarRequired: false));
      } else {
        chatBloc!.add(SendMessageEvent(
            msg: state.controller.text,
            photos: state.attachedImages,
            attachments: state.attachedFiles));
        state.controller.text = '';
        emit(state.copyWith(
            attachedImages: [], isBottomBarRequired: false, attachedFiles: []));
      }
    }
  }

  void enableAttachMode() {
    emit(state.copyWith(isBottomBarRequired: !state.isBottomBarRequired));
  }

  void appendImages(List<File> images) {
    emit(state.copyWith(attachedImages: [...state.attachedImages, ...images]));
  }

  void appendWebImages(List<Uint8List> images) {
    emit(state.copyWith(webImages: [...state.webImages, ...images]));
  }

  void appendFiles(List<File> files) {
    emit(state.copyWith(attachedFiles: [...state.attachedFiles, ...files]));
  }

  void appendWebFiles(List<Uint8List> files) {
    emit(state.copyWith(webFiles: [...state.webFiles, ...files]));
  }

  void removeImage({required int idx}) {
    final nState = kIsWeb ? state.webImages : state.attachedImages;
    nState.removeAt(idx);
    if (kIsWeb) {
      emit(state.copyWith(webImages: nState as List<Uint8List>));
    } else {
      emit(state.copyWith(attachedImages: nState as List<File>));
    }
  }

  void removeAttachment({required int idx}) {
    if (!kIsWeb) {
      final nState = state.attachedFiles;
      nState.removeAt(idx);
      emit(state.copyWith(attachedFiles: nState));
    } else {
      final nState = state.webFiles;
      nState.removeAt(idx);
      emit(state.copyWith(webFiles: nState));
    }
  }
}
