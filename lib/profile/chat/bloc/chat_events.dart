import 'dart:io';

import 'dart:typed_data';

abstract class ChatEvent {}

class SendMessageEvent extends ChatEvent {
  final String msg;
  final List<File>? photos;
  final List<File>? attachments;
  final List<Uint8List>? webPhotos;

  SendMessageEvent(
      {required this.msg, this.photos, this.attachments, this.webPhotos});
}

class LoadMessagesEvent extends ChatEvent {
  final bool silent;
  final bool initial;
  final bool prev;
  LoadMessagesEvent(
      {this.silent = false, required this.initial, this.prev = false});
}

class DisselectAllMessages extends ChatEvent {}

class AppendImagesToMessage extends ChatEvent {}

class ReadAllMessagesEvent extends ChatEvent {}

class SaveMessagesEvent extends ChatEvent {}

class LoadPhotosEvent extends ChatEvent {
  final bool loadAttachments;
  LoadPhotosEvent({this.loadAttachments = false});
}

class ProgressEvent extends ChatEvent {
  final double progress;
  ProgressEvent(this.progress);
}

class SelectMessageEvent extends ChatEvent {
  final int idx;
  final bool isMyMessage;
  SelectMessageEvent({required this.idx, required this.isMyMessage});
}

class EditMessageEvent extends ChatEvent {}

class EditMessageTextEvent extends ChatEvent {
  String body;
  EditMessageTextEvent({required this.body});
}

class DeleteMessagesEvent extends ChatEvent {
  final bool isDeleting;
  DeleteMessagesEvent({required this.isDeleting});
}

class BanUserEvent extends ChatEvent {}

class UploadGroupChatPhotoEvent extends ChatEvent {
  final File file;
  UploadGroupChatPhotoEvent({required this.file});
}
