import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

enum ChatListPage { Chats, Groups, Important, Saved }

class ChatListPageState {
  final ChatListPage page;
  final PageController controller;
  final ScrollController scrollController;

  ChatListPageState(
      {required this.page,
      required this.controller,
      required this.scrollController});

  ChatListPageState copyWith({ChatListPage? page}) {
    return ChatListPageState(
        controller: controller,
        page: page ?? this.page,
        scrollController: scrollController);
  }
}

typedef PageControllerListener = void Function(PageController);

class ChatListPageCubit extends Cubit<ChatListPageState> {
  ChatListPageCubit(ChatListPageState initialState) : super(initialState);
  ChatListPageCubit.initial()
      : super(ChatListPageState(
            page: ChatListPage.Chats,
            controller: PageController(),
            scrollController: ScrollController()));

  bool _wasInitialized = false;

  void addPageControllerListener(PageControllerListener callback) {
    if (!_wasInitialized) {
      state.controller.addListener(() {
        callback(state.controller);
      });
    }
  }

  void selectPage(ChatListPage page, {jumpToPage = false}) {
    if (page != state) {
      try {
        state.controller.jumpToPage(page.index);

        state.scrollController.animateTo(20.0 * page.index,
            duration: const Duration(milliseconds: 500),
            curve: Curves.decelerate);
      } catch (e) {}
      emit(state.copyWith(page: page));
    }
  }

  @override
  Future<void> close() async {
    state.controller.dispose();
    state.scrollController.dispose();
    return super.close();
  }
}
