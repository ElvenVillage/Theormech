import 'dart:async';
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:teormech/profile/chat/bloc/chat_list_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_list_page_cubit.dart';
import 'package:teormech/profile/chat/model/chats_state.dart';
import 'package:teormech/profile/chat/model/message_model.dart';
import 'package:teormech/profile/chat/rep/chat_rep.dart';
import 'package:teormech/profile/profile/rep/user_profile_rep.dart';

class SavedMessagesState {
  final MessagesList messages;
  final CustomStatus status;

  final MessagesList mailings;
  final List<int> selectedMailings;

  final List<int> selected;
  final bool selectionMode;

  final TextEditingController controller;

  final ItemScrollController scrollController;
  final ItemPositionsListener positionsListener;

  final int unreadMailings;

  final bool attachmentsBarRequired;

  static const SAVED_MESSAGES_LIST = 'saved';
  static const MAILING_LIST = 'mailings';
  static const SAVED_MESSAGES_FILE = 'spec_messages';

  late final String myId;

  int? _firstUnreadIdx;
  int get firstUnreadIdx {
    final messages = this.mailings.messages;
    if (_firstUnreadIdx != null) return _firstUnreadIdx!;
    for (var i = messages.length - 1; i > 1; i--) {
      if (!messages[i].isRead && messages[i].uid != myId) {
        _firstUnreadIdx = i;
        return _firstUnreadIdx!;
      }
    }
    return -1;
  }

  SavedMessagesState(
      {required this.messages,
      required this.controller,
      required this.status,
      required this.selected,
      required this.mailings,
      required this.selectedMailings,
      required this.attachmentsBarRequired,
      required this.scrollController,
      required this.positionsListener,
      this.unreadMailings = 0,
      required this.selectionMode}) {
    myId = UserProfileRepository().credentials.id;
  }

  SavedMessagesState copyWith(
      {MessagesList? messages,
      CustomStatus? status,
      bool? selectionMode,
      List<int>? selectedMailings,
      MessagesList? mailings,
      int? unreadMailings,
      bool? attachmentsBarRequired,
      List<int>? selected}) {
    return SavedMessagesState(
        controller: this.controller,
        scrollController: this.scrollController,
        messages: messages ?? this.messages,
        status: status ?? this.status,
        selectionMode: selectionMode ?? this.selectionMode,
        selected: selected ?? this.selected,
        positionsListener: this.positionsListener,
        attachmentsBarRequired:
            attachmentsBarRequired ?? this.attachmentsBarRequired,
        mailings: mailings ?? this.mailings,
        selectedMailings: selectedMailings ?? this.selectedMailings,
        unreadMailings: unreadMailings ?? this.unreadMailings);
  }
}

class SavedMessagesBloc extends Cubit<SavedMessagesState> {
  SavedMessagesBloc(
      {required this.chatListBloc, required this.chatListPageCubit})
      : super(SavedMessagesState(
            controller: TextEditingController(),
            scrollController: ItemScrollController(),
            positionsListener: ItemPositionsListener.create(),
            messages: MessagesList(const []),
            status: CustomStatus.Initial,
            selectionMode: false,
            attachmentsBarRequired: false,
            mailings: MessagesList(const []),
            selectedMailings: const [],
            selected: const [])) {
    init();
  }

  Box<MessagesList>? _box;
  final ChatListBloc chatListBloc;
  final ChatListPageCubit chatListPageCubit;

  Timer? _timer;

  bool _tryLoadPrevious = true;
  bool _isListening = false;

  void _positionsListener() {
    final pos = state.positionsListener.itemPositions.value;
    if (pos.isNotEmpty &&
        pos.last.index > state.mailings.messages.length - 5 &&
        state.status != CustomStatus.Loading) loadPrevious();
  }

  Future<void> init({bool needToCheckHive = true}) async {
    if (!_isListening) {
      state.positionsListener.itemPositions.addListener(_positionsListener);
      _isListening = true;
    }

    if (needToCheckHive) {
      if (_box == null) {
        _box = await Hive.openBox(SavedMessagesState.SAVED_MESSAGES_FILE);
      }
      final savedMessagesFromHive =
          _box?.get(SavedMessagesState.SAVED_MESSAGES_LIST);
      final mailingsFromHive = _box?.get(SavedMessagesState.MAILING_LIST);
      emit(state.copyWith(
          messages: savedMessagesFromHive,
          mailings: mailingsFromHive,
          status: CustomStatus.Loading));
    }
    final mailingsResponse =
        await ChatRepository().fetchSavedMessages(fetchMailings: true);
    final fetchedMailings = MessagesList(mailingsResponse);
    print('mailings ${fetchedMailings.messages.length}');
    if (fetchedMailings.messages.isNotEmpty) {
      final nList = state.mailings..sync(fetchedMailings.messages, false);
      _box?.put(SavedMessagesState.MAILING_LIST, nList);
    }

    emit(state.copyWith(
        mailings: fetchedMailings, status: CustomStatus.Success));

    final fetchedMessages =
        MessagesList(await ChatRepository().fetchSavedMessages());
    if (_box?.isOpen ?? false) {
      _box?.put(SavedMessagesState.SAVED_MESSAGES_LIST, fetchedMessages);
      print('saved ${fetchedMessages.messages.length}');
    }
    emit(state.copyWith(
        messages: fetchedMessages, status: CustomStatus.Success));

    final unreadMailings =
        await ChatRepository().countUnreadMessages(isMailings: true);
    emit(state.copyWith(
        unreadMailings: unreadMailings == 'false'
            ? 0
            // ignore: deprecated_member_use
            : int.parse(unreadMailings, onError: (_) => 0)));

    if (_timer == null) {
      _timer = Timer.periodic(
          Duration(seconds: 30), (timer) => init(needToCheckHive: false));
    }
  }

  void loadPrevious() async {
    if (_tryLoadPrevious) {
      final lid = state.mailings.messages.first.id;
      emit(state.copyWith(status: CustomStatus.Loading));
      final messages = await ChatRepository()
          .fetchSavedMessages(fetchMailings: true, lid: lid.toString());
      if (messages.isNotEmpty) {
        final mList = state.mailings..sync(messages, true);
        _box?.put(SavedMessagesState.MAILING_LIST, mList);

        emit(state.copyWith(mailings: mList));
      } else
        _tryLoadPrevious = false;
    }
    emit(state.copyWith(status: CustomStatus.Success));
  }

  @override
  Future<void> close() async {
    _box?.close();
    _timer?.cancel();
    state.controller.dispose();
    state.positionsListener.itemPositions.removeListener(_positionsListener);
    return super.close();
  }

  Future<bool> readMailings() async {
    emit(state.copyWith(unreadMailings: 0));
    final a = await ChatRepository().readChatMessages(isMailing: true);
    init(needToCheckHive: false);
    return a;
  }

  void jumpToUnread() {
    if (state.firstUnreadIdx > 0)
      state.scrollController.jumpTo(index: state.firstUnreadIdx);
  }

  void selectMessage(int idx) {
    if (state.selected.contains(idx)) {
      final nSelected = state.selected..remove(idx);
      emit(state.copyWith(
          selected: nSelected, selectionMode: nSelected.isNotEmpty));
    } else
      emit(state
          .copyWith(selected: [...state.selected, idx], selectionMode: true));
  }

  void disselectAllMessages() {
    emit(state.copyWith(selected: [], selectionMode: false));
  }

  Future<bool> unsaveMessages() async {
    final response = await Future.wait(state.selected
        .map((mid) => ChatRepository().unsaveMessage(mid: mid.toString())));
    emit(state.copyWith(selected: const []));
    await init(needToCheckHive: false);
    return !response.contains(false);
  }

  void saveMessage() {
    final message = state.messages.messages
        .firstWhere((element) => element.id == state.selected.first);
    Clipboard.setData(ClipboardData(text: message.text));
  }

  void repostMessages() {
    chatListBloc.addMessagesForForwarding(
        messages: state.messages.messages
            .where((msg) => state.selected.contains(msg.id))
            .toList(),
        chatId: '0',
        isFromGroupChat: false,
        fromSaves: true);
    disselectAllMessages();
    chatListPageCubit.selectPage(ChatListPage.Chats);
  }

  Future<void> sendMailing(
      {List<File>? photos, List<File>? attachments}) async {
    final body = state.controller.text;
    await ChatRepository()
        .sendMailing(body: body, attachments: attachments, photos: photos);
    init(needToCheckHive: false);
    state.controller.text = '';
  }

  void toggleAttachments() {
    emit(state.copyWith(attachmentsBarRequired: !state.attachmentsBarRequired));
  }
}
