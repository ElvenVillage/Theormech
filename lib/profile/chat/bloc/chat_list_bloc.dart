import 'dart:async';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:teormech/auth/bloc/auth_bloc.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_list_page_cubit.dart';
import 'package:teormech/profile/chat/model/chat_list_state.dart';
import 'package:teormech/profile/chat/model/chat_model.dart';
import 'package:teormech/profile/chat/model/message_model.dart';
import 'package:teormech/profile/chat/rep/chat_rep.dart';
import 'package:teormech/profile/chat/rep/group_chat_rep.dart';
import 'package:teormech/profile/chat/ui/pages/chat_screen.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';
import 'package:teormech/profile/subscription/model/followers_state.dart';

typedef AfterLoadingCallback = bool Function(List<Chat> a);

class ChatListBloc extends Cubit<ChatListState> {
  ChatListBloc(
      {required this.pageCubit,
      required this.bottomBarBloc,
      required this.clearMetadataCallback})
      : super(ChatListState.empty()) {
    _init();
    _initPushNotifications();
  }

  final ChatListPageCubit pageCubit;
  final BottomBarBloc bottomBarBloc;

  final VoidCallback clearMetadataCallback;

  Box<Chat>? _chats;
  late final Timer _updateTimer;

  bool _prohibitUpdating = false;

  StreamSubscription<RemoteMessage>? _pushSubscription;
  StreamSubscription<RemoteMessage>? _backgroundPushSubscription;

  @override
  Future<void> close() {
    _chats?.close();
    _updateTimer.cancel();
    _pushSubscription?.cancel();
    _backgroundPushSubscription?.cancel();
    return super.close();
  }

  void authBlocListener(Authorized event) async {
    if (event.navigationToken?.type == TokenType.JoinChatToken) {
      emit(state.copyWith(status: FetchingStatus.InProgress));
      final chatId = await ChatRepository()
          .joinChatByLink(token: event.navigationToken!.token);
      print(chatId);

      if (chatId != 'false') {
        await load(
            loadFromHive: false,
            init: true,
            updateOnlyGroupChats: true,
            loadUnreadCount: false,
            callback: (chats) {
              final chat = chats.where(
                (e) => chatId == e.id && e.isGroupChat,
              );
              if (chat.isNotEmpty) {
                bottomBarBloc.go(ChatScreenWrapper.route(chat: chat.first),
                    hideBottombar: true);
                Future.delayed(Duration.zero, clearMetadataCallback);
                return true;
              } else {
                Future.delayed(Duration.zero, clearMetadataCallback);
                return false;
              }
            });
      }
    }
    if (event.initialMessage != null) {
      _handleNotificationTap(event.initialMessage!);
    }
  }

  void _handleNotificationTap(RemoteMessage message) async {
    if (message.data['type'] == 'chat') {
      //bottomBarBloc.setTab(BottomBars.Chat);
      final isGroupChat = message.data['uid'] == null;
      final id = isGroupChat ? message.data['cid'] : message.data['uid'];
      await load(
          init: true,
          loadFromHive: false,
          loadUnreadCount: false,
          updateOnlyGroupChats: isGroupChat,
          updateOnlyPersonalChats: !isGroupChat,
          callback: (chats) {
            final chat = isGroupChat
                ? chats.where((e) => e.isGroupChat && e.id == id)
                : chats.where((e) => !e.isGroupChat && e.id == id);
            if (chat.isNotEmpty) {
              bottomBarBloc.go(ChatScreenWrapper.route(chat: chat.first),
                  hideBottombar: true);
              return true;
            } else {
              return false;
            }
          });

      clearMetadataCallback();
    }
  }

  void _remoteMessageHandler(RemoteMessage msg) async {
    final isGroupChat = msg.data['cid'] != null;
    print(msg.data);
    await load(
        init: false,
        loadFromHive: false,
        updateOnlyGroupChats: isGroupChat,
        updateOnlyPersonalChats: !isGroupChat,
        loadUnreadCount: true);
  }

  void _initPushNotifications() async {
    _backgroundPushSubscription =
        FirebaseMessaging.onMessageOpenedApp.listen(_remoteMessageHandler);
    _pushSubscription =
        FirebaseMessaging.onMessage.listen(_remoteMessageHandler);
  }

  void _init() {
    _updateTimer = Timer.periodic(
        kIsWeb ? Duration(seconds: 15) : Duration(minutes: 2),
        (timer) => load(loadFromHive: false));
  }

  void _save(List<Chat> chats) async {
    if (_chats?.isOpen ?? false) {
      final keys = <String>[];
      for (var chat in chats) {
        final key = chat.isGroupChat ? ('g' + chat.id) : chat.id;
        keys.add(key);

        await _chats?.put(key, chat);
      }

      final savedChatsKeys = _chats?.keys ?? [];
      for (var key in savedChatsKeys)
        if (!keys.contains(key)) await _chats?.delete(key);
    }
  }

  void readChat(Chat chat) {
    emit(state.copyWith(chats: [
      ...state.chatList.map((e) =>
          e.id == chat.id && e.isGroupChat == chat.isGroupChat
              ? (e..unreadMessages = 0)
              : e)
    ]));
  }

  Future<void> load(
      {bool loadFromHive = true,
      bool loadFromNetwork = true,
      bool init = false,
      bool updateOnlyGroupChats = false,
      bool updateOnlyPersonalChats = false,
      AfterLoadingCallback? callback,
      bool loadUnreadCount = true}) async {
    if (_prohibitUpdating && !init) return;

    bool callbackWasExecuted = false;

    if (callback != null) {
      callbackWasExecuted = callback(state.chatList);
    }

    if (init) emit(state.copyWith(status: FetchingStatus.InProgress));

    if (loadFromHive) {
      if (_chats == null) {
        _chats = await Hive.openBox('chats');
      }

      final hiveChats = <Chat>[];
      for (var chat in _chats?.values ?? Iterable.empty()) {
        hiveChats.add(chat);
      }

      hiveChats
        ..sort((chatA, chatB) =>
            chatB.messageDateParsed.compareTo(chatA.messageDateParsed));

      emit(state.copyWith(chats: hiveChats));
    }

    var fetchedChats = <Chat>[];
    var fetchedGroupChats = <Chat>[];
    var errorOccured = false;
    if (loadFromNetwork) {
      if (!updateOnlyGroupChats) {
        try {
          fetchedChats = await ChatRepository().fetchChats();
          if (callback != null && !callbackWasExecuted) {
            callbackWasExecuted = callback(fetchedChats);
          }
        } catch (e) {
          emit(state.copyWith(status: FetchingStatus.Error));
          errorOccured = true;
        }
      }
      if (!updateOnlyPersonalChats) {
        try {
          fetchedGroupChats = await GroupChatsRepository().fetchChats();
          if (callback != null && !callbackWasExecuted) {
            callbackWasExecuted = callback(fetchedGroupChats);
          }
        } catch (e) {
          emit(state.copyWith(status: FetchingStatus.Error));
          errorOccured = true;
        }
      }

      if (!errorOccured) {
        emit(state.copyWith(status: FetchingStatus.Loaded));

        return await _loadUnreadMessagesCount(fetchedChats, fetchedGroupChats,
            loadUnreadCount: loadUnreadCount,
            onlyGroupChats: updateOnlyGroupChats,
            onlyPersonalChats: updateOnlyPersonalChats);
      }
    }
  }

  Future<void> _loadUnreadMessagesCount(List<Chat> chats, List<Chat> groupChats,
      {bool loadUnreadCount = true,
      bool onlyGroupChats = false,
      bool onlyPersonalChats = false}) async {
    final nChats = <Chat>[];
    final nGroupChats = <Chat>[];

    final prevChats = state.chatList.where((c) => !c.isGroupChat).toList();
    final prevGroupChats = state.chatList.where((c) => c.isGroupChat).toList();

    for (var chat in [...chats, ...groupChats]) {
      final countOfUnread = loadUnreadCount
          ? await ChatRepository().countUnreadMessages(
              chatId: chat.id, isGroupChat: chat.isGroupChat)
          : chat.unreadMessages.toString();
      try {
        final count = int.parse(countOfUnread);
        final wasNotRead = (chat.senderId == ChatRepository().credentials.id &&
            !chat.isGroupChat &&
            !chat.wasRead);

        chat.unreadMessages = wasNotRead ? -1 : count;
      } catch (e) {
        chat.unreadMessages = 0;
      } finally {
        if (chat.isGroupChat)
          nGroupChats.add(chat);
        else
          nChats.add(chat);
      }
    }
    final emittedChats = (onlyPersonalChats)
        ? [...nChats, ...prevGroupChats]
        : (onlyGroupChats)
            ? [...prevChats, ...nGroupChats]
            : [...nChats, ...nGroupChats];
    emit(
      state.copyWith(chats: emittedChats, status: FetchingStatus.Loaded),
    );
    _save(emittedChats);
  }

  void addNewsfeedObjectForForwarding(NewsfeedObject newsfeedObject) {
    emit(state.copyWith(newsfeedObjectsForForwarding: [
      ...state.newsfeedObjectsForForwarding,
      newsfeedObject
    ]));
  }

  void addMessagesForForwarding(
      {required List<Message> messages,
      required String chatId,
      required bool isFromGroupChat,
      required bool fromSaves}) {
    messages.forEach((msg) {
      msg.chatId = chatId;
      msg.isFromSaves = fromSaves;
      msg.isFromGroupChat = isFromGroupChat;
    });
    final nMessages = [...state.messagesForForwarding, ...messages];
    emit(state.copyWith(messagesForForwarding: nMessages));
  }

  void addPhotosForForwarding({required List<File> files}) {
    final nPhotos = [...state.photosForForwarding, ...files];
    emit(state.copyWith(photosForForwarding: nPhotos));
  }

  void removeMessageFromForwarding({required Message message}) {
    var nMessages = state.messagesForForwarding
        .where((msg) => msg.id != message.id || msg.chatId != message.chatId)
        .toList();
    emit(state.copyWith(messagesForForwarding: nMessages));
  }

  void forwardMessages() {
    emit(state.copyWith(
        messagesForForwarding: const [],
        photosForForwarding: const [],
        newsfeedObjectsForForwarding: const []));
  }

  void query(String query) async {
    if (query.trim().isEmpty) {
      _prohibitUpdating = false;
      load(loadFromNetwork: false);
      return;
    }
    if (pageCubit.state.page == ChatListPage.Chats) {
      _prohibitUpdating = true;
      emit(state.copyWith(status: FetchingStatus.InProgress));
      final fetchedChats = await ChatRepository().fetchChats(query: query);
      emit(state.copyWith(chats: fetchedChats, status: FetchingStatus.Loaded));
    }
  }
}
