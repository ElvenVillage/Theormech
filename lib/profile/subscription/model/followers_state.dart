import 'package:flutter/cupertino.dart';
import 'package:hive/hive.dart';
import 'package:teormech/profile/bloc/online_status.dart';

part 'followers_state.g.dart';

@HiveType(typeId: 1)
class Follower extends HiveObject {
  @HiveField(0)
  final String name;

  @HiveField(1)
  final String surname;

  @HiveField(2)
  final String? avatar;

  @HiveField(3)
  final String? lastOnline;

  @HiveField(4)
  final String id;

  @HiveField(5)
  final String sex;

  late final DateTime? _lastOnline;

  bool get online {
    if (_lastOnline == null) return false;
    var diff = (-1) * _lastOnline!.difference(DateTime.now()).inMinutes;
    return diff < 5;
  }

  String get onlineString {
    if (!online) {
      return OnlineStatus.onlineString(
          lastOnline: _lastOnline, sex: sex, now: DateTime.now());
    } else
      return '';
  }

  factory Follower.fromJson(Map<String, dynamic> json) {
    return Follower(
        id: json['id'],
        name: json['name'],
        avatar: json['photo'],
        lastOnline: json['last_online'],
        sex: json['sex'],
        surname: json['surname']);
  }

  Follower(
      {required this.name,
      required this.surname,
      required this.avatar,
      required this.lastOnline,
      required this.sex,
      required this.id}) {
    if (lastOnline == null || lastOnline == '0000-00-00 00:00:00')
      this._lastOnline = null;
    else
      this._lastOnline = DateTime.tryParse(lastOnline!);
  }
}

@HiveType(typeId: 2)
class Followers extends HiveObject {
  @HiveField(0)
  final List<Follower> dataFollowers;

  Followers({required this.dataFollowers});
}

enum FollowerScreenState { Followers, Followings, Recommendations }
enum FetchingStatus { Loaded, InProgress, Initial, Error }

class FollowersState {
  final List<Follower> baseFollowings;
  final List<Follower> baseFollowers;
  final List<Follower> baseRecommendations;

  final FollowerScreenState state;

  List<Follower> get followings => _filter(baseFollowings);
  List<Follower> get followers => _filter(baseFollowers);
  List<Follower> get recommendations => _filter(baseRecommendations);

  List<Follower> _filter(List<Follower> list) {
    return list.where((user) {
      final goodByOnline = !filterByOnline || user.online;
      final goodByGender = !filterByGender || user.sex == filteredGender;
      return goodByOnline && goodByGender;
    }).toList();
  }

  final bool filterByOnline;
  final String filteredGender;
  final bool filterByGender;

  final FetchingStatus status;

  final TextEditingController followingsQueryController;
  final TextEditingController followersQueryController;
  final TextEditingController recommendationsQueryController;

  bool isInFollowings({required String uid}) {
    for (var i in baseFollowings) {
      if (i.id == uid) return true;
    }
    return false;
  }

  FollowersState({
    required this.state,
    required this.baseFollowers,
    required this.baseRecommendations,
    required this.baseFollowings,
    required this.followersQueryController,
    required this.followingsQueryController,
    required this.recommendationsQueryController,
    this.filterByGender = false,
    this.status = FetchingStatus.InProgress,
    this.filteredGender = '0',
    this.filterByOnline = false,
  });

  FollowersState copyWith(
      {FollowerScreenState? state,
      List<Follower>? followers,
      List<Follower>? followings,
      List<Follower>? recommendations,
      FetchingStatus? status,
      String? filteredGender,
      bool? filterByOnline,
      bool? filterByGender}) {
    return FollowersState(
        state: state ?? this.state,
        baseFollowers: followers ?? this.baseFollowers,
        baseFollowings: followings ?? this.baseFollowings,
        baseRecommendations: recommendations ?? this.baseRecommendations,
        status: status ?? this.status,
        filteredGender: filteredGender ?? this.filteredGender,
        filterByGender: filterByGender ?? this.filterByGender,
        filterByOnline: filterByOnline ?? this.filterByOnline,
        followersQueryController: followersQueryController,
        followingsQueryController: followingsQueryController,
        recommendationsQueryController: recommendationsQueryController);
  }

  factory FollowersState.empty() {
    return FollowersState(
        baseFollowings: [],
        baseFollowers: [],
        baseRecommendations: [],
        state: FollowerScreenState.Followings,
        status: FetchingStatus.Initial,
        filterByOnline: false,
        filterByGender: false,
        filteredGender: '0',
        followersQueryController: TextEditingController(),
        followingsQueryController: TextEditingController(),
        recommendationsQueryController: TextEditingController());
  }
}
