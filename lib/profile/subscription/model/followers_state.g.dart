// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'followers_state.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class FollowerAdapter extends TypeAdapter<Follower> {
  @override
  final int typeId = 1;

  @override
  Follower read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Follower(
      name: fields[0] as String,
      surname: fields[1] as String,
      avatar: fields[2] as String?,
      lastOnline: fields[3] as String?,
      sex: fields[5] as String,
      id: fields[4] as String,
    );
  }

  @override
  void write(BinaryWriter writer, Follower obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.surname)
      ..writeByte(2)
      ..write(obj.avatar)
      ..writeByte(3)
      ..write(obj.lastOnline)
      ..writeByte(4)
      ..write(obj.id)
      ..writeByte(5)
      ..write(obj.sex);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FollowerAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class FollowersAdapter extends TypeAdapter<Followers> {
  @override
  final int typeId = 2;

  @override
  Followers read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Followers(
      dataFollowers: (fields[0] as List).cast<Follower>(),
    );
  }

  @override
  void write(BinaryWriter writer, Followers obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.dataFollowers);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FollowersAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
