import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:hive/hive.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/subscription/model/followers_state.dart';
import 'package:teormech/profile/profile/rep/user_profile_rep.dart';
import 'package:teormech/profile/subscription/rep/subscription_rep.dart';

enum FilterByGender { NoFilter, Male, Female }

enum FilterByOnline { NoFilter, Online }

typedef SearchFunction = Future<void> Function({String query});

class FollowersCubit extends Cubit<FollowersState> {
  Box<Followers>? _box;
  Timer? _debounce;

  bool _followersInit = false;
  bool _recommendationsInit = false;

  final BottomBarBloc bottomBarBloc;

  static const BOX_KEY = 'followers';
  static const FOLLOWERS_KEY = 'root';
  static const FOLLOWINGS_KEY = 'followings';
  static const SEARCH_RESULTS_KEY = 'root_users';

  @override
  Future<void> close() async {
    _debounce?.cancel();
    return super.close();
  }

  _onSearchChanged(String query, SearchFunction callback) {
    if (_debounce?.isActive ?? false) _debounce?.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      callback(query: query);
    });
  }

  FollowersCubit({required this.bottomBarBloc})
      : super(FollowersState.empty()) {
    initFollowings();
    state.followersQueryController.addListener(() {
      _onSearchChanged(
          state.followersQueryController.text.trim(), updateFollowers);
    });
    state.followingsQueryController.addListener(() {
      _onSearchChanged(
          state.followingsQueryController.text.trim(), updateFollowings);
    });
    state.recommendationsQueryController.addListener(() {
      _onSearchChanged(
          state.recommendationsQueryController.text.trim(), updateSearches);
    });
  }

  void selectFollowings() {
    if (state.state == FollowerScreenState.Followings) return;
    emit(state.copyWith(
      state: FollowerScreenState.Followings,
    ));
  }

  void selectFollowers() {
    if (state.state == FollowerScreenState.Followers) return;
    if (!_followersInit) {
      initFollowers();
    }
    emit(state.copyWith(
      state: FollowerScreenState.Followers,
    ));
  }

  void selectRecommendations() {
    if (state.state == FollowerScreenState.Recommendations) return;
    if (!_recommendationsInit) {
      updateSearches();
    }
    emit(state.copyWith(
      state: FollowerScreenState.Recommendations,
    ));
  }

  Future<void> initFollowings() async {
    if (_box == null) _box = await Hive.openBox<Followers>(BOX_KEY);

    await loadFollowingsFromHive();
    await updateFollowings();
  }

  Future<void> initFollowers() async {
    if (_box == null) _box = await Hive.openBox<Followers>(BOX_KEY);
    _followersInit = true;
    await loadFollowersFromHive();
    await updateFollowers();
  }

  Future<void> loadFollowingsFromHive() async {
    final followings = _box?.get(FOLLOWINGS_KEY);
    if (followings != null) {
      emit(state.copyWith(followings: followings.dataFollowers));
    }
  }

  Future<void> loadFollowersFromHive() async {
    final followers = _box?.get(FOLLOWERS_KEY);
    if (followers != null) {
      emit(state.copyWith(followers: followers.dataFollowers));
    }
  }

  Future<void> follow({required String id, bool unsub = false}) async {
    var nFollowers = state.baseFollowings;
    if (unsub)
      nFollowers.removeWhere((sub) => sub.id == id);
    else
      nFollowers = [
        ...nFollowers,
        Follower(
            id: id, surname: '', name: '', avatar: '', lastOnline: '', sex: '1')
      ];
    emit(state.copyWith(followings: nFollowers));

    await UserProfileRepository().followUser(uid: id, unsub: unsub);

    updateFollowings().then((_) => updateSearches());
    updateFollowers();

    bottomBarBloc.toggleUpdateProfile(true);
  }

  Future<void> updateFollowings({String query = ''}) async {
    emit(state.copyWith(status: FetchingStatus.InProgress));

    final followings = await SubscriptionsRepository()
        .searchUsers(query: query, mode: SearchWithin.Followings);

    if (followings.isNotEmpty && query.isEmpty) {
      _box?.put(FOLLOWINGS_KEY, Followers(dataFollowers: followings));
    }
    print("${followings.length} followings");
    emit(state.copyWith(status: FetchingStatus.Loaded, followings: followings));
  }

  Future<void> updateSearches({String query = ''}) async {
    emit(state.copyWith(status: FetchingStatus.InProgress));

    _recommendationsInit = true;

    final searchResults = await SubscriptionsRepository()
        .searchUsers(query: query, mode: SearchWithin.Other);

    emit(state.copyWith(
        status: FetchingStatus.Loaded, recommendations: searchResults));
  }

  Future<void> updateFollowers({String query = ''}) async {
    emit(state.copyWith(status: FetchingStatus.InProgress));

    final followers = await SubscriptionsRepository()
        .searchUsers(mode: SearchWithin.Followers, query: query);

    if (followers.isNotEmpty && query.isEmpty) {
      _box?.put(FOLLOWERS_KEY, Followers(dataFollowers: followers));
    }

    emit(state.copyWith(
      followers: followers,
      status: FetchingStatus.Loaded,
    ));
  }

  void filterByOnline(bool filter) {
    emit(state.copyWith(filterByOnline: filter));
  }

  void filterByGender(String gender) {
    if (!state.filterByGender) {
      emit(state.copyWith(filterByGender: true, filteredGender: gender));
    } else {
      if (state.filteredGender == gender) {
        emit(state.copyWith(filterByGender: false, filteredGender: gender));
      } else {
        emit(state.copyWith(filterByGender: true, filteredGender: gender));
      }
    }
  }
}
