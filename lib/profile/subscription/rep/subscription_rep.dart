import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/profile/subscription/model/followers_state.dart';
import 'package:teormech/profile/profile/rep/user_profile_rep.dart';

enum SearchWithin { Followings, Followers, Other, All }

class SubscriptionsRepository {
  static SubscriptionsRepository? _instance;
  late final Dio _dio;
  
  SubscriptionsRepository._internal() {
    _dio = Dio()..options.contentType = Headers.formUrlEncodedContentType;
  }

  factory SubscriptionsRepository() {
    if (_instance == null) {
      _instance = SubscriptionsRepository._internal();
    }
    return _instance!;
  }

  Future<List<Follower>> searchUsers(
      {required String query, required SearchWithin mode}) async {
    final data = UserProfileRepository().credentials.toJson();
    data['query'] = query;
    switch (mode) {
      case SearchWithin.Followings:
        data['show'] = 'followings';
        break;
      case SearchWithin.Followers:
        data['show'] = 'followers';
        break;
      case SearchWithin.Other:
        data['show'] = 'other';
        break;
      case SearchWithin.All:
        data['show'] = 'all';
        break;
    }

    final response = await _dio.post(SubscriptionsApi.searchUser,
        data: data, options: Options(responseType: ResponseType.plain));
    final responseString = response.data.toString();
    if (responseString.trim() == 'false') {
      return const [];
    }
    final responseJson = jsonDecode(responseString);

    final users = <Follower>[];
    for (var user in responseJson) {
      users.add(Follower.fromJson(user));
    }
    return users;
  }
}
