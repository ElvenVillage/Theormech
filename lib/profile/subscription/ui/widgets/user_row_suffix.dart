import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/generated/l10n.dart';
import 'package:star_menu/star_menu.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/subscription/bloc/followers_bloc.dart';
import 'package:teormech/profile/subscription/model/followers_state.dart';
import 'package:teormech/profile/chat/ui/pages/chat_screen.dart';
import 'package:teormech/widgets/star_menu_item.dart';

abstract class UserRowSuffix {
  static Widget followingSuffix(
      { //required Map<int, ValueKey> btnKeys,
      required double width,
      required int idx,
      required Follower user,
      bool compact = false,
      required BuildContext context}) {
    final button = Padding(
      child: IconButton(
        constraints: compact
            ? null
            : BoxConstraints(maxHeight: width / 15, maxWidth: width / 15),
        padding: EdgeInsets.zero,
        icon: const ImageIcon(
          AssetImage('images/icons/comm.png'),
          color: Colors.grey,
        ),
        onPressed: () {
          context
              .read<BottomBarBloc>()
              .go(ChatScreenWrapper.route(user: user), hideBottombar: true);
        },
      ),
      padding: const EdgeInsets.only(right: 8.0),
    );
    final more = Padding(
        padding: const EdgeInsets.only(right: 8.0),
        child: IconButton(
          //key: btnKeys.putIfAbsent(idx, () => ValueKey(idx)),
          constraints:
              BoxConstraints(maxHeight: width / 15, maxWidth: width / 15),
          padding: EdgeInsets.zero,
          icon: const Icon(
            Icons.more_horiz,
          ),
          onPressed: () {},
        ).addStarMenu(
            context,
            [
              first(width * 0.35),
              generateMenuItem(
                  title: S.of(context).unfollow_from_profile,
                  icon: 'images/icons/unsubscribe.png',
                  width: width * 0.35),
              last(width * 0.35),
            ],
            StarMenuParameters(
              shape: MenuShape.linear,
              onItemTapped: (buttonId, controller) {
                if (buttonId == 1) {
                  context
                      .read<FollowersCubit>()
                      .follow(id: user.id, unsub: true);
                  controller.closeMenu();
                }
              },
              //useScreenCenter: true,
              centerOffset: const Offset(-70, 0),
              backgroundParams: BackgroundParams(),
              linearShapeParams: LinearShapeParams(
                  angle: 270, alignment: LinearAlignment.center),
            )));

    return Padding(
      padding: const EdgeInsets.only(right: 5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [button, if (!compact) more],
      ),
    );
  }

  static Widget followerSuffix(
      {required BuildContext context,
      required Follower user,
      required double width}) {
    return Padding(
      padding: const EdgeInsets.only(right: 5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: IconButton(
              constraints:
                  BoxConstraints(maxHeight: width / 15, maxWidth: width / 15),
              padding: EdgeInsets.zero,
              icon: Icon(Icons.add_circle_rounded),
              color: Colors.lightBlue,
              onPressed: () {
                context.read<FollowersCubit>().follow(id: user.id);
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: IconButton(
                constraints:
                    BoxConstraints(maxHeight: width / 15, maxWidth: width / 15),
                padding: EdgeInsets.zero,
                icon: Icon(Icons.more_horiz),
                onPressed: () {}),
          ),
        ],
      ),
    );
  }
}
