import 'package:flutter/material.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/generated/l10n.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/empty_avatar.dart';
import 'package:teormech/profile/subscription/model/followers_state.dart';
import 'package:teormech/profile/profile/ui/profile_wrapper.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/styles/transitions/slide_fade.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/clipped_avatar.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

class ProfileRow extends StatelessWidget {
  final String? profileAvatar;
  final String surname;
  final String userId;
  final String name;
  final bool online;
  final bool isAdmin;
  final Function? disableCallback;
  final FetchingStatus status;
  final String? caption;
  final Widget? rightSuffix;
  ProfileRow(
      {Key? key,
      required this.profileAvatar,
      required this.name,
      required this.userId,
      required this.surname,
      required this.online,
      this.isAdmin = false,
      this.disableCallback,
      required this.caption,
      this.rightSuffix,
      this.status = FetchingStatus.Initial})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width > 500
        ? 500
        : MediaQuery.of(context).size.width;
    final color =
        (status == FetchingStatus.Loaded) ? Colors.black : Colors.grey;
    var avatar = (profileAvatar != '' && profileAvatar != null)
        ? UserAvatar.fromNetwork(
            size: width / 8,
            key: Key(profileAvatar!),
            online: online,
            avatarUrl: ProfileApi.profileAvatar + profileAvatar!)
        : EmptyAvatar(
            size: width / 8,
            online: online,
            groupChat: false,
          );
    return InkWell(
      child: Container(
        padding: EdgeInsets.only(left: 16, top: 4, bottom: 4),
        child: Row(
          children: [
            avatar,
            Container(
              padding: const EdgeInsets.only(left: 10),
              width: width / 1.8,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(
                        surname + ' ' + name,
                        style: profileTextStyle.copyWith(
                            color: color,
                            fontWeight: FontWeight.w500,
                            fontSize: 14),
                      ),
                      if (isAdmin)
                        Padding(
                          padding: const EdgeInsets.only(left: 5.0),
                          child: Icon(
                            Icons.star,
                            color: Colors.lightBlue,
                            size: 16,
                          ),
                        ),
                    ],
                  ),
                  Padding(
                      padding: EdgeInsets.only(top: 3),
                      child: (online)
                          ? Text(
                              S.of(context).online,
                              style: h2DrawerGreyText.copyWith(
                                  fontSize: 12,
                                  color: (status == FetchingStatus.Loaded)
                                      ? Colors.cyan
                                      : Colors.grey),
                            )
                          : Text(
                              caption!,
                              style: (status == FetchingStatus.Loaded)
                                  ? h2DrawerGreyText.copyWith(
                                      fontSize: 12,
                                    )
                                  : h2DrawerGreyText.copyWith(
                                      color: Colors.grey,
                                      fontSize: 12,
                                    ),
                            ))
                ],
              ),
            ),
            if (rightSuffix != null) Expanded(child: rightSuffix!)
          ],
        ),
      ),
      onTap: () {
        if (disableCallback == null)
          context.read<BottomBarBloc>().go(PageRouteBuilder(
              pageBuilder: (context, f1, f2) => ProfileScreenWrapper.fromLid(
                    lid: userId,
                  ),
              transitionsBuilder: slideTransition));
        else
          disableCallback!();

        FocusScope.of(context).requestFocus(FocusNode());
      },
    );
  }
}
