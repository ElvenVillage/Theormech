import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/generated/l10n.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/subscription/bloc/followers_bloc.dart';
import 'package:teormech/profile/subscription/model/followers_state.dart';
import 'package:teormech/profile/ui/widgets/filter.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/profile/subscription/ui/widgets/profile_row.dart';
import 'package:teormech/profile/ui/widgets/select_app_bar.dart';
import 'package:teormech/profile/subscription/ui/widgets/user_row_suffix.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/styles/transitions/slide_fade.dart';

class SubscriptionScreenWrapper extends StatefulWidget {
  SubscriptionScreenWrapper({this.selectedPeople, this.syncSelectionCallback});

  final List<int>? selectedPeople;
  final SelectingPeopleCallback? syncSelectionCallback;

  @override
  _SubscriptionScreenWrapperState createState() =>
      _SubscriptionScreenWrapperState();
}

class _SubscriptionScreenWrapperState extends State<SubscriptionScreenWrapper> {
  final _pageController = PageController();

  List<int> selectedPeople = [];

  @override
  void initState() {
    if (widget.selectedPeople != null)
      selectedPeople = List<int>.from(widget.selectedPeople!);
    super.initState();
    context.read<FollowersCubit>().selectFollowings();
  }

  void selectPeople(int id) {
    final selected = selectedPeople.contains(id);
    if (!selected) {
      setState(() {
        selectedPeople.add(id);
      });
    } else {
      setState(() {
        selectedPeople.removeWhere((element) => element == id);
      });
    }
    widget.syncSelectionCallback!(selectedPeople);
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return BlocBuilder<FollowersCubit, FollowersState>(
      builder: (context, state) {
        final back = state.status == FetchingStatus.InProgress;
        return Stack(
          children: [
            RefreshIndicator(
                notificationPredicate: (notification) =>
                    notification.depth == 2,
                onRefresh: () async {
                  switch (state.state) {
                    case FollowerScreenState.Followers:
                      context.read<FollowersCubit>().updateFollowers(
                          query: state.followersQueryController.text.trim());
                      break;
                    case FollowerScreenState.Followings:
                      context.read<FollowersCubit>().updateFollowings(
                          query: state.followingsQueryController.text.trim());
                      break;
                    case FollowerScreenState.Recommendations:
                      context.read<FollowersCubit>().updateSearches(
                          query:
                              state.recommendationsQueryController.text.trim());
                  }
                },
                child: NestedScrollView(
                  headerSliverBuilder: (context, innerBoxIsScrolled) => [
                    SliverAppBar(
                      pinned: false,
                      backgroundColor: Colors.white,
                      automaticallyImplyLeading: false,
                      flexibleSpace: SearchAppBar(
                          controller: (state.state ==
                                  FollowerScreenState.Followers)
                              ? state.followersQueryController
                              : state.state == FollowerScreenState.Followings
                                  ? state.followingsQueryController
                                  : state.recommendationsQueryController,
                          button: widget.selectedPeople != null
                              ? Container(
                                  child: Text(
                                    'Выбрано: ${selectedPeople.length}',
                                    textAlign: TextAlign.center,
                                  ),
                                )
                              : FilterWidget()),
                    ),
                    SliverAppBar(
                      pinned: true,
                      backgroundColor: Colors.white,
                      automaticallyImplyLeading: false,
                      flexibleSpace: SelectAppBar(
                        status: state.state,
                        controller: _pageController,
                      ),
                    )
                  ],
                  body: PageView(
                    controller: _pageController,
                    physics: const NeverScrollableScrollPhysics(),
                    children: [
                      CustomScrollView(
                        slivers: [
                          SliverList(
                              delegate:
                                  SliverChildBuilderDelegate((context, idx) {
                            var user = state.followings[idx];
                            final isSelected =
                                selectedPeople.contains(int.parse(user.id));
                            return Container(
                              decoration: widget.selectedPeople != null
                                  ? isSelected
                                      ? BoxDecoration(
                                          gradient: LinearGradient(colors: [
                                          Colors.blue.withAlpha(50),
                                          Colors.white.withAlpha(50)
                                        ]))
                                      : null
                                  : null,
                              child: ProfileRow(
                                  key: Key(user.id),
                                  name: user.name,
                                  online: user.online,
                                  profileAvatar: user.avatar,
                                  surname: user.surname,
                                  disableCallback: widget.selectedPeople != null
                                      ? () {
                                          selectPeople(int.parse(user.id));
                                        }
                                      : null,
                                  userId: user.id,
                                  caption: user.onlineString,
                                  status: state.status,
                                  rightSuffix: widget.selectedPeople != null
                                      ? null
                                      : UserRowSuffix.followingSuffix(
                                          //btnKeys: _btnFollowingsKeys,
                                          width: width,
                                          idx: idx,
                                          user: user,
                                          context: context)),
                            );
                          }, childCount: state.followings.length)),
                        ],
                      ),
                      CustomScrollView(
                        slivers: [
                          SliverList(
                            delegate:
                                SliverChildBuilderDelegate((context, idx) {
                              var user = state.followers[idx];
                              final isSelected =
                                  selectedPeople.contains(int.parse(user.id));
                              return Container(
                                decoration: widget.selectedPeople != null
                                    ? isSelected
                                        ? BoxDecoration(
                                            gradient: LinearGradient(colors: [
                                            Colors.blue.withAlpha(50),
                                            Colors.white.withAlpha(50)
                                          ]))
                                        : null
                                    : null,
                                child: ProfileRow(
                                    name: user.name,
                                    online: user.online,
                                    surname: user.surname,
                                    profileAvatar: user.avatar,
                                    userId: user.id,
                                    disableCallback: widget.selectedPeople !=
                                            null
                                        ? () {
                                            selectPeople(int.parse(user.id));
                                          }
                                        : null,
                                    caption: user.onlineString,
                                    status: state.status,
                                    rightSuffix: widget.selectedPeople != null
                                        ? null
                                        : (state.isInFollowings(uid: user.id))
                                            ? UserRowSuffix.followingSuffix(
                                                //btnKeys: _btnFollowersKeys,
                                                width: width,
                                                idx: idx,
                                                compact: true,
                                                user: user,
                                                context: context)
                                            : UserRowSuffix.followerSuffix(
                                                width: width,
                                                context: context,
                                                user: user)),
                              );
                            }, childCount: state.followers.length),
                          )
                        ],
                      ),
                      CustomScrollView(slivers: [
                        SliverList(
                          delegate: SliverChildBuilderDelegate((context, idx) {
                            var user = state.recommendations[idx];
                            final isSelected =
                                selectedPeople.contains(int.parse(user.id));
                            return Container(
                              decoration: widget.selectedPeople != null
                                  ? isSelected
                                      ? BoxDecoration(
                                          gradient: LinearGradient(colors: [
                                          Colors.blue.withAlpha(50),
                                          Colors.white.withAlpha(50)
                                        ]))
                                      : null
                                  : null,
                              child: ProfileRow(
                                  name: user.name,
                                  surname: user.surname,
                                  userId: user.id,
                                  disableCallback: widget.selectedPeople != null
                                      ? () {
                                          selectPeople(int.parse(user.id));
                                        }
                                      : null,
                                  online: user.online,
                                  profileAvatar: user.avatar,
                                  caption: user.onlineString,
                                  status: state.status,
                                  rightSuffix: widget.selectedPeople != null
                                      ? null
                                      : UserRowSuffix.followerSuffix(
                                          width: width,
                                          context: context,
                                          user: user)),
                            );
                          }, childCount: state.recommendations.length),
                        )
                      ]),
                    ],
                  ),
                )),
            if (back)
              Positioned.fill(
                  child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                child: Container(
                  color: GradientColors.updatingIndicatorColor,
                ),
              )),
            if (back)
              Positioned.fromRect(
                rect: Rect.fromLTWH(width / 3, width / 3, width / 3, width / 3),
                child: CircularProgressIndicator(),
              ),
          ],
        );
      },
    );
  }
}

typedef SelectingPeopleCallback = void Function(List<int> selected);

class SubscriptionsScreen extends StatefulWidget {
  SubscriptionsScreen({this.preselectedPeople, this.selectingPeopleCallback});

  final List<int>? preselectedPeople;
  final SelectingPeopleCallback? selectingPeopleCallback;

  static PageRouteBuilder route(
      {List<int>? preselectedPeople,
      SelectingPeopleCallback? selectingPeopleCallback}) {
    return PageRouteBuilder(
        pageBuilder: (context, f1, f2) => SubscriptionsScreen(
              preselectedPeople: preselectedPeople,
              selectingPeopleCallback: selectingPeopleCallback,
            ),
        transitionsBuilder: slideTransition);
  }

  @override
  _SubscriptionsScreenState createState() => _SubscriptionsScreenState();
}

class _SubscriptionsScreenState extends State<SubscriptionsScreen>
    with AutomaticKeepAliveClientMixin {
  List<int> selectedPeople = [];

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        FocusScope.of(context).unfocus();
        context.read<BottomBarBloc>().nullController();
      },
      child: SafeArea(
        child: Scaffold(
          appBar: ProfileAppBar(
            leftAlignedTitle: true,
            rightButton: widget.preselectedPeople != null
                ? IconButton(
                    icon: ImageIcon(AssetImage('images/icons/ok.png')),
                    color: Colors.white,
                    onPressed: () {
                      widget.selectingPeopleCallback!(selectedPeople);
                      context.read<BottomBarBloc>().pop();
                    })
                : null,
            rightmostButton: widget.preselectedPeople != null
                ? IconButton(
                    color: Colors.white,
                    onPressed: () {
                      context.read<BottomBarBloc>().pop();
                    },
                    icon: ImageIcon(AssetImage('images/icons/krestik.png')),
                  )
                : null,
            title: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                    widget.preselectedPeople != null
                        ? "Участники"
                        : S.of(context).subscriptions,
                    style: appBarTextStyle),
              ],
            ),
          ),
          backgroundColor: Colors.transparent,
          body: SubscriptionScreenWrapper(
            selectedPeople: widget.preselectedPeople,
            syncSelectionCallback: (list) => selectedPeople = list,
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
