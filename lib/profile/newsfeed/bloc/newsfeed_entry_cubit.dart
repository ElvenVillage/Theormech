import 'package:bloc/bloc.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_state.dart';
import 'package:teormech/profile/profile/rep/user_portfolio_rep.dart';

class NewsfeedEntryCubit extends Cubit<NewsfeedSearchEntry> {
  NewsfeedEntryCubit(
    NewsfeedSearchEntry initialState,
  ) : super(initialState) {
    _init();
  }

  void _init() {
    UserPortfolioRepository()
        .fetchUserTags(uid: state.profile.lid)
        .then((tags) {
      final portfolioWithTags = state.portfolio.copyWith(tags: tags);
      emit(NewsfeedSearchEntry(
          profile: state.profile, portfolio: portfolioWithTags));
    });

    UserPortfolioRepository()
        .fetchUserPortfolioAchievements(uid: state.profile.lid)
        .then((achievements) {
      final portfolioWithAchievements =
          state.portfolio.copyWith(achievements: achievements);
      emit(NewsfeedSearchEntry(
          profile: state.profile, portfolio: portfolioWithAchievements));
    });
  }
}
