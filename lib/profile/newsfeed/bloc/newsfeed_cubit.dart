import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_state.dart';
import 'package:teormech/profile/newsfeed/rep/newsfeed_repository.dart';
import 'package:teormech/profile/newsfeed/ui/newsfeed_screen.dart';
import 'package:teormech/profile/profile/model/portfolio_state.dart';
import 'package:teormech/profile/profile/rep/user_profile_rep.dart';

class NewsfeedCubit extends Cubit<NewsfeedState> {
  NewsfeedCubit() : super(NewsfeedState.empty());

  Future<void> init(NewsfeedPageEnum page, {bool forceReload = false}) async {
    switch (page) {
      case NewsfeedPageEnum.News:
        break;
      case NewsfeedPageEnum.Events:
        if (state.events == null || forceReload) {
          final fetchedEvents = await _filterEvents(state.eventTags);
          emit(state.copyWith(
              events: fetchedEvents, filteredEvents: fetchedEvents));
        }
        break;
      case NewsfeedPageEnum.Portfolio:
        if (state.portfolios == null || forceReload) {
          final fetchedPortfolios =
              await NewsfeedRepository().fetchPortfolios();
          if (fetchedPortfolios.error == null) {
            emit(state.copyWith(
                portfolios: fetchedPortfolios.entries, syncing: false));
          }
        }
        break;
      case NewsfeedPageEnum.Projects:
        if (state.projects == null || forceReload) {
          final fetchedProjects = await _filterProjects(state.projectTags);
          final fetchedMyProjects = await NewsfeedRepository()
              .fetchProjects(uid: UserProfileRepository().credentials.id);
          emit(state.copyWith(
              projects: fetchedProjects,
              filteredProjects: fetchedProjects,
              myProjects: fetchedMyProjects));
        }
        break;
      case NewsfeedPageEnum.Jobs:
        if (state.jobs == null || forceReload) {
          final response = await NewsfeedRepository().getVacancies();
          emit(state.copyWith(jobs: response, filteredJobs: response));
        }
    }
    //TODO: News
  }

  void handleSearch(NewsfeedPageEnum page) async {
    if (state.syncing) return;
    String query;
    switch (page) {
      case NewsfeedPageEnum.Portfolio:
        query = state.textSearchController.text.trim();
        break;
      case NewsfeedPageEnum.Events:
        emit(state.copyWith(syncing: true));
        final filteredEvents = await _filterEvents(state.eventTags);
        emit(state.copyWith(filteredEvents: filteredEvents, syncing: false));
        return;
      case NewsfeedPageEnum.Projects:
        emit(state.copyWith(syncing: true));
        final filteredProjetcs = await _filterProjects(state.projectTags);
        emit(
            state.copyWith(filteredProjects: filteredProjetcs, syncing: false));
        return;
      case NewsfeedPageEnum.Jobs:
        emit(state.copyWith(syncing: true));
        final filteredJobs = await _filterJobs(
            state.jobTags, state.jobsTextSearchController.text);
        emit(state.copyWith(filteredJobs: filteredJobs, syncing: false));
        return;
      default:
        query = state.jobsTextSearchController.text.trim();
    }
    if (page == NewsfeedPageEnum.Portfolio) {
      emit(state.copyWith(syncing: true));
      final response = await NewsfeedRepository()
          .fetchPortfolios(query: query, tags: state.tags);
      if (response.error == null) {
        emit(state.copyWith(syncing: false, portfolios: response.entries));
      }
      await _update();
    }
  }

  Future<void> likeEvent(int eid) async {
    await NewsfeedRepository().likeEvent(eid);
    init(NewsfeedPageEnum.Events, forceReload: true);
  }

  Future<void> cleanFilter(NewsfeedPageEnum page) async {
    emit(state.copyWith(syncing: true));
    switch (page) {
      case NewsfeedPageEnum.Portfolio:
        state.textSearchController.clear();
        state.tagController.clear();
        emit(state.copyWith(tags: const []));
        break;
      case NewsfeedPageEnum.Jobs:
        state.jobsTextSearchController.clear();
        state.jobsTagController.clear();
        final jobs = await _filterJobs(const [], '');
        emit(state.copyWith(
            jobTags: const [], jobs: jobs, filteredJobs: jobs, syncing: false));
        break;
      case NewsfeedPageEnum.News:
        break;
      case NewsfeedPageEnum.Events:
        emit(state.copyWith(eventTags: const [], syncing: true));
        final events = await _filterEvents(const []);
        emit(state.copyWith(filteredEvents: events, syncing: false));
        break;
      case NewsfeedPageEnum.Projects:
        emit(state.copyWith(syncing: true, projectTags: const []));
        final projects = await _filterProjects(const []);
        emit(state.copyWith(syncing: false, filteredProjects: projects));
        break;
    }
    init(page, forceReload: true);
  }

  Future<void> _update() async {
    final response = await NewsfeedRepository().fetchPortfolios(
        query: state.textSearchController.text, tags: state.tags);
    if (response.error == null)
      emit(state.copyWith(syncing: false, portfolios: response.entries));
  }

  Future<void> addTag(PortfolioTag tag, NewsfeedPageEnum page) async {
    switch (page) {
      case NewsfeedPageEnum.Portfolio:
        emit(state.copyWith(tags: [...state.tags, tag]));
        break;
      case NewsfeedPageEnum.Events:
        emit(state.copyWith(eventTags: [...state.eventTags, tag]));
        break;
      case NewsfeedPageEnum.Jobs:
        emit(state.copyWith(jobTags: [...state.jobTags, tag]));
        break;
      case NewsfeedPageEnum.Projects:
        emit(state.copyWith(
          projectTags: [...state.projectTags, tag],
        ));
        break;
      case NewsfeedPageEnum.News:
        break; //TODO News
    }
  }

  Future<void> removeTag(String tag, NewsfeedPageEnum page) async {
    switch (page) {
      case NewsfeedPageEnum.Portfolio:
        final nTags = state.tags..removeWhere((element) => element.tag == tag);
        emit(state.copyWith(tags: nTags));
        break;
      case NewsfeedPageEnum.Jobs:
        final nTags = state.jobTags
          ..removeWhere((element) => element.tag == tag);
        emit(state.copyWith(jobTags: nTags));
        break;
      case NewsfeedPageEnum.Events:
        final nTags = state.eventTags
          ..removeWhere((element) => element.tag == tag);
        emit(state.copyWith(eventTags: nTags));
        break;
      case NewsfeedPageEnum.Projects:
        final nTags = state.projectTags
          ..removeWhere((element) => element.tag == tag);
        emit(state.copyWith(projectTags: nTags));
        break;
      case NewsfeedPageEnum.News:
        break; //TODO News
    }
  }

  Future<void> close() async {
    state.tagController.dispose();
    state.textSearchController.dispose();
    state.jobsTagController.dispose();
    state.jobsTextSearchController.dispose();
    return super.close();
  }

  static Future<List<EventModel>?> _filterEvents(
      List<PortfolioTag> tags) async {
    final now = DateTime.now();

    List<EventModel> sortedEvents =
        await NewsfeedRepository().fetchEvents(tags: tags);

    sortedEvents = sortedEvents.where((event) {
      if (event.tags?.isEmpty ?? true) return false;
      bool contains = true;
      tags.forEach((tag) {
        if (!event.tags!.contains(tag)) contains = false;
      });
      return contains && event.date.isAfter(now);
    }).toList();

    sortedEvents.sort(((a, b) => b.date.compareTo(a.date)));
    return sortedEvents;
  }

  static Future<List<ProjectModel>?> _filterProjects(
      List<PortfolioTag> tags) async {
    final projects = await NewsfeedRepository().fetchProjects(tags: tags);

    return projects;
  }

  static Future<List<JobModel>?> _filterJobs(
      List<PortfolioTag> tags, String query) async {
    final jobs =
        await NewsfeedRepository().getVacancies(query: query, tags: tags);

    return jobs;
  }
}
