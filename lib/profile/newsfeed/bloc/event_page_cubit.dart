import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/profile/newsfeed/bloc/newsfeed_cubit.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';
import 'package:teormech/profile/newsfeed/rep/newsfeed_repository.dart';
import 'package:teormech/profile/newsfeed/ui/newsfeed_screen.dart';

class EventPageCubit extends Cubit<EventModel?> {
  final int id;
  final NewsfeedCubit? newsfeedCubit;

  EventPageCubit({required this.id, this.newsfeedCubit}) : super(null) {
    _load();
  }

  Future<void> _load() async {
    emit(await NewsfeedRepository().fetchEvent(id: id));
  }

  Future<void> like(bool liked) async {
    if (!liked) {
      await NewsfeedRepository().likeEvent(id);
    } else {
      await NewsfeedRepository().likeEvent(id, dislike: true);
    }
    _load();
    newsfeedCubit?.init(NewsfeedPageEnum.Events, forceReload: true);
  }
}
