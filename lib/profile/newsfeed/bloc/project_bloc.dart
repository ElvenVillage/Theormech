import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';
import 'package:teormech/profile/newsfeed/model/project_state.dart';
import 'package:teormech/profile/newsfeed/rep/newsfeed_repository.dart';
import 'package:teormech/profile/profile/model/portfolio_state.dart';

class ProjectCubit extends Cubit<ProjectState> {
  ProjectCubit()
      : super(ProjectState(
            posts: null,
            project: null,
            editedTags: const [],
            photos: null,
            invited: false,
            attachments: null));

  ProjectCubit.fromBase({required ProjectModel base})
      : super(ProjectState(
            posts: null,
            project: base,
            attachments: null,
            invited: false,
            editedTags: base.tags ?? const [],
            photos: null)) {
    fetchFromBase(base.id);
    this.pid = base.id;
  }

  int? pid;

  ProjectCubit.fromPid({required int pid})
      : super(ProjectState(
            posts: null,
            attachments: const [],
            project: null,
            invited: false,
            editedTags: const [],
            photos: const [])) {
    this.pid = pid;

    fetchProject();
  }

  void fetchFromBase(int pid) async {
    final invited = (await NewsfeedRepository().showMyInvites()).contains(pid);
    emit(state.copyWith(invited: invited));
    fetchPosts();
  }

  void fetchProject() {
    int? pid = this.pid ?? state.project?.id;

    if (pid == null) return;

    Future.wait([
      NewsfeedRepository().fetchProject(pid: pid),
      NewsfeedRepository().showMyInvites()
    ])
        .then((value) => emit(state.copyWith(
            project: value[0] as ProjectModel?,
            editedTags: (value[0] as ProjectModel?)?.tags,
            invited: (value[1] as List<int>).contains(pid))))
        .then((_) => fetchPosts());
  }

  void init(int idx) async {
    if (idx == 1 && state.photos == null) {
      await fetchPhotos();
    }
    if (idx == 2 && state.attachments == null) {
      await fetchAttachments();
    }
  }

  Future<void> fetchPhotos() async {
    final photos = await NewsfeedRepository()
        .getProjectFeedAttachments(pid: pid!, proejctList: true, photos: true);
    emit(state.copyWith(photos: photos));
  }

  Future<void> fetchAttachments() async {
    final attachments = await NewsfeedRepository()
        .getProjectFeedAttachments(pid: pid!, proejctList: true, photos: false);
    emit(state.copyWith(attachments: attachments));
  }

  Future<void> fetchPosts() async {
    final posts =
        await NewsfeedRepository().fetchProjectPosts(pid: state.project!.id);
    emit(state.copyWith(posts: posts));
  }

  Future<void> toggleAdmin({required int uid}) async {
    final participant = state.project?.participants?.firstWhere(
      (e) => e.id == uid,
    );

    await NewsfeedRepository().editProjectRole(
        uid: participant!.id.toString(),
        role: participant.role == ProjectParticipantRole.Admin ? '1' : '3',
        pid: pid!);
    fetchProject();
  }

  Future<void> editMembers({required int uid, required String status}) async {
    final participant = state.project?.participants?.firstWhere(
      (e) => e.id == uid,
    );
    if (participant?.status != status) {
      await NewsfeedRepository().editProjectMemberStatus(
          uid: uid.toString(), key: 'status', val: status, pid: pid!);
      fetchProject();
    }
  }

  Future<bool> publicatePost(
      {required String title,
      required String description,
      required List<String> attachments,
      required List<String> photos}) async {
    try {
      await NewsfeedRepository().publicatePost(
          pid: state.project!.id,
          title: title,
          attachments: attachments,
          description: description,
          photos: photos);
    } catch (_) {
      return false;
    }
    fetchPosts();
    return true;
  }

  Future<void> manageInvite(int userId, {required bool accept}) async {
    await NewsfeedRepository().manageInvitesForProjects(
        pid: pid!, uid: userId.toString(), dismiss: !accept);
    fetchProject();
  }

  Future<void> leaveProject() async {
    await NewsfeedRepository().leaveProject(pid: pid!);
    fetchProject();
  }

  void addTag(String tag) {
    emit(state.copyWith(editedTags: [
      ...state.editedTags,
      PortfolioTag(tag: tag, level: ProfessionalLevel.Newsfeed)
    ]));
  }

  void removeTag(String tag) {
    final nTags = List<PortfolioTag>.from(state.editedTags)
      ..removeWhere((e) => e.tag == tag);
    emit(state.copyWith(editedTags: nTags));
  }

  Future<void> deleteParticipant(
      {required String uid, required String text}) async {
    await NewsfeedRepository()
        .addProjectMember(pid: pid!, uid: uid, text: text);
    fetchProject();
  }

  Future<void> syncParticipants(List<int> selectedParticipants) async {
    final oldParticipants =
        state.project?.participants?.map((e) => e.id).toList() ?? [];

    oldParticipants.forEach((old) async {
      if (!selectedParticipants.contains(old))
        await NewsfeedRepository()
            .addProjectMember(pid: pid!, uid: old.toString(), text: '');
    });

    selectedParticipants.forEach((newParticipant) async {
      if (!oldParticipants.contains(newParticipant))
        await NewsfeedRepository()
            .addProjectMember(pid: pid!, uid: newParticipant.toString());
    });

    fetchProject();
  }

  Future<void> syncTags() async {
    final projectTagsStrings =
        state.project?.tags?.map((e) => e.tag).toList() ?? [];
    final editedTagsStrings = state.editedTags.map((e) => e.tag).toList();

    projectTagsStrings.forEach((oldTag) async {
      if (!editedTagsStrings.contains(oldTag))
        await NewsfeedRepository().pinTag(
            pid: pid!, tag: oldTag, unpin: true, mode: RequestMode.Project);
    });

    editedTagsStrings.forEach((newTag) async {
      if (!projectTagsStrings.contains(newTag))
        await NewsfeedRepository()
            .pinTag(pid: pid!, tag: newTag, mode: RequestMode.Project);
    });
  }

  Future<void> editProject(
      {required String description,
      required String title,
      required String end,
      String? file,
      required String start}) async {
    await syncTags();
    if (file != null)
      await NewsfeedRepository().uploadProjectPhoto(pid: pid!, photo: file);
    await NewsfeedRepository().editProject(
        pid: pid!,
        description:
            description != state.project!.description ? description : null,
        title: title != state.project!.title ? title : null,
        start: start != state.project!.start.toString() ? start : null,
        end: end != state.project!.end.toString() ? end : null);
    fetchProject();
  }

  Future<void> acceptInvite({required int pid}) async {
    await NewsfeedRepository().acceptInvite(pid: pid);
    fetchProject();
  }
}
