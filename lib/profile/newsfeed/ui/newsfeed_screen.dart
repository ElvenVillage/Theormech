import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/ui/widgets/choice_container.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/newsfeed/bloc/newsfeed_cubit.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_state.dart';
import 'package:teormech/profile/newsfeed/ui/pages/jobs/create_job_page.dart';
import 'package:teormech/profile/newsfeed/ui/pages/preferred_tags_page.dart';
import 'package:teormech/profile/newsfeed/ui/pages/jobs/jobs_page.dart';
import 'package:teormech/profile/newsfeed/ui/pages/events/events_page.dart';
import 'package:teormech/profile/newsfeed/ui/pages/newsfeed_page.dart';
import 'package:teormech/profile/newsfeed/ui/pages/portfolio/portfolio_page.dart';
import 'package:teormech/profile/newsfeed/ui/pages/projects/create_project_screen.dart';
import 'package:teormech/profile/newsfeed/ui/pages/projects/projects_page.dart';
import 'package:teormech/profile/newsfeed/ui/widgets/newsfeed_filter.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/widgets/loading_progress_indicator.dart';

enum NewsfeedPageEnum { News, Events, Projects, Jobs, Portfolio }

class NewsfeedScreen extends StatefulWidget {
  final Key? key;

  NewsfeedScreen({this.key}) : super(key: key);

  @override
  State<NewsfeedScreen> createState() => _NewsfeedScreenState();
}

class _NewsfeedScreenState extends State<NewsfeedScreen>
    with AutomaticKeepAliveClientMixin {
  NewsfeedPageEnum _chosen = NewsfeedPageEnum.News;

  final _controller = PageController();
  final _scrollController = ScrollController();

  final _expandableController = ExpandableController();
  late final NewsfeedCubit _cubit;

  @override
  void initState() {
    _cubit = context.read<NewsfeedCubit>()..init(_chosen);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    _expandableController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  void _onChoseCallback(int idx) {
    _cubit.init(NewsfeedPageEnum.values[idx]);
    _scrollController.animateTo(idx * 50,
        duration: const Duration(milliseconds: 500), curve: Curves.decelerate);
    _controller.jumpToPage(idx);
    setState(() {
      _chosen = NewsfeedPageEnum.values[idx];
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: ProfileAppBar(
        leftAlignedTitle: true,
        rightmostButton: IconButton(
          icon: ImageIcon(
            AssetImage('images/icons/filter.png'),
            color: Colors.white,
          ),
          onPressed: () async {
            _expandableController.toggle();
          },
        ),
        rightButton: IconButton(
          icon: Icon(_chosen == NewsfeedPageEnum.Jobs ||
                  _chosen == NewsfeedPageEnum.Projects
              ? Icons.add
              : Icons.lightbulb),
          color: (_chosen == NewsfeedPageEnum.Jobs ||
                  _chosen == NewsfeedPageEnum.Events ||
                  _chosen == NewsfeedPageEnum.Projects)
              ? Colors.white
              : Colors.transparent,
          onPressed: () {
            switch (_chosen) {
              case NewsfeedPageEnum.Jobs:
                context.read<BottomBarBloc>().go(CreateJobScreen.route());
                break;
              case NewsfeedPageEnum.Events:
                context.read<BottomBarBloc>().go(PreferredTagsPage.route());
                break;
              case NewsfeedPageEnum.Projects:
                context.read<BottomBarBloc>().go(CreateProjectScreen.route());
                break;
              default:
            }
          },
        ),
        title: Text(
          'Лента',
          style: appBarTextStyle,
        ),
      ),
      backgroundColor: Colors.white,
      body: BlocBuilder<NewsfeedCubit, NewsfeedState>(
        builder: (context, state) {
          return Stack(
            children: [
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  FocusScope.of(context).unfocus();
                },
                child: NestedScrollView(
                    headerSliverBuilder: (context, innerBoxIsScrolled) => [
                          SliverToBoxAdapter(
                              child: ExpandablePanel(
                            collapsed: NewsfeedFilter(
                              expanded: false,
                              page: _chosen,
                              expandableController: _expandableController,
                            ),
                            expanded: NewsfeedFilter(
                              expanded: true,
                              page: _chosen,
                              expandableController: _expandableController,
                            ),
                            controller: _expandableController,
                          )),
                          SliverAppBar(
                            pinned: true,
                            backgroundColor: Colors.white,
                            flexibleSpace: Padding(
                              padding: const EdgeInsets.only(
                                  left: 4, right: 4, top: 10),
                              child: SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  controller: _scrollController,
                                  child: Row(
                                    children: [
                                      ChoiceContainer(
                                        text: 'Новости',
                                        chosen:
                                            _chosen == NewsfeedPageEnum.News,
                                        onClickCallback: () {
                                          _onChoseCallback(0);
                                        },
                                      ),
                                      ChoiceContainer(
                                        text: 'Мероприятия',
                                        chosen:
                                            _chosen == NewsfeedPageEnum.Events,
                                        onClickCallback: () {
                                          _onChoseCallback(1);
                                        },
                                      ),
                                      ChoiceContainer(
                                        text: 'Проекты',
                                        chosen: _chosen ==
                                            NewsfeedPageEnum.Projects,
                                        onClickCallback: () {
                                          _onChoseCallback(2);
                                        },
                                      ),
                                      ChoiceContainer(
                                        text: 'Вакансии',
                                        chosen:
                                            _chosen == NewsfeedPageEnum.Jobs,
                                        onClickCallback: () {
                                          _onChoseCallback(3);
                                        },
                                      ),
                                      ChoiceContainer(
                                        text: 'Портфолио',
                                        chosen: _chosen ==
                                            NewsfeedPageEnum.Portfolio,
                                        onClickCallback: () {
                                          _onChoseCallback(4);
                                        },
                                      ),
                                    ],
                                  )),
                            ),
                          ),
                        ],
                    body: Container(
                      decoration: BoxDecoration(
                          border: Border(
                              top: BorderSide(color: Colors.grey.shade300))),
                      child: PageView.builder(
                          controller: _controller,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: 5,
                          itemBuilder: (context, idx) {
                            switch (idx) {
                              case 1:
                                return EventsNewsfeedPage();
                              case 4:
                                return NewsfeedPortfolioPage();
                              case 2:
                                return const ProjectsNewsfeedPage();
                              case 3:
                                return JobsNewsfeedPage();
                              case 0:
                              default:
                                return const NewsfeedPage();
                            }
                          }),
                    )),
              ),
              LoadingProgressIndicator(
                  needToShow: state.syncing, listenToChatBloc: false)
            ],
          );
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
