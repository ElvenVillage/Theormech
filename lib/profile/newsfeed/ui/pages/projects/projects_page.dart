import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/newsfeed/bloc/newsfeed_cubit.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_state.dart';
import 'package:teormech/profile/newsfeed/ui/pages/projects/project_page.dart';
import 'package:teormech/profile/newsfeed/ui/pages/projects/project_content.dart';

class ProjectsNewsfeedPage extends StatelessWidget {
  const ProjectsNewsfeedPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewsfeedCubit, NewsfeedState>(builder: (context, state) {
      if (state.filteredProjects == null)
        return const Center(
          child: CircularProgressIndicator(),
        );
      return ListView.builder(
          itemCount: state.filteredProjects?.length ?? 0,
          itemBuilder: (context, idx) {
            return InkWell(
                onTap: () {
                  context
                      .read<BottomBarBloc>()
                      .go(ProjectPage.route(project: state.projects![idx]));
                },
                child: Column(
                  children: [
                    ProjectContent(
                      project: state.filteredProjects![idx],
                      expanded: false,
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 12.0),
                      height: 1,
                      color: Colors.grey.shade300,
                    )
                  ],
                ));
          });
    });
  }
}
