import 'package:file_picker/file_picker.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:teormech/auth/ui/widgets/register_text_field.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/newsfeed/bloc/project_bloc.dart';
import 'package:teormech/profile/newsfeed/ui/widgets/create_post_button.dart';
import 'package:teormech/profile/newsfeed/ui/widgets/post_documents.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/edge_alert_notification.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/styles/transitions/slide_fade.dart';
import 'package:teormech/widgets/images_staggered_grid_view.dart';
import 'package:teormech/widgets/multilne_text_field.dart';

class CreatePostScreen extends StatefulWidget {
  const CreatePostScreen({Key? key, required this.projectCubit})
      : super(key: key);

  final ProjectCubit projectCubit;

  static PageRouteBuilder route({required ProjectCubit cubit}) =>
      PageRouteBuilder(
          pageBuilder: (context, f1, f2) =>
              CreatePostScreen(projectCubit: cubit),
          transitionsBuilder: slideTransition);

  @override
  State<CreatePostScreen> createState() => _CreatePostScreenState();
}

class _CreatePostScreenState extends State<CreatePostScreen> {
  final _captionController = TextEditingController();
  final _valueController = TextEditingController();

  var _updating = false;

  final _textStyle = profileTextStyle.copyWith(
      color: Colors.black, fontWeight: FontWeight.w500, fontSize: 16);

  final picker = ImagePicker();
  final filePicker = FilePicker.platform;

  final _images = <String>[];
  var _files = <PlatformFile>[];

  @override
  void dispose() {
    _captionController.dispose();
    _valueController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        backgroundColor: GradientColors.backWhite,
        appBar: ProfileAppBar(
            title: Text(
          "Создание поста",
          style: appBarTextStyle,
        )),
        body: Stack(
          children: [
            Positioned.fill(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    create_post_input(
                        caption: 'Заголовок', controller: _captionController),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10, horizontal: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Текст поста',
                            style: _textStyle,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: MultilineTextField(
                              collapsed: _images.isNotEmpty,
                              controller: _valueController,
                              inARow: false,
                            ),
                          ),
                        ],
                      ),
                    ),
                    if (_images.isNotEmpty)
                      ImagesStaggeredGridView(
                          fileImages: true,
                          images: _images,
                          enableDeleting: true,
                          deleteCallback: (idx) {
                            setState(() {
                              _images.removeAt(idx);
                            });
                          }),
                    if (_files.isNotEmpty)
                      PostDocumentsBlock(
                          files: _files,
                          editable: true,
                          removeCallback: (int idx) {
                            setState(() {
                              _files.removeAt(idx);
                            });
                          }),
                    Row(children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0, right: 8.0),
                        child: IconButton(
                          iconSize: 32,
                          icon: ImageIcon(
                            AssetImage('images/icons/projects/addpict.png'),
                            color: GradientColors.pink,
                          ),
                          onPressed: () async {
                            final images = await picker.getMultiImage();
                            if (images != null)
                              for (var img in images)
                                setState(() {
                                  _images.add(img.path);
                                });
                          },
                        ),
                      ),
                      IconButton(
                        iconSize: 32,
                        icon: ImageIcon(
                          AssetImage('images/icons/projects/adddoc.png'),
                          color: GradientColors.pink,
                        ),
                        onPressed: () async {
                          final files =
                              await filePicker.pickFiles(allowMultiple: true);
                          if ((files?.count ?? 0) > 0) {
                            setState(() {
                              _files = [..._files, ...files!.files];
                            });
                          }
                        },
                      ),
                      Spacer(),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: CreatePostButton(
                          onPressed: _publicatePost,
                          icon: 'images/icons/projects/addpost.png',
                          caption: 'Создать пост',
                        ),
                      )
                    ])
                  ],
                ),
              ),
            ),
            if (_updating)
              Positioned.fill(
                  child: Container(
                color: GradientColors.updatingIndicatorColor,
                child: const Center(child: CircularProgressIndicator()),
              ))
          ],
        ),
      ),
    ));
  }

  Widget create_post_input(
      {required String caption, required TextEditingController controller}) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 20),
            child: Text(
              caption,
              style: _textStyle,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: RegisterTextField(controller: controller),
          ),
        ],
      ),
    );
  }

  void _publicatePost() async {
    setState(() {
      _updating = true;
    });

    final result = await widget.projectCubit.publicatePost(
        title: _captionController.text,
        attachments: _files.map((e) => e.path!).toList(),
        description: _valueController.text,
        photos: _images);

    if (!result)
      showEdgeAlertNotification(
          okText: '',
          errorText: 'Не удалось создать пост',
          context: context,
          result: false);

    setState(() {
      _updating = false;
    });

    context.read<BottomBarBloc>().pop();
  }
}
