import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/ui/widgets/choice_container.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_list_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_list_page_cubit.dart';
import 'package:teormech/profile/chat/ui/widgets/chat_gallery.dart';
import 'package:teormech/profile/newsfeed/bloc/project_bloc.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';
import 'package:teormech/profile/newsfeed/model/project_state.dart';
import 'package:teormech/profile/newsfeed/ui/pages/projects/create_post_screen.dart';
import 'package:teormech/profile/newsfeed/ui/pages/projects/create_project_screen.dart';
import 'package:teormech/profile/newsfeed/ui/pages/projects/project_content.dart';
import 'package:teormech/profile/newsfeed/ui/pages/projects/project_participants.dart';
import 'package:teormech/profile/newsfeed/ui/widgets/create_post_button.dart';
import 'package:teormech/profile/newsfeed/ui/widgets/post_documents.dart';
import 'package:teormech/profile/profile/rep/user_profile_rep.dart';
import 'package:teormech/profile/profile/ui/widgets/post.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/styles/transitions/slide_fade.dart';

enum ProjectPageEnum { Newsfeed, Images, Documents }

class ProjectPage extends StatefulWidget {
  const ProjectPage({this.project, this.pid, Key? key}) : super(key: key);

  final ProjectModel? project;
  final int? pid;

  static PageRouteBuilder route({required ProjectModel project}) {
    return PageRouteBuilder(
        transitionsBuilder: slideTransition,
        pageBuilder: (context, f1, f2) => ProjectPage(
              project: project,
            ));
  }

  static PageRouteBuilder pidRoute({required int pid}) {
    return PageRouteBuilder(
        transitionsBuilder: slideTransition,
        pageBuilder: (context, f1, f2) => ProjectPage(
              pid: pid,
            ));
  }

  @override
  State<ProjectPage> createState() => _ProjectPageState();
}

class _ProjectPageState extends State<ProjectPage> {
  var _page = ProjectPageEnum.Newsfeed;
  final _pageController = PageController();

  late final ProjectCubit _projectBloc;
  late final bool isAdmin;
  late final bool isMember;

  @override
  void initState() {
    if (widget.pid != null) {
      _projectBloc = ProjectCubit.fromPid(pid: widget.pid!);
      isAdmin = true;
      isMember = true;
    } else {
      isAdmin = widget.project!
          .isAdmin(int.parse(UserProfileRepository().credentials.id));
      isMember = widget.project!
          .isMember(int.parse(UserProfileRepository().credentials.id));
      _projectBloc = ProjectCubit.fromBase(base: widget.project!);
    }
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    _projectBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final shareButton = IconButton(
      icon: ImageIcon(AssetImage('images/icons/share.png')),
      color: Colors.white,
      iconSize: 32,
      onPressed: () {
        context.read<ChatListBloc>()
          ..addNewsfeedObjectForForwarding(NewsfeedObject(
              widget.pid ?? widget.project!.id, RequestMode.Project))
          ..pageCubit.selectPage(ChatListPage.Chats);
        context.read<BottomBarBloc>().setTab(BottomBars.Chat);
      },
    );

    return BlocProvider<ProjectCubit>.value(
      value: _projectBloc,
      child: Scaffold(
          appBar: ProfileAppBar(
            rightmostButton: isAdmin
                ? IconButton(
                    iconSize: 20,
                    color: Colors.white,
                    icon: ImageIcon(
                        AssetImage('images/icons/selection_bar/pensil.png')),
                    onPressed: () {
                      if (_projectBloc.state.project != null)
                        context
                            .read<BottomBarBloc>()
                            .go(CreateProjectScreen.routeEditing(_projectBloc));
                    },
                  )
                : shareButton,
            rightButton: isAdmin ? shareButton : null,
            title: Row(
              children: [
                Text(
                  'Просмотр проекта',
                  style: appBarTextStyle,
                )
              ],
            ),
          ),
          body: BlocBuilder<ProjectCubit, ProjectState>(
              builder: (context, projectState) {
            if (projectState.project == null)
              return const Center(
                child: CircularProgressIndicator(),
              );
            return NestedScrollView(
                headerSliverBuilder: (context, innerBoxIsScrolled) {
                  return [
                    SliverToBoxAdapter(
                      child: ProjectContent(
                        project: projectState.project!,
                        invited: projectState.invited,
                        expanded: true,
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: Padding(
                        padding: const EdgeInsets.only(
                          bottom: 12.0,
                        ),
                        child: ProjectPartipantsList(
                          project: _projectBloc,
                        ),
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border(
                              top: BorderSide(
                                  width: 0.5, color: Colors.grey.shade400),
                            )),
                      ),
                    ),
                    SliverAppBar(
                      pinned: true,
                      automaticallyImplyLeading: false,
                      backgroundColor: Colors.white,
                      flexibleSpace: Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            ChoiceContainer(
                                text: 'Новости',
                                chosen: _page == ProjectPageEnum.Newsfeed,
                                onClickCallback: () {
                                  setState(() {
                                    _page = ProjectPageEnum.Newsfeed;
                                    _pageController.jumpToPage(0);
                                  });
                                }),
                            ChoiceContainer(
                                text: 'Изображения',
                                chosen: _page == ProjectPageEnum.Images,
                                onClickCallback: () {
                                  setState(() {
                                    _page = ProjectPageEnum.Images;
                                    _pageController.jumpToPage(1);
                                  });
                                }),
                            ChoiceContainer(
                                text: 'Документы',
                                chosen: _page == ProjectPageEnum.Documents,
                                onClickCallback: () {
                                  setState(() {
                                    _page = ProjectPageEnum.Documents;
                                    _pageController.jumpToPage(2);
                                  });
                                }),
                          ],
                        ),
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: Container(
                        margin: const EdgeInsets.only(bottom: 8.0),
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    if (isMember)
                      SliverToBoxAdapter(
                        child: Container(
                          color: Colors.white,
                          width: double.infinity,
                          child: Padding(
                            padding:
                                const EdgeInsets.only(bottom: 8.0, left: 8.0),
                            child: Row(children: [
                              CreatePostButton(
                                  caption: 'Создать пост',
                                  icon: 'images/icons/projects/addpost.png',
                                  onPressed: () {
                                    context.read<BottomBarBloc>().go(
                                        CreatePostScreen.route(
                                            cubit: _projectBloc));
                                  }),
                              Padding(
                                padding: const EdgeInsets.only(
                                    right: 8.0, left: 8.0),
                                child: Container(
                                  height: 35,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(30),
                                      color: GradientColors.pink),
                                  child: IconButton(
                                    icon: ImageIcon(
                                      AssetImage(
                                          'images/icons/projects/addpict.png'),
                                      color: Colors.white,
                                    ),
                                    onPressed: () {},
                                  ),
                                ),
                              ),
                              Container(
                                height: 35,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    color: GradientColors.pink),
                                child: IconButton(
                                  icon: ImageIcon(
                                    AssetImage(
                                        'images/icons/projects/adddoc.png'),
                                    color: Colors.white,
                                  ),
                                  onPressed: () {},
                                ),
                              )
                            ]),
                          ),
                        ),
                      ),
                  ];
                },
                body: PageView.builder(
                  itemCount: 10,
                  controller: _pageController,
                  onPageChanged: (int idx) {
                    context.read<ProjectCubit>().init(idx);
                  },
                  physics: const NeverScrollableScrollPhysics(),
                  itemBuilder: (context, page) {
                    if (page == 0)
                      return BlocBuilder<ProjectCubit, ProjectState>(
                          builder: (context, projectState) {
                        if (projectState.posts == null)
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        return ListView.builder(
                            itemCount: projectState.posts!.length,
                            itemBuilder: (context, idx) {
                              final post = projectState.posts![idx];
                              return GestureDetector(
                                child: Post(
                                  key: ValueKey(post.id),
                                  isProjectPost: true,
                                  photos: post.photos,
                                  attachments: post.attachments,
                                  avatar: projectState.project!.image,
                                  caption: post.title,
                                  text: post.description,
                                  date: post.date,
                                ),
                              );
                            });
                      });
                    if (page == 1)
                      return BlocBuilder<ProjectCubit, ProjectState>(
                          builder: (context, state) {
                        if (state.photos == null)
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        return GridView.count(
                          crossAxisCount: 3,
                          children: [
                            for (var i = 0; i < state.photos!.length; i++)
                              GestureDetector(
                                onTap: () {
                                  context.read<BottomBarBloc>().go(
                                      MaterialPageRoute(
                                          builder: (context) => ChatGallery(
                                                files: state.photos!,
                                                idx: i,
                                                mode: PhotoGallery.Post,
                                                isFromChat: false,
                                              )),
                                      hideBottombar: true);
                                },
                                child: SizedBox(
                                  width: 100,
                                  height: 100,
                                  child: CachedNetworkImage(
                                    imageUrl: state.photos![i],
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              )
                          ],
                        );
                      });
                    return BlocBuilder<ProjectCubit, ProjectState>(
                        builder: (context, state) {
                      if (state.attachments == null)
                        return Center(child: const CircularProgressIndicator());
                      if (state.attachments!.isEmpty) return const SizedBox();
                      return PostDocumentsBlock(
                        editable: false,
                        onlineDocuments: state.attachments,
                      );
                    });
                  },
                ));
          })),
    );
  }
}
