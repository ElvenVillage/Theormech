import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:star_menu/star_menu.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/chat/ui/pages/chat_screen.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/profile/model/user_profile.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';
import 'package:teormech/profile/profile/rep/user_profile_rep.dart';
import 'package:teormech/profile/subscription/bloc/followers_bloc.dart';
import 'package:teormech/profile/subscription/model/followers_state.dart';
import 'package:teormech/profile/subscription/ui/widgets/profile_row.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/widgets/star_menu_item.dart';

typedef ToggleUserRoleCallback = void Function(int uid);
typedef ChangeUserStatusCallback = void Function(String status, int uid);
typedef ToggleUserDeletingCallback = void Function(String uid, String name);

class ProjectProfileRow extends StatefulWidget {
  const ProjectProfileRow(
      {required Key key,
      required this.toggleUserDeletingCallback,
      required this.user,
      required this.userRoleCallback,
      required this.changeUserStatusCallback,
      required this.amIAdmin})
      : super(key: key);

  final ProjectParticipant user;
  final ToggleUserRoleCallback userRoleCallback;
  final bool amIAdmin;
  final ChangeUserStatusCallback changeUserStatusCallback;
  final ToggleUserDeletingCallback toggleUserDeletingCallback;

  @override
  _ProjectProfileRowState createState() => _ProjectProfileRowState();
}

class _ProjectProfileRowState extends State<ProjectProfileRow> {
  final controller = ExpandableController();
  final textController = TextEditingController();

  @override
  void initState() {
    textController.text = widget.user.status ?? '';
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    textController.dispose();
    super.dispose();
  }

  Widget rightSuffix(
      {required int idx,
      required double width,
      required UserProfile user,
      required VoidCallback expand,
      required bool isAdmin}) {
    if (user.lid == UserProfileRepository().credentials.id)
      return const SizedBox();
    const share = 0.7;
    if (isAdmin) {
      return IconButton(
        key: widget.key,
        padding: EdgeInsets.zero,
        icon: const Icon(
          Icons.more_horiz,
        ),
        onPressed: () {},
      ).addStarMenu(
          context,
          [
            first(width * share),
            generateMenuItem(
                title: 'Перейти в чат',
                icon: 'images/icons/comm.png',
                width: width * share),
            generateMenuItem(
                title: 'Изменить роль',
                icon: 'images/icons/selection_bar/pensil.png',
                width: width * share),
            generateMenuItem(
                title: 'Назначить руководителем',
                icon: 'images/icons/selection_bar/unsave.png',
                width: width * share),
            generateMenuItem(
                title: 'Назначить наставником',
                icon: 'images/icons/chat_options/profile.png',
                width: width * share),
            generateMenuItem(
                title: 'Исключить из проекта',
                icon: 'images/icons/chat_options/forbid.png',
                width: width * share),
            last(width * share)
          ],
          StarMenuParameters(
            shape: MenuShape.linear,
            onItemTapped: (buttonId, controller) {
              if (buttonId == 2) {
                controller.closeMenu();
                print('2');
                expand();
              }
              if (buttonId == 3) {
                widget.userRoleCallback(widget.user.id);
                controller.closeMenu();
              }
              if (buttonId == 4) {}
              if (buttonId == 5) {
                controller.closeMenu();
                Future.delayed(
                  Duration(milliseconds: 300),
                  () => widget.toggleUserDeletingCallback(
                      widget.user.id.toString(),
                      user.surname + ' ' + user.name),
                );
              }
            },
            centerOffset: const Offset(-100, 0),
            backgroundParams: BackgroundParams(),
            linearShapeParams: LinearShapeParams(
                angle: 270, alignment: LinearAlignment.center),
          ));
    } else {
      return BlocBuilder<FollowersCubit, FollowersState>(
          builder: (context, followersState) {
        if (followersState.isInFollowings(uid: widget.user.id.toString())) {
          return IconButton(
              onPressed: () {
                context.read<BottomBarBloc>().go(
                    ChatScreenWrapper.route(
                      user: followersState.followings
                          .firstWhere((e) => e.id == widget.user.id.toString()),
                    ),
                    hideBottombar: true);
              },
              icon: const ImageIcon(
                AssetImage('images/icons/comm.png'),
                color: Colors.grey,
              ));
        } else {
          return IconButton(
            padding: EdgeInsets.zero,
            icon: Icon(Icons.add_circle_rounded),
            color: Colors.lightBlue,
            onPressed: () {
              context
                  .read<FollowersCubit>()
                  .follow(id: widget.user.id.toString());
            },
          );
        }
      });
    }
  }

  Widget profileRow(
      {required double width,
      required ProjectParticipant participant,
      required UserProfile profile,
      required bool isAdmin}) {
    return ProfileRow(
      caption: participant.status ??
          profile.employeeStatus ??
          profile.teacherStatus ??
          profile.group ??
          'Участник проекта',
      name: profile.name,
      isAdmin: participant.role == ProjectParticipantRole.Admin,
      status: FetchingStatus.Loaded,
      surname: profile.surname,
      online: false,
      rightSuffix: rightSuffix(
          idx: 0,
          isAdmin: isAdmin,
          width: width,
          user: profile,
          expand: () {
            controller.toggle();
            setState(() {});
          }),
      profileAvatar: profile.avatar,
      userId: widget.user.id.toString(),
    );
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return BlocProvider<UserProfileBloc>(
      create: (context) => UserProfileBloc.fromLid(
          lid: widget.user.id.toString(), loadFromHive: true),
      child: Builder(builder: (context) {
        return BlocBuilder<UserProfileBloc, UserProfileState>(
            builder: (context, state) {
          return Expandable(
            controller: controller,
            collapsed: profileRow(
              participant: widget.user,
              width: width,
              profile: state.profile,
              isAdmin: widget.amIAdmin,
            ),
            expanded: Column(
              children: [
                profileRow(
                  width: width,
                  participant: widget.user,
                  profile: state.profile,
                  isAdmin: widget.amIAdmin,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  child: LayoutBuilder(builder: (context, constraints) {
                    return SizedBox(
                      height: 40,
                      width: constraints.maxWidth * 0.8,
                      child: TextField(
                        controller: textController,
                        decoration: InputDecoration(
                            contentPadding:
                                const EdgeInsets.fromLTRB(10, 0, 10, 10),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(32.0),
                                borderSide:
                                    BorderSide(color: Colors.grey.shade400))),
                      ),
                    );
                  }),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    MaterialButton(
                      onPressed: () {
                        setState(() {
                          controller.toggle();
                          widget.changeUserStatusCallback(
                              textController.text, widget.user.id);
                        });
                      },
                      child: Row(
                        children: [
                          Icon(
                            Icons.check,
                            color: GradientColors.pink,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 5.0),
                            child: Text(
                              'Принять',
                              style: TextStyle(color: GradientColors.pink),
                            ),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    MaterialButton(
                      onPressed: () {
                        setState(() {
                          controller.toggle();
                        });
                      },
                      child: Row(
                        children: [
                          Icon(
                            Icons.cancel_sharp,
                            color: GradientColors.pink,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 5.0),
                            child: Text(
                              'Отменить',
                              style: TextStyle(color: GradientColors.pink),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                )
              ],
            ),
          );
        });
      }),
    );
  }
}
