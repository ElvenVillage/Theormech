import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/chat/ui/chat_screen/delete_message_dialog.dart';
import 'package:teormech/profile/newsfeed/bloc/newsfeed_cubit.dart';
import 'package:teormech/profile/newsfeed/bloc/project_bloc.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';
import 'package:teormech/profile/newsfeed/model/project_state.dart';
import 'package:teormech/profile/newsfeed/rep/newsfeed_repository.dart';
import 'package:teormech/profile/newsfeed/ui/newsfeed_screen.dart';
import 'package:teormech/profile/newsfeed/ui/pages/projects/project_profile_row.dart';
import 'package:teormech/profile/newsfeed/ui/widgets/create_project_event.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';
import 'package:teormech/profile/profile/rep/user_profile_rep.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/clipped_avatar.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/empty_avatar.dart';
import 'package:teormech/profile/subscription/model/followers_state.dart';
import 'package:teormech/profile/subscription/ui/subscriptions_screen.dart';
import 'package:teormech/profile/subscription/ui/widgets/profile_row.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/styles/transitions/slide_fade.dart';

class ProjectPartipantsList extends StatelessWidget {
  const ProjectPartipantsList({Key? key, required this.project})
      : super(key: key);
  final ProjectCubit project;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        context.read<BottomBarBloc>().go(ProjectParticipantsScreen.route(
            project: project,
            isAdmin: project.state.project?.isAdmin(
                    int.parse(UserProfileRepository().credentials.id)) ??
                false));
      },
      child: BlocBuilder<ProjectCubit, ProjectState>(
          bloc: project,
          builder: (context, projectState) {
            return Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: SizedBox(
                height: 32,
                child: Row(
                  children: [
                    Flexible(
                      flex: 2,
                      child: const Text(
                        'Рабочая группа: ',
                        style: TextStyle(
                            color: Colors.grey, fontWeight: FontWeight.w500),
                      ),
                    ),
                    if (projectState.project?.participants?.isNotEmpty ?? false)
                      Expanded(
                        flex: 4,
                        child: ListView(
                            scrollDirection: Axis.horizontal,
                            children: [
                              for (var participant
                                  in projectState.project!.participants!)
                                SizedBox(
                                  height: 24,
                                  width: 36,
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8.0),
                                    child: BlocProvider(
                                      create: (context) =>
                                          UserProfileBloc.fromLid(
                                              lid: participant.id.toString(),
                                              loadFromHive: false),
                                      child: Builder(
                                          builder: (context) => Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        horizontal: 4.0),
                                                child: BlocBuilder<
                                                        UserProfileBloc,
                                                        UserProfileState>(
                                                    builder: (context, state) {
                                                  if (state.profile.avatar
                                                          ?.isNotEmpty ??
                                                      false)
                                                    return UserAvatar.fromNetwork(
                                                        size: 24,
                                                        online: state.online,
                                                        avatarUrl: ProfileApi
                                                                .profileAvatar +
                                                            state.profile
                                                                .avatar!);
                                                  else
                                                    return EmptyAvatar(
                                                      online: state.online,
                                                      size: 24,
                                                    );
                                                }),
                                              )),
                                    ),
                                  ),
                                )
                            ]),
                      ),
                    if ((projectState.project?.participants?.length ?? 0) >= 5)
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: const Text('+5 чел.'),
                      )
                    //Spacer()
                  ],
                ),
              ),
            );
          }),
    );
  }
}

class ProjectParticipantsScreen extends StatefulWidget {
  const ProjectParticipantsScreen(
      {Key? key, required this.isAdmin, required this.projectCubit})
      : super(key: key);

  final bool isAdmin;
  final ProjectCubit projectCubit;

  static PageRouteBuilder route(
          {required bool isAdmin, required ProjectCubit project}) =>
      PageRouteBuilder(
          pageBuilder: (context, f1, f2) => ProjectParticipantsScreen(
                isAdmin: isAdmin,
                projectCubit: project,
              ),
          transitionsBuilder: slideTransition);

  @override
  State<ProjectParticipantsScreen> createState() =>
      _ProjectParticipantsScreenState();
}

class _ProjectParticipantsScreenState extends State<ProjectParticipantsScreen> {
  var _showPopup = false;
  var _showDatePopup = false;

  final _captionProjectEventController = TextEditingController();
  final _descriptionProjectEventController = TextEditingController();

  var _selectedDateTime = DateTime.now();
  var _selectedTimeOfDay = TimeOfDay.now();

  String? _deletingId, _deletingName;

  List<int>? _invites;

  void togglePopup() {
    setState(() {
      _showPopup = false;
      _showDatePopup = false;
    });
  }

  void toggleDeletePopup(String uid, String name) {
    setState(() {
      _showPopup = !_showPopup;
      _deletingId = uid;
      _deletingName = name;
    });
  }

  void fetchInvites() async {
    final invites = await NewsfeedRepository()
        .showProjectInvites(pid: widget.projectCubit.state.project?.id ?? 0);
    setState(() {
      _invites = invites;
    });
  }

  @override
  void initState() {
    if (widget.isAdmin) fetchInvites();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ProfileAppBar(
        title: Text(
          'Участники проекта',
          style: appBarTextStyle,
        ),
        rightmostButton: widget.isAdmin
            ? IconButton(
                icon: ImageIcon(AssetImage('images/icons/meeting.png')),
                color: Colors.white,
                onPressed: () {
                  setState(() {
                    _showDatePopup = !_showDatePopup;
                  });
                },
              )
            : null,
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              behavior: HitTestBehavior.translucent,
              child: BlocBuilder<ProjectCubit, ProjectState>(
                  bloc: widget.projectCubit,
                  builder: (context, state) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if (state.project?.tutors.isNotEmpty ?? false)
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 12.0, left: 16, bottom: 12.0),
                            child: Text(
                              'Наставник',
                              style: profileTextStyle.copyWith(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16),
                            ),
                          ),
                        for (var tutor in (state.project?.tutors ??
                            const <ProjectParticipant>[]))
                          ProjectProfileRow(
                            key: ValueKey(tutor.id),
                            user: tutor,
                            toggleUserDeletingCallback: toggleDeletePopup,
                            changeUserStatusCallback: changeUserStatus,
                            userRoleCallback: userRoleCallback,
                            amIAdmin: widget.isAdmin,
                          ),
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 12.0, left: 16, bottom: 12.0),
                          child: Text(
                            'Рабочая группа',
                            style: profileTextStyle.copyWith(
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 16),
                          ),
                        ),
                        for (var user in (state.project?.users ??
                            const <ProjectParticipant>[]))
                          ProjectProfileRow(
                              key: ValueKey(user.id),
                              userRoleCallback: userRoleCallback,
                              user: user,
                              changeUserStatusCallback: changeUserStatus,
                              toggleUserDeletingCallback: toggleDeletePopup,
                              amIAdmin: widget.isAdmin),
                        if (_invites?.isNotEmpty ?? false)
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 12.0, left: 16, bottom: 12.0),
                            child: Text(
                              'Заявки',
                              style: profileTextStyle.copyWith(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16),
                            ),
                          ),
                        if (_invites?.isNotEmpty ?? false)
                          for (var user in _invites!)
                            BlocProvider<UserProfileBloc>(
                                create: (context) => UserProfileBloc.fromLid(
                                    lid: user.toString(), loadFromHive: false),
                                child: Builder(builder: (context) {
                                  return BlocBuilder<UserProfileBloc,
                                          UserProfileState>(
                                      builder: (context, userProfileState) {
                                    final profile = userProfileState.profile;
                                    return ProfileRow(
                                      surname: profile.surname,
                                      profileAvatar: profile.avatar,
                                      userId: user.toString(),
                                      rightSuffix: Row(
                                        children: [
                                          IconButton(
                                              onPressed: () async {
                                                await widget.projectCubit
                                                    .manageInvite(user,
                                                        accept: true);
                                                fetchInvites();
                                              },
                                              icon: ImageIcon(AssetImage(
                                                  'images/icons/dobavit.png'))),
                                          IconButton(
                                              onPressed: () async {
                                                await widget.projectCubit
                                                    .manageInvite(user,
                                                        accept: false);
                                                fetchInvites();
                                              },
                                              icon: ImageIcon(AssetImage(
                                                  'images/icons/chat_options/forbid.png')))
                                        ],
                                      ),
                                      status: FetchingStatus.Loaded,
                                      name: profile.name,
                                      online: userProfileState.online,
                                      caption: '',
                                    );
                                  });
                                })),
                        Container(
                          color: Colors.grey,
                          height: 1,
                          margin: const EdgeInsets.fromLTRB(
                            12,
                            12,
                            12,
                            5,
                          ),
                        ),
                        if (widget.isAdmin)
                          MaterialButton(
                            onPressed: () {
                              context.read<BottomBarBloc>().go(
                                  SubscriptionsScreen.route(
                                      preselectedPeople: state
                                          .project?.participants
                                          ?.map((e) => e.id)
                                          .toList(),
                                      selectingPeopleCallback: (list) async {
                                        await widget.projectCubit
                                            .syncParticipants(list);
                                        context.read<NewsfeedCubit>().init(
                                            NewsfeedPageEnum.Projects,
                                            forceReload: true);
                                      }));
                            },
                            child: Row(children: [
                              Icon(
                                Icons.add_circle,
                                color: GradientColors.pink,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 10.0),
                                child: Text('Добавить участников'),
                              )
                            ]),
                          )
                      ],
                    );
                  }),
            ),
          ),
          Positioned.fill(
              child: Offstage(
            offstage: (!_showPopup && !_showDatePopup),
            child: GestureDetector(
                onTap: togglePopup,
                child: Container(
                  color: GradientColors.updatingIndicatorColor,
                )),
          )),
          DeleteDialog(
            isDeleting: !_showPopup,
            noCallback: (_) {
              togglePopup();
              setState(() {
                _deletingId = null;
                _deletingName = null;
              });
            },
            yesCallback: (_) {
              widget.projectCubit.deleteParticipant(uid: _deletingId!, text: _);
              togglePopup();
            },
            enableMultilineInput: true,
            noCaption: 'Отменить',
            yesCaption: 'Исключить',
            text:
                'Вы действительно хотите исключить пользователя ${_deletingName} из проекта?',
          ),
          CreateProjectEventDialog(
            captionController: _captionProjectEventController,
            descriptionController: _descriptionProjectEventController,
            selectedDateTime: _selectedDateTime,
            selectedTimeOfDay: _selectedTimeOfDay,
            isCreating: !_showDatePopup,
            dateTimeCallback: (date) {
              setState(() {
                if (date != null) _selectedDateTime = date;
              });
            },
            timeOfDayCallback: (time) {
              if (time != null)
                setState(() {
                  _selectedTimeOfDay = time;
                });
            },
          )
        ],
      ),
    );
  }

  void changeUserStatus(String status, int uid) {
    widget.projectCubit.editMembers(status: status, uid: uid);
  }

  void userRoleCallback(int uid) {
    widget.projectCubit.toggleAdmin(uid: uid);
  }
}
