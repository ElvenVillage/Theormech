import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/newsfeed/bloc/newsfeed_cubit.dart';
import 'package:teormech/profile/newsfeed/bloc/project_bloc.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';
import 'package:teormech/profile/newsfeed/model/project_state.dart';
import 'package:teormech/profile/newsfeed/rep/newsfeed_repository.dart';
import 'package:teormech/profile/newsfeed/ui/newsfeed_screen.dart';
import 'package:teormech/profile/newsfeed/ui/pages/projects/project_page.dart';
import 'package:teormech/profile/newsfeed/ui/widgets/create_post_button.dart';
import 'package:teormech/profile/newsfeed/ui/widgets/newsfeed_filter.dart';
import 'package:teormech/profile/newsfeed/ui/widgets/project_text_field.dart';
import 'package:teormech/profile/profile/model/portfolio_state.dart';
import 'package:teormech/profile/profile/rep/user_portfolio_rep.dart';
import 'package:teormech/profile/profile/ui/widgets/tags_container.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/styles/transitions/slide_fade.dart';
import 'package:teormech/widgets/calendar_input.dart';
import 'package:teormech/widgets/multilne_text_field.dart';

class CreateProjectScreen extends StatefulWidget {
  const CreateProjectScreen(
      {Key? key, this.isEditing = false, this.projectCubit})
      : super(key: key);

  final bool isEditing;
  final ProjectCubit? projectCubit;

  static PageRouteBuilder route() => PageRouteBuilder(
      pageBuilder: (context, f1, f3) => CreateProjectScreen(isEditing: false),
      transitionsBuilder: slideTransition);

  static PageRouteBuilder routeEditing(ProjectCubit project) =>
      PageRouteBuilder(
          pageBuilder: (context, f1, f3) => CreateProjectScreen(
                isEditing: true,
                projectCubit: project,
              ),
          transitionsBuilder: slideTransition);

  @override
  State<CreateProjectScreen> createState() => _CreateProjectScreenState();
}

class _CreateProjectScreenState extends State<CreateProjectScreen> {
  final captionController = TextEditingController();
  final dateStartController = TextEditingController();
  final dateEndController = TextEditingController();
  final descriptionController = TextEditingController();
  final tagController = TextEditingController();

  final _dropdownGlobalKey = GlobalKey<DropdownSearchState>();

  final picker = ImagePicker();
  String? fetchedAvatar;

  File? pickedFile;
  bool loading = false;

  var tagList = <PortfolioTag>[];

  @override
  void initState() {
    super.initState();

    if (widget.projectCubit != null &&
        widget.projectCubit?.state.project != null) {
      fetchedAvatar = widget.projectCubit!.state.project?.image;
      captionController.text = widget.projectCubit!.state.project!.title;
      descriptionController.text =
          widget.projectCubit!.state.project!.description;
      dateEndController.text =
          widget.projectCubit!.state.project!.end.toString();
      dateStartController.text =
          widget.projectCubit!.state.project!.start.toString();
    }
  }

  @override
  void dispose() {
    captionController.dispose();
    dateStartController.dispose();
    dateEndController.dispose();
    descriptionController.dispose();
    tagController.dispose();

    super.dispose();
  }

  final textStyle = TextStyle(fontWeight: FontWeight.w500);

  void handleCalendarClick(TextEditingController controller) async {
    final date = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2015),
        lastDate: DateTime(2030));
    if (date != null) {
      controller.text = date.toString().substring(0, 10);
    }
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Stack(
        children: [
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              FocusScope.of(context).unfocus();
            },
            child: Scaffold(
              backgroundColor: GradientColors.backWhite,
              appBar: ProfileAppBar(
                  leftAlignedTitle: true,
                  title: Text(
                    !widget.isEditing
                        ? 'Создание проекта'
                        : 'Редактирование проекта',
                    style: appBarTextStyle,
                  )),
              body: ListView(children: [
                ProjectTextField(
                    controller: captionController,
                    textStyle: textStyle,
                    caption: 'Название'),
                Padding(
                  padding: const EdgeInsets.only(top: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        flex: 3,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 16.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Дата начала проекта', style: textStyle),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 32.0,
                                ),
                                child: Text('Дата окончания проекта',
                                    style: textStyle),
                              )
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 16.0),
                              child: CalendarInput(
                                controller: dateStartController,
                                handleDate: (date) {
                                  if (date != null) {
                                    dateStartController.text =
                                        date.toString().substring(0, 10);
                                  }
                                },
                              ),
                            ),
                            Padding(
                                padding: const EdgeInsets.only(
                                    top: 16.0, right: 16.0),
                                child: CalendarInput(
                                  controller: dateEndController,
                                  handleDate: (date) {
                                    if (date != null) {
                                      dateEndController.text =
                                          date.toString().substring(0, 10);
                                    }
                                  },
                                ))
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 16,
                    left: 16,
                    right: 16,
                  ),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Описание проекта', style: textStyle),
                        Padding(
                          padding: const EdgeInsets.only(top: 6.0),
                          child: MultilineTextField(
                            controller: descriptionController,
                          ),
                        )
                      ]),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16, left: 16),
                  child: Row(
                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                          child: Text('Фотография проекта', style: textStyle)),
                      Expanded(
                        child: Center(
                          child: InkWell(
                            onTap: () async {
                              final image = await picker.getImage(
                                  source: ImageSource.gallery);
                              if (image != null) {
                                setState(() {
                                  pickedFile = File(image.path);
                                });
                              }
                            },
                            child: Text(
                              'Загрузить фото',
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  color: GradientColors.pink),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
                  child: Row(children: [
                    ConstrainedBox(
                      constraints: BoxConstraints(
                          maxHeight: width * 0.45, maxWidth: width * 0.9),
                      child: pickedFile == null && fetchedAvatar == null
                          ? Image(
                              image: AssetImage('images/blank_photo.png'),
                            )
                          : Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                  if (pickedFile != null)
                                    Image(image: FileImage(pickedFile!))
                                  else
                                    CachedNetworkImage(
                                        imageUrl: ProjectsApi.getProjectPhoto +
                                            fetchedAvatar!)
                                ]),
                    ),
                  ]),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(16, 16, 8, 8),
                  child: Row(
                    children: [
                      Text('Выберите теги'),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SizedBox(
                            height: 40,
                            child: DropdownSearch<String>(
                                searchBoxController: tagController,
                                key: _dropdownGlobalKey,
                                mode: Mode.BOTTOM_SHEET,
                                onChanged: (str) {
                                  // ignore: unnecessary_null_comparison
                                  if (str == null) return;
                                  if (widget.isEditing) {
                                    widget.projectCubit!.addTag(str);
                                  } else {
                                    setState(() {
                                      tagList.add(PortfolioTag(
                                          tag: str,
                                          level: ProfessionalLevel.Newsfeed));
                                    });
                                  }

                                  tagController.text = '';
                                  _dropdownGlobalKey.currentState
                                      ?.changeSelectedItem(null);
                                  FocusScope.of(context).unfocus();
                                },
                                isFilteredOnline: true,
                                onFind: (mask) {
                                  return UserPortfolioRepository().filterTags(
                                      mask: mask,
                                      tags: tagList.map((e) => e.tag).toList());
                                },
                                showSearchBox: true,
                                showAsSuffixIcons: true,
                                dropdownBuilder: (context, a, b) => Text(
                                      b,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                popupItemBuilder: NewsfeedFilter.popupBuilder,
                                autoFocusSearchBox: true,
                                dropdownSearchDecoration:
                                    NewsfeedFilter.decoration,
                                selectedItem: ''),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: !widget.isEditing
                      ? TagsContainer(
                          tags: tagList,
                          isCompact: true,
                        )
                      : BlocBuilder<ProjectCubit, ProjectState>(
                          bloc: widget.projectCubit!,
                          builder: (context, state) => TagsContainer(
                                tags: state.editedTags,
                                isCompact: true,
                                tapCallback: widget.projectCubit!.removeTag,
                              )),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(8.0, 8.0, 16.0, 16.0),
                  child: Row(
                    children: [
                      Spacer(flex: 3),
                      CreatePostButton(
                          onPressed: () async {
                            if (!widget.isEditing)
                              _createProject();
                            else {
                              setState(() {
                                loading = true;
                              });
                              await widget.projectCubit!.editProject(
                                  description: descriptionController.text,
                                  title: captionController.text,
                                  end: dateEndController.text,
                                  file: pickedFile?.path,
                                  start: dateStartController.text);
                              context.read<NewsfeedCubit>().init(
                                  NewsfeedPageEnum.Projects,
                                  forceReload: true);

                              setState(() {
                                loading = false;
                              });

                              context.read<BottomBarBloc>().pop();
                            }
                          },
                          caption: widget.isEditing
                              ? 'Сохранить проект'
                              : 'Создать проект'),
                    ],
                  ),
                )
              ]),
            ),
          ),
          if (widget.isEditing)
            Positioned.fill(
              child: (loading)
                  ? Container(
                      color: GradientColors.updatingIndicatorColor,
                      child: const Center(child: CircularProgressIndicator()),
                    )
                  : const SizedBox.shrink(),
            )
        ],
      ),
    );
  }

  void _createProject() async {
    setState(() {
      loading = true;
    });
    final pid = await NewsfeedRepository().createProject(
        title: captionController.text,
        description: descriptionController.text,
        end: DateTime.parse(dateEndController.text),
        start: DateTime.tryParse(dateStartController.text));

    if (pid != null) {
      if (pickedFile != null) {
        await NewsfeedRepository()
            .uploadProjectPhoto(pid: pid, photo: pickedFile!.path);
      }
      for (var tag in tagList)
        await NewsfeedRepository()
            .pinTag(pid: pid, tag: tag.tag, mode: RequestMode.Project);
      context
          .read<NewsfeedCubit>()
          .init(NewsfeedPageEnum.Projects, forceReload: true);
      context.read<BottomBarBloc>()
        ..pop()
        ..go(ProjectPage.pidRoute(pid: pid));
    }
    setState(() {
      loading = false;
    });
  }
}
