import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/profile/newsfeed/bloc/newsfeed_cubit.dart';
import 'package:teormech/profile/newsfeed/bloc/project_bloc.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';
import 'package:teormech/profile/newsfeed/rep/newsfeed_repository.dart';
import 'package:teormech/profile/newsfeed/ui/newsfeed_screen.dart';
import 'package:teormech/profile/newsfeed/ui/widgets/rounded_material_button.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';
import 'package:teormech/profile/profile/rep/user_profile_rep.dart';
import 'package:teormech/profile/profile/ui/widgets/tags_container.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/edge_alert_notification.dart';

class ProjectContent extends StatefulWidget {
  ProjectContent(
      {Key? key,
      required this.project,
      this.expanded = false,
      this.invited = false})
      : super(key: key);

  final ProjectModel project;
  final bool expanded;
  final bool invited;

  @override
  State<ProjectContent> createState() => _ProjectContentState();
}

class _ProjectContentState extends State<ProjectContent> {
  final titleFontStyle = TextStyle(fontWeight: FontWeight.w500, fontSize: 16);

  final captionFontStyle = TextStyle(color: Colors.grey.shade600);

  final dataFontStyle =
      TextStyle(fontWeight: FontWeight.w500, color: Colors.black);

  final controller = ExpandableController();

  @override
  Widget build(BuildContext context) {
    final body = Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              widget.project.title,
              style: titleFontStyle,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          LayoutBuilder(builder: (context, constraints) {
            final width = constraints.maxWidth;

            return Row(
              children: [
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: widget.expanded
                          ? Container(
                              height: width * 0.4,
                              child: widget.project.image != null
                                  ? Image.network(
                                      ProjectsApi.getProjectPhoto +
                                          widget.project.image!,
                                      fit: BoxFit.cover,
                                      width: width * 0.4,
                                    )
                                  : Image(
                                      image: AssetImage(
                                          'images/empty_project.png'),
                                      fit: BoxFit.cover,
                                      width: width * 0.4,
                                    ),
                            )
                          : SizedBox(
                              height: width * 0.3,
                              width: width * 0.4,
                              child: widget.project.image != null
                                  ? Image.network(
                                      ProjectsApi.getProjectPhoto +
                                          widget.project.image!,
                                      fit: BoxFit.cover,
                                    )
                                  : Image(
                                      image: AssetImage(
                                          'images/empty_project.png'),
                                      fit: BoxFit.cover,
                                    ),
                            ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (widget.project.tutors.isNotEmpty)
                        Padding(
                          padding: const EdgeInsets.only(bottom: 4.0),
                          child: Text('Наставник:', style: captionFontStyle),
                        ),
                      Padding(
                        padding: widget.expanded
                            ? const EdgeInsets.only(bottom: 16.0)
                            : const EdgeInsets.only(bottom: 8.0),
                        child: widget.project.tutor != null
                            ? BlocProvider<UserProfileBloc>(
                                create: (context) => UserProfileBloc.fromLid(
                                    lid: widget.project.tutor!.id.toString(),
                                    loadFromHive: false),
                                child: Builder(builder: (context) {
                                  return BlocBuilder<UserProfileBloc,
                                          UserProfileState>(
                                      builder: (context, profileState) {
                                    return Text(profileState.fullName,
                                        style: dataFontStyle);
                                  });
                                }))
                            : Text(' — ', style: dataFontStyle),
                      ),
                      if (!widget.expanded)
                        Padding(
                          padding: const EdgeInsets.only(bottom: 8.0),
                          child: RichText(
                              text: TextSpan(children: [
                            TextSpan(
                                text: 'Рабочая группа: ',
                                style: captionFontStyle),
                            TextSpan(
                                text:
                                    '${widget.project.participants?.length.toString()} чел.',
                                style: dataFontStyle)
                          ])),
                        ),
                      if (widget.project.dateString.isNotEmpty)
                        Padding(
                          padding: const EdgeInsets.only(bottom: 4.0),
                          child: Text(
                            'Сроки реализации:',
                            style: captionFontStyle,
                          ),
                        ),
                      if (widget.project.dateString.isNotEmpty)
                        Text(widget.project.dateString, style: dataFontStyle),
                      if (widget.expanded)
                        Padding(
                          padding: const EdgeInsets.only(top: 24.0),
                          child: SizedBox(
                            height: 30,
                            child: RoundedMaterialButton(
                                color: GradientColors.pink,
                                callback: () async {
                                  if (widget.invited) {
                                    context
                                        .read<ProjectCubit>()
                                        .acceptInvite(pid: widget.project.id);
                                    await context.read<NewsfeedCubit>().init(
                                        NewsfeedPageEnum.Projects,
                                        forceReload: true);
                                  } else {
                                    final includes = widget.project.isMember(
                                        int.parse(UserProfileRepository()
                                            .credentials
                                            .id));
                                    if (includes) {
                                      await context
                                          .read<ProjectCubit>()
                                          .leaveProject();
                                      await context.read<NewsfeedCubit>().init(
                                          NewsfeedPageEnum.Projects,
                                          forceReload: true);
                                    } else {
                                      final response =
                                          await NewsfeedRepository()
                                              .requestToProject(
                                                  widget.project.id);
                                      showEdgeAlertNotification(
                                          okText: 'Заявка отправлена',
                                          errorText:
                                              'Не удалось отправить заявку',
                                          context: context,
                                          result: response);
                                    }
                                  }
                                },
                                caption: widget.invited
                                    ? 'Принять приглашение       '
                                    : widget.project.isMember(int.parse(
                                            UserProfileRepository()
                                                .credentials
                                                .id))
                                        ? 'Покинуть проект       '
                                        : 'Подать заявку'),
                          ),
                        ),
                    ],
                  ),
                )
              ],
            );
          }),
          if (widget.expanded)
            Padding(
              padding: const EdgeInsets.only(left: 8.0, top: 14.0),
              child: Text(
                'Описание проекта',
                style: captionFontStyle,
              ),
            ),
          if (!widget.expanded)
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                widget.project.description,
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
              ),
            )
          else
            InkWell(
              onTap: () {
                controller.toggle();
              },
              child: ExpandablePanel(
                  controller: controller,
                  collapsed: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RichText(
                        text: widget.project.description.length > 200
                            ? TextSpan(
                                text: widget.project.description
                                        .substring(0, 200) +
                                    '...\n',
                                style: TextStyle(color: Colors.black),
                                children: [
                                    TextSpan(
                                        text: 'Читать дальше',
                                        style: TextStyle(
                                            color: GradientColors.pink,
                                            decoration:
                                                TextDecoration.underline))
                                  ])
                            : TextSpan(
                                text: widget.project.description,
                                style: TextStyle(color: Colors.black),
                              ),
                      )),
                  expanded: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      widget.project.description,
                    ),
                  )),
            ),
          if (widget.project.tags?.isNotEmpty ?? false)
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TagsContainer(
                tags: widget.project.tags!,
                isCompact: true,
              ),
            ),
        ],
      ),
    );

    return body;
  }
}
