import 'package:flutter/material.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_list_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_list_page_cubit.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';
import 'package:teormech/profile/newsfeed/ui/pages/jobs/job_apply_screen.dart';
import 'package:teormech/profile/profile/ui/widgets/tags_container.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/styles/transitions/slide_fade.dart';

class JobContent extends StatelessWidget {
  const JobContent({Key? key, required this.full, required this.job})
      : super(key: key);

  final bool full;
  final JobModel job;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(12, 8, 12, 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 5.0),
            child: Text(job.position,
                style: const TextStyle(fontWeight: FontWeight.bold)),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Text(job.type, style: const TextStyle(color: Colors.grey)),
          ),
          Geotag(caption: job.company),
          Padding(
            padding: const EdgeInsets.only(
              top: 8.0,
            ),
            child: Padding(
              padding: const EdgeInsets.only(left: 5.0, bottom: 5.0),
              child: Text(
                job.description,
                maxLines: 6,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
          if (job.tags.isNotEmpty)
            Padding(
              padding: const EdgeInsets.only(bottom: 8, left: 5),
              child: Text('#теги',
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.grey)),
            ),
          if (job.tags.isNotEmpty)
            TagsContainer(
              tags: job.tags,
              isCompact: true,
            ),
          SizedBox(height: 10),
          Container(
              decoration: const BoxDecoration(
                  border: Border(bottom: BorderSide(color: Colors.grey)))),
          if (full)
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                      child: Text('Вы можете прямо сейчас',
                          style: h2DrawerGreyText)),
                  Container(
                    height: 40,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30.0),
                        color: GradientColors.pink),
                    child: MaterialButton(
                      minWidth: 120,
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      onPressed: () {
                        context.read<BottomBarBloc>().go(MaterialPageRoute(
                            builder: (_) => JobApplyScreen(
                                  job: job,
                                )));
                      },
                      child: Text('Откликнуться',
                          textAlign: TextAlign.center,
                          style: h1TextStyle.copyWith(
                              color: Colors.white,
                              fontSize: 14,
                              fontWeight: FontWeight.bold)),
                    ),
                  )
                ],
              ),
            )
        ],
      ),
    );
  }
}

class Geotag extends StatelessWidget {
  const Geotag({Key? key, required this.caption}) : super(key: key);

  final String caption;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          Icons.location_on,
          color: GradientColors.pink,
        ),
        Text(caption)
      ],
    );
  }
}

class JobScreen extends StatelessWidget {
  const JobScreen({Key? key, required this.job}) : super(key: key);

  final JobModel job;

  static PageRouteBuilder route(JobModel job) {
    return PageRouteBuilder(
        transitionsBuilder: slideTransition,
        pageBuilder: (context, f1, f2) => JobScreen(
              job: job,
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ProfileAppBar(
        title: Text(
          'Вакансия',
          style: appBarTextStyle,
        ),
        rightmostButton: IconButton(
          icon: ImageIcon(AssetImage('images/icons/share.png')),
          color: Colors.white,
          iconSize: 32,
          onPressed: () {
            context.read<ChatListBloc>()
              ..addNewsfeedObjectForForwarding(job)
              ..pageCubit.selectPage(ChatListPage.Chats);
            context.read<BottomBarBloc>().setTab(BottomBars.Chat);
          },
        ),
      ),
      body: JobContent(full: true, job: job),
    );
  }
}
