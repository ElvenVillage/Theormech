import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';
import 'package:teormech/profile/newsfeed/rep/newsfeed_repository.dart';
import 'package:teormech/profile/newsfeed/ui/pages/jobs/job_screen.dart';
import 'package:teormech/profile/newsfeed/ui/widgets/post_documents.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/edge_alert_notification.dart';
import 'package:teormech/styles/text.dart';

class JobApplyScreen extends StatefulWidget {
  const JobApplyScreen({Key? key, required this.job}) : super(key: key);

  final JobModel job;

  @override
  State<JobApplyScreen> createState() => _JobApplyScreenState();
}

class _JobApplyScreenState extends State<JobApplyScreen> {
  final _controller = TextEditingController();

  List<PlatformFile> attachedFiles = [];
  bool _syncing = false;

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _sendResponse() async {
    if (_syncing) return;

    setState(() {
      _syncing = true;
    });

    final result = await NewsfeedRepository()
        .applyToJob(vacancy_id: widget.job.id.toString());

    setState(() {
      _syncing = false;
    });
    showEdgeAlertNotification(
        okText: 'Ваша заявка отправлена на рассмотрение',
        errorText: 'Не удалось отправить заявку',
        context: context,
        result: result);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
          appBar: ProfileAppBar(
            title: Text('Отклик на вакансию', style: appBarTextStyle),
          ),
          body: SizedBox(
            height: double.infinity,
            child: Stack(
              children: [
                SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Geotag(caption: widget.job.company),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: RichText(
                              text: TextSpan(
                                  text: 'Тема:',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black),
                                  children: [
                                TextSpan(
                                    text: '  ${widget.job.position}',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.normal))
                              ])),
                        ),
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text('Сопроводительное письмо',
                                style: TextStyle(fontWeight: FontWeight.bold))),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(8.0, 2.0, 8.0, 8.0),
                          child: TextField(
                            controller: _controller,
                            minLines: 5,
                            maxLines: null,
                            decoration: InputDecoration(
                              isDense: true,
                              isCollapsed: true,
                              contentPadding: const EdgeInsets.fromLTRB(
                                  15.0, 10.0, 15.0, 10.0),
                              errorStyle: TextStyle(height: 0),
                              filled: true,
                              hintStyle: TextStyle(fontSize: 14),
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(24.0),
                                  borderSide:
                                      BorderSide(color: Colors.grey.shade400)),
                            ),
                          ),
                        ),
                        if (attachedFiles.isNotEmpty)
                          PostDocumentsBlock(
                              editable: true,
                              files: attachedFiles,
                              removeCallback: (int idx) {
                                setState(() {
                                  attachedFiles.removeAt(idx);
                                });
                              }),
                        MaterialButton(
                          onPressed: () async {
                            final pickedFiles = await FilePicker.platform
                                .pickFiles(allowMultiple: true);
                            if (pickedFiles != null) {
                              setState(() {
                                attachedFiles = [
                                  ...attachedFiles,
                                  ...pickedFiles.files
                                ];
                              });
                            }
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  Icon(Icons.attach_file),
                                  Text('Прикрепить документы')
                                ],
                              ),
                              Container(
                                height: 40,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30.0),
                                    color: GradientColors.pink),
                                child: MaterialButton(
                                  minWidth: 120,
                                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                  onPressed: _sendResponse,
                                  child: Text('Отправить',
                                      textAlign: TextAlign.center,
                                      style: h1TextStyle.copyWith(
                                          color: Colors.white,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold)),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                if (_syncing)
                  Positioned.fill(
                      child: Container(
                    color: GradientColors.updatingIndicatorColor.withAlpha(30),
                    child: Center(
                      child: const SizedBox(
                        height: 120,
                        width: 120,
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  )),
              ],
            ),
          )),
    );
  }
}
