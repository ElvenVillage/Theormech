import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/ui/widgets/gradient_button.dart';
import 'package:teormech/auth/ui/widgets/register_text_field.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/newsfeed/bloc/newsfeed_cubit.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';
import 'package:teormech/profile/newsfeed/rep/newsfeed_repository.dart';
import 'package:teormech/profile/newsfeed/ui/newsfeed_screen.dart';
import 'package:teormech/profile/newsfeed/ui/widgets/newsfeed_filter.dart';
import 'package:teormech/profile/profile/model/portfolio_state.dart';
import 'package:teormech/profile/profile/rep/user_portfolio_rep.dart';
import 'package:teormech/profile/profile/ui/widgets/tags_container.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/styles/transitions/slide_fade.dart';
import 'package:teormech/widgets/modal_point.dart';
import 'package:teormech/widgets/multilne_text_field.dart';

class CreateJobMultilineTextField extends StatelessWidget {
  const CreateJobMultilineTextField(
      {Key? key, required this.controller, required this.caption})
      : super(key: key);

  final TextEditingController controller;
  final String caption;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(caption),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: MultilineTextField(
              controller: controller,
            ),
          ),
        ],
      ),
    );
  }
}

class CreateJobScreen extends StatefulWidget {
  const CreateJobScreen({Key? key}) : super(key: key);

  @override
  State<CreateJobScreen> createState() => _CreateJobScreenState();

  static PageRouteBuilder route() => PageRouteBuilder(
      pageBuilder: (context, f1, f3) => CreateJobScreen(),
      transitionsBuilder: slideTransition);
}

class _CreateJobScreenState extends State<CreateJobScreen> {
  final _jobPositionController = TextEditingController();
  final _jobCompanyController = TextEditingController();
  final _jobTypeController = TextEditingController();

  final _jobDescriptionController = TextEditingController();
  final _jobRequirementsController = TextEditingController();
  final _jobConditionsController = TextEditingController();

  final _tagController = TextEditingController();
  final _dropdownGlobalKey = GlobalKey<DropdownSearchState>();

  var syncing = false;

  final List<PortfolioTag> _tags = [];

  @override
  void dispose() {
    super.dispose();
    _jobCompanyController.dispose();
    _jobPositionController.dispose();
    _jobTypeController.dispose();
    _jobDescriptionController.dispose();
    _jobRequirementsController.dispose();
    _jobConditionsController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ProfileAppBar(
        leftAlignedTitle: true,
        title: Text('Создание вакансии', style: appBarTextStyle),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(top: 16.0, left: 20, right: 20),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 8.0, vertical: 16.0),
                              child: Text('Должность'),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 8.0, vertical: 16.0),
                              child: Text('Компания'),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 8.0, vertical: 16.0),
                              child: Text('Тип вакансии'),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: RegisterTextField(
                                  controller: _jobPositionController),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: RegisterTextField(
                                  controller: _jobCompanyController),
                            ),
                            InkWell(
                              onTap: () {
                                showModalSheetTypes(context);
                              },
                              child: Container(
                                height: 48,
                                padding: const EdgeInsets.only(top: 12.0),
                                child: RegisterTextField(
                                  readOnly: true,
                                  controller: _jobTypeController,
                                  suffix: TextButton(
                                      child: Icon(Icons.more_vert),
                                      onPressed: () {
                                        showModalSheetTypes(context);
                                      }),
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                  CreateJobMultilineTextField(
                      controller: _jobDescriptionController,
                      caption: 'Описание вакансии'),
                  CreateJobMultilineTextField(
                      controller: _jobRequirementsController,
                      caption: 'Требования к кандидату'),
                  CreateJobMultilineTextField(
                      controller: _jobConditionsController, caption: 'Условия'),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(16, 16, 8, 8),
                    child: Row(
                      children: [
                        Text('Выберите теги'),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: SizedBox(
                              height: 40,
                              child: DropdownSearch<String>(
                                  searchBoxController: _tagController,
                                  key: _dropdownGlobalKey,
                                  mode: Mode.BOTTOM_SHEET,
                                  onChanged: (str) {
                                    // ignore: unnecessary_null_comparison
                                    if (str == null) return;

                                    setState(() {
                                      _tags.add(PortfolioTag(
                                          tag: str,
                                          level: ProfessionalLevel.Newsfeed));
                                    });

                                    _tagController.text = '';
                                    _dropdownGlobalKey.currentState
                                        ?.changeSelectedItem(null);
                                    FocusScope.of(context).unfocus();
                                  },
                                  isFilteredOnline: true,
                                  onFind: (mask) {
                                    return UserPortfolioRepository().filterTags(
                                        mask: mask,
                                        tags: _tags.map((e) => e.tag).toList());
                                  },
                                  showSearchBox: true,
                                  showAsSuffixIcons: true,
                                  dropdownBuilder: (context, a, b) => Text(
                                        b,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                  popupItemBuilder: NewsfeedFilter.popupBuilder,
                                  autoFocusSearchBox: true,
                                  dropdownSearchDecoration:
                                      NewsfeedFilter.decoration,
                                  selectedItem: ''),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: TagsContainer(
                        tags: _tags,
                        isCompact: true,
                      )),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        GradientMaterialButton(
                            text: 'Создать',
                            onTapCallback: () async {
                              if (syncing) return;

                              final job = JobModel(
                                  id: 1,
                                  conditions: _jobConditionsController.text,
                                  requirements: _jobRequirementsController.text,
                                  description: _jobDescriptionController.text,
                                  type: 'type',
                                  tags: _tags,
                                  company: _jobCompanyController.text,
                                  position: _jobPositionController.text,
                                  salary: '0',
                                  status: 'status',
                                  responses: 'responses');
                              await NewsfeedRepository()
                                  .createVacancy(vacancy: job);
                              context.read<NewsfeedCubit>().init(
                                  NewsfeedPageEnum.Jobs,
                                  forceReload: true);
                              syncing = false;

                              Future.delayed(Duration(milliseconds: 300)).then(
                                  (_) => context.read<BottomBarBloc>().pop());
                            },
                            from: GradientColors.pink,
                            to: GradientColors.pink)
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          if (syncing)
            Positioned.fill(
                child: Container(
              color: GradientColors.updatingIndicatorColor.withAlpha(30),
              child: Center(
                child: const SizedBox(
                  height: 120,
                  width: 120,
                  child: CircularProgressIndicator(),
                ),
              ),
            )),
        ],
      ),
    );
  }

  Future<dynamic> showModalSheetTypes(BuildContext context) {
    return showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            height: MediaQuery.of(context).size.height / 3,
            child: Column(
              children: [
                for (var i in JobType.values)
                  ModalPoint(
                    text: i.toReadable(),
                    callback: () {
                      _jobTypeController.text = i.toReadable();
                      Navigator.of(context).pop();
                    },
                  ),
              ],
            ),
          );
        });
  }
}
