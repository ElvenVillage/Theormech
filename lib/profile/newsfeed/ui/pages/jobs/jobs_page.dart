import 'package:flutter/material.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/newsfeed/bloc/newsfeed_cubit.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_state.dart';
import 'package:teormech/profile/newsfeed/ui/pages/jobs/job_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class JobsNewsfeedPage extends StatelessWidget {
  const JobsNewsfeedPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewsfeedCubit, NewsfeedState>(
      builder: (context, state) {
        if (state.filteredJobs != null)
          return ListView.builder(
            itemBuilder: (context, idx) {
              return InkWell(
                child: JobContent(full: false, job: state.filteredJobs![idx]),
                onTap: () {
                  context.read<BottomBarBloc>().go(MaterialPageRoute(
                      builder: (_) =>
                          JobScreen(job: state.filteredJobs![idx])));
                },
              );
            },
            itemCount: state.filteredJobs?.length ?? 0,
          );
        return const SizedBox.shrink();
      },
    );
  }
}
