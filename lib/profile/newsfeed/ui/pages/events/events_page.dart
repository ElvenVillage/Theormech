import 'package:flutter/material.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/profile/newsfeed/bloc/newsfeed_cubit.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_state.dart';
import 'package:teormech/profile/newsfeed/ui/pages/events/event_screen.dart';

class EventsNewsfeedPage extends StatelessWidget {
  const EventsNewsfeedPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewsfeedCubit, NewsfeedState>(
      builder: (context, state) {
        if (state.filteredEvents == null) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else {
          return ListView.builder(
              itemCount: state.filteredEvents!.length,
              itemBuilder: (context, idx) => InkWell(
                  onTap: () {
                    context.read<BottomBarBloc>().go(
                        EventScreen.route(
                            eventId: state.filteredEvents![idx].id),
                        hideBottombar: false);
                  },
                  child: EventContent(
                    full: false,
                    key: Key('$idx'),
                    event: state.filteredEvents![idx],
                  )));
        }
      },
    );
  }
}
