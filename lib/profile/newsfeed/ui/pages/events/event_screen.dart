import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_list_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_list_page_cubit.dart';
import 'package:teormech/profile/newsfeed/bloc/event_page_cubit.dart';
import 'package:teormech/profile/newsfeed/bloc/newsfeed_cubit.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';
import 'package:teormech/profile/profile/ui/widgets/tags_container.dart';
import 'package:teormech/profile/ui/widgets/like_footer.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';

class EventContent extends StatefulWidget {
  const EventContent({Key? key, required this.full, required this.event})
      : super(key: key);

  final bool full;
  final EventModel event;

  @override
  State<EventContent> createState() => _EventContentState();
}

class _EventContentState extends State<EventContent> {
  String? _locale;

  @override
  void initState() {
    super.initState();
    initializeDateFormatting().then((_) {
      setState(() {
        _locale = Localizations.maybeLocaleOf(context)?.languageCode;
      });
    });
  }

  Widget _dateTimeBlock() {
    return Column(
      children: [
        Row(
          children: [
            ImageIcon(AssetImage('images/icons/schedule.png'),
                color: GradientColors.pink),
            Padding(
              padding: const EdgeInsets.only(left: 5.0),
              child: Text(
                  '${DateFormat.yMMMMd(_locale).format(widget.event.date)} ${TimeOfDay.fromDateTime(widget.event.date).format(context)}'),
            ),
          ],
        ),
        Row(
          children: [
            Icon(Icons.fmd_good_outlined, color: GradientColors.pink),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 5.0),
                child: Text(widget.event.location),
              ),
            )
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(widget.event.title,
              style: const TextStyle(fontWeight: FontWeight.bold)),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: Text(widget.event.type,
                style: const TextStyle(color: Colors.grey)),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 3.0),
            child: _dateTimeBlock(),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0, bottom: 5.0),
            child: Text(
              widget.event.description,
              maxLines: widget.full ? 10000 : 2,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          if (widget.event.logo != null)
            ConstrainedBox(
              constraints: BoxConstraints(maxHeight: 200),
              child: Center(
                child: FittedBox(
                  fit: BoxFit.cover,
                  clipBehavior: Clip.hardEdge,
                  child: CachedNetworkImage(
                      imageUrl: EventsApi.eventAvatar + widget.event.logo!),
                ),
              ),
            ),
          if (widget.event.tags?.isNotEmpty ?? false)
            Padding(
              padding: const EdgeInsets.only(top: 8.0, bottom: 8),
              child: Text('#теги',
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.grey)),
            ),
          if (widget.event.tags?.isNotEmpty ?? false)
            TagsContainer(
              tags: widget.event.tags!,
              isCompact: true,
            ),
          LikeFooter(
            liked: widget.event.liked ?? false,
            repostCallback: () {
              context
                  .read<ChatListBloc>()
                  .addNewsfeedObjectForForwarding(widget.event);
              context.read<BottomBarBloc>().setTab(BottomBars.Chat);
            },
            likeCallback: () {
              if (!widget.full) {
                context.read<NewsfeedCubit>().likeEvent(widget.event.id);
              } else {
                context
                    .read<EventPageCubit>()
                    .like(widget.event.liked ?? false);
              }
            },
            likes: widget.event.likes,
          ),
          if (!widget.full)
            Container(
                decoration: const BoxDecoration(
                    border: Border(bottom: BorderSide(color: Colors.grey)))),
        ],
      ),
    );
  }
}

class EventScreen extends StatefulWidget {
  const EventScreen(
      {Key? key,
      required this.eventId,
      this.registrationSuccess = false,
      this.wasRegisteredBefore = false})
      : super(key: key);

  final int eventId;
  final bool wasRegisteredBefore;
  final bool registrationSuccess;

  EventScreen.fromId(
      {required this.eventId,
      required this.wasRegisteredBefore,
      required this.registrationSuccess});

  static MaterialPageRoute route(
      {required int eventId,
      bool wasRegisteredBefore = false,
      bool registrationSuccess = false}) {
    return MaterialPageRoute(
        builder: (_) => EventScreen.fromId(
              eventId: eventId,
              registrationSuccess: registrationSuccess,
              wasRegisteredBefore: wasRegisteredBefore,
            ));
  }

  @override
  State<EventScreen> createState() => _EventScreenState();
}

class _EventScreenState extends State<EventScreen> {
  var _togglePopup = false;
  var _success = false;

  final tStyle = TextStyle(color: Colors.black);
  final tBoldStyle =
      TextStyle(color: Colors.black, fontWeight: FontWeight.bold);

  @override
  void initState() {
    if (widget.registrationSuccess) {
      _togglePopup = true;
      _success = true;
    }
    if (widget.wasRegisteredBefore) {
      _togglePopup = true;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: ProfileAppBar(
          leftAlignedTitle: true,
          title: Text('Мероприятие', style: appBarTextStyle),
          rightmostButton: IconButton(
            icon: ImageIcon(AssetImage('images/icons/share.png')),
            color: Colors.white,
            onPressed: () {
              context.read<ChatListBloc>()
                ..addNewsfeedObjectForForwarding(
                    NewsfeedObject(widget.eventId, RequestMode.Event))
                ..pageCubit.selectPage(ChatListPage.Chats);
              context.read<BottomBarBloc>().setTab(BottomBars.Chat);
            },
          ),
        ),
        backgroundColor: Colors.white,
        body: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            setState(() {
              _togglePopup = false;
            });
          },
          child: Stack(
            children: [
              SingleChildScrollView(
                child: BlocProvider<EventPageCubit>(
                  create: (context) => EventPageCubit(
                      newsfeedCubit: context.read<NewsfeedCubit>(),
                      id: widget.eventId),
                  child: Builder(builder: (context) {
                    return BlocBuilder<EventPageCubit, EventModel?>(
                        builder: (context, state) {
                      if (state != null) {
                        return EventContent(full: true, event: state);
                      } else {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                    });
                  }),
                ),
              ),
              if (_togglePopup)
                Positioned.fill(
                    child: Container(
                  color: GradientColors.updatingIndicatorColor,
                )),
              if (_togglePopup)
                Align(
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(32),
                    ),
                    child: _success
                        ? Row(
                            children: [
                              Icon(Icons.verified),
                              RichText(
                                  text: TextSpan(children: [
                                TextSpan(text: 'Вы', style: tStyle),
                                TextSpan(text: 'успешно', style: tBoldStyle),
                                TextSpan(
                                    text: 'зарегистрировались на мероприятие',
                                    style: tStyle)
                              ])),
                            ],
                          )
                        : RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(children: [
                              TextSpan(text: 'Вы ', style: tStyle),
                              TextSpan(
                                  text: 'уже зарегистрированы \n',
                                  style: tBoldStyle),
                              TextSpan(
                                  text: 'на это мероприятие', style: tStyle)
                            ])),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16, vertical: 32),
                  ),
                )
            ],
          ),
        ));
  }
}
