import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/newsfeed/bloc/newsfeed_cubit.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_state.dart';
import 'package:teormech/profile/newsfeed/ui/pages/portfolio/search_entry_row.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';
import 'package:teormech/profile/profile/ui/profile_wrapper.dart';

class NewsfeedPortfolioPage extends StatelessWidget {
  const NewsfeedPortfolioPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewsfeedCubit, NewsfeedState>(
      builder: (context, state) {
        if (state.portfolios == null) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else {
          return ListView.builder(
            itemBuilder: (context, idx) {
              final entry = state.portfolios![idx];
              return SearchEntryRow(
                key: Key(entry.profile.lid),
                selectedTags: state.tags,
                entry: entry,
                query: state.textSearchController.text,
                goToProfileCallback: () {
                  FocusScope.of(context).unfocus();
                  context.read<BottomBarBloc>().go(MaterialPageRoute(
                      builder: (_) => ProfileScreenWrapper.fromLid(
                            lid: entry.profile.lid,
                            startPage: ProfilePage.Portfolio,
                          )));
                },
              );
            },
            itemCount: state.portfolios!.length,
          );
        }
      },
    );
  }
}
