import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/profile/bloc/online_status.dart';
import 'package:teormech/profile/newsfeed/bloc/newsfeed_entry_cubit.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_state.dart';
import 'package:teormech/profile/profile/model/portfolio_state.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/clipped_avatar.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/empty_avatar.dart';
import 'package:teormech/profile/profile/ui/widgets/tags_container.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/widgets/user_lang_container.dart';

class SearchEntryRow extends StatelessWidget {
  const SearchEntryRow(
      {Key? key,
      required this.entry,
      this.selectedTags,
      this.query,
      required this.goToProfileCallback})
      : super(key: key);

  final VoidCallback goToProfileCallback;
  final NewsfeedSearchEntry entry;
  final List<PortfolioTag>? selectedTags;
  final String? query;

  List<PortfolioTag> _filterTags(List<PortfolioTag> unsorted) {
    final tags = unsorted.toList(growable: true)
      ..sort((a, b) {
        final containsA =
            selectedTags?.where((e) => e.tag == a.tag).isNotEmpty ?? false;
        final containsB =
            selectedTags?.where((e) => e.tag == b.tag).isNotEmpty ?? false;

        if (containsA && !containsB) return -1;
        if (!containsA && containsB) return 1;
        return 0;
      });

    if (tags.length <= 6) return tags;

    return <PortfolioTag>[
      ...tags.take(6),
      PortfolioTag(tag: 'и т.д..', level: ProfessionalLevel.Service)
    ];
  }

  TextSpan _filterText() {
    if (query == null || (query?.trim().isEmpty ?? false)) {
      return TextSpan(
          text: (entry.portfolio.professionalSkills?.isEmpty ?? true)
              ? ''
              : 'Профессиональные навыки \n',
          children: [
            TextSpan(
                text: entry.portfolio.professionalSkills,
                style: const TextStyle(
                    fontSize: 14, fontWeight: FontWeight.normal)),
          ],
          style: const TextStyle(
              color: Colors.black, fontWeight: FontWeight.w500));
    }
    final children = <TextSpan>[];

    final wordsVariants = [
      entry.portfolio.professionalSkills,
      entry.portfolio.languages.join('\n'),
      ...entry.portfolio.workExperience
    ];
    var lQuery = query!.toLowerCase();
    for (var blocIndex = 0; blocIndex < wordsVariants.length; blocIndex++) {
      final wordsVariant = wordsVariants[blocIndex];
      final words = wordsVariant?.split(RegExp(r'[ /.,]+')) ?? [];
      final shownWords = words.map((_) => '').toList();
      final highlitedIndices = <int>[];

      for (var i = 0; i < words.length; i++) {
        final word = words[i];
        if (word.length <= 2) continue;

        if (word.toLowerCase().contains(lQuery)) {
          //if (lQuery.contains(word.toLowerCase())) {
          shownWords[i] = word;
          highlitedIndices.add(i);
          for (var j = 1; j < 15; j++) {
            if (i - j >= 0) shownWords[i - j] = words[i - j];
            if (i + j < words.length) shownWords[i + j] = words[i + j];
          }
        }
      }

      if (highlitedIndices.isEmpty) {
        continue;
      } else {
        children.add(TextSpan(
            text: blocIndex == 0
                ? 'Проф. навыки \n'
                : blocIndex == 1
                    ? 'Языки \n'
                    : blocIndex == 2
                        ? '\nОпыт работы \n'
                        : '\n',
            style: const TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold)));
      }

      for (var i = 0; i < shownWords.length; i++) {
        if (shownWords[i].isNotEmpty) {
          children.add(TextSpan(
              text: shownWords[i] + ' ',
              style: highlitedIndices.contains(i)
                  ? const TextStyle(color: Colors.red)
                  : const TextStyle(color: Colors.black)));
        }
      }
    }
    return TextSpan(children: children);
  }

  Widget _publicationsContainer(
      {bool isLast = false, required String num, required String caption}) {
    return Flexible(
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 5),
        decoration:
            isLast ? null : BoxDecoration(border: Border(right: BorderSide())),
        child: Column(
          children: [
            Text(num,
                style: const TextStyle(
                    color: Colors.pinkAccent,
                    fontSize: 24,
                    fontWeight: FontWeight.bold)),
            Text(
              caption,
              style: const TextStyle(fontSize: 10),
              overflow: TextOverflow.ellipsis,
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final now = DateTime.now();

    return BlocProvider<NewsfeedEntryCubit>(
      create: (_) => NewsfeedEntryCubit(
        entry,
      ),
      child: Builder(
        builder: (context) {
          return BlocBuilder<NewsfeedEntryCubit, NewsfeedSearchEntry>(
              builder: (context, state) {
            final filteredTags = _filterTags(state.portfolio.tags.tags);
            final filteredText = _filterText();
            final online = OnlineStatus.onlineString(
                    lastOnline: DateTime.tryParse(
                        state.profile.lastOnline ?? '1970-01-01'),
                    sex: '1',
                    now: now) ==
                'В сети';
            return InkWell(
              onTap: goToProfileCallback,
              child: Container(
                padding:
                    const EdgeInsets.only(left: 8.0, top: 16.0, bottom: 16.0),
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(color: Colors.grey.shade300))),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (state.profile.avatar != null)
                      UserAvatar.fromNetwork(
                          size: width / 8,
                          key: ValueKey(state.profile.lid),
                          online: online,
                          avatarUrl:
                              ProfileApi.profileAvatar + state.profile.avatar!)
                    else
                      EmptyAvatar(
                        online: online,
                        size: width / 8,
                        groupChat: false,
                      ),
                    SizedBox(width: 10),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: width / 16,
                            child: Text(
                                '${state.profile.surname} ${state.profile.name}',
                                style: profileTextStyle.copyWith(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 14)),
                          ),
                          if (filteredTags.isNotEmpty)
                            Padding(
                              padding: const EdgeInsets.only(bottom: 8),
                              child: TagsContainer(
                                tags: filteredTags,
                                isCompact: true,
                              ),
                            ),
                          if ((filteredText.text?.isNotEmpty ?? false) ||
                              (query?.isNotEmpty ?? false))
                            RichText(text: filteredText),
                          if ((query?.isEmpty ?? true) &&
                              (selectedTags?.isEmpty ?? true) &&
                              (state.portfolio.languages.isNotEmpty))
                            Column(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(top: 5.0),
                                      child: Text('Знание языков',
                                          style: const TextStyle(
                                              fontWeight: FontWeight.w500)),
                                    ),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    for (var lang in state.portfolio.languages)
                                      LanguageContainer(lang),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          if (state.portfolio.languages.isEmpty &&
                              (filteredText.text?.isNotEmpty ?? false))
                            const SizedBox(
                              height: 10,
                            ),
                          Row(
                            children: [
                              SizedBox(
                                width: MediaQuery.of(context).size.width / 1.2,
                                child: Row(
                                  children: [
                                    _publicationsContainer(
                                        num: state.portfolio.confirmedSum
                                            .toString(),
                                        caption: 'Достижений'),
                                    _publicationsContainer(
                                        num: state
                                            .portfolio.workExperience.length
                                            .toString(),
                                        caption: 'Опыта раб...'),
                                    _publicationsContainer(
                                        num: '1',
                                        caption: 'Мероприятий',
                                        isLast: true),
                                  ],
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            );
          });
        },
      ),
    );
  }
}
