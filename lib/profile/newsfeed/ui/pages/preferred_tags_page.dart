import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';
import 'package:teormech/profile/newsfeed/rep/newsfeed_repository.dart';
import 'package:teormech/profile/newsfeed/ui/widgets/newsfeed_filter.dart';
import 'package:teormech/profile/profile/model/portfolio_state.dart';
import 'package:teormech/profile/profile/ui/widgets/tags_container.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/styles/transitions/slide_fade.dart';

class PreferredTagsPage extends StatefulWidget {
  const PreferredTagsPage({Key? key}) : super(key: key);

  @override
  State<PreferredTagsPage> createState() => _PreferredTagsPageState();

  static PageRouteBuilder route() => PageRouteBuilder(
      pageBuilder: (context, f1, f3) => PreferredTagsPage(),
      transitionsBuilder: slideTransition);
}

class _PreferredTagsPageState extends State<PreferredTagsPage> {
  var tags = <PortfolioTag>[];

  final controller = TextEditingController();
  final tagKey = GlobalKey<DropdownSearchState>();

  Future<void> updateTags() async {
    final fetchedTags = await NewsfeedRepository()
        .fetchNewsfeedTags(id: 0, mode: RequestMode.Interest);
    setState(() {
      tags = fetchedTags;
    });
  }

  @override
  void initState() {
    updateTags();
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            appBar: ProfileAppBar(
              title: Text(
                'Интересы',
                style: appBarTextStyle,
              ),
            ),
            body: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0, left: 15),
                    child: Text('#теги',
                        style: const TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0, top: 10),
                    child: Row(
                      children: [
                        Text('Введите тег',
                            style: TextStyle(
                                color: Colors.grey.shade600, fontSize: 16)),
                        SizedBox(
                          width: 15,
                        ),
                        Expanded(
                          flex: 2,
                          child: Padding(
                            padding: const EdgeInsets.only(
                                top: 16, bottom: 16, left: 8, right: 30),
                            child: SizedBox(
                              height: 40,
                              child: DropdownSearch<String>(
                                  key: tagKey,
                                  searchBoxController: controller,
                                  mode: Mode.BOTTOM_SHEET,
                                  onChanged: (str) async {
                                    // ignore: unnecessary_null_comparison
                                    if (str == null || str.trim().isEmpty)
                                      return;
                                    setState(() {
                                      tags = [
                                        ...tags,
                                        PortfolioTag(
                                            tag: str,
                                            level: ProfessionalLevel.Newsfeed)
                                      ];
                                    });
                                    await NewsfeedRepository().pinTag(
                                        pid: 0,
                                        tag: str,
                                        unpin: false,
                                        mode: RequestMode.Interest);
                                    controller.text = '';
                                    tagKey.currentState
                                        ?.changeSelectedItem(null);
                                    FocusScope.of(context).unfocus();
                                  },
                                  isFilteredOnline: true,
                                  onFind: (mask) {
                                    return NewsfeedRepository().searchEventTags(
                                      mask: mask,
                                    );
                                  },
                                  showSearchBox: true,
                                  showAsSuffixIcons: true,
                                  dropdownBuilder: (context, a, b) => Text(
                                        b,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                  popupItemBuilder: NewsfeedFilter.popupBuilder,
                                  autoFocusSearchBox: true,
                                  dropdownSearchDecoration:
                                      NewsfeedFilter.decoration,
                                  selectedItem: ''),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 19.0),
                    child: Text(
                        'Укажите навыки и программы, которые вам интересны. На основе этих данных ' +
                            'мы поможем подобрать вам подходящие мероприятия, проекты и вакансии.',
                        textAlign: TextAlign.center),
                  ),
                  if (tags.isNotEmpty)
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Выбранные теги',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          Text('${tags.length}/10')
                        ],
                      ),
                    ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TagsContainer(
                      tags: tags,
                      isCompact: true,
                      tapCallback: (tag) async {
                        setState(() {
                          tags.removeWhere((e) => e.tag == tag);
                        });
                        await NewsfeedRepository().pinTag(
                            pid: 0,
                            tag: tag,
                            unpin: true,
                            mode: RequestMode.Interest);
                      },
                    ),
                  )
                ],
              ),
            )));
  }
}
