import 'package:flutter/material.dart';
import 'package:teormech/profile/newsfeed/ui/widgets/project_text_field.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/widgets/calendar_picker_row.dart';
import 'package:teormech/widgets/multilne_text_field.dart';

typedef DateTimeCallback = void Function(DateTime? date);
typedef TimeOfDayCallback = void Function(TimeOfDay? timeOfDay);

class CreateProjectEventDialog extends StatelessWidget {
  CreateProjectEventDialog(
      {Key? key,
      required this.captionController,
      required this.descriptionController,
      required this.selectedDateTime,
      required this.selectedTimeOfDay,
      required this.dateTimeCallback,
      required this.timeOfDayCallback,
      required this.isCreating})
      : super(key: key);

  final TextEditingController captionController;
  final TextEditingController descriptionController;

  final DateTime selectedDateTime;
  final TimeOfDay selectedTimeOfDay;

  final DateTimeCallback dateTimeCallback;
  final TimeOfDayCallback timeOfDayCallback;

  final bool isCreating;

  final _textStyle = TextStyle();

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width * 0.9;
    return Align(
      alignment: Alignment.center,
      child: Offstage(
        offstage: isCreating,
        child: Container(
            height: 300,
            width: width,
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(16)),
            child: Column(
              children: [
                Row(
                  children: [
                    Padding(
                      child: Text(
                        'Создание встречи',
                        style: appBarTextStyle.copyWith(color: Colors.black),
                      ),
                      padding: const EdgeInsets.only(left: 24, right: 8),
                    ),
                    Spacer(),
                    IconButton(
                      padding: const EdgeInsets.only(left: 16.0),
                      onPressed: () {},
                      icon: ImageIcon(AssetImage('images/icons/ok.png')),
                      color: GradientColors.backColor,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 24.0),
                      child: IconButton(
                        padding: EdgeInsets.zero,
                        onPressed: () {},
                        icon: ImageIcon(AssetImage('images/icons/krestik.png')),
                        color: GradientColors.backColor,
                      ),
                    )
                  ],
                ),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 24.0),
                  width: double.infinity,
                  height: 1,
                  color: Colors.grey.shade400,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: ProjectTextField(
                      controller: captionController,
                      textStyle: _textStyle,
                      caption: 'Название'),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16, left: 24, right: 24),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Описание задачи', style: _textStyle),
                        MultilineTextField(
                          collapsed: true,
                          controller: descriptionController,
                        )
                      ]),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 24.0, top: 5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      CalendarPickerRow(
                          dateTimeCallback: dateTimeCallback,
                          selectedDateTime: selectedDateTime,
                          timeOfDayCallback: timeOfDayCallback,
                          selectedTimeOfDay: selectedTimeOfDay,
                          locale: null),
                    ],
                  ),
                )
              ],
            )),
      ),
    );
  }
}
