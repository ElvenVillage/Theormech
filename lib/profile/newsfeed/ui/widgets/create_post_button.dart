import 'package:flutter/material.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';

class CreatePostButton extends StatelessWidget {
  const CreatePostButton(
      {Key? key, required this.onPressed, this.icon, required this.caption})
      : super(key: key);

  final VoidCallback onPressed;
  final String? icon;
  final String caption;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 35,
      padding: const EdgeInsets.only(right: 8.0),
      decoration: BoxDecoration(
        color: GradientColors.pink,
        borderRadius: BorderRadius.circular(30.0),
      ),
      child: MaterialButton(
        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
        height: 20,
        onPressed: onPressed,
        child: Row(
          children: [
            if (icon != null)
              ImageIcon(
                AssetImage(icon!),
                color: Colors.white,
                size: 20,
              ),
            const SizedBox(
              width: 10,
            ),
            Text(caption,
                textAlign: TextAlign.center,
                style: h2DrawerGreyText.copyWith(
                    color: Colors.white,
                    fontSize: 14,
                    fontWeight: FontWeight.bold)),
          ],
        ),
      ),
    );
  }
}
