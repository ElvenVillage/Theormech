import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:open_file/open_file.dart';
import 'package:teormech/rep/native_downloader.dart';
import 'package:teormech/styles/colors.dart';

typedef DocumentCallback = void Function(int idx);

class PostDocumentsBlock extends StatelessWidget {
  const PostDocumentsBlock(
      {Key? key,
      this.files,
      this.onlineDocuments,
      this.removeCallback,
      required this.editable})
      : super(key: key);

  final bool editable;
  final List<PlatformFile>? files;
  final List<String>? onlineDocuments;
  final DocumentCallback? removeCallback;

  @override
  Widget build(BuildContext context) {
    final length = files?.length ?? onlineDocuments?.length ?? 0;
    return SizedBox(
      height: length < 3
          ? 50
          : length < 5
              ? 120
              : 150,
      child: StaggeredGridView.countBuilder(
          crossAxisCount: 4,
          mainAxisSpacing: 0,
          crossAxisSpacing: 0,
          itemCount: length,
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (context, idx) {
            final text =
                (Uri.tryParse(files?[idx].path ?? onlineDocuments?[idx] ?? '')
                        ?.pathSegments
                        .last ??
                    '');
            return Padding(
              key: ValueKey(idx),
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 2),
              child: Stack(
                children: [
                  GestureDetector(
                    onTap: () async {
                      if (!editable && removeCallback == null) {
                        // NativeDownloader.download(url: onlineDocuments![idx]);
                        await NativeDownloader.checkPermission();
                        final dir =
                            await NativeDownloader.getDownloadDirectory();
                        final path = dir!.path +
                            '/' +
                            Uri.parse(onlineDocuments![idx]).pathSegments.last;
                        if (File(path).existsSync()) {
                          OpenFile.open(path);
                        } else {
                          await FlutterDownloader.enqueue(
                              url: onlineDocuments![idx],
                              savedDir: dir.path,
                              openFileFromNotification: true,
                              saveInPublicStorage: true);
                        }
                      }
                    },
                    child: Container(
                      height: 40,
                      padding: const EdgeInsets.only(
                          left: 10, right: 10, top: 5, bottom: 5),
                      decoration: BoxDecoration(
                          color: GradientColors.documentsBackground,
                          border:
                              Border.all(width: 1, color: Colors.transparent),
                          borderRadius: BorderRadius.circular(8)),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text(
                              text.length > 25 && length > 1
                                  ? text.substring(0, 7) +
                                      '...' +
                                      text.substring(
                                          text.length - 7, text.length)
                                  : text,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  if (editable)
                    Positioned(
                      right: -6,
                      top: -6,
                      child: IconButton(
                          iconSize: 16,
                          icon: ImageIcon(AssetImage(
                              'images/icons/selection_bar/cross.png')),
                          color: Colors.blue,
                          onPressed: () {
                            if (editable && removeCallback != null)
                              removeCallback!(idx);
                          }),
                    )
                ],
              ),
            );
          },
          staggeredTileBuilder: (idx) {
            if (length == 1) {
              return StaggeredTile.count(4, 0.5);
            }
            return StaggeredTile.count(2, 0.5);
          }),
    );
  }
}
