import 'package:dropdown_search/dropdown_search.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/ui/widgets/register_text_field.dart';
import 'package:teormech/profile/newsfeed/bloc/newsfeed_cubit.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_state.dart';
import 'package:teormech/profile/newsfeed/rep/newsfeed_repository.dart';
import 'package:teormech/profile/newsfeed/ui/newsfeed_screen.dart';
import 'package:teormech/profile/profile/model/portfolio_state.dart';
import 'package:teormech/profile/profile/rep/user_portfolio_rep.dart';
import 'package:teormech/profile/profile/ui/widgets/tags_container.dart';
import 'package:teormech/styles/colors.dart';

class NewsfeedFilter extends StatefulWidget {
  NewsfeedFilter(
      {Key? key,
      required this.expanded,
      required this.expandableController,
      required this.page})
      : super(key: key);
  final bool expanded;
  final NewsfeedPageEnum page;
  final ExpandableController expandableController;

  @override
  State<NewsfeedFilter> createState() => _NewsfeedFilterState();

  static InputDecoration decoration = InputDecoration(
    contentPadding: const EdgeInsets.only(left: 10),
    isDense: true,
    filled: true,
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: Colors.grey,
      ),
      borderRadius: BorderRadius.circular(32.0),
    ),
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Colors.grey.shade400),
      borderRadius: BorderRadius.circular(32.0),
    ),
    fillColor: Colors.white,
  );

  static Widget popupBuilder(BuildContext context, String a, bool b) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
            width: double.infinity,
            decoration: BoxDecoration(
                border:
                    Border(bottom: BorderSide(color: Colors.grey.shade200))),
            padding: const EdgeInsets.only(left: 20, bottom: 10, top: 10),
            child: Text(a, style: TextStyle(fontSize: 22))),
      ],
    );
  }
}

class _NewsfeedFilterState extends State<NewsfeedFilter> {
  final _filterGlobalKey = GlobalKey<DropdownSearchState>();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Color(0xFFfafafa),
      ),
      child: BlocBuilder<NewsfeedCubit, NewsfeedState>(
        builder: (context, state) {
          final tagsNotEmpty = ((widget.page == NewsfeedPageEnum.Portfolio &&
                  state.tags.isNotEmpty) ||
              (widget.page == NewsfeedPageEnum.Jobs &&
                  state.jobTags.isNotEmpty) ||
              (widget.page == NewsfeedPageEnum.Projects &&
                  state.projectTags.isNotEmpty) ||
              (widget.page == NewsfeedPageEnum.Events &&
                  state.eventTags.isNotEmpty));

          List<PortfolioTag>? displayedTags;

          switch (widget.page) {
            case NewsfeedPageEnum.Jobs:
              displayedTags = state.jobTags;
              break;
            case NewsfeedPageEnum.Events:
              displayedTags = state.eventTags;
              break;
            case NewsfeedPageEnum.Portfolio:
              displayedTags = state.tags;
              break;
            case NewsfeedPageEnum.Projects:
              displayedTags = state.projectTags;
              break;
            case NewsfeedPageEnum.News:
          }
          return Container(
            decoration: BoxDecoration(
                border:
                    Border(bottom: BorderSide(color: Colors.grey.shade300))),
            padding: const EdgeInsets.only(left: 12.0),
            child: Column(
              children: [
                if (widget.expanded)
                  Row(
                    children: [
                      Text(
                        'Параметры поиска',
                        style: TextStyle(
                            color: Colors.blue.shade800,
                            fontWeight: FontWeight.bold),
                      ),
                      MaterialButton(
                        onPressed: () {
                          context
                              .read<NewsfeedCubit>()
                              .cleanFilter(widget.page);
                          FocusScope.of(context).unfocus();
                          _filterGlobalKey.currentState
                              ?.changeSelectedItem(null);
                        },
                        child: const Text(
                          'Очистить фильтр',
                          style: TextStyle(
                              decoration: TextDecoration.underline,
                              color: Colors.grey,
                              fontSize: 12,
                              fontWeight: FontWeight.normal),
                        ),
                      )
                    ],
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  ),
                if (widget.expanded) ...[
                  if (widget.page == NewsfeedPageEnum.Portfolio)
                    Row(
                      children: [
                        Text(
                          'Введите текст',
                          style: TextStyle(
                              color: Colors.grey.shade600, fontSize: 16),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Expanded(
                            flex: 2,
                            child: Padding(
                              padding: const EdgeInsets.only(right: 30.0),
                              child: RegisterTextField(
                                  controller: state.textSearchController),
                            )),
                      ],
                    ),
                  if (widget.page == NewsfeedPageEnum.Jobs)
                    Row(
                      children: [
                        Text(
                          'Выберите тип',
                          style: TextStyle(
                              color: Colors.grey.shade600, fontSize: 16),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Expanded(
                          flex: 2,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8, right: 30),
                            child: SizedBox(
                              height: 40,
                              child: DropdownSearch<String>(
                                searchBoxController:
                                    state.jobsTextSearchController,
                                mode: Mode.BOTTOM_SHEET,
                                popupItemBuilder: NewsfeedFilter.popupBuilder,
                                items: [
                                  'Оплачиваемая стажировка',
                                  'Стажировка',
                                  'Практика'
                                ],
                                onChanged: (str) {
                                  state.jobsTextSearchController.text = str;
                                },
                                showSearchBox: true,
                                showAsSuffixIcons: true,
                                dropdownSearchDecoration:
                                    NewsfeedFilter.decoration,
                                dropdownBuilder: (context, a, b) => Text(
                                  b,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                autoFocusSearchBox: true,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                ],
                if (widget.expanded)
                  Row(
                    children: [
                      Text('Выберите тег',
                          style: TextStyle(
                              color: Colors.grey.shade600, fontSize: 16)),
                      SizedBox(
                        width: 15,
                      ),
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: (state.tags.isEmpty && state.jobTags.isEmpty)
                              ? const EdgeInsets.only(
                                  top: 16, bottom: 0, left: 8, right: 30)
                              : const EdgeInsets.only(
                                  top: 16, bottom: 16, left: 8, right: 30),
                          child: SizedBox(
                            height: 40,
                            child: DropdownSearch<String>(
                                key: _filterGlobalKey,
                                searchBoxController: state.tagController,
                                mode: Mode.BOTTOM_SHEET,
                                onChanged: (str) {
                                  // ignore: unnecessary_null_comparison
                                  if (str == null) return;
                                  context.read<NewsfeedCubit>().addTag(
                                      PortfolioTag(
                                          tag: str,
                                          level: ProfessionalLevel.Newsfeed),
                                      widget.page);
                                  state.tagController.text = '';
                                  _filterGlobalKey.currentState
                                      ?.changeSelectedItem(null);
                                  FocusScope.of(context).unfocus();
                                },
                                isFilteredOnline: true,
                                onFind: (mask) {
                                  if (widget.page == NewsfeedPageEnum.Events) {
                                    return NewsfeedRepository()
                                        .searchEventTags(mask: mask);
                                  }
                                  return UserPortfolioRepository().filterTags(
                                      mask: mask,
                                      tags: state.tags
                                          .map((e) => e.tag)
                                          .toList());
                                },
                                showSearchBox: true,
                                showAsSuffixIcons: true,
                                dropdownBuilder: (context, a, b) => Text(
                                      b,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                popupItemBuilder: NewsfeedFilter.popupBuilder,
                                autoFocusSearchBox: true,
                                dropdownSearchDecoration:
                                    NewsfeedFilter.decoration,
                                selectedItem: ''),
                          ),
                        ),
                      ),
                    ],
                  ),
                if (widget.expanded && tagsNotEmpty && displayedTags != null)
                  Padding(
                    padding: const EdgeInsets.only(right: 10, left: 8),
                    child: TagsContainer(
                      tags: displayedTags,
                      isCompact: true,
                      tapCallback: (tag) {
                        FocusScope.of(context).unfocus();
                        context
                            .read<NewsfeedCubit>()
                            .removeTag(tag, widget.page);
                        state.tagController.clear();
                        state.jobsTagController.clear();
                      },
                    ),
                  ),
                if (widget.expanded)
                  Row(
                    children: [
                      MaterialButton(
                          padding: EdgeInsets.zero,
                          onPressed: () {
                            FocusScope.of(context).unfocus();
                            widget.expandableController.toggle();
                          },
                          child: Text(
                            'Скрыть фильтр',
                            style: TextStyle(
                                decoration: TextDecoration.underline,
                                color: Colors.grey,
                                fontSize: 12,
                                fontWeight: FontWeight.normal),
                          )),
                      Spacer(),
                      MaterialButton(
                          onPressed: () {
                            FocusScope.of(context).unfocus();
                            context
                                .read<NewsfeedCubit>()
                                .handleSearch(widget.page);
                          },
                          child: Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(right: 4.0),
                                child: ImageIcon(
                                  AssetImage('images/icons/ok.png'),
                                  color: GradientColors.pink,
                                  size: 18,
                                ),
                              ),
                              Text('Готово',
                                  style: TextStyle(
                                      color: GradientColors.pink,
                                      fontSize: 16)),
                            ],
                          ))
                    ],
                  )
              ],
            ),
          );
        },
      ),
    );
  }
}
