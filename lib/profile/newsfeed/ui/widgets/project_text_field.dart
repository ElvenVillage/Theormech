import 'package:flutter/material.dart';
import 'package:teormech/auth/ui/widgets/register_text_field.dart';

class ProjectTextField extends StatelessWidget {
  const ProjectTextField(
      {Key? key,
      required this.controller,
      required this.textStyle,
      this.verticalAlign = false,
      required this.caption})
      : super(key: key);

  final TextEditingController controller;
  final TextStyle textStyle;
  final String caption;

  final bool verticalAlign;

  @override
  Widget build(BuildContext context) {
    final children = [
      Expanded(
          flex: 2,
          child: Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Text(caption, style: textStyle),
          )),
      Expanded(
          flex: 3,
          child: Padding(
            padding: const EdgeInsets.only(right: 16.0, left: 10.0),
            child: RegisterTextField(
              controller: controller,
              maxLines: 1,
            ),
          ))
    ];
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: verticalAlign
          ? Column(
              children: children,
            )
          : Row(children: children),
    );
  }
}
