import 'package:flutter/material.dart';
import 'package:teormech/styles/text.dart';

class RoundedMaterialButton extends StatelessWidget {
  const RoundedMaterialButton(
      {Key? key,
      required this.color,
      required this.callback,
      required this.caption})
      : super(key: key);

  final Color color;
  final VoidCallback callback;
  final String caption;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.0), color: color),
      child: MaterialButton(
        minWidth: 120,
        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
        onPressed: callback,
        child: Text(caption,
            textAlign: TextAlign.center,
            style: h1TextStyle.copyWith(
                color: Colors.white,
                fontSize: 14,
                fontWeight: FontWeight.bold)),
      ),
    );
  }
}
