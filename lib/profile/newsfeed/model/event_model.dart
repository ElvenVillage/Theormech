part of 'newsfeed_model.dart';

class EventModel extends NewsfeedObject {
  final String title;
  final String description;
  final DateTime date;
  final String location;
  final String type;
  final int likes;
  final String? logo;

  bool? liked;

  List<PortfolioTag>? tags;

  EventModel({
    required this.title,
    required this.description,
    required this.date,
    required this.location,
    required this.likes,
    required this.type,
    required this.logo,
    required this.liked,
    required int id,
    this.tags,
  }) : super(id, RequestMode.Event);

  factory EventModel.fromJson(Map<String, dynamic> json) {
    return EventModel(
        title: json['title'],
        description: json['description'],
        location: json['location'],
        type: json['type'],
        liked: null,
        logo: json['logo'],
        likes: json['likes'] == null ? 0 : int.tryParse(json['likes']) ?? 0,
        id: int.tryParse(json['id']) ?? int.tryParse(json['eid']) ?? 0,
        date: DateTime.parse(json['date'] + ' ' + json['time']));
  }
}
