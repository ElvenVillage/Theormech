import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';
import 'package:teormech/profile/profile/model/portfolio_state.dart';
import 'package:teormech/profile/profile/model/user_profile.dart';

class NewsfeedSearchEntry {
  final UserProfile profile;
  final PortfolioState portfolio;

  NewsfeedSearchEntry({required this.profile, required this.portfolio});
}

class NewsfeedState {
  final TextEditingController textSearchController;
  final TextEditingController tagController;

  final TextEditingController jobsTextSearchController;
  final TextEditingController jobsTagController;

  final bool syncing;
  final bool offstageFilter;

  final List<PortfolioTag> tags;
  final List<PortfolioTag> jobTags;
  final List<PortfolioTag> eventTags;
  final List<PortfolioTag> projectTags;

  final List<NewsfeedSearchEntry>? portfolios;
  final List<EventModel>? events;
  final List<JobModel>? jobs;
  final List<ProjectModel>? projects;
  final List<ProjectModel>? myProjects;

  final List<EventModel>? filteredEvents;
  final List<ProjectModel>? filteredProjects;
  final List<JobModel>? filteredJobs;

  NewsfeedState(
      {required this.tags,
      required this.jobTags,
      required this.eventTags,
      required this.projectTags,
      required this.textSearchController,
      required this.jobsTextSearchController,
      required this.jobsTagController,
      required this.tagController,
      required this.syncing,
      required this.offstageFilter,
      required this.events,
      required this.filteredEvents,
      required this.filteredProjects,
      required this.myProjects,
      required this.filteredJobs,
      required this.jobs,
      required this.portfolios,
      required this.projects});

  NewsfeedState.empty()
      : tags = const [],
        jobs = null,
        jobTags = const [],
        eventTags = const [],
        projectTags = const [],
        myProjects = null,
        portfolios = null,
        events = null,
        projects = null,
        filteredEvents = null,
        filteredProjects = null,
        textSearchController = TextEditingController(),
        jobsTagController = TextEditingController(),
        jobsTextSearchController = TextEditingController(),
        tagController = TextEditingController(),
        filteredJobs = null,
        syncing = false,
        offstageFilter = false;

  NewsfeedState copyWith(
      {List<PortfolioTag>? tags,
      List<PortfolioTag>? jobTags,
      List<PortfolioTag>? eventTags,
      List<PortfolioTag>? projectTags,
      bool? offstageFilter,
      bool? syncing,
      List<ProjectModel>? projects,
      List<ProjectModel>? filteredProjects,
      List<NewsfeedSearchEntry>? portfolios,
      List<EventModel>? events,
      List<EventModel>? filteredEvents,
      List<JobModel>? jobs,
      List<ProjectModel>? myProjects,
      List<JobModel>? filteredJobs}) {
    return NewsfeedState(
        tags: tags ?? this.tags,
        jobTags: jobTags ?? this.jobTags,
        eventTags: eventTags ?? this.eventTags,
        filteredEvents: filteredEvents ?? this.filteredEvents,
        filteredProjects: filteredProjects ?? this.filteredProjects,
        projectTags: projectTags ?? this.projectTags,
        portfolios: portfolios ?? this.portfolios,
        projects: projects ?? this.projects,
        events: events ?? this.events,
        jobs: jobs ?? this.jobs,
        myProjects: myProjects ?? this.myProjects,
        filteredJobs: filteredJobs ?? this.filteredJobs,
        textSearchController: this.textSearchController,
        jobsTagController: this.jobsTagController,
        jobsTextSearchController: this.jobsTextSearchController,
        tagController: this.tagController,
        offstageFilter: offstageFilter ?? this.offstageFilter,
        syncing: syncing ?? this.syncing);
  }
}
