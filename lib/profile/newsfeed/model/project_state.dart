import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';
import 'package:teormech/profile/profile/model/portfolio_state.dart';

class ProjectState {
  final ProjectModel? project;
  final List<ProjectPost>? posts;
  final List<String>? photos;
  final List<String>? attachments;
  final List<PortfolioTag> editedTags;

  final bool invited;

  ProjectState(
      {required this.posts,
      required this.invited,
      required this.project,
      required this.editedTags,
      required this.photos,
      required this.attachments});

  ProjectState copyWith(
      {ProjectModel? project,
      List<ProjectPost>? posts,
      List<String>? photos,
      bool? invited,
      List<String>? attachments,
      List<PortfolioTag>? editedTags}) {
    return ProjectState(
        posts: posts ?? this.posts,
        invited: invited ?? this.invited,
        project: project ?? this.project,
        attachments: attachments ?? this.attachments,
        photos: photos ?? this.photos,
        editedTags: editedTags ?? this.editedTags);
  }
}
