part of 'newsfeed_model.dart';

enum JobType { PRACTICE, PAYED, UNPAYED }

extension JobTypeToString on JobType {
  String toReadable() {
    switch (this) {
      case JobType.PRACTICE:
        return 'Практика';
      case JobType.PAYED:
        return 'Оплачиваемая стажировка';
      case JobType.UNPAYED:
        return 'Стажировка';
    }
  }
}

class JobModel extends NewsfeedObject {
  final String conditions, requirements, description;
  final String type;
  final String company;
  final String position;
  final String salary;
  final String status;
  final String responses;

  List<PortfolioTag> tags;

  JobModel(
      {required int id,
      required this.conditions,
      required this.requirements,
      required this.description,
      required this.type,
      required this.company,
      required this.position,
      required this.tags,
      required this.salary,
      required this.status,
      required this.responses})
      : super(id, RequestMode.Job);

  Map<String, String> toJson() {
    return {
      'conditions': conditions,
      'requirements': requirements,
      'description': description,
      'type': type,
      'company': company,
      'position': position,
      'salary': salary,
      'status': status
    };
  }

  factory JobModel.fromJson(Map<String, dynamic> json) {
    return JobModel(
        id: int.parse(json['id'] ?? json['vid']),
        conditions: json['conditions'],
        requirements: json['requirements'],
        description: json['description'],
        type: json['type'],
        company: json['company'],
        position: json['position'],
        salary: json['salary'],
        tags: const [],
        status: json['status'],
        responses: json['responses']);
  }
}
