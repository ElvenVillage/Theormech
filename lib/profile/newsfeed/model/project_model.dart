part of 'newsfeed_model.dart';

enum ProjectParticipantRole { User, Tutor, Admin }

class ProjectPost extends NewsfeedObject {
  final int uid;
  final int pid;

  final DateTime date;

  final String title;
  final String description;

  List<String>? photos;
  List<String>? attachments;

  ProjectPost(
      {required int id,
      required this.uid,
      required this.pid,
      required this.date,
      required this.title,
      required this.description})
      : super(id, RequestMode.Project);

  factory ProjectPost.fromJson(Map<String, dynamic> json) {
    return ProjectPost(
        id: int.parse(json['id']),
        uid: int.parse(json['uid'] ?? '0'),
        pid: int.parse(json['pid']),
        date: DateTime.parse(json['time']),
        title: json['title'],
        description: json['description']);
  }
}

class ProjectParticipant {
  final int id;
  final ProjectParticipantRole role;
  final String? status;

  ProjectParticipant(
      {required this.id, required this.role, required this.status});

  factory ProjectParticipant.fromJson(Map<String, dynamic> json) {
    final role =
        ProjectParticipantRole.values[(int.tryParse(json['role']) ?? 2) - 1];
    return ProjectParticipant(
        id: int.tryParse(json['id']) ?? -1, role: role, status: json['status']);
  }
}

class ProjectModel {
  final int id;

  final String title;
  final DateTime? start;
  final DateTime? end;
  final String description;
  final String? image;

  List<PortfolioTag>? tags;
  List<ProjectParticipant>? participants;
  List<int>? invites;

  ProjectModel(
      {required this.id,
      required this.title,
      this.start,
      this.end,
      required this.description,
      required this.image,
      this.invites,
      this.tags,
      this.participants});

  String get dateString {
    var dateString = '';
    if (start != null && end != null) {
      dateString = '${start!.year} — ${end!.year}';
    }
    return dateString;
  }

  ProjectParticipant? get tutor => tutors.isEmpty ? null : tutors.first;

  List<ProjectParticipant> get tutors =>
      participants
          ?.where((e) => e.role == ProjectParticipantRole.Tutor)
          .toList() ??
      [];

  List<ProjectParticipant> get users =>
      participants
          ?.where((e) => e.role != ProjectParticipantRole.Tutor)
          .toList() ??
      [];

  bool isMember(int id) => participants?.map((e) => e.id).contains(id) ?? false;

  bool isAdmin(int id) =>
      participants
          ?.where((e) => e.role == ProjectParticipantRole.Admin)
          .map((e) => e.id)
          .contains(id) ??
      false;

  factory ProjectModel.fake() {
    return ProjectModel(
        id: 0,
        title: 'Разработка бионического протеза руки',
        start: DateTime(2021),
        end: DateTime(2024),
        description:
            'Являясь всего лишь частью общей картины, непосредственные участники технического прогресса своевременно верифицированы. В целом, конечно, консультация с широким активом прекрасно подходит для реализации анализа существующих паттернов поведения. Имеется спорная точка зрения, гласящая примерно следующее: предприниматели в сети интернет лишь добавляют фракционных разногласий и объявлены нарушающими общечеловеческие нормы этики и морали. Следует отметить, что консультация с широким активом создаёт необходимость включения в производственный план целого ряда внеочередных мероприятий с учётом комплекса системы массового участия. Глубокий уровень погружения позволяет выполнить важные задания по разработке поставленных обществом задач.',
        image:
            'http://mech.spbstu.ru/images/thumb/7/7d/KovalevHand1.jpg/320px-KovalevHand1.jpg',
        tags: [
          PortfolioTag.caption('#теги:'),
          PortfolioTag.newsfeed('PHP'),
          PortfolioTag.newsfeed('HTML'),
          PortfolioTag.newsfeed('CSS')
        ],
        participants: [
          ProjectParticipant(
              id: 1, role: ProjectParticipantRole.Admin, status: 'Человек'),
          ProjectParticipant(
              id: 2, role: ProjectParticipantRole.Tutor, status: 'Человек'),
          ProjectParticipant(
              id: 3, role: ProjectParticipantRole.User, status: 'Человек'),
        ]);
  }

  factory ProjectModel.fromJson(Map<String, dynamic> json) {
    return ProjectModel(
      id: int.tryParse(json['id']) ?? -1,
      title: json['title'],
      start: DateTime.tryParse(json['start']),
      end: DateTime.tryParse(json['end']),
      description: json['description'],
      image: json['image'],
    );
  }
}
