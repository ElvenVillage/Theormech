import 'package:teormech/profile/profile/model/portfolio_state.dart';

part 'event_model.dart';
part 'job_model.dart';
part 'project_model.dart';

enum RequestMode { Event, Job, Project, Interest }

class NewsfeedObject {
  final int id;
  final RequestMode newsfeedType;

  NewsfeedObject(this.id, this.newsfeedType);
}
