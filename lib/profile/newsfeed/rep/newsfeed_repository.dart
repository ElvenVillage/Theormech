import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/auth/model/auth_model.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_state.dart';
import 'package:teormech/profile/profile/model/portfolio_state.dart';
import 'package:teormech/profile/profile/model/user_profile.dart';
import 'package:teormech/profile/profile/rep/user_profile_rep.dart';

class NewsfeedSearchResponse {
  final String? error;
  final List<NewsfeedSearchEntry>? entries;

  NewsfeedSearchResponse.error({required this.error}) : entries = null;
  NewsfeedSearchResponse.done({required this.entries}) : error = null;
}

class NewsfeedRepository {
  late final Dio _dio;
  SessionToken _credentials;

  Map<String, Future<JobModel>> _jobMemCache = {};
  Map<int, Future<EventModel>> _eventMemCache = {};

  NewsfeedRepository._internal(this._credentials) {
    _dio = Dio()..options.contentType = Headers.formUrlEncodedContentType;
  }

  void clearMem() {
    _jobMemCache = {};
    _eventMemCache = {};
  }

  static NewsfeedRepository? _instance;

  factory NewsfeedRepository() {
    if (_instance == null) {
      _instance =
          NewsfeedRepository._internal(UserProfileRepository().credentials);
    }
    return _instance!.._credentials = UserProfileRepository().credentials;
  }

  Future<List<int>> showMyInvites() async {
    final data = _credentials.toJson();
    final response = await _dio.post(ProjectsApi.showMyInvites, data: data);
    if (response.data.toString() == 'null') {
      return const [];
    } else {
      var result = <int>[];
      for (var invite in response.data) {
        result.add(int.parse(invite['pid']));
      }
      return result;
    }
  }

  Future<String> confirmQR({required String qr}) async {
    final data = _credentials.toJson();
    data['qr'] = qr;

    return (await _dio.post(EventsApi.confirmQR,
            data: data, options: Options(responseType: ResponseType.plain)))
        .data
        .toString();
  }

  Future<List<String>> searchEventTags({required String mask}) async {
    final result = <String>[];
    final data = _credentials.toJson();

    if (mask.isNotEmpty) {
      data['name'] = mask;
    }
    final response = await _dio.post(
        mask.isEmpty ? EventsApi.showAllEventTags : EventsApi.searchEventTags,
        data: data);
    if (response.data.toString() != 'false') {
      for (var i in response.data) {
        result.add(i.toString());
      }
    }
    return result;
  }

  Future<List<PortfolioTag>> fetchNewsfeedTags(
      {required int id, required RequestMode mode}) async {
    var result = <PortfolioTag>[];
    final data = _credentials.toJson();

    var api = '';
    var id_name = 'id';
    switch (mode) {
      case RequestMode.Event:
        api = EventsApi.eventTags;
        id_name = 'eid';
        break;
      case RequestMode.Project:
        api = ProjectsApi.getProjectTags;
        id_name = 'pid';
        break;
      case RequestMode.Job:
        api = VacanciesApi.getVacancyTags;
        id_name = 'vid';
        break;
      case RequestMode.Interest:
        api = EventsApi.showInterestTags;
        break;
      default:
    }

    data[id_name] = id.toString();

    final tagsResponse = await _dio.post(api,
        data: data, options: Options(responseType: ResponseType.plain));
    final tagsResponseString = tagsResponse.data.toString();

    if (tagsResponseString != 'false') {
      try {
        final tagNames = jsonDecode(tagsResponseString);
        if (mode == RequestMode.Interest) {
          result = [
            for (var tag in tagNames)
              PortfolioTag(tag: tag, level: ProfessionalLevel.Newsfeed)
          ];
        } else {
          result = [
            for (var tag in tagNames)
              PortfolioTag(tag: tag['name'], level: ProfessionalLevel.Newsfeed)
          ];
        }
      } catch (_) {
        // print(e.toString());
        return result;
      }
    }
    return result;
  }

  Future<JobModel> getVacancy(String vid) async {
    final cachedResponse = _jobMemCache[vid];

    if (cachedResponse != null) {
      final response = await cachedResponse;
      return response;
    } else {
      final data = _credentials.toJson();
      data['vid'] = vid;

      _jobMemCache[vid] =
          _dio.post(VacanciesApi.getVacancy, data: data).then((response) async {
        final model = JobModel.fromJson(response.data);
        return model;
      });
      return _jobMemCache[vid]!;
    }
  }

  Future<List<JobModel>?> getVacancies(
      {List<PortfolioTag>? tags, String? query}) async {
    final data = _credentials.toJson();
    if (tags != null) {
      data['tags'] = tags.map((e) => e.tag).join(';');
    }
    if (query != null) {
      data['q'] = query;
    }

    final response = await _dio.post(VacanciesApi.getVacancies, data: data);
    final responseString = response.data.toString();

    if (responseString == 'false') {
      return const [];
    } else {
      // final responsejson = jsonDecode(responseString);
      final a = <JobModel>[];

      for (var event in response.data) {
        final model = JobModel.fromJson(event);

        model.tags =
            await fetchNewsfeedTags(id: model.id, mode: RequestMode.Job);
        // print(model.tags);
        a.add(model);
      }
      return a;
    }
  }

  Future<bool> createVacancy({required JobModel vacancy}) async {
    final data = vacancy.toJson();
    data['session'] = _credentials.session;
    data['token'] = _credentials.token;

    final response = await _dio.post(VacanciesApi.createVacancy, data: data);
    final vid = response.data.toString();
    if (vid != 'false') {
      final id = int.parse(vid);
      for (var tag in vacancy.tags)
        await pinTag(pid: id, tag: tag.tag, mode: RequestMode.Job);
    }
    return response.statusCode == 200;
  }

  Future<bool> applyToJob({required String vacancy_id}) async {
    final data = _credentials.toJson();
    data['vid'] = vacancy_id;

    try {
      final response =
          await _dio.post(VacanciesApi.vacancyResponse, data: data);
      return response.statusCode == 200 &&
          !response.data.toString().contains('false');
    } catch (_) {
      return false;
    }
  }

  Future<EventModel?> fetchEvent({required int id}) async {
    final cachedResponse = _eventMemCache[id];

    if (cachedResponse != null) {
      return await cachedResponse;
    } else {
      final data = _credentials.toJson();
      data['eid'] = id.toString();

      _eventMemCache[id] = _dio
          .post(EventsApi.showEvent,
              data: data, options: Options(responseType: ResponseType.plain))
          .then((response) async {
        final responseString = response.data.toString();

        final model = EventModel.fromJson(jsonDecode(responseString).first);
        final tags = await fetchNewsfeedTags(id: id, mode: RequestMode.Event);
        final liked = (await showLikers(id)).contains(this._credentials.id);
        model.liked = liked;
        model.tags = tags;
        return model;
      });
    }
    return _eventMemCache[id]!;
  }

  Future<List<EventModel>> fetchEvents(
      {String? query, List<PortfolioTag>? tags}) async {
    final data = _credentials.toJson();

    if (query?.isNotEmpty ?? false) {
      data['q'] = query!;
    }

    if (tags?.isNotEmpty ?? false) {
      data['tags'] = tags!.map((e) => e.tag).join(';');
    }

    final response = await _dio.post(EventsApi.getEvents,
        data: data, options: Options(responseType: ResponseType.plain));
    final responseString = response.data.toString();

    if (responseString == 'false') {
      return const [];
    } else {
      final responsejson = jsonDecode(responseString);
      final a = <EventModel>[];

      for (var event in responsejson) {
        final model = EventModel.fromJson(event);
        if (DateTime.now().isAfter(model.date.toLocal())) {
          continue;
        }
        final tags =
            await fetchNewsfeedTags(id: model.id, mode: RequestMode.Event);
        final liked =
            (await showLikers(model.id)).contains(this._credentials.id);
        a.add(model
          ..tags = tags
          ..liked = liked);
      }
      return a;
    }
  }

  Future<NewsfeedSearchResponse> fetchPortfolios({
    List<PortfolioTag>? tags,
    String? query,
  }) async {
    final data = _credentials.toJson();

    if (tags != null && tags.isNotEmpty)
      data['tags'] = tags.map((e) => e.tag).join(';');

    if (query != null) data['text'] = query;

    try {
      final responseJson = await _dio.post(PortfolioApi.searchResume,
          data: data, options: Options(responseType: ResponseType.plain));
      final responseData = responseJson.data.toString();

      if (responseData.contains('false')) {
        return NewsfeedSearchResponse.done(entries: const []);
      }
      final response = jsonDecode(responseData);
      final entries = <NewsfeedSearchEntry>[];

      for (var entry in response) {
        final user = UserProfile(
            lid: entry['id'],
            name: entry['name'],
            surname: entry['surname'],
            patronymic: entry['patronymic'],
            avatar: entry['photo'],
            sex: entry['sex'],
            phone: '',
            email: '',
            isAdmin: false,
            isEmployee: false,
            isStudent: false,
            isTeacher: false);
        final langs = jsonDecode(entry['lang'] ?? '[]') ?? const [];
        final exps = jsonDecode(entry['experience'] ?? '[]') ?? const [];
        final portfolio = PortfolioState(
            initializing: false,
            tags: PortfolioTagList(const []),
            dirty: false,
            professionalSkills: entry['professionals'] ?? '',
            workExperience: <String>[for (var exp in exps) exp],
            achievements: const [],
            languages: <String>[for (var lang in langs) lang]);
        entries.add(NewsfeedSearchEntry(profile: user, portfolio: portfolio));
      }
      return NewsfeedSearchResponse.done(entries: entries);
    } catch (e) {
      return NewsfeedSearchResponse.error(error: e.toString());
    }
  }

  Future<List<String>> showLikers(int eid) async {
    final data = _credentials.toJson();
    data['eid'] = eid.toString();

    final response = await _dio.post(EventsApi.showLikers, data: data);
    final result = <String>[];
    try {
      jsonDecode(response.data.toString())
          .forEach((a) => result.add(a.toString()));
    } catch (_) {
      return result;
    }
    return result;
  }

  Future<void> likeEvent(int eid, {bool dislike = false}) async {
    final data = _credentials.toJson();
    data['eid'] = eid.toString();
    if (dislike) {
      data['add'] = '1';
    }

    await _dio.post(EventsApi.likeEvent, data: data);
  }

  Future<bool> requestToProject(int pid) async {
    final data = _credentials.toJson();
    data['pid'] = pid.toString();
    final response = await _dio.post(ProjectsApi.requestToProject, data: data);
    return !response.data.toString().contains('false');
  }

  Future<void> sendFromNewsfeed(
      {String? uid, String? cid, required NewsfeedObject sendedObject}) async {
    final data = _credentials.toJson();
    final id = sendedObject.id;
    String api;

    switch (sendedObject.newsfeedType) {
      case RequestMode.Event:
        data['eid'] = id.toString();
        api = EventsApi.sendEvent;
        break;
      case RequestMode.Job:
        data['vid'] = id.toString();
        api = VacanciesApi.sendVacancyToChat;
        break;
      case RequestMode.Project:
        data['pid'] = id.toString();
        api = ProjectsApi.sendProject;
        break;
      default:
        api = '';
    }

    if (cid != null) {
      data['type'] = 'group';
      data['cid'] = cid;
    }
    if (uid != null) {
      data['type'] = 'chat';
      data['cid'] = uid;
    }
    if (api.isNotEmpty) await _dio.post(api, data: data);
  }

  Future<List<int>?> showProjectInvites({required int pid}) async {
    final data = _credentials.toJson();
    data['pid'] = pid.toString();

    final response =
        await _dio.post(ProjectsApi.showProjectInvites, data: data);
    if (response.data.toString() == 'null') return null;
    var result = <int>[];
    try {
      for (var i in response.data) {
        result.add(int.parse(i['uid']));
      }
      return result;
    } catch (_) {
      return null;
    }
  }

  Future<ProjectModel> fetchProject({required int pid}) async {
    final data = _credentials.toJson();
    data['pid'] = pid.toString();

    final response = await _dio.post(ProjectsApi.getProject, data: data);
    final tags = await fetchNewsfeedTags(id: pid, mode: RequestMode.Project);
    final participants = await fetchParticipants(projectId: pid);

    final project = ProjectModel.fromJson(response.data);

    project
      ..participants = participants
      ..tags = tags;

    return project;
  }

  Future<List<ProjectModel>> fetchProjects(
      {String? uid, String? query, List<PortfolioTag>? tags}) async {
    final data = _credentials.toJson();
    var api = ProjectsApi.getProjects;

    if (uid != null) {
      api = ProjectsApi.getProjectList;
      data['uid'] = uid;
    }

    if (query?.isNotEmpty ?? false) {
      data['q'] = query!;
    }

    if (tags?.isNotEmpty ?? false) {
      data['tags'] = tags!.map((e) => e.tag).join(';');
    }

    final response = await _dio.post(api, data: data);
    if (response.data.toString() == 'false') return [];
    var result = response.data.map((e) => ProjectModel.fromJson(e));

    final projects = <ProjectModel>[];
    for (var project in result) {
      projects.add(project);
    }

    await Future.wait(projects.map((ProjectModel project) =>
        fetchNewsfeedTags(id: project.id, mode: RequestMode.Project)
            .then((value) => project.tags = value)
            .then((value) => fetchParticipants(projectId: project.id)
                .then((value) => project.participants = value))));

    return projects;
  }

  Future<List<ProjectParticipant>> fetchParticipants(
      {required int projectId}) async {
    final result = <ProjectParticipant>[];
    final data = _credentials.toJson();
    data['pid'] = projectId.toString();

    final response = await _dio.post(ProjectsApi.getProjectMembers, data: data);
    if (response.data.toString() == 'false') return result;
    final res = response.data.map((e) => ProjectParticipant.fromJson(e));

    for (var i in res) {
      result.add(i);
    }

    return result;
  }

  Future<List<ProjectPost>> fetchProjectPosts({required int pid}) async {
    final result = <ProjectPost>[];

    final data = _credentials.toJson();
    data['pid'] = pid.toString();

    final response = await _dio.post(ProjectsApi.getProjectFeed, data: data);
    if (response.data.toString() == 'false') return result;

    for (var i in response.data) {
      final post = ProjectPost.fromJson(i);

      if (i['isPhoto'] == '1') {
        post.photos = await getProjectFeedAttachments(
            pid: post.id, proejctList: false, photos: true);
      }

      if (i['isFile'] == '1') {
        post.attachments = await getProjectFeedAttachments(
            pid: post.id, proejctList: false, photos: false);
      }

      result.add(post);
    }
    result.sort((a, b) => b.date.compareTo(a.date));
    return result;
  }

  Future<bool> publicatePost(
      {required int pid,
      required String title,
      required List<String>? photos,
      required List<String>? attachments,
      required String description}) async {
    final Map<String, dynamic> data = {
      'token': _credentials.token,
      'session': _credentials.session
    };
    final dio = Dio();
    final withoutFiles =
        (photos?.isEmpty ?? true) && (attachments?.isEmpty ?? true);
    if (withoutFiles)
      dio.options.contentType = Headers.formUrlEncodedContentType;

    if (photos != null)
      for (var i = 0; i < photos.length; i++)
        data['photo_$i'] = await MultipartFile.fromFile(photos[i]);

    if (attachments != null)
      for (var i = 0; i < attachments.length; i++)
        data['file_$i'] = await MultipartFile.fromFile(attachments[i]);

    data['pid'] = pid.toString();
    data['title'] = title;
    data['description'] = description;

    final response = await dio.post(ProjectsApi.publicateProjectPost,
        data: withoutFiles ? data : FormData.fromMap(data));

    return (response.statusCode == 200);
  }

  Future<int?> createProject(
      {required String title,
      required String description,
      required DateTime end,
      DateTime? start}) async {
    final data = _credentials.toJson();

    data['title'] = title;
    data['description'] = description;
    data['end'] = end.toString();
    if (start != null) data['start'] = start.toString();

    final response = await _dio.post(ProjectsApi.createProject, data: data);

    final responseString = response.data.toString();
    if (responseString == 'false') return null;
    return int.tryParse(responseString);
  }

  Future<void> editProject(
      {required int pid,
      String? title,
      String? description,
      String? start,
      String? end}) async {
    final List<Map<String, String>> requestMap = [];

    if (title != null) requestMap.add({'key': 'title', 'val': title});
    if (description != null)
      requestMap.add({'key': 'description', 'val': description});
    if (start != null) requestMap.add({'key': 'start', 'val': start});
    if (end != null) requestMap.add({'key': 'end', 'val': end});

    for (var request in requestMap) {
      final data = _credentials.toJson();
      data['pid'] = pid.toString();
      data.addAll(request);

      await _dio.post(ProjectsApi.editProject, data: data);
    }
  }

  Future<void> pinTag(
      {required int pid,
      required String tag,
      bool unpin = false,
      required RequestMode mode}) async {
    final data = _credentials.toJson();
    String api;
    switch (mode) {
      case RequestMode.Project:
        api = unpin ? ProjectsApi.unpinProjectTag : ProjectsApi.pinProjectTag;
        data['pid'] = pid.toString();
        break;
      case RequestMode.Interest:
        api = unpin ? EventsApi.removeInterestTag : EventsApi.pinInterestTag;
        break;
      case RequestMode.Job:
        api = VacanciesApi.addVacancyTag;
        data['vid'] = pid.toString();
        break;
      default:
        api = '';
    }

    data['name'] = tag;
    if (api.isNotEmpty)
      await _dio.post(api,
          data: data, options: Options(responseType: ResponseType.plain));
  }

  Future<List<String>> getProjectFeedAttachments(
      {required int pid,
      required bool proejctList,
      required bool photos}) async {
    final data = _credentials.toJson();
    if (proejctList)
      data['pid'] = pid.toString();
    else
      data['fied'] = pid.toString();

    final api = photos
        ? proejctList
            ? ProjectsApi.getProejctFeedPhotosList
            : ProjectsApi.getFeedPhotos
        : proejctList
            ? ProjectsApi.getProjectFeedFilesList
            : ProjectsApi.getFeedFiles;

    final response = await _dio.post(api, data: data);
    if (response.data.toString() == 'false') return const [];

    var result = <String>[];
    for (var attachment in response.data) {
      result.add(
          (photos ? ProjectsApi.showFeedPhotos : ProjectsApi.showFeedFiles) +
              attachment['name']);
    }

    return result;
  }

  Future<bool> addProjectMember(
      {required int pid, required String uid, String? text}) async {
    final api = text != null
        ? ProjectsApi.deleteProjectMember
        : ProjectsApi.inviteProjectMember;

    final data = _credentials.toJson();
    data['uid'] = uid;
    data['pid'] = pid.toString();
    if (text != null) data['text'] = text;

    try {
      final response = await _dio.post(api, data: data);
      return response.statusCode == 200;
    } catch (_) {
      return false;
    }
  }

  Future<void> uploadProjectPhoto(
      {required int pid, required String photo}) async {
    final Map<String, dynamic> data = {
      'token': _credentials.token,
      'session': _credentials.session,
      'pid': pid.toString()
    };
    final dio = Dio();
    data['photo'] = await MultipartFile.fromFile(photo);

    await dio.post(ProjectsApi.uploadProjectPhoto,
        data: FormData.fromMap(data));
  }

  Future<void> editProjectRole(
      {required String uid, required String role, required int pid}) async {
    final data = _credentials.toJson();
    data['uid'] = uid;
    data['pid'] = pid.toString();
    data['key'] = 'role';
    data['val'] = role;

    await _dio.post(ProjectsApi.editStatus, data: data);
  }

  Future<void> editProjectMemberStatus(
      {required String uid,
      required String key,
      required String val,
      required int pid}) async {
    final data = _credentials.toJson();
    data['key'] = key;
    data['uid'] = uid;
    data['val'] = val;
    data['pid'] = pid.toString();

    await _dio.post(ProjectsApi.editStatus, data: data);
  }

  Future<void> leaveProject({required int pid}) async {
    final data = _credentials.toJson();
    data['pid'] = pid.toString();

    await _dio.post(ProjectsApi.leaveProject, data: data);
  }

  Future<bool> manageInvitesForProjects(
      {required int pid, required String uid, bool dismiss = false}) async {
    final api = dismiss
        ? ProjectsApi.dismissRequestToProject
        : ProjectsApi.approveRequestToProject;
    final data = _credentials.toJson();
    data['pid'] = pid.toString();
    data['uid'] = uid;
    final response = await _dio.post(api, data: data);

    return response.data == true;
  }

  Future<void> acceptInvite({required int pid}) async {
    final data = _credentials.toJson();
    data['pid'] = pid.toString();
    await _dio.post(ProjectsApi.approveMyRequestToProject, data: data);
  }
}
