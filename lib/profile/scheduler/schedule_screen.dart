import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/newsfeed/ui/pages/events/event_screen.dart';
import 'package:teormech/profile/scheduler/bloc/schedule_bloc.dart';
import 'package:teormech/profile/scheduler/event_qr_scanner.dart';
import 'package:teormech/profile/scheduler/widgets/note_dialog.dart';
import 'package:teormech/profile/scheduler/widgets/schedule_element.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/styles/transitions/slide_fade.dart';

import 'model/schedule_model.dart';

class ScheduleScreen extends StatefulWidget {
  ScheduleScreen({required this.teacher});

  final String? teacher;

  @override
  _ScheduleScreenState createState() => _ScheduleScreenState();

  static PageRouteBuilder route({required String? teacher}) {
    return PageRouteBuilder(
        transitionsBuilder: slideTransition,
        reverseTransitionDuration: Duration.zero,
        pageBuilder: (context, f1, f2) => ScheduleScreen(teacher: teacher));
  }
}

class _ScheduleScreenState extends State<ScheduleScreen>
    with SingleTickerProviderStateMixin {
  String _title(DateTime date) {
    final rawString = DateFormat.yMMMM(_locale).format(date);
    return rawString.substring(0, 1).toUpperCase() + rawString.substring(1);
  }

  PageController? _controller;

  String? _locale;

  String _focusedDay = '';
  DateTime _focusedDateTime = DateTime.now();

  late final AnimationController _animationController;
  late final Animation<Offset> _animation;
  bool _dialogShowed = false;

  final _captionController = TextEditingController();
  final _descriptionController = TextEditingController();

  DateTime _selectedDateTime = DateTime.now();
  TimeOfDay _selectedTimeOfDay = TimeOfDay.now();

  bool _sameDay(DateTime? a, DateTime? b) {
    return a?.year == b?.year && a?.month == b?.month && a?.day == b?.day;
  }

  SchedulerCubit? schedulerCubit;

  @override
  void initState() {
    if (widget.teacher != null)
      schedulerCubit = SchedulerCubit(teacher: widget.teacher);
    initializeDateFormatting().then((_) {
      setState(() {
        _locale = Localizations.maybeLocaleOf(context)?.languageCode;

        final rawString = DateFormat.yMMMM(_locale).format(_focusedDateTime);
        _focusedDay =
            rawString.substring(0, 1).toUpperCase() + rawString.substring(1);
      });
    });

    _animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 500));
    Tween<Offset> tween = Tween(begin: Offset(0, -1), end: Offset(0, 0.1));
    _animation = tween.animate(_animationController);

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _animationController.dispose();
    _captionController.dispose();
    _descriptionController.dispose();
    schedulerCubit?.close();
  }

  void _clear() {
    _captionController.clear();
    _descriptionController.clear();

    FocusScope.of(context).unfocus();
  }

  void _prevPage() {
    _controller?.previousPage(
        duration: Duration(milliseconds: 300), curve: Curves.easeOut);
  }

  void _nextPage() {
    _controller?.nextPage(
        duration: Duration(milliseconds: 300), curve: Curves.easeOut);
  }

  void _togglePopup(bool show) {
    if (show && !_dialogShowed) {
      _animationController.forward();
      setState(() {
        _dialogShowed = true;
      });
    }
    if (!show && _dialogShowed) {
      _animationController.reverse();
      setState(() {
        _dialogShowed = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final scheduleScreen = GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
        _togglePopup(false);
      },
      behavior: HitTestBehavior.translucent,
      child: SafeArea(child: BlocBuilder<SchedulerCubit, ScheduleState>(
        builder: (context, state) {
          return Scaffold(
              appBar: ProfileAppBar(
                leftAlignedTitle: true,
                rightmostButton: IconButton(
                    onPressed: () {
                      context
                          .read<BottomBarBloc>()
                          .go(EventQrScannerPage.route(), hideBottombar: true);
                    },
                    icon: Icon(
                      Icons.qr_code,
                      color: Colors.white,
                    )),
                title: Text(
                  widget.teacher == null ? 'Расписание' : widget.teacher!,
                  style: appBarTextStyle,
                ),
              ),
              backgroundColor: Colors.transparent,
              body: Stack(
                children: [
                  Positioned.fill(
                    child: Column(children: [
                      PhysicalModel(
                        color: Colors.white,
                        elevation: 2,
                        shadowColor: Colors.black.withAlpha(120),
                        child: Row(
                          children: [
                            IconButton(
                                onPressed: _prevPage,
                                icon: Icon(CupertinoIcons.arrow_left,
                                    color: Colors.grey)),
                            Spacer(),
                            Text(_focusedDay,
                                style: appBarTextStyle.copyWith(
                                    color: GradientColors.pink,
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold)),
                            IconButton(
                              onPressed: () async {
                                final _date = await showDatePicker(
                                    context: context,
                                    initialDate: DateTime.now(),
                                    initialEntryMode:
                                        DatePickerEntryMode.inputOnly,
                                    firstDate: DateTime(2010),
                                    lastDate: DateTime(2050));
                                if (_date == null) return;
                                setState(() {
                                  _focusedDateTime = _date;
                                  _selectedDateTime = _date;
                                  context
                                      .read<SchedulerCubit>()
                                      .selectDay(_date);
                                });
                              },
                              icon: Icon(
                                CupertinoIcons.calendar,
                                color: GradientColors.pink,
                              ),
                            ),
                            Spacer(),
                            IconButton(
                                onPressed: _nextPage,
                                icon: Icon(CupertinoIcons.arrow_right,
                                    color: Colors.grey)),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0),
                        child: TableCalendar<CalendarEvent>(
                          firstDay: DateTime(2010),
                          lastDay: DateTime(2050),
                          eventLoader: (day) {
                            return state.events.events
                                .where((event) =>
                                    _sameDay(day, event.startDateTime))
                                .toList();
                          },
                          headerVisible: false,
                          onPageChanged: (focusedDate) {
                            setState(() {
                              _focusedDay = _title(focusedDate);
                            });
                            _focusedDateTime = focusedDate;
                            context
                                .read<SchedulerCubit>()
                                .selectDay(focusedDate, selected: false);
                          },
                          rowHeight: 48,
                          holidayPredicate: (day) {
                            return (state.events.events
                                .where((e) => _sameDay(e.startDateTime, day))
                                .where((day) =>
                                    day.type == CalendarEventType.Lecture)
                                .isEmpty);
                          },
                          calendarBuilders: CalendarBuilders(
                            holidayBuilder: (context, day, focusedDay) {
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 4.0),
                                    child: Text(
                                      day.day.toString(),
                                      textAlign: TextAlign.center,
                                      style: appBarTextStyle.copyWith(
                                          color: Colors.grey.shade500,
                                          fontWeight: FontWeight.w300,
                                          fontSize: 16),
                                    ),
                                  ),
                                ],
                              );
                            },
                            selectedBuilder: (context, day, focusedDay) {
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 4.0),
                                    child: Container(
                                      padding: day.day < 10
                                          ? const EdgeInsets.symmetric(
                                              horizontal: 7, vertical: 2.5)
                                          : const EdgeInsets.symmetric(
                                              vertical: 2.5, horizontal: 3.5),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          color: GradientColors.pink),
                                      child: Text(
                                        day.day.toString(),
                                        textAlign: TextAlign.center,
                                        style: appBarTextStyle.copyWith(
                                            fontWeight: FontWeight.w300,
                                            fontSize: 16),
                                      ),
                                    ),
                                  ),
                                ],
                              );
                            },
                            defaultBuilder: (context, day, focusedDay) {
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 4.0),
                                    child: Text(
                                      day.day.toString(),
                                      textAlign: TextAlign.center,
                                      style: appBarTextStyle.copyWith(
                                          color: Colors.black, fontSize: 16),
                                    ),
                                  ),
                                ],
                              );
                            },
                            dowBuilder: (context, day) {
                              final dow =
                                  DateFormat.E(_locale ?? 'ru_RU').format(day);

                              return Center(
                                child: Text(dow,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold)),
                              );
                            },
                            markerBuilder: (context, day, events) {
                              final types = events
                                  .map((event) => event.type)
                                  .toList()
                                ..sort((a, b) => a.index - b.index);

                              var typesCount = 0;
                              final containsEventMeeting = types
                                  .contains(CalendarEventType.EventMeeting);
                              if (containsEventMeeting) typesCount++;
                              final containsNotes =
                                  types.contains(CalendarEventType.Note);
                              if (containsNotes) typesCount++;
                              final containsInterestingEvents = types
                                  .contains(CalendarEventType.RecommendedEvent);
                              if (containsInterestingEvents) typesCount++;
                              final containsEvents =
                                  types.contains(CalendarEventType.Event);
                              if (containsEvents) typesCount++;

                              if (typesCount == 1) {
                                return Padding(
                                  padding: const EdgeInsets.only(bottom: 10.0),
                                  child: CalendarDotMarker(
                                      color:
                                          ScheduleElement.mapEventTypeToColor(
                                              types.last)),
                                );
                              }

                              if (events.isNotEmpty)
                                return Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: SizedBox(
                                      width: 14,
                                      height: 14,
                                      child: Column(
                                        children: [
                                          Row(children: [
                                            containsNotes
                                                ? CalendarDotMarker(
                                                    color: ScheduleElement
                                                        .mapEventTypeToColor(
                                                            CalendarEventType
                                                                .Note),
                                                  )
                                                : SizedBox(width: 7, height: 7),
                                            containsInterestingEvents
                                                ? CalendarDotMarker(
                                                    color: ScheduleElement
                                                        .mapEventTypeToColor(
                                                            CalendarEventType
                                                                .RecommendedEvent),
                                                  )
                                                : SizedBox(width: 7, height: 7),
                                          ]),
                                          Row(
                                            children: [
                                              containsEventMeeting
                                                  ? CalendarDotMarker(
                                                      color: ScheduleElement
                                                          .mapEventTypeToColor(
                                                              CalendarEventType
                                                                  .EventMeeting),
                                                    )
                                                  : SizedBox(
                                                      width: 7, height: 7),
                                              containsEvents
                                                  ? CalendarDotMarker(
                                                      color: ScheduleElement
                                                          .mapEventTypeToColor(
                                                              CalendarEventType
                                                                  .Event),
                                                    )
                                                  : SizedBox(
                                                      width: 7, height: 7),
                                            ],
                                          )
                                        ],
                                      )),
                                );
                              return null;
                            },
                          ),
                          onDaySelected: (selectedDay, focusedDay) {
                            context
                                .read<SchedulerCubit>()
                                .selectDay(selectedDay);
                            setState(() {
                              _selectedDateTime = selectedDay;
                              _focusedDateTime = selectedDay;
                            });
                          },
                          daysOfWeekHeight: 24,
                          selectedDayPredicate: (day) =>
                              _sameDay(state.selectedDay, day),
                          calendarStyle: CalendarStyle(
                            outsideDaysVisible: false,
                            cellMargin: EdgeInsets.zero,
                            isTodayHighlighted: false,
                            rowDecoration: BoxDecoration(
                                border: Border(
                                    top: BorderSide(
                                        color: Colors.grey.shade300))),
                          ),
                          onCalendarCreated: (controller) =>
                              _controller = controller,
                          startingDayOfWeek: StartingDayOfWeek.monday,
                          //locale: 'ru_RU',
                          focusedDay: _focusedDateTime,
                        ),
                      ),
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                              color: Color(0xFFFAFAFA),
                              border: Border(
                                  top:
                                      BorderSide(color: Colors.grey.shade200))),
                          child: Builder(builder: (context) {
                            final events = state.events.events
                                .where((element) => _sameDay(
                                    state.selectedDay, element.startDateTime))
                                .toList();
                            events.sort((eventA, eventB) =>
                                eventA.startDateTime?.compareTo(
                                    eventB.startDateTime ?? DateTime.now()) ??
                                0);
                            if (events.isNotEmpty) {
                              final eventsElements = events.map((e) =>
                                  ScheduleElement(
                                      event: e,
                                      onTap:
                                          e.type == CalendarEventType.Event ||
                                                  e.type ==
                                                      CalendarEventType
                                                          .RecommendedEvent
                                              ? () {
                                                  context
                                                      .read<BottomBarBloc>()
                                                      .go(EventScreen.route(
                                                          eventId: e.id));
                                                }
                                              : null,
                                      rightIcon:
                                          (e.type == CalendarEventType.Note)
                                              ? IconButton(
                                                  color: GradientColors.pink,
                                                  onPressed: () {
                                                    context
                                                        .read<SchedulerCubit>()
                                                        .deleteNote(id: e.id);
                                                  },
                                                  icon: ImageIcon(AssetImage(
                                                      'images/icons/chat_options/clean.png')),
                                                )
                                              : null,
                                      key: Key(e.startDateTime.toString())));
                              return ListView(
                                children: [
                                  ...eventsElements,
                                  appendEventRow(context, centered: false)
                                ],
                              );
                            } else
                              return Center(
                                  child: Column(
                                children: [
                                  Spacer(flex: 2),
                                  Flexible(
                                      flex: 2,
                                      child: Text(
                                          'В этот день у вас нет событий.')),
                                  appendEventRow(context),
                                  Spacer(
                                    flex: 3,
                                  )
                                ],
                              ));
                          }),
                        ),
                      )
                    ]),
                  ),
                  Positioned.fill(
                      child: Offstage(
                    offstage: !_dialogShowed,
                    child: Container(
                      color: GradientColors.updatingIndicatorColor,
                    ),
                  )),
                  Align(
                    alignment: Alignment.topCenter,
                    child: SlideTransition(
                      position: _animation,
                      child: Offstage(
                          offstage: !_dialogShowed,
                          child: NoteDialog(
                            captionController: _captionController,
                            descriptionController: _descriptionController,
                            selectedDateTime: _selectedDateTime,
                            selectedTimeOfDay: _selectedTimeOfDay,
                            locale: _locale,
                            dateTimeCallback: (p0) => setState(() {
                              _selectedDateTime = p0;
                            }),
                            timeOfDayCallback: (p0) => setState(() {
                              _selectedTimeOfDay = p0;
                            }),
                            send: () {
                              context.read<SchedulerCubit>()
                                ..addNote(
                                    date: DateTime(
                                        _selectedDateTime.year,
                                        _selectedDateTime.month,
                                        _selectedDateTime.day,
                                        _selectedTimeOfDay.hour,
                                        _selectedTimeOfDay.minute),
                                    title: _captionController.text.trim(),
                                    body: _descriptionController.text.trim())
                                ..selectDay(_selectedDateTime, selected: false);
                              _clear();
                              _togglePopup(false);
                            },
                          )),
                    ),
                  ),
                ],
              ));
        },
      )),
    );
    if (schedulerCubit == null) {
      return scheduleScreen;
    } else {
      return BlocProvider<SchedulerCubit>.value(
        value: schedulerCubit!,
        child: Builder(builder: (context) => scheduleScreen),
      );
    }
  }

  MaterialButton appendEventRow(BuildContext context, {bool centered = true}) {
    return MaterialButton(
      onPressed: () {
        FocusScope.of(context).unfocus();

        _togglePopup(true);
      },
      child: Row(
        mainAxisAlignment:
            centered ? MainAxisAlignment.center : MainAxisAlignment.start,
        children: [
          Icon(
            Icons.add_circle,
            color: GradientColors.pink,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Добавить заметку',
              style: TextStyle(color: Colors.pink),
            ),
          )
        ],
      ),
    );
  }
}

class CalendarDotMarker extends StatelessWidget {
  const CalendarDotMarker({Key? key, required this.color}) : super(key: key);

  final Color color;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 7,
      height: 7,
      decoration:
          BoxDecoration(color: color, borderRadius: BorderRadius.circular(12)),
    );
  }
}
