import 'package:bloc/bloc.dart';
import 'package:hive/hive.dart';
import 'package:teormech/profile/scheduler/model/schedule_model.dart';
import 'package:teormech/profile/scheduler/rep/schedule_repository.dart';

class ScheduleState {
  final DateTime selectedDay;

  final CalendarEventList events;

  ScheduleState({
    required this.selectedDay,
    required this.events,
  });

  ScheduleState.empty()
      : selectedDay = DateTime.now(),
        events = CalendarEventList(const []);

  ScheduleState copyWith(
      {DateTime? selectedDay,
      CalendarEventList? events,
      DateTime? focusedDay}) {
    return ScheduleState(
        selectedDay: selectedDay ?? this.selectedDay,
        events: events ?? this.events);
  }
}

class SchedulerCubit extends Cubit<ScheduleState> {
  SchedulerCubit({required this.teacher}) : super(ScheduleState.empty()) {
    _load(DateTime.now(), fetchFromServer: true, select: false);
  }

  final String? teacher;

  Box<CalendarEventList>? _box;

  void _load(DateTime month,
      {bool? fetchFromServer = true,
      DateTime? selectedDay,
      required bool select}) async {
    if (_box == null && teacher == null)
      _box = await Hive.openBox<CalendarEventList>('months_events');

    if (teacher == null) {
      final events = _box!.get('${month.year}_${month.month}');
      emit(state.copyWith(
          events: events, selectedDay: select ? selectedDay : null));
    }

    final fetchedEvents = await ScheduleRepository()
        .getSchedule(month: '${month.year}-${month.month}-1', teacher: teacher);
    if (fetchedEvents.isNotEmpty) {
      final list = CalendarEventList(fetchedEvents);

      emit(state.copyWith(
          events: list, selectedDay: select ? selectedDay : null));
      if (teacher == null) _box!.put('${month.year}_${month.month}', list);
    }
  }

  @override
  Future<void> close() async {
    _box?.close();
    return super.close();
  }

  void selectDay(DateTime day, {bool selected = true}) {
    if (!selected) {
      _load(day, select: false);
    } else {
      emit(state.copyWith(selectedDay: day));
    }
  }

  void addNote(
      {required DateTime date,
      required String title,
      required String body}) async {
    await ScheduleRepository().addNote(start: date, title: title, body: body);
    selectDay(date);
    _load(date, select: true);
  }

  void deleteNote({required int id}) async {
    await ScheduleRepository().deleteNote(nid: id);
    _load(DateTime.now(), fetchFromServer: true, select: false);
  }
}
