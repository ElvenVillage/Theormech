import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/profile/profile/rep/user_profile_rep.dart';
import 'package:teormech/profile/scheduler/model/schedule_model.dart';

class ScheduleRepository {
  static ScheduleRepository? _instance;
  late final Dio _dio;

  ScheduleRepository._internal() {
    _dio = Dio()..options.contentType = Headers.formUrlEncodedContentType;
  }

  factory ScheduleRepository() {
    return _instance ??= ScheduleRepository._internal();
  }

  Future<List<CalendarEvent>> getSchedule(
      {required String month, bool mocked = false, String? teacher}) async {
    if (mocked) {
      return [
        CalendarEvent.fromJson({
          "type": "2",
          "notified": "0",
          "date": "2021-10-28",
          "time_start": "13:27:00",
          "time_end": "13:28:00",
          "title": "Test Note",
          "body": "TEST TEST TEST"
        })
      ];
    }
    final data = UserProfileRepository().credentials.toJson();
    data['date'] = month;

    if (teacher?.isNotEmpty ?? false) {
      data['teacher'] = teacher!;
    }

    final response = await _dio.post(SchedulerApi.getSchedule,
        data: data, options: Options(responseType: ResponseType.plain));
    final List<dynamic> responseJson = jsonDecode(response.data.toString());
    return responseJson.map((e) => CalendarEvent.fromJson(e)).toList();
  }

  Future<int?> addNote(
      {required DateTime start,
      required String title,
      required String body}) async {
    final data = UserProfileRepository().credentials.toJson();
    final str = start.toIso8601String();
    data['date'] = str.substring(0, 10);
    data['time_start'] = str.substring(11, 16);
    data['time_end'] = str.substring(11, 16);
    data['title'] = title;
    data['body'] = body;

    try {
      final response = await _dio.post(SchedulerApi.addNote,
          data: data, options: Options(responseType: ResponseType.plain));
      return int.tryParse(response.data.toString()) ?? -1;
    } catch (_e) {
      return -1;
    }
  }

  Future<bool> deleteNote({required int nid}) async {
    final data = UserProfileRepository().credentials.toJson();
    data['nid'] = nid.toString();

    final response = await _dio.post(SchedulerApi.deleteNote, data: data);
    return response.statusCode == 200;
  }
}
