import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/scheduler/model/schedule_model.dart';
import 'package:teormech/profile/scheduler/schedule_screen.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ScheduleElement extends StatefulWidget {
  const ScheduleElement(
      {Key? key,
      required this.event,
      this.isNotification = false,
      this.withoutTimeColumn = false,
      this.onTap,
      this.rightIcon})
      : super(key: key);

  final CalendarEvent event;
  final bool isNotification;
  final bool withoutTimeColumn;
  final Widget? rightIcon;
  final VoidCallback? onTap;

  @override
  _ScheduleElementState createState() => _ScheduleElementState();

  static Color mapEventTypeToColor(CalendarEventType event) {
    switch (event) {
      case CalendarEventType.Note:
        return GradientColors.pink;
      case CalendarEventType.Lecture:
        return Colors.black;
      case CalendarEventType.RecommendedEvent:
        return GradientColors.backColor;
      case CalendarEventType.Event:
        return Colors.blueGrey;
      case CalendarEventType.EventMeeting:
        return Colors.orange;
    }
  }
}

class _ScheduleElementState extends State<ScheduleElement> {
  final _controller = ExpandableController(initialExpanded: false);
  bool _expanded = false;
  final _contentStyle = TextStyle(color: Colors.grey.shade600, fontSize: 12);

  Widget _contentColumn(bool collapsed) {
    final column = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.event.title,
          style: h2DrawerGreyText.copyWith(color: Colors.black, fontSize: 15),
        ),
        if (widget.isNotification && widget.event.startTime != null)
          Padding(
            padding: const EdgeInsets.only(top: 4.0),
            child: Text(
              widget.event.startTime!,
              style: TextStyle(
                  color: GradientColors.backColor,
                  fontSize: 14,
                  fontWeight: FontWeight.bold),
            ),
          ),
        if (widget.event.object != null)
          Padding(
            padding: const EdgeInsets.only(top: 4.0),
            child: Text(
              widget.event.object!,
              style: _contentStyle,
            ),
          ),
        if (widget.event.body.isNotEmpty || widget.event.etype != null)
          Padding(
            padding: const EdgeInsets.only(top: 4.0),
            child: Text(
              widget.event.etype ??
                  (widget.event.body.length > 100
                      ? widget.event.body.substring(0, 100) + '...'
                      : widget.event.body),
              style: _contentStyle,
            ),
          ),
        if (widget.event.place != null && (!collapsed || widget.onTap != null))
          Padding(
            padding: const EdgeInsets.only(top: 4.0),
            child: Text(
              widget.event.place!,
              style: _contentStyle,
            ),
          ),
        if (widget.event.teacherName != null &&
            !collapsed &&
            (widget.event.teacherName?.isNotEmpty ?? false))
          InkWell(
            onTap: () {
              context
                  .read<BottomBarBloc>()
                  .go(ScheduleScreen.route(teacher: widget.event.teacherName));
            },
            child: Padding(
              padding: const EdgeInsets.only(top: 4.0),
              child: Text(widget.event.teacherName!,
                  style: _contentStyle.copyWith(
                      decoration: TextDecoration.underline)),
            ),
          ),
        if (widget.event.groupOwner != null && !collapsed)
          Padding(
            padding: const EdgeInsets.only(top: 4.0),
            child: Text(widget.event.groupOwner!, style: _contentStyle),
          ),
      ],
    );
    if (widget.onTap != null) {
      return InkWell(
        onTap: widget.onTap,
        child: column,
      );
    } else {
      return column;
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          _controller.toggle();
          setState(() {
            _expanded = !_expanded;
          });
        },
        child: Row(
          children: [
            if (!widget.withoutTimeColumn)
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Container(
                    child: Text((widget.event.type == CalendarEventType.Lecture)
                        ? '${widget.event.startTime?.substring(0, 5)} - ${widget.event.endTime?.substring(0, 5)}'
                        : '       ${widget.event.startTime?.substring(0, 5)}      ')),
              ),
            Padding(
              padding: const EdgeInsets.only(left: 10.0, top: 8.0),
              child: AnimatedContainer(
                width: 3,
                duration: const Duration(milliseconds: 300),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: ScheduleElement.mapEventTypeToColor(widget.event.type),
                ),
                height: _expanded ? 80 : 60,
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  padding: const EdgeInsets.only(left: 10, top: 10, bottom: 10),
                  child: ExpandablePanel(
                    controller: _controller,
                    collapsed: _contentColumn(true),
                    expanded: _contentColumn(false),
                  ),
                ),
              ),
            ),
            if (widget.rightIcon != null)
              Padding(
                padding: const EdgeInsets.only(right: 5.0),
                child: widget.rightIcon!,
              )
          ],
        ));
  }
}
