import 'package:flutter/material.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/widgets/calendar_picker_row.dart';

class NoteDialog extends StatelessWidget {
  NoteDialog(
      {Key? key,
      this.selectedDateTime,
      this.selectedTimeOfDay,
      required this.captionController,
      required this.descriptionController,
      required this.dateTimeCallback,
      required this.send,
      this.locale,
      required this.timeOfDayCallback})
      : super(key: key);

  final DateTime? selectedDateTime;
  final TimeOfDay? selectedTimeOfDay;
  final TextEditingController captionController;
  final TextEditingController descriptionController;

  final SetDateTimeCallback dateTimeCallback;
  final SetTimeOfDayCallback timeOfDayCallback;
  final VoidCallback send;

  final String? locale;

  final _decoration = InputDecoration(
    isDense: true,
    isCollapsed: true,
    contentPadding: const EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
    filled: true,
    fillColor: Colors.white,
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(10.0),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      height: MediaQuery.of(context).size.width * 0.75,
      width: MediaQuery.of(context).size.width * 0.8,
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: GradientColors.updatingIndicatorColor),
          borderRadius: BorderRadius.circular(15)),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Row(
            children: [
              Text('Создание заметки', style: h2TextStyle),
              Spacer(),
              InkWell(
                  onTap: () {
                    if (selectedDateTime == null || selectedTimeOfDay == null) {
                      return;
                    }
                    send();
                  },
                  child: ImageIcon(
                    const AssetImage('images/icons/ok.png'),
                    color: ((selectedDateTime != null) ||
                            (selectedTimeOfDay != null))
                        ? GradientColors.pink
                        : Colors.white,
                  )),
            ],
          ),
          Divider(color: Colors.grey.shade800),
          Padding(
            padding: const EdgeInsets.only(top: 4.0),
            child: Row(
              children: [
                Expanded(
                  child: Text('Название', style: h2TextStyle),
                ),
                Expanded(
                    flex: 2,
                    child: TextField(
                      controller: captionController,
                      maxLines: 1,
                      decoration: _decoration,
                    ))
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Row(
              children: [
                Expanded(child: Text('Описание', style: h2TextStyle)),
                Expanded(
                    flex: 2,
                    child: TextField(
                      controller: descriptionController,
                      decoration: _decoration,
                      maxLines: 3,
                    ))
              ],
            ),
          ),
          Padding(
              padding: const EdgeInsets.only(top: 5),
              child: CalendarPickerRow(
                dateTimeCallback: dateTimeCallback,
                timeOfDayCallback: timeOfDayCallback,
                locale: locale,
                selectedDateTime: selectedDateTime,
                selectedTimeOfDay: selectedTimeOfDay,
              )),
        ]),
      ),
    );
  }
}
