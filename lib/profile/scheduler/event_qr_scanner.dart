import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/styles/text.dart';
import 'package:url_launcher/url_launcher.dart';

class EventQrScannerPage extends StatefulWidget {
  EventQrScannerPage();
  @override
  _EventQrScannerPageState createState() => _EventQrScannerPageState();

  static MaterialPageRoute route() =>
      MaterialPageRoute(builder: (_) => EventQrScannerPage());
}

class _EventQrScannerPageState extends State<EventQrScannerPage> {
  final GlobalKey qrKey = GlobalKey();
  QRViewController? controller;
  StreamSubscription<Barcode>? _subscription;

  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    } else if (Platform.isIOS) {
      controller!.resumeCamera();
    }
  }

  @override
  void dispose() {
    controller?.dispose();
    _subscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            appBar: ProfileAppBar(
              title: Text('Отсканируйте QR-код мероприятия',
                  style: appBarTextStyle),
            ),
            body: Stack(
              children: [
                Positioned.fill(
                    child: QRView(
                  key: qrKey,
                  onQRViewCreated: (controller) {
                    this.controller = controller;
                    _subscription =
                        controller.scannedDataStream.listen((barcode) {
                      _subscription?.cancel();
                      launch(barcode.code!);
                    });
                  },
                )),
                //Align(alignment: Alignment.center, child: Text('TEST')),
                Align(
                    alignment: Alignment.center,
                    child: CustomPaint(
                      painter: CrossingPainter(),
                      size: Size(150, 150),
                    ))
              ],
            )));
  }
}

class CrossingPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = Colors.white
      ..strokeWidth = 5;

    final coords = [
      [-0.01, 0, 0.2, 0],
      [0, -0.01, 0, 0.2],
      [1, -0.01, 1, 0.2],
      [1, 0, 0.8, -0.01],
      [0, 1.01, 0, 0.8],
      [-0.01, 1, 0.2, 1],
      [0.99, 1, 0.99, 0.79],
      [1, 0.99, 0.79, 0.99],
    ];

    for (var angle in coords) {
      final x = size.width * angle[0];
      final y = size.width * angle[1];
      final x1 = size.width * angle[2];
      final y1 = size.width * angle[3];
      canvas.drawLine(Offset(x, y), Offset(x1, y1), paint);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
