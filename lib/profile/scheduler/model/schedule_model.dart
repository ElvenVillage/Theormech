import 'package:hive/hive.dart';

part 'schedule_model.g.dart';

@HiveType(typeId: 13)
class CalendarEventList {
  @HiveField(0)
  final List<CalendarEvent> events;
  CalendarEventList(this.events);
}

@HiveType(typeId: 11)
enum CalendarEventType {
  @HiveField(0)
  Lecture,
  @HiveField(1)
  Note,
  @HiveField(2)
  RecommendedEvent,
  @HiveField(3)
  EventMeeting,
  @HiveField(4)
  Event
}

@HiveType(typeId: 12)
class CalendarEvent {
  @HiveField(0)
  final CalendarEventType type;
  @HiveField(1)
  final String title;
  @HiveField(2)
  final String body;
  @HiveField(3)
  final String? object;
  @HiveField(4)
  final String? place;
  @HiveField(5)
  final String? teacherName;
  @HiveField(6)
  final String date;
  @HiveField(7)
  final String? startTime;
  @HiveField(8)
  final String? endTime;
  @HiveField(9)
  final String? groupOwner;
  @HiveField(10)
  final int id;

  String? etype;

  DateTime? _startDateTime;
  DateTime? _endDateTime;

  DateTime? get startDateTime =>
      _startDateTime ??= DateTime.tryParse('$date $startTime');
  DateTime? get endDateTime =>
      _endDateTime ??= DateTime.tryParse('$date $endTime');

  CalendarEvent(
      {required this.type,
      required this.id,
      required this.title,
      required this.body,
      required this.date,
      this.place,
      this.etype,
      this.object,
      this.teacherName,
      this.startTime,
      this.endTime,
      this.groupOwner});

  factory CalendarEvent.fromJson(Map<String, dynamic> json) {
    return CalendarEvent(
        id: int.tryParse(json['id'] ?? '-1') ?? -1,
        title: json['title'],
        body: json['body'],
        etype: json['etype'],
        type: CalendarEventType.values[int.parse(json['type']) - 1],
        date: json['date'],
        endTime: json['time_end'],
        startTime: json['time_start'],
        object: json['object'],
        place: json['place'],
        groupOwner: json['group_owner'],
        teacherName: json['teacher']);
  }
}
