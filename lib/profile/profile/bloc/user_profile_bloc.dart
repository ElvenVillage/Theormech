import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:hive/hive.dart';
import 'package:teormech/auth/model/contacts.dart';
import 'package:teormech/auth/model/education.dart';
import 'package:teormech/profile/profile/model/user_profile.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';
import 'package:teormech/profile/profile/rep/user_profile_rep.dart';

class UserProfileBloc extends Cubit<UserProfileState> {
  bool myProfile;
  final String lid;

  final bool needToFetchHeadman;
  final Function? headmanCallback;
  final Function? teacherCallback;

  Box<UserProfile>? _box;

  UserProfileBloc.myProfile({this.headmanCallback, this.teacherCallback})
      : this.myProfile = true,
        needToFetchHeadman = true,
        this.lid = UserProfile.myProfileId,
        super(UserProfileState(lid: UserProfile.myProfileId));

  UserProfileBloc.fromLid(
      {required this.lid, bool forceReload = false, required bool loadFromHive})
      : myProfile = false,
        headmanCallback = null,
        teacherCallback = null,
        needToFetchHeadman = false,
        super(UserProfileState(lid: lid)) {
    fetchFromServer(loadFromHive: loadFromHive, forceReload: forceReload);
  }

  @override
  Future<void> close() {
    _box?.close();
    return super.close();
  }

  Future<Box<UserProfile>> openBox() async {
    return await Hive.openBox('profiles');
  }

  Future<UserProfile?> _loadFromHive() async {
    _box = await openBox();
    if (_box?.isOpen ?? false) {
      var profile = _box?.get(myProfile ? UserProfile.myProfileId : lid);
      return profile;
    }
    return null;
  }

  Future<void> fetchFromServer(
      {required bool loadFromHive, bool forceReload = false}) async {
    if (myProfile) {
      await loadBellNotifications();
    }

    if (this.lid == UserProfileRepository().credentials.id && !myProfile) {
      myProfile = true;
      emit(state.copyWith(lid: '-1'));
    }

    if (_box == null && loadFromHive) _box = await openBox();

    if (loadFromHive) {
      var profile = await _loadFromHive();
      if (profile != null)
        emit(state.copyWith(
            userProfile: profile,
            wasFetched: false,
            contacts: LightContact.fromJSON(json: profile.contacts ?? '[]'),
            education:
                LightEducation.fromJSON(json: profile.education ?? '[]')));
    }
    late final UserProfileResponse fetchedProfile;
    try {
      fetchedProfile = await UserProfileRepository().fetchUserInfo(
          lid: lid, extendLifespan: !loadFromHive, forceReload: forceReload);
    } catch (e) {
      fetchedProfile = UserProfileResponse.error(
          error: 'Сетевая ошибка', time: DateTime.now());
    }
    if (needToFetchHeadman) {
      if (fetchedProfile.profile?.isHeadman != null) {
        if (headmanCallback != null)
          headmanCallback!(fetchedProfile.profile?.isHeadman);
      }
      if (fetchedProfile.profile?.isTeacher ?? false) {
        if (teacherCallback != null) teacherCallback!();
      }
    }
    if (fetchedProfile.error == null) {
      if (_box?.isOpen ?? false) {
        _box?.put(
          myProfile ? UserProfile.myProfileId : lid,
          fetchedProfile.profile!,
        );
      }

      var online = false;
      if (fetchedProfile.profile?.lastOnline != null) {
        try {
          var lastOnline =
              DateTime.tryParse(fetchedProfile.profile!.lastOnline!);
          var diff =
              (-1) * (lastOnline?.difference(DateTime.now()).inMinutes ?? 100);
          online = diff < 5;
        } catch (e) {
          online = false;
        }
      }

      emit(state.copyWith(
          userProfile: fetchedProfile.profile,
          wasFetched: true,
          error: '',
          online: online,
          contacts: LightContact.fromJSON(
              json: fetchedProfile.profile?.contacts ?? '[]'),
          education: LightEducation.fromJSON(
              json: fetchedProfile.profile?.education ?? '[]')));
    } else
      emit(state.copyWith(error: fetchedProfile.error));
  }

  Future<void> setLimits(int limit) async {
    await UserProfileRepository().setLimits(limits: limit.toString());
    getLimits();
  }

  Future<void> getLimits() async {
    emit(state.copyWith(wasFetched: false));
    final limits = await UserProfileRepository().getLimits();
    emit(state.copyWith(limits: limits, wasFetched: true));
  }

  void updatePrivacySettings({required String key}) async {
    var currentSetting = state.profile.privacySettings[key] ?? Privacy.Public;
    var tempSetting = Privacy.Public;
    var newSetting = Privacy.Public;

    switch (currentSetting) {
      case Privacy.Private:
        tempSetting = Privacy.SubscribersUpdating;
        newSetting = Privacy.Subscribers;
        break;
      case (Privacy.Subscribers):
        tempSetting = Privacy.PublicUpdating;
        newSetting = Privacy.Public;
        break;
      case (Privacy.Public):
        tempSetting = Privacy.PrivateUpdating;
        newSetting = Privacy.Private;
        break;
      default:
        tempSetting = Privacy.Public;
        newSetting = Privacy.Public;
    }

    final nPrivacy = state.profile.privacySettings;
    nPrivacy[key] = tempSetting;

    emit(state.copyWith(
        userProfile: state.profile.copyWith(privacySettings: nPrivacy)));

    final result = await UserProfileRepository()
        .updatePrivacySettings(key: key, value: newSetting);

    if (result) {
      emit(state.copyWith(
          userProfile: state.profile.copyWith(
              privacySettings:
                  await UserProfileRepository().fetchPrivacySettings())));
    }
  }

  void update() {
    emit(state.copyWith());
  }

  Future<void> loadBellNotifications() async {
    final notifications = await UserProfileRepository().fetchNotifications();
    emit(state.copyWith(notifications: notifications));
  }
}
