import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:hive/hive.dart';
import 'package:teormech/profile/profile/model/portfolio_state.dart';
import 'package:teormech/profile/profile/rep/user_portfolio_rep.dart';

class ShowPortfolioCubit extends Cubit<PortfolioState> {
  ShowPortfolioCubit({this.uid})
      : super(PortfolioState(
          initializing: false,
          achievements: const [],
          languages: const [],
          professionalSkills: '',
          dirty: false,
          workExperience: const [],
          tags: PortfolioTagList(const []),
        ));

  final String? uid;

  Box<PortfolioState>? _box;
  StreamSubscription<PortfolioResponse>? _subscription;

  Future<void> load() async {
    if (_box == null) _box = await Hive.openBox('userPortfolioBox');
    if (_subscription != null) {
      _subscription?.cancel();
      _subscription = null;
    }

    _subscription = UserPortfolioRepository()
        .fetchUserPortfolio(uid: uid, forceReload: true)
        .listen((event) {
      if (event.isEnded) {
        _subscription?.cancel();
        _subscription = null;
      }

      if (event.state != null) emit(event.state!);
    });
  }

  @override
  Future<void> close() {
    state.achievements.forEach((e) => e.dispose());
    if (_box != null) {
      _subscription?.cancel();
    }
    return super.close();
  }
}
