import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';

import 'package:teormech/profile/profile/model/portfolio_state.dart';
import 'package:teormech/profile/profile/rep/user_portfolio_rep.dart';

class PortfolioBloc extends Cubit<PortfolioState> {
  final VoidCallback? afterSyncCallback;
  Timer? _timer;

  PortfolioBloc({this.afterSyncCallback}) : super(PortfolioState.empty()) {
    _init();
  }

  @override
  Future<void> close() {
    _box?.close();
    _timer?.cancel();

    state.languagesControllers?.forEach((c) => c.dispose());
    state.professionalSkillsController?.dispose();
    state.workExperienceControllers?.forEach((c) => c.dispose());
    state.achievements.forEach((e) => e.dispose());

    return super.close();
  }

  Future<PortfolioState?> _fetchServerState() async {
    final stream =
        UserPortfolioRepository().fetchUserPortfolio(forceReload: true);

    await for (var serverState in stream) {
      if (serverState.isEnded) return serverState.state;
    }
    return null;
  }

  Box<PortfolioState>? _box;
  static const _HIVE_INDEX = 0;

  Future<bool> _syncTags(PortfolioState? serverState) async {
    final tags = serverState?.tags.tags ?? [];
    final localTags = state.tags;

    final results = <bool>[];

    for (var tag in tags) {
      if (localTags.tags.where((e) => e == tag).isEmpty) {
        results
            .add(await UserPortfolioRepository().pinTag(tag: tag, unpin: true));
        print('removed tag ${tag.tag} with level ${tag.level}');
      }
    }

    for (var tag in localTags.tags) {
      if (tags.where((e) => e == tag).isEmpty) {
        results.add(await UserPortfolioRepository().pinTag(
          tag: tag,
          unpin: false,
        ));
        print('added tag ${tag.tag} with level ${tag.level}');
      }
    }

    return !results.contains(false);
  }

  Future<bool> _syncAchievements(PortfolioState? serverState) async {
    final achievements = serverState?.achievements ?? [];
    final localAchievements = state.achievements;

    final results = <bool>[];

    for (var achievement in achievements) {
      if (localAchievements
          .where((PortfolioAchievement e) => e.equals(achievement))
          .isEmpty) {
        results.add(await UserPortfolioRepository().deleteAchievement(
          achievement,
        ));

        print('achievement removed ${achievement.caption}');
      }
    }

    for (var achievement in localAchievements) {
      if (achievements
          .where((PortfolioAchievement e) => e.equals(achievement))
          .isEmpty) {
        print(localAchievements.length);
        print('added achievement ${achievement.caption}');
        results.add(await UserPortfolioRepository().addAchievement(
          achievement,
        ));
      }
    }

    return !results.contains(false);
  }

  void sync({VoidCallback? callback}) async {
    _save();
    emit(state.copyWith(syncing: true));

    final lang = jsonEncode(
        state.languages.where((lang) => lang.trim().isNotEmpty).toList());
    final experience = jsonEncode(
        state.workExperience.where((exp) => exp.trim().isNotEmpty).toList());
    final professionals = state.professionalSkills;

    final fetchedPortfolio = await _fetchServerState();

    try {
      await Future.wait(
          [_syncTags(fetchedPortfolio), _syncAchievements(fetchedPortfolio)]);

      final serverLangs = fetchedPortfolio == null
          ? <String>[]
          : jsonEncode(fetchedPortfolio.languages);
      final serverWorkExperience = fetchedPortfolio == null
          ? ''
          : jsonEncode(fetchedPortfolio.workExperience);

      await Future.wait([
        if (lang != serverLangs)
          UserPortfolioRepository().addResumeInfo(key: 'lang', val: lang),
        if (experience != serverWorkExperience)
          UserPortfolioRepository()
              .addResumeInfo(key: 'experience', val: experience),
        if (professionals != fetchedPortfolio?.professionalSkills &&
            professionals != null)
          UserPortfolioRepository()
              .addResumeInfo(key: 'professionals', val: professionals)
      ]);
    } finally {
      emit(state.copyWith(syncing: false, dirty: false));
    }
    if (afterSyncCallback != null) afterSyncCallback!();
    if (callback != null) callback();
  }

  Future<bool> _synchronized(PortfolioState state) async {
    final fetchedState = await _fetchServerState();
    if (fetchedState == null) return false;
    return state.equals(fetchedState);
  }

  void _init() async {
    if (_box == null)
      _box = await Hive.openBox<PortfolioState>('edit_portfolio');
    if (_timer == null)
      _timer = Timer.periodic(Duration(seconds: 3), (_) => _save());

    var hiveState = _box?.get(_HIVE_INDEX);
    var dirty = false;

    if (hiveState == null ||
        //state.equals(hiveState) ||
        state.equals(PortfolioState.empty())) {
      hiveState = await _fetchServerState();
    } else
      dirty = await _synchronized(hiveState);

    final nState = hiveState?.copyWith(
        professionalSkillsController: TextEditingController.fromValue(
            TextEditingValue(text: hiveState.professionalSkills ?? '')),
        languagesControllers: hiveState.languages
            .map((e) =>
                TextEditingController.fromValue(TextEditingValue(text: e)))
            .toList(),
        workExperienceControllers: hiveState.workExperience
            .map((e) =>
                TextEditingController.fromValue(TextEditingValue(text: e)))
            .toList(),
        initializing: false,
        dirty: dirty);

    nState?.achievements.forEach((e) => e.init());

    if (nState != null) emit(nState);
  }

  void addLanguage() {
    emit(state.copyWith(languagesControllers: [
      ...?state.languagesControllers,
      TextEditingController()
    ], dirty: true));
  }

  void deleteLanguage(int idx) {
    final controllers = state.languagesControllers;
    controllers?.removeAt(idx);
    emit(state.copyWith(languagesControllers: controllers));
  }

  void addFile(
      {required PortfolioAchievement achievement, required String file}) {
    final nState = state.copyWith(dirty: true);
    final nAchievement = nState.achievements
        .firstWhere((element) => element.caption == achievement.caption);
    nAchievement.file = file;
    nAchievement.link = '';
    nAchievement.linkController.clear();
    emit(nState);
  }

  void clearFile({required PortfolioAchievement achievement}) {
    final nState = state.copyWith(dirty: true);
    final nAchievement = nState.achievements
        .firstWhere((element) => element.caption == achievement.caption);
    nAchievement.file = '';
    nAchievement.submittedFile = '';
    emit(nState);
  }

  void deleteAchievement(PortfolioAchievement achievement) {
    emit(state.copyWith(
        dirty: true,
        achievements: state.achievements
            .where((e) =>
                e.caption != achievement.caption ||
                e.date != achievement.date ||
                e.link != achievement.link)
            .toList()));
  }

  void addWorkExperience() {
    emit(state.copyWith(workExperienceControllers: [
      ...?state.workExperienceControllers,
      TextEditingController()
    ], dirty: true));
  }

  void deleteWorkExperience(int idx) {
    final nState = state.copyWith(dirty: true);
    if (idx < nState.workExperience.length) nState.workExperience.removeAt(idx);
    nState.workExperienceControllers?.removeAt(idx);
    emit(nState);
  }

  void addTag(
      {required String tag,
      required ProfessionalLevel level,
      bool userTag = false}) {
    final portfolioTag = PortfolioTag(tag: tag, level: level, userTag: userTag);
    if (state.tags.tags.where((e) => e.tag == tag).isEmpty)
      emit(state
          .copyWith(tags: [...state.tags.tags, portfolioTag], dirty: true));
  }

  void deleteTag({required String tag}) {
    final nTags = state.tags.tags.where((e) => e.tag != tag);
    emit(state.copyWith(tags: nTags.toList(), dirty: true));
  }

  void addPublication() {
    emit(state.copyWith(achievements: [
      ...state.achievements,
      PortfolioAchievement.emptyPublication()
    ], dirty: true));
  }

  void addAchievement() {
    emit(state.copyWith(achievements: [
      ...state.achievements,
      PortfolioAchievement.emptyAchievement()
    ], dirty: true));
  }

  void _save() {
    state.achievements.forEach((e) => e.sync());
    var nState = state.copyWith(
      languages: state.languagesControllers?.map((e) => e.text).toList(),
      professionalSkills: state.professionalSkillsController?.text ?? '',
      workExperience:
          state.workExperienceControllers?.map((e) => e.text).toList(),
    );
    if (!nState.equals(state)) nState = nState.copyWith(dirty: true);

    emit(nState);

    _box?.put(_HIVE_INDEX, nState);
  }
}
