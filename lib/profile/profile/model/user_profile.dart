import 'package:hive/hive.dart';

part 'user_profile.g.dart';

enum Privacy {
  Private,
  Subscribers,
  Public,
  PrivateUpdating,
  PublicUpdating,
  SubscribersUpdating
}

@HiveType(typeId: 0)
class UserProfile extends HiveObject {
  static const myProfileId = '-1';

  @HiveField(0)
  final String lid;

  @HiveField(1)
  final String name;

  @HiveField(2)
  final String surname;

  @HiveField(3)
  final String patronymic;

  @HiveField(4)
  final String? phone;

  @HiveField(5)
  final bool isStudent;

  @HiveField(6)
  final String? group;

  @HiveField(7)
  final String? avatar;

  @HiveField(8)
  final String email;

  @HiveField(9)
  final String? education;

  @HiveField(10)
  final String? hometown;

  @HiveField(11)
  final String? birth;

  @HiveField(12)
  final String? about;

  @HiveField(13)
  final String? contacts;

  @HiveField(14)
  final String? lastOnline;

  @HiveField(15)
  final int? followings;

  @HiveField(16)
  final int? followers;

  @HiveField(17)
  final int? likes;

  @HiveField(18)
  final String? teacherStatus;

  @HiveField(19)
  final String? employeeStatus;

  @HiveField(20)
  final String sex;

  @HiveField(21)
  final bool isEmployee;

  @HiveField(22)
  final bool isTeacher;

  @HiveField(23)
  final bool isAdmin;

  final bool isHeadman;

  Map<String, Privacy> privacySettings;

  UserProfile(
      {required this.lid,
      required this.surname,
      required this.name,
      required this.patronymic,
      required this.phone,
      required this.avatar,
      required this.isEmployee,
      required this.isStudent,
      required this.isTeacher,
      required this.isAdmin,
      this.education,
      this.about,
      this.hometown,
      this.isHeadman = false,
      this.birth,
      this.contacts,
      required this.email,
      this.lastOnline,
      this.followers,
      this.followings,
      this.likes,
      this.employeeStatus,
      this.teacherStatus,
      this.sex = '1',
      this.privacySettings = const {},
      this.group = ''});

  UserProfile copyWith(
      {String? lid,
      String? surname,
      String? name,
      String? patronymic,
      String? phone,
      String? role,
      String? avatar,
      String? email,
      String? group,
      String? about,
      String? hometown,
      String? birth,
      String? education,
      String? contacts,
      String? lastOnline,
      int? followings,
      int? followers,
      int? likes,
      String? teacherStatus,
      String? employeeStatus,
      String? sex,
      bool? isEmployee,
      bool? isTeacher,
      bool? isStudent,
      bool? isAdmin,
      Map<String, Privacy>? privacySettings}) {
    return UserProfile(
        lid: lid ?? this.lid,
        isAdmin: isAdmin ?? this.isAdmin,
        surname: surname ?? this.surname,
        name: name ?? this.name,
        patronymic: patronymic ?? this.patronymic,
        phone: phone ?? this.phone,
        avatar: avatar ?? this.avatar,
        email: email ?? this.email,
        about: about ?? this.about,
        birth: birth ?? this.birth,
        education: education ?? this.education,
        hometown: hometown ?? this.hometown,
        followers: followers ?? this.followers,
        followings: followings ?? this.followings,
        likes: likes ?? this.likes,
        contacts: contacts ?? this.contacts,
        employeeStatus: employeeStatus ?? this.employeeStatus,
        teacherStatus: teacherStatus ?? this.teacherStatus,
        sex: sex ?? this.sex,
        isEmployee: isEmployee ?? this.isEmployee,
        isStudent: isStudent ?? this.isStudent,
        isTeacher: isTeacher ?? this.isTeacher,
        privacySettings: privacySettings ?? this.privacySettings,
        lastOnline: lastOnline ?? this.lastOnline,
        group: group ?? this.group);
  }

  static String _processPriv(String key, Map<String, dynamic> json) {
    return ((json[key] != null) && (json[key] != 'PRIVATE')) ? json[key] : '';
  }

  UserProfile.fromJson(
    Map<String, dynamic> json,
    String? lid, {
    Map<String, Privacy>? privacySettings,
    Map<String, dynamic> followersJson = const {
      'followings': 0,
      'followers': 0
    },
  })  : lid = lid ?? UserProfile.myProfileId,
        surname = json['surname'],
        name = json['name'],
        patronymic = json['patronymic'] ?? '',
        isStudent = (json['isStudent'] == '1'),
        isEmployee = (json['isEmployee'] == '1'),
        isTeacher = (json['isTeacher'] == '1'),
        isAdmin = (json['isAdmin'] == '1'),
        isHeadman = (json['isHeadman'] == '1'),
        group = json['isStudent'] == '1' ? json['group'] : '',
        avatar = json['photo'],
        email = _processPriv('email', json),
        education = _processPriv('study', json),
        about = _processPriv('about', json),
        hometown = _processPriv('hometown', json),
        birth = _processPriv('birth', json),
        privacySettings = privacySettings ?? <String, Privacy>{},
        lastOnline = json['last_online'] == '0000-00-00 00:00:00'
            ? null
            : json['last_online'],
        contacts = _processPriv('contacts', json),
        followers = followersJson['followers'],
        followings = followersJson['followings'],
        sex = json['sex'],
        employeeStatus = json['employee_status'],
        teacherStatus = json['teacher_status'],
        likes = followersJson['likes'],
        phone = _processPriv('phone', json);

  UserProfile.empty()
      : lid = '-2',
        surname = '',
        name = '',
        isAdmin = false,
        patronymic = '',
        group = '',
        about = null,
        isHeadman = false,
        birth = null,
        privacySettings = {},
        contacts = '',
        education = null,
        hometown = null,
        isStudent = false,
        isEmployee = false,
        isTeacher = false,
        email = '',
        lastOnline = null,
        avatar = '',
        followers = 0,
        followings = 0,
        likes = 0,
        employeeStatus = null,
        teacherStatus = null,
        sex = '1',
        phone = '';
}
