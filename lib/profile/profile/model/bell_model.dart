import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:teormech/profile/chat/model/message_model.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';

class BellNotification {
  final int id;
  final DateTime date;
  TimeOfDay get time {
    return TimeOfDay.fromDateTime(date.toLocal());
  }

  String get dateDayFormatted {
    final now = DateTime.now();
    final sendDateTime = this.date;
    final diff = now.day - sendDateTime.day;
    final diffMonths = now.month - sendDateTime.month;
    if (diff < 1 && diffMonths == 0)
      return 'Сегодня, ${now.day.toString() + ' ' + Message.formatMonth(now.month)}';
    if (diff >= 1 && diff < 2)
      return 'Вчера, ${now.day.toString() + ' ' + Message.formatMonth(now.month)}';

    return sendDateTime.day.toString() +
        ' ' +
        Message.formatMonth(sendDateTime.month);
  }

  BellNotification(this.id, this.date);
  factory BellNotification.fromJson(Map<String, dynamic> json) {
    if (json['type'] == 'new_follower') {
      return FollowerNotification.fromJson(json, false);
    }
    if (json['type'] == 'invite_to_project') {
      return ProjectRequestBellNotification.fromJson(json);
    }

    if (json['type'] == 'interest_vacancy') {
      return InterestVacancyNotification.fromJson(json);
    }
    if (json['type'] == 'leave_project') {
      return ProjectBellNotification.fromJson(json, true);
    }
    if (json['type'] == 'join_project') {
      return ProjectBellNotification.fromJson(json, false);
    }
    if (json['type'] == 'interest_event') {
      return InterestEventNotification.fromJson(json);
    }
    return BellNotification(-1, DateTime.now());
  }
}

class InterestVacancyNotification extends BellNotification {
  final JobModel job;
  InterestVacancyNotification(
      {required int id, required DateTime date, required this.job})
      : super(id, date);
  factory InterestVacancyNotification.fromJson(Map<String, dynamic> json) {
    final body = jsonDecode(json['body'].replaceAll("\r\n", ""));
    final job = JobModel.fromJson(body);
    return InterestVacancyNotification(
        id: int.parse(json['id']),
        date: DateTime.parse(json['create_time']),
        job: job);
  }
}

class InterestEventNotification extends BellNotification {
  final EventModel event;
  InterestEventNotification(
      {required int id, required DateTime date, required this.event})
      : super(id, date);
  factory InterestEventNotification.fromJson(Map<String, dynamic> json) {
    final body = jsonDecode(json['body']);
    final event = EventModel.fromJson(body);

    return InterestEventNotification(
        id: int.parse(json['id']),
        date: DateTime.parse(json['create_time']),
        event: event);
  }
}

class FollowerNotification extends BellNotification {
  final String name;
  final String surname;
  final String photo;
  final String uid;
  final bool unsub;

  FollowerNotification({
    required int id,
    required DateTime date,
    required this.uid,
    required this.name,
    required this.photo,
    required this.surname,
    required this.unsub,
  }) : super(id, date);

  String get caption => '${name} ${surname}';

  factory FollowerNotification.fromJson(Map<String, dynamic> json, bool unsub) {
    final body = jsonDecode(json['body']);
    return FollowerNotification(
        id: int.parse(json['id']),
        uid: body['id'],
        name: body['name'],
        photo: body['photo'],
        date: DateTime.parse(json['create_time']),
        surname: body['surname'],
        unsub: unsub);
  }
}

class ProjectBellNotification extends BellNotification {
  final String uid;
  final String name;
  final String surname;
  final String? avatar;
  final int pid;
  final bool leave;

  String get caption => '${name} ${surname}';

  ProjectBellNotification(int id, DateTime date, this.uid, this.name,
      this.surname, this.avatar, this.pid, this.leave)
      : super(id, date);

  factory ProjectBellNotification.fromJson(
      Map<String, dynamic> json, bool leave) {
    final body = jsonDecode(json['body']);
    return ProjectBellNotification(
        int.parse(json['id']),
        DateTime.parse(json['create_time']),
        body['id'],
        body['name'],
        body['surname'],
        body['avatar'],
        int.parse(body['pid']),
        leave);
  }
}

class ProjectRequestBellNotification extends BellNotification {
  final String name;
  final String surname;
  final String uid;
  final String? avatar;
  final int pid;

  ProjectRequestBellNotification(int id, DateTime date, this.name, this.surname,
      this.uid, this.avatar, this.pid)
      : super(id, date);

  factory ProjectRequestBellNotification.fromJson(Map<String, dynamic> json) {
    final body = jsonDecode(json['body']);
    return ProjectRequestBellNotification(
        int.parse(json['id']),
        DateTime.parse(json['create_time']),
        body['name'],
        body['surname'],
        body['id'],
        body['photo'],
        int.parse(body['pid']));
  }
}
