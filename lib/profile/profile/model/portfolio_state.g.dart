// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'portfolio_state.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PortfolioTagListAdapter extends TypeAdapter<PortfolioTagList> {
  @override
  final int typeId = 10;

  @override
  PortfolioTagList read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PortfolioTagList(
      (fields[0] as List).cast<PortfolioTag>(),
    );
  }

  @override
  void write(BinaryWriter writer, PortfolioTagList obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.tags);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PortfolioTagListAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class PortfolioTagAdapter extends TypeAdapter<PortfolioTag> {
  @override
  final int typeId = 6;

  @override
  PortfolioTag read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PortfolioTag(
      tag: fields[0] as String,
      level: fields[1] as ProfessionalLevel,
      userTag: fields[2] as bool,
    );
  }

  @override
  void write(BinaryWriter writer, PortfolioTag obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.tag)
      ..writeByte(1)
      ..write(obj.level)
      ..writeByte(2)
      ..write(obj.userTag);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PortfolioTagAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class PortfolioAchievementAdapter extends TypeAdapter<PortfolioAchievement> {
  @override
  final int typeId = 8;

  @override
  PortfolioAchievement read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PortfolioAchievement(
      isPublication: fields[0] as bool,
      caption: fields[1] as String,
      date: fields[2] as String,
      link: fields[3] as String,
      file: fields[4] as String,
      submittedFile: fields[5] as String,
    );
  }

  @override
  void write(BinaryWriter writer, PortfolioAchievement obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.isPublication)
      ..writeByte(1)
      ..write(obj.caption)
      ..writeByte(2)
      ..write(obj.date)
      ..writeByte(3)
      ..write(obj.link)
      ..writeByte(4)
      ..write(obj.file)
      ..writeByte(5)
      ..write(obj.submittedFile);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PortfolioAchievementAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class PortfolioStateAdapter extends TypeAdapter<PortfolioState> {
  @override
  final int typeId = 7;

  @override
  PortfolioState read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PortfolioState(
      tags: fields[1] as PortfolioTagList,
      dirty: fields[6] as bool,
      professionalSkills: fields[3] as String?,
      workExperience: (fields[4] as List).cast<String>(),
      achievements: (fields[5] as List).cast<PortfolioAchievement>(),
      languages: (fields[2] as List).cast<String>(),
    );
  }

  @override
  void write(BinaryWriter writer, PortfolioState obj) {
    writer
      ..writeByte(6)
      ..writeByte(1)
      ..write(obj.tags)
      ..writeByte(2)
      ..write(obj.languages)
      ..writeByte(3)
      ..write(obj.professionalSkills)
      ..writeByte(4)
      ..write(obj.workExperience)
      ..writeByte(5)
      ..write(obj.achievements)
      ..writeByte(6)
      ..write(obj.dirty);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PortfolioStateAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class ProfessionalLevelAdapter extends TypeAdapter<ProfessionalLevel> {
  @override
  final int typeId = 9;

  @override
  ProfessionalLevel read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return ProfessionalLevel.Beginner;
      case 1:
        return ProfessionalLevel.Advanced;
      case 2:
        return ProfessionalLevel.Professional;
      case 3:
        return ProfessionalLevel.Initial;
      default:
        return ProfessionalLevel.Beginner;
    }
  }

  @override
  void write(BinaryWriter writer, ProfessionalLevel obj) {
    switch (obj) {
      case ProfessionalLevel.Beginner:
        writer.writeByte(0);
        break;
      case ProfessionalLevel.Advanced:
        writer.writeByte(1);
        break;
      case ProfessionalLevel.Professional:
        writer.writeByte(2);
        break;
      case ProfessionalLevel.Initial:
        writer.writeByte(3);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ProfessionalLevelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
