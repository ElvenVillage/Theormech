// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_profile.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class UserProfileAdapter extends TypeAdapter<UserProfile> {
  @override
  final int typeId = 0;

  @override
  UserProfile read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return UserProfile(
      lid: fields[0] as String,
      surname: fields[2] as String,
      name: fields[1] as String,
      patronymic: fields[3] as String,
      phone: fields[4] as String?,
      avatar: fields[7] as String?,
      isEmployee: fields[21] as bool,
      isStudent: fields[5] as bool,
      isTeacher: fields[22] as bool,
      isAdmin: fields[23] as bool,
      education: fields[9] as String?,
      about: fields[12] as String?,
      hometown: fields[10] as String?,
      birth: fields[11] as String?,
      contacts: fields[13] as String?,
      email: fields[8] as String,
      lastOnline: fields[14] as String?,
      followers: fields[16] as int?,
      followings: fields[15] as int?,
      likes: fields[17] as int?,
      employeeStatus: fields[19] as String?,
      teacherStatus: fields[18] as String?,
      sex: fields[20] as String,
      group: fields[6] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, UserProfile obj) {
    writer
      ..writeByte(24)
      ..writeByte(0)
      ..write(obj.lid)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.surname)
      ..writeByte(3)
      ..write(obj.patronymic)
      ..writeByte(4)
      ..write(obj.phone)
      ..writeByte(5)
      ..write(obj.isStudent)
      ..writeByte(6)
      ..write(obj.group)
      ..writeByte(7)
      ..write(obj.avatar)
      ..writeByte(8)
      ..write(obj.email)
      ..writeByte(9)
      ..write(obj.education)
      ..writeByte(10)
      ..write(obj.hometown)
      ..writeByte(11)
      ..write(obj.birth)
      ..writeByte(12)
      ..write(obj.about)
      ..writeByte(13)
      ..write(obj.contacts)
      ..writeByte(14)
      ..write(obj.lastOnline)
      ..writeByte(15)
      ..write(obj.followings)
      ..writeByte(16)
      ..write(obj.followers)
      ..writeByte(17)
      ..write(obj.likes)
      ..writeByte(18)
      ..write(obj.teacherStatus)
      ..writeByte(19)
      ..write(obj.employeeStatus)
      ..writeByte(20)
      ..write(obj.sex)
      ..writeByte(21)
      ..write(obj.isEmployee)
      ..writeByte(22)
      ..write(obj.isTeacher)
      ..writeByte(23)
      ..write(obj.isAdmin);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserProfileAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
