import 'package:teormech/auth/model/contacts.dart';
import 'package:teormech/auth/model/education.dart';
import 'package:teormech/profile/bloc/online_status.dart';
import 'package:teormech/profile/profile/model/bell_model.dart';
import 'package:teormech/profile/profile/model/user_profile.dart';

enum ProfilePage { Newsfeed, Portfolio, Projects }

class UserProfileState {
  String lid;
  bool wasFetched;
  bool online;

  int? limits;

  String? error;

  late UserProfile profile;
  List<LightContact> contacts;
  List<LightEducation> education;

  List<BellNotification>? notifications;

  String get fullName =>
      '${profile.surname} ${profile.name} ${profile.patronymic}';

  String get onlineString {
    if (!online) {
      return OnlineStatus.onlineString(
          lastOnline: profile.lastOnline == null
              ? null
              : DateTime.tryParse(profile.lastOnline!),
          sex: profile.sex,
          now: DateTime.now());
    } else
      return '';
  }

  String? get educationString {
    final sorted = education.sublist(0)
      ..sort((a, b) => a.level.index - b.level.index);
    return sorted.isEmpty
        ? null
        : sorted.last.level == EducationLevel.School
            ? '${sorted.last.org} по ${sorted.last.date}'
            : '${LightEducation.levelString(sorted.last.level)} ${sorted.last.org} ${sorted.last.fac}, ${sorted.last.date}';
  }

  UserProfileState(
      {required this.lid,
      this.wasFetched = false,
      this.contacts = const [],
      this.education = const [],
      this.online = false,
      this.notifications = null,
      this.limits,
      this.error})
      : this.profile = UserProfile.empty();

  UserProfileState copyWith({
    String? lid,
    bool? wasFetched,
    UserProfile? userProfile,
    List<LightContact>? contacts,
    List<LightEducation>? education,
    List<BellNotification>? notifications,
    String? error,
    bool? online,
    int? limits,
  }) {
    return UserProfileState(
        lid: lid ?? this.lid,
        notifications: notifications ?? this.notifications,
        wasFetched: wasFetched ?? this.wasFetched,
        contacts: contacts ?? this.contacts,
        education: education ?? this.education,
        online: online ?? this.online,
        limits: limits ?? this.limits,
        error: error)
      ..profile = userProfile ?? this.profile;
  }
}
