import 'package:flutter/cupertino.dart';
import 'package:hive/hive.dart';

part 'portfolio_state.g.dart';

@HiveType(typeId: 9)
enum ProfessionalLevel {
  @HiveField(0)
  Beginner,
  @HiveField(1)
  Advanced,
  @HiveField(2)
  Professional,
  @HiveField(3)
  Initial,

  Service,
  Newsfeed,
  Caption
}

@HiveType(typeId: 10)
class PortfolioTagList {
  @HiveField(0)
  final List<PortfolioTag> tags;
  PortfolioTagList(this.tags);
}

@HiveType(typeId: 6)
class PortfolioTag {
  @HiveField(0)
  final String tag;
  @HiveField(1)
  final ProfessionalLevel level;
  @HiveField(2)
  final bool userTag;

  PortfolioTag({required this.tag, required this.level, this.userTag = false});

  PortfolioTag.userTag({required this.tag, required this.level})
      : userTag = true;

  PortfolioTag.newsfeed(this.tag)
      : level = ProfessionalLevel.Newsfeed,
        userTag = false;

  PortfolioTag.caption(this.tag)
      : level = ProfessionalLevel.Caption,
        userTag = false;

  bool operator ==(Object other) {
    if (other is PortfolioTag) {
      return tag == other.tag &&
          level == other.level &&
          userTag == other.userTag;
    } else
      return false;
  }
}

@HiveType(typeId: 8)
class PortfolioAchievement {
  @HiveField(0)
  final bool isPublication;
  @HiveField(1)
  String caption;
  @HiveField(2)
  String date;
  @HiveField(3)
  String link;
  @HiveField(4)
  String file;
  @HiveField(5)
  String submittedFile;
  int? id;
  final bool isApproved;

  final captionController = TextEditingController();
  final dateController = TextEditingController();
  final linkController = TextEditingController();

  bool equals(PortfolioAchievement other) {
    return other.isPublication == this.isPublication &&
        other.caption == this.caption &&
        (other.submittedFile == this.submittedFile) &&
        other.file.isEmpty &&
        this.file.isEmpty &&
        other.link == this.link;
  }

  void dispose() {
    captionController.dispose();
    dateController.dispose();
    linkController.dispose();
  }

  void init() {
    captionController.text = caption;
    dateController.text = date;
    linkController.text = link;
  }

  void sync() {
    caption = captionController.text;
    date = dateController.text;
    link = linkController.text;
  }

  PortfolioAchievement(
      {required this.isPublication,
      required this.caption,
      required this.date,
      required this.link,
      required this.file,
      required this.submittedFile,
      this.id,
      this.isApproved = false});

  PortfolioAchievement.emptyAchievement()
      : isPublication = false,
        caption = '',
        date = '',
        link = '',
        file = '',
        isApproved = false,
        submittedFile = '';

  PortfolioAchievement.emptyPublication()
      : isPublication = true,
        caption = '',
        date = '',
        link = '',
        file = '',
        isApproved = false,
        submittedFile = '';
}

@HiveType(typeId: 7)
class PortfolioState {
  @HiveField(1)
  final PortfolioTagList tags;
  @HiveField(2)
  final List<String> languages;
  final List<TextEditingController>? languagesControllers;

  @HiveField(3)
  final String? professionalSkills;
  final TextEditingController? professionalSkillsController;

  @HiveField(4)
  final List<String> workExperience;
  final List<TextEditingController>? workExperienceControllers;

  @HiveField(5)
  final List<PortfolioAchievement> achievements;

  @HiveField(6)
  final bool dirty;

  final bool syncing;
  final bool initializing;

  int get confirmedSum => achievements.where((a) => a.isApproved).length;

  String toString() {
    return (professionalSkills ?? '') +
        ' ' +
        workExperience.join(' ') +
        languages.join(' ');
  }

  bool get isEmpty =>
      tags.tags.isEmpty &&
      (professionalSkills?.isEmpty ?? true) &&
      languages.isEmpty &&
      workExperience.isEmpty;

  bool equals(PortfolioState other) {
    if (tags.tags.length != other.tags.tags.length ||
        languages.length != other.languages.length ||
        professionalSkills != other.professionalSkills ||
        workExperience.length != other.workExperience.length)
      return false;
    else {
      for (var i = 0; i < tags.tags.length; i++) {
        if (tags.tags[i] != other.tags.tags[i]) {
          return false;
        }
      }
      for (var i = 0; i < languages.length; i++) {
        if (languages[i] != other.languages[i]) {
          return false;
        }
      }
      for (var i = 0; i < workExperience.length; i++) {
        if (workExperience[i] != other.workExperience[i]) {
          return false;
        }
      }
      return true;
    }
  }

  List<PortfolioAchievement> get achievementsOnly =>
      achievements.where((e) => !e.isPublication).toList();

  List<PortfolioAchievement> get publicationsOnly =>
      achievements.where((e) => e.isPublication).toList();

  PortfolioState(
      {required this.tags,
      required this.dirty,
      this.initializing = false,
      this.syncing = false,
      required this.professionalSkills,
      required this.workExperience,
      required this.achievements,
      this.professionalSkillsController,
      this.workExperienceControllers,
      required this.languages,
      this.languagesControllers});
  PortfolioState.empty()
      : languages = const [],
        initializing = true,
        languagesControllers = [TextEditingController()],
        achievements = const [],
        dirty = false,
        syncing = false,
        professionalSkills = '',
        professionalSkillsController = TextEditingController(),
        workExperience = const [],
        workExperienceControllers = [TextEditingController()],
        tags = PortfolioTagList(const []);

  PortfolioState copyWith(
      {bool? dirty,
      bool? initializing,
      List<PortfolioTag>? tags,
      List<PortfolioAchievement>? achievements,
      bool? openSecondPage,
      List<String>? workExperience,
      TextEditingController? professionalSkillsController,
      List<TextEditingController>? workExperienceControllers,
      String? professionalSkills,
      bool? syncing,
      List<String>? languages,
      List<TextEditingController>? languagesControllers}) {
    final tagList = (tags == null) ? null : PortfolioTagList(tags);
    return PortfolioState(
        initializing: initializing ?? this.initializing,
        syncing: syncing ?? this.syncing,
        dirty: dirty ?? this.dirty,
        tags: tagList ?? this.tags,
        achievements: achievements ?? this.achievements,
        professionalSkills: professionalSkills ?? this.professionalSkills,
        workExperience: workExperience ?? this.workExperience,
        languages: languages ?? this.languages,
        languagesControllers: languagesControllers ?? this.languagesControllers,
        professionalSkillsController:
            professionalSkillsController ?? this.professionalSkillsController,
        workExperienceControllers:
            workExperienceControllers ?? this.workExperienceControllers);
  }
}
