import 'package:flutter/material.dart';
import 'package:teormech/styles/text.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfileDataRow extends StatelessWidget {
  final String caption;
  final String value;
  final String? link;

  ProfileDataRow({required this.caption, required this.value, this.link});

  @override
  Widget build(BuildContext context) {
    final text = Text(value,
        style: h1TextStyle.copyWith(
            color: Colors.white,
            fontSize: 14,
            fontWeight: FontWeight.bold,
            decoration: link == null ? null : TextDecoration.underline));
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 500),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 6),
        child: Row(
          children: [
            Text(
              caption,
              style: h1TextStyle.copyWith(color: Colors.white, fontSize: 14),
            ),
            Spacer(),
            if (link == null) text,
            if (link != null)
              InkWell(
                child: text,
                onTap: () {
                  launch(link!);
                },
              )
          ],
        ),
      ),
    );
  }
}
