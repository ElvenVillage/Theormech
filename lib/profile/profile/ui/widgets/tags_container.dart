import 'package:flutter/material.dart';
import 'package:teormech/profile/profile/model/portfolio_state.dart';
import 'package:teormech/profile/ui/widgets/tag.dart';

class TagsContainer extends StatelessWidget {
  TagsContainer(
      {Key? key, required this.tags, this.tapCallback, this.isCompact = false})
      : super(key: key);

  final List<PortfolioTag> tags;
  final bool isCompact;
  final TagTapCallback? tapCallback;

  List<PortfolioTag> _reorder(List<PortfolioTag> tags) {
    if (tags.isEmpty || tags.length == 1) return tags;

    List<PortfolioTag> temp = List.from(tags)
      ..sort((a, b) {
        if (b.level == ProfessionalLevel.Service) return -1;
        if (a.level == ProfessionalLevel.Caption &&
            b.level != ProfessionalLevel.Caption) return -1;
        return a.tag.length.compareTo(b.tag.length);
      });

    return temp;
  }

  @override
  Widget build(BuildContext context) {
    final orderedTags = _reorder(tags);
    return SizedBox(
      width: double.infinity,
      child: Wrap(spacing: 3, runSpacing: 3, children: [
        for (var tag in orderedTags)
          Tag(
            key: UniqueKey(),
            isCompact: isCompact,
            tag: tag,
            tapCallback: tapCallback,
          )
      ]),
    );
  }
}
