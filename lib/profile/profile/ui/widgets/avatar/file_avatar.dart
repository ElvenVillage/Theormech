import 'dart:io';

import 'package:flutter/material.dart';
import 'dart:ui' as ui;

import 'package:teormech/profile/profile/ui/widgets/avatar/avatar_mask.dart';

import 'clipped_avatar.dart';

class FileAvatar extends StatefulWidget {
  const FileAvatar({Key? key, required this.file, required this.size})
      : super(key: key);
  final File file;
  final double size;
  @override
  _FileAvatarState createState() => _FileAvatarState();
}

class _FileAvatarState extends State<FileAvatar> {
  ui.Image? _image;

  Future<void> _init() async {
    final file = await widget.file.readAsBytes();
    final image = await decodeImageFromList(file);

    setState(() {
      _image = image;
    });
  }

  @override
  void didChangeDependencies() {
    _init();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    if (_image != null)
      return SizedBox(
        height: widget.size + 5,
        width: widget.size + 5,
        child: CustomPaint(
          painter: MaskedAvatar(
              mask: UserAvatarComponentsUtils.mask,
              image: _image!,
              width: widget.size + 5),
        ),
      );
    else
      return SizedBox(
        width: widget.size + 5,
        height: widget.size + 5,
      );
  }
}
