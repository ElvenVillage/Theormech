import 'dart:ui';

import 'package:flutter/rendering.dart';

class MaskedAvatar extends CustomPainter {
  final Image image;
  final Image mask;
  Image? frame;
  final double width;

  MaskedAvatar(
      {required this.image,
      required this.mask,
      required this.width,
      this.frame});

  @override
  void paint(Canvas canvas, Size size) {
    var rect = Rect.fromLTRB(0, 0, width, width);
    Size outputSize = rect.size;
    Paint paint = new Paint();

    //Mask
    Size maskInputSize = Size(mask.width.toDouble(), mask.height.toDouble());
    final FittedSizes maskFittedSizes =
        applyBoxFit(BoxFit.cover, maskInputSize, outputSize);
    final Size maskSourceSize = maskFittedSizes.source;

    final Rect maskSourceRect =
        Alignment.center.inscribe(maskSourceSize, Offset.zero & maskInputSize);

    canvas.saveLayer(rect, paint);
    canvas.drawImageRect(mask, maskSourceRect, rect, paint);

    //Image
    Size inputSize = Size(image.width.toDouble(), image.height.toDouble());
    final FittedSizes fittedSizes =
        applyBoxFit(BoxFit.cover, inputSize, outputSize);
    final Size sourceSize = fittedSizes.source;
    final Rect sourceRect =
        Alignment.center.inscribe(sourceSize, Offset.zero & inputSize);

    canvas.drawImageRect(
        image, sourceRect, rect, paint..blendMode = BlendMode.srcIn);

    if (frame != null) {
      //Frame
      Size frameInputSize =
          Size(frame!.width.toDouble(), frame!.height.toDouble());
      final FittedSizes frameFittedSizes =
          applyBoxFit(BoxFit.cover, frameInputSize, outputSize);
      final Size frameSourceSize = frameFittedSizes.source;
      final Rect frameRect = Alignment.center
          .inscribe(frameSourceSize, const Offset(0, 4) & frameSourceSize);
      canvas.drawImageRect(
          frame!, frameRect, rect, paint..blendMode = BlendMode.srcOver);
    }

    canvas.restore();
  }

  @override
  bool shouldRepaint(MaskedAvatar oldDelegate) {
    return oldDelegate.image != this.image ||
        oldDelegate.mask != this.mask ||
        oldDelegate.frame != this.frame;
  }
}
