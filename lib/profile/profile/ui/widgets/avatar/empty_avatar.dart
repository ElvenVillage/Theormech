import 'package:flutter/widgets.dart';
import 'avatar_mask.dart';
import 'clipped_avatar.dart';

class EmptyAvatar extends StatelessWidget {
  final double size;
  final bool blank;
  final bool online;
  final bool? groupChat;
  final bool ignoreOnline;
  final bool isPostAvatar;
  const EmptyAvatar(
      {Key? key,
      required this.size,
      this.blank = false,
      this.groupChat,
      this.ignoreOnline = false,
      this.isPostAvatar = false,
      required this.online})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: size + 5,
        height: size + 5,
        child: CustomPaint(
          painter: MaskedAvatar(
              image: isPostAvatar
                  ? UserAvatarComponentsUtils.postBlank
                  : groupChat == null
                      ? blank
                          ? UserAvatarComponentsUtils.blankAvatar
                          : UserAvatarComponentsUtils.emptyAvatar
                      : groupChat == true
                          ? UserAvatarComponentsUtils.blankGroupChat
                          : UserAvatarComponentsUtils.blankChat,
              mask: UserAvatarComponentsUtils.mask,
              frame: ignoreOnline
                  ? null
                  : online
                      ? UserAvatarComponentsUtils.onlineFrame
                      : UserAvatarComponentsUtils.offlineFrame,
              width: size + 5),
        ));
  }
}
