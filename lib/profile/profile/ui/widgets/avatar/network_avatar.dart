import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'avatar_mask.dart';
import 'clipped_avatar.dart';

class NetworkAvatar extends StatefulWidget {
  final double size;
  final String avatarString;
  final bool online;
  final bool ignoreOnline;
  final bool showPlaceholder;
  const NetworkAvatar(
      {Key? key,
      this.size = 60,
      required this.avatarString,
      this.ignoreOnline = false,
      this.showPlaceholder = false,
      required this.online})
      : super(key: key);

  @override
  _NetworkAvatarState createState() => _NetworkAvatarState();
}

class _NetworkAvatarState extends State<NetworkAvatar> {
  ImageStream? _imageStream;
  ImageInfo? _imageInfo;
  CachedNetworkImageProvider? _provider;

  void _getImage() {
    final ImageStream? oldImageStream = _imageStream;
    _imageStream = _provider?.resolve(createLocalImageConfiguration(context));
    if (_imageStream!.key != oldImageStream?.key) {
      final ImageStreamListener listener = ImageStreamListener(_updateImage);
      oldImageStream?.removeListener(listener);
      _imageStream!.addListener(listener);
    }
  }

  void _updateImage(ImageInfo imageInfo, bool synchronousCall) {
    setState(() {
      _imageInfo?.dispose();
      _imageInfo = imageInfo;
    });
  }

  @override
  void dispose() {
    _imageStream?.removeListener(ImageStreamListener(_updateImage));
    _imageInfo?.dispose();
    _imageInfo = null;
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _getImage();
  }

  @override
  void initState() {
    _provider = CachedNetworkImageProvider(widget.avatarString);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (_imageInfo != null)
      return Container(
          width: widget.size + 5,
          height: widget.size + 5,
          child: CustomPaint(
            painter: MaskedAvatar(
                image: _imageInfo!.image,
                mask: UserAvatarComponentsUtils.mask,
                frame: (widget.ignoreOnline)
                    ? null
                    : widget.online
                        ? UserAvatarComponentsUtils.onlineFrame
                        : UserAvatarComponentsUtils.offlineFrame,
                width: widget.size + 5),
          ));
    else
      return Container(
          width: widget.size + 5,
          height: widget.size + 5,
          child: widget.showPlaceholder
              ? Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: CircularProgressIndicator(),
                )
              : null);
  }
}
