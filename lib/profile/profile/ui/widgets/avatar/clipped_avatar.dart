import 'dart:io';
import 'dart:ui' as ui;
import 'package:edge_alert/edge_alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/empty_avatar.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/network_avatar.dart';

import 'file_avatar.dart';

class UserAvatarComponentsUtils {
  static Future<ui.Image> get _mask async =>
      _maskInstance ??= await _load(_USER_AVATAR_MASK);
  static ui.Image? _maskInstance;
  static ui.Image get mask => _maskInstance!;

  static Future<ui.Image> get _onlineFrame async =>
      _onlineFrameInstance ??= await _load(_ONLINE_FRAME);
  static ui.Image? _onlineFrameInstance;
  static ui.Image get onlineFrame => _onlineFrameInstance!;

  static Future<ui.Image> get _offlineFrame async =>
      _offlineFrameInstance ??= await _load(_OFFLINE_FRAME);
  static ui.Image? _offlineFrameInstance;
  static ui.Image get offlineFrame => _offlineFrameInstance!;

  static Future<ui.Image> get _emptyAvatar async =>
      _emptyAvatarInstance ??= await _load(_EMPTY_USER_AVATAR);
  static ui.Image? _emptyAvatarInstance;
  static ui.Image get emptyAvatar => _emptyAvatarInstance!;
  static bool get initiailized => _emptyAvatarInstance != null;

  static Future<ui.Image> get _blankAvatar async =>
      _blankAvatarInstance ??= await _load(_BLANK_AVATAR);
  static ui.Image? _blankAvatarInstance;
  static ui.Image get blankAvatar => _blankAvatarInstance!;

  static Future<ui.Image> get _blankChat async =>
      _blankChatInstance ??= await _load(_BLANK_CHAT);
  static ui.Image? _blankChatInstance;
  static ui.Image get blankChat => _blankChatInstance!;

  static Future<ui.Image> get _blankGroupChat async =>
      _blankGroupChatInstance ??= await _load(_BLANK_GROUP_CHAT);
  static ui.Image? _blankGroupChatInstance;
  static ui.Image get blankGroupChat => _blankGroupChatInstance!;

  static Future<ui.Image> get _postBlankAvatar async =>
      _postBlankInstance ??= await _load(_BLANK_POST_AVATAR);
  static ui.Image? _postBlankInstance;
  static ui.Image get postBlank => _blankGroupChatInstance!;

  static Future<void> init() async {
    _maskInstance = await _mask;
    _onlineFrameInstance = await _onlineFrame;
    _offlineFrameInstance = await _offlineFrame;
    _emptyAvatarInstance = await _emptyAvatar;
    _blankAvatarInstance = await _blankAvatar;
    _blankChatInstance = await _blankChat;
    _blankGroupChatInstance = await _blankGroupChat;
    _postBlankInstance = await _postBlankAvatar;
  }

  static const _ONLINE_FRAME = 'images/online_frame.png';
  static const _OFFLINE_FRAME = 'images/offline_frame.png';
  static const _USER_AVATAR_MASK = 'images/frame_mask.png';
  static const _EMPTY_USER_AVATAR = 'images/stud.png';
  static const _BLANK_AVATAR = 'images/blank_photo.png';
  static const _BLANK_POST_AVATAR = 'images/empty_project.png';

  static const _BLANK_CHAT = 'images/chat_placeholder.png';
  static const _BLANK_GROUP_CHAT = 'images/group_chat_placeholder.png';

  static Future<ui.Image> _load(String asset) async {
    ByteData data = await rootBundle.load(asset);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List());
    ui.FrameInfo fi = await codec.getNextFrame();
    return fi.image;
  }
}

class UserAvatar extends StatelessWidget {
  final double size;
  final File? avatarFile;
  final String? avatarString;
  final String? avatarUrl;
  final bool online;
  final bool isProjectAvatar;

  final bool showEditButton;
  final Function? onTapCallback;

  final bool ignoreFrame;

  static Future<File?> selectFile(
      {bool fromCamera = false, BuildContext? context}) async {
    final picker = ImagePicker();
    try {
      final pickedFile = await picker.getImage(
          source: fromCamera ? ImageSource.camera : ImageSource.gallery);
      if (pickedFile == null) return null;
      final croppedFile = await ImageCropper.cropImage(
          sourcePath: pickedFile.path,
          maxHeight: 512,
          maxWidth: 512,
          androidUiSettings: AndroidUiSettings(
              toolbarTitle: 'Редактирование',
              hideBottomControls: true,
              lockAspectRatio: true),
          aspectRatioPresets: [CropAspectRatioPreset.square]);
      return croppedFile;
    } on PlatformException {
      if (context != null) {
        EdgeAlert.show(context,
            title: 'Ошибка',
            description: 'Установите разрешения',
            duration: EdgeAlert.LENGTH_LONG,
            gravity: EdgeAlert.BOTTOM,
            backgroundColor: Colors.red,
            icon: Icons.error);
      }
      return Future.error('could not open file');
    }
  }

  UserAvatar(
      this.size,
      this.avatarFile,
      this.avatarString,
      this.showEditButton,
      this.onTapCallback,
      this.isProjectAvatar,
      this.avatarUrl,
      this.online,
      this.ignoreFrame,
      this.key);
  UserAvatar.fromFile(
      {required this.size,
      required this.avatarFile,
      this.key,
      this.onTapCallback,
      this.isProjectAvatar = false,
      this.ignoreFrame = false,
      this.online = false,
      this.showEditButton = false})
      : this.avatarUrl = null,
        this.avatarString = null;
  UserAvatar.fromString(
      {required this.size,
      required this.avatarString,
      this.key,
      this.isProjectAvatar = false,
      this.onTapCallback,
      this.online = false,
      this.ignoreFrame = false,
      this.showEditButton = false})
      : this.avatarUrl = null,
        this.avatarFile = null;

  UserAvatar.fromNetwork(
      {required this.size,
      this.key,
      required this.avatarUrl,
      this.onTapCallback,
      this.isProjectAvatar = false,
      this.ignoreFrame = false,
      this.online = false,
      this.showEditButton = false})
      : this.avatarFile = null,
        this.avatarString = null;

  final Key? key;

  @override
  Widget build(BuildContext context) {
    Widget child = Container();
    if (avatarFile != null)
      child = FileAvatar(
        size: size,
        file: avatarFile!,
      );
    if (avatarString != null) {
      if (UserAvatarComponentsUtils.initiailized)
        child = EmptyAvatar(
          online: online,
          size: size,
          isPostAvatar: isProjectAvatar,
        );
      else
        child = Container(
          width: size,
          height: size,
        );
    }
    if (avatarUrl != null)
      child = NetworkAvatar(
        key: key,
        avatarString: avatarUrl!,
        online: online,
        ignoreOnline: ignoreFrame,
        size: size,
      );
    return Stack(children: [
      if ((avatarFile != null) || (avatarString != null) || (avatarUrl != null))
        Positioned(
          child: child,
        ),
      ((avatarFile == null) && (avatarString == null) && (avatarUrl == null))
          ? Image(image: AssetImage('images/logostud.png'), width: size)
          : SizedBox.fromSize(
              size: Size(size, size),
            ),
      if (showEditButton)
        Positioned(
            right: 0,
            bottom: 0,
            child: SizedBox(
              height: size / 4,
              width: size / 4,
              child: Stack(
                children: [
                  Positioned.fill(
                      child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(123)),
                  )),
                  IconButton(
                      icon: ImageIcon(
                        AssetImage('images/pensil.png'),
                      ),
                      onPressed: () {
                        if (onTapCallback != null) onTapCallback!();
                      }),
                ],
              ),
            ))
    ]);
  }
}
