import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/profile/newsfeed/ui/widgets/post_documents.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/clipped_avatar.dart';
import 'package:teormech/profile/ui/widgets/like_footer.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/widgets/images_staggered_grid_view.dart';

class Post extends StatelessWidget {
  final Key? key;

  final String caption;
  final String text;
  final bool isProjectPost;
  final DateTime date;

  final String? avatar;
  final List<String>? photos;
  final List<String>? attachments;

  Post(
      {this.key,
      required this.caption,
      required this.text,
      required this.isProjectPost,
      required this.avatar,
      this.photos,
      this.attachments,
      required this.date})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size.width > 500
        ? 500
        : MediaQuery.of(context).size.width;
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 500),
      child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.symmetric(
                  horizontal:
                      BorderSide(width: 0.5, color: Colors.grey.shade400),
                  vertical: BorderSide.none)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  avatar == null
                      ? UserAvatar.fromString(
                          avatarString: 'images/empty_project.png',
                          size: size / 8,
                          ignoreFrame: isProjectPost,
                          isProjectAvatar: true,
                        )
                      : UserAvatar.fromNetwork(
                          size: size / 8,
                          ignoreFrame: isProjectPost,
                          avatarUrl: isProjectPost
                              ? ProjectsApi.getProjectPhoto + avatar!
                              : avatar),
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          caption,
                          style: profileTextStyle.copyWith(
                              fontWeight: FontWeight.w500,
                              fontSize: 14,
                              color: Colors.black),
                          overflow: TextOverflow.ellipsis,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 3.0),
                          child: Text(
                            '${DateFormat.yMMMd().format(date)} ${TimeOfDay.fromDateTime(date).format(context)}',
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Text(
                  text,
                ),
              ),
              if (photos != null)
                ImagesStaggeredGridView(fileImages: false, images: photos!),
              if (attachments != null)
                PostDocumentsBlock(
                  editable: false,
                  onlineDocuments: attachments,
                ),
              LikeFooter(
                liked: false,
              )
            ],
          )),
    );
  }
}
