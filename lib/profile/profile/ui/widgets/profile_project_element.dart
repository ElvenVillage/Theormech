import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';
import 'package:teormech/profile/newsfeed/ui/pages/projects/project_page.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';

class ProfileProjectElement extends StatelessWidget {
  ProfileProjectElement({Key? key, required this.project}) : super(key: key);

  final ProjectModel project;
  final captionFontStyle = TextStyle(color: Colors.grey);
  final dataFontStyle = TextStyle(fontWeight: FontWeight.w500);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;

    return InkWell(
      onTap: () {
        context.read<BottomBarBloc>().go(ProjectPage.route(project: project));
      },
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 8.0, top: 12.0),
              child: Text(
                project.title,
                style: dataFontStyle,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: Row(
                children: [
                  Container(
                      margin: const EdgeInsets.all(8.0),
                      height: width / 4,
                      child: project.image != null
                          ? Image.network(
                              ProjectsApi.getProjectPhoto + project.image!,
                              fit: BoxFit.cover,
                              width: width / 4,
                            )
                          : Image(
                              image: AssetImage('images/empty_project.png'),
                              fit: BoxFit.cover,
                              width: width / 4,
                            )),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 4.0),
                            child: Text('Наставник', style: captionFontStyle),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: project.tutor != null
                                ? BlocProvider<UserProfileBloc>(
                                    create: (context) =>
                                        UserProfileBloc.fromLid(
                                            lid: project.tutor!.id.toString(),
                                            loadFromHive: false),
                                    child: Builder(builder: (context) {
                                      return BlocBuilder<UserProfileBloc,
                                              UserProfileState>(
                                          builder: (context, userProfileState) {
                                        return Text(userProfileState.fullName,
                                            style: dataFontStyle);
                                      });
                                    }))
                                : Text(
                                    ' — ',
                                    style: dataFontStyle,
                                  ),
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 5.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      if (project.dateString.isNotEmpty)
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              bottom: 4.0),
                                          child: Text(
                                            'Сроки реализации',
                                            style: captionFontStyle,
                                          ),
                                        ),
                                      if (project.dateString.isNotEmpty)
                                        Text(project.dateString,
                                            style: dataFontStyle),
                                    ],
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  padding: const EdgeInsets.only(
                                      left: 8.0, right: 8.0),
                                  decoration: BoxDecoration(
                                      border: Border(
                                          left:
                                              BorderSide(color: Colors.grey))),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 4.0),
                                        child: Text(
                                          'Рабочая группа',
                                          style: captionFontStyle,
                                        ),
                                      ),
                                      Text(
                                          '${project.participants?.length.toString()} чел.',
                                          style: dataFontStyle),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
        padding: const EdgeInsets.only(bottom: 8.0),
        margin: const EdgeInsets.symmetric(horizontal: 10.0),
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.grey.shade300))),
      ),
    );
  }
}
