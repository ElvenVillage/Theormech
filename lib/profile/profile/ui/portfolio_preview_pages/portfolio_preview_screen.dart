import 'dart:io';
import 'dart:ui';
import 'package:dio/dio.dart';
import 'package:path/path.dart' as path;
import 'package:share_plus/share_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'dart:isolate';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:path_provider/path_provider.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';
import 'package:teormech/profile/profile/rep/user_profile_rep.dart';
import 'package:teormech/rep/downloader.dart';
import 'package:teormech/rep/native_downloader.dart';
import 'package:teormech/styles/text.dart';

class PortfolioPreviewScreen extends StatefulWidget {
  const PortfolioPreviewScreen({Key? key, this.url, this.surname})
      : super(key: key);

  final String? url;
  final String? surname;

  @override
  State<PortfolioPreviewScreen> createState() => _PortfolioPreviewScreenState();
}

class _PortfolioPreviewScreenState extends State<PortfolioPreviewScreen> {
  double _progress = 0;
  final _port = ReceivePort();
  late final String url;

  Map<String, String> _taskIds = {};

  void _initIds() async {
    final list = await FlutterDownloader.loadTasksWithRawQuery(
        query: 'SELECT * FROM task WHERE status!=3');

    list?.forEach((task) {
      _taskIds[task.url] = task.taskId;
    });
  }

  @override
  void initState() {
    final credentials = UserProfileRepository().credentials;
    url = PortfolioApi.getPdfResume +
        '?session=${credentials.session}&token=${credentials.token}';
    IsolateNameServer.registerPortWithName(
        _port.sendPort, Downloader.DOWNLOAD_SEND_PORT);

    _initIds();

    _port.listen((message) {
      setState(() {
        if (message[0] == _taskIds[url]) {
          _progress = message[2] / 100;
        }
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
            toolbarHeight: 64,
            backgroundColor: Color(0xFF0a2192),
            title: Text('Предпросмотр', style: appBarTextStyle)),
        body: Stack(
          children: [
            Column(
              children: [
                Container(
                  color: Colors.blue.shade900,
                  height: 45,
                  width: double.infinity,
                  child: widget.url == null
                      ? null
                      : Row(
                          children: [
                            Expanded(
                              flex: 6,
                              child: BlocProvider<UserProfileBloc>(
                                create: (context) =>
                                    UserProfileBloc.myProfile(),
                                child: Builder(builder: (context) {
                                  return BlocBuilder<UserProfileBloc,
                                      UserProfileState>(
                                    builder: (context, userProfileState) {
                                      return Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: Text(
                                            '${userProfileState.profile.surname} Резюме.pdf',
                                            style: appBarTextStyle.copyWith(
                                              fontSize: 14,
                                            )),
                                      );
                                    },
                                  );
                                }),
                              ),
                            ),
                            Spacer(),
                            Flexible(
                              child: IconButton(
                                  icon: Icon(Icons.download),
                                  iconSize: 20,
                                  color: Colors.white,
                                  onPressed: () async {
                                    final dir = await NativeDownloader
                                        .getDownloadDirectory();
                                    if (dir != null) {
                                      final id =
                                          await FlutterDownloader.enqueue(
                                              url: url,
                                              savedDir: dir.path,
                                              showNotification: true,
                                              openFileFromNotification: true,
                                              saveInPublicStorage: true);
                                      if (id != null) {
                                        _taskIds[url] = id;
                                      }
                                    }
                                  }),
                            ),
                            Flexible(
                              child: IconButton(
                                icon: ImageIcon(
                                    AssetImage('images/icons/share.png')),
                                iconSize: 32,
                                color: Colors.white,
                                onPressed: () async {
                                  final dir = await getTemporaryDirectory();
                                  final downloadPath =
                                      path.join(dir.path, 'cv.pdf');
                                  final f = File(downloadPath);
                                  if (f.existsSync()) {
                                    await f.delete();
                                  }
                                  final dio = Dio();
                                  await dio.download(url, downloadPath);
                                  Share.shareFiles([downloadPath]);
                                },
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            )
                          ],
                        ),
                ),
                PdfContainer(url: widget.url),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class PdfContainer extends StatefulWidget {
  const PdfContainer({
    Key? key,
    required this.url,
  }) : super(key: key);

  final String? url;

  @override
  State<PdfContainer> createState() => _PdfContainerState();
}

class _PdfContainerState extends State<PdfContainer> {
  late final Widget _pdf;
  @override
  void initState() {
    final credentials = UserProfileRepository().credentials;
    _pdf = widget.url == null
        ? PDF(
            enableSwipe: true,
            autoSpacing: false,
            pageFling: false,
          ).fromAsset('images/portfolio_assets/sample.pdf')
        : PDF(
            enableSwipe: true,
            autoSpacing: false,
            pageFling: false,
          ).fromUrl(
            '${widget.url}session=${credentials.session}&token=${credentials.token}');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: _pdf,
    );
  }
}
