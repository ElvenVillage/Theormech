import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/profile/newsfeed/bloc/newsfeed_cubit.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_model.dart';
import 'package:teormech/profile/newsfeed/model/newsfeed_state.dart';
import 'package:teormech/profile/newsfeed/rep/newsfeed_repository.dart';
import 'package:teormech/profile/profile/ui/widgets/profile_project_element.dart';
import 'package:teormech/styles/text.dart';

class ProfileProjectsScreen extends StatelessWidget {
  const ProfileProjectsScreen(
      {Key? key, required this.uid, this.myProfile = false})
      : super(key: key);

  final String uid;
  final bool myProfile;
  @override
  Widget build(BuildContext context) {
    if (myProfile)
      return BlocBuilder<NewsfeedCubit, NewsfeedState>(
          builder: (context, state) {
        final projects = state.myProjects;
        if (projects == null)
          return Center(
            child: CircularProgressIndicator(),
          );
        if (projects.isEmpty)
          return Center(
            child: Text('У вас пока нет проектов', style: h2DrawerGreyText),
          );
        return ListView.builder(
            itemCount: projects.length,
            itemBuilder: (context, idx) => ProfileProjectElement(
                  project: projects[idx],
                ));
      });
    else
      return _UserProjectsScreen(uid: uid);
  }
}

class _UserProjectsScreen extends StatefulWidget {
  const _UserProjectsScreen({Key? key, required this.uid}) : super(key: key);

  final String uid;

  @override
  State<_UserProjectsScreen> createState() => __UserProjectsScreenState();
}

class __UserProjectsScreenState extends State<_UserProjectsScreen> {
  List<ProjectModel>? projects = null;

  void fetchProjects() async {
    final fetchedProjects =
        await NewsfeedRepository().fetchProjects(uid: widget.uid);

    setState(() {
      projects = fetchedProjects;
    });
  }

  @override
  void initState() {
    fetchProjects();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (projects == null)
      return Center(
        child: CircularProgressIndicator(),
      );
    if (projects!.isEmpty)
      return Center(
        child: Text('У вас пока нет проектов', style: h2DrawerGreyText),
      );
    return ListView.builder(
        itemCount: projects!.length,
        itemBuilder: (context, idx) => ProfileProjectElement(
              project: projects![idx],
            ));
  }
}
