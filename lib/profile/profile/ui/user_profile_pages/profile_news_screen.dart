import 'package:flutter/material.dart';
import 'package:teormech/profile/profile/ui/widgets/post.dart';

class ProfileNewsScreen extends StatelessWidget {
  const ProfileNewsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final posts = [
      for (var i = 0; i < 7; i++)
        Post(
          date: DateTime.now(),
          avatar: null,
          caption: 'Симонов Иван Александрович',
          isProjectPost: false,
          text: 'Конференция по проблемам современной механики прошла на "Ура"',
        )
    ];
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: posts,
      ),
    );
  }
}
