import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/auth/bloc/auth_bloc.dart';
import 'package:teormech/generated/l10n.dart';
import 'package:teormech/profile/profile/bloc/portfolio_bloc.dart';
import 'package:teormech/profile/profile/bloc/show_portfolio_bloc.dart';
import 'package:teormech/profile/profile/model/portfolio_state.dart';
import 'package:teormech/profile/profile/ui/portfolio_preview_pages/portfolio_preview_screen.dart';
import 'package:teormech/profile/profile/ui/widgets/tags_container.dart';
import 'package:teormech/rep/native_downloader.dart'
    if (dart.library.js) 'package:teormech/rep/web_downloader.dart';

import 'package:teormech/styles/text.dart';
import 'package:teormech/widgets/user_lang_container.dart';

class ProfilePortfolioScreen extends StatelessWidget {
  const ProfilePortfolioScreen({Key? key, required this.isMyProfile})
      : super(key: key);

  final bool isMyProfile;

  Widget _achievementsBlock(
      {required List<PortfolioAchievement> list, required String name}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (list.isNotEmpty)
          Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 8,
            ),
            child: Text(name,
                style: h2TextStyle.copyWith(fontWeight: FontWeight.bold)),
          ),
        for (var achievement in list)
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                '${DateTime.parse(achievement.date).year.toString()} г.',
                style: const TextStyle(color: Colors.pinkAccent),
              ),
              Expanded(
                flex: 7,
                child: Padding(
                  padding: const EdgeInsets.only(left: 5.0),
                  child: Text(achievement.caption),
                ),
              ),
              //Spacer(),
              if (achievement.isApproved)
                Padding(
                  padding: const EdgeInsets.only(right: 16.0),
                  child: ImageIcon(
                    AssetImage('images/icons/ok.png'),
                    color: Colors.pinkAccent,
                    size: 16,
                  ),
                )
            ]),
          )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ShowPortfolioCubit, PortfolioState>(
      builder: (context, state) {
        return SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (state.tags.tags.isNotEmpty)
                Padding(
                  padding: const EdgeInsets.only(left: 15.0, top: 15),
                  child: Text(S.of(context).specializations,
                      style: h2TextStyle.copyWith(fontWeight: FontWeight.bold)),
                ),
              if (state.tags.tags.isNotEmpty)
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 12, horizontal: 15),
                  child: TagsContainer(
                    tags: state.tags.tags,
                    isCompact: true,
                    tapCallback: (str) => print(str),
                  ),
                ),
              if (state.professionalSkills?.isNotEmpty ?? false) ...[
                Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 8, left: 15),
                  child: Text(S.of(context).professional_skills,
                      style: h2TextStyle.copyWith(fontWeight: FontWeight.bold)),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Text(state.professionalSkills!),
                ),
              ],
              if (state.languages.isNotEmpty)
                Padding(
                  padding: const EdgeInsets.only(top: 8, bottom: 8, left: 15),
                  child: Text(S.of(context).language_skills,
                      style: h2TextStyle.copyWith(fontWeight: FontWeight.bold)),
                ),
              for (var lang in state.languages)
                if (lang.isNotEmpty)
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: LanguageContainer(lang),
                  ),
              if (state.workExperience.isNotEmpty)
                Padding(
                  padding: const EdgeInsets.only(top: 8, bottom: 8, left: 15),
                  child: Text(S.of(context).work_experience,
                      style: h2TextStyle.copyWith(fontWeight: FontWeight.bold)),
                ),
              for (var experience in state.workExperience)
                if (experience.isNotEmpty)
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: ImageIcon(
                            AssetImage('images/icons/exp.png'),
                            color: Colors.blueAccent,
                          ),
                        ),
                        Expanded(
                            child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(experience.trim()),
                        )),
                      ],
                    ),
                  ),
              Padding(
                padding: const EdgeInsets.only(left: 15.0),
                child: _achievementsBlock(
                    list: state.achievementsOnly, name: 'Достижения'),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0, left: 15),
                child: _achievementsBlock(
                    list: state.publicationsOnly, name: 'Публикации'),
              ),
              if (!isMyProfile) const SizedBox(height: 30),
              if (isMyProfile)
                MaterialButton(
                    onPressed: () {
                      final credentials =
                          (context.read<AuthBloc>().state as Authorized).token;
                      if (kIsWeb) {
                        download(
                            url: PortfolioApi.getPdfResume +
                                '?session=${credentials.session}&token=${credentials.token}');
                      } else {
                        Navigator.of(context, rootNavigator: true).push(
                          MaterialPageRoute(
                            builder: (context) => MultiBlocProvider(
                              providers: [
                                BlocProvider<PortfolioBloc>(
                                  create: (context) => PortfolioBloc(),
                                )
                              ],
                              child: PortfolioPreviewScreen(
                                url: PortfolioApi.getPdfResume,
                                key: Key(PortfolioApi.getPdfResume),
                              ),
                            ),
                          ),
                        );
                      }
                    },
                    child: Text(
                      'Посмотреть резюме',
                      style: const TextStyle(
                          color: Colors.grey,
                          decoration: TextDecoration.underline),
                    ))
            ],
          ),
        );
      },
    );
  }
}
