import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/bloc/auth_bloc.dart';
import 'package:teormech/auth/bloc/reg_cubit.dart';
import 'package:teormech/auth/ui/widgets/input_container.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/styles/edge_alert_notification.dart';
import 'package:teormech/styles/text.dart';

class SecuritySettings extends StatefulWidget {
  const SecuritySettings({Key? key}) : super(key: key);

  @override
  _SecuritySettingsState createState() => _SecuritySettingsState();
}

class _SecuritySettingsState extends State<SecuritySettings> {
  bool _wasEdited = false;
  bool _isVisible = false;
  final _controller = TextEditingController();
  final _confirmController = TextEditingController();
  final _oldPasswordController = TextEditingController();

  void _toggleEdited() {
    if (_confirmController.text.isNotEmpty ||
        _controller.text.isNotEmpty ||
        _oldPasswordController.text.isNotEmpty)
      setState(() {
        _wasEdited = true;
      });
  }

  @override
  void initState() {
    _controller.addListener(_toggleEdited);
    _confirmController.addListener(_toggleEdited);
    _oldPasswordController.addListener(_toggleEdited);
    super.initState();
  }

  @override
  void dispose() {
    _confirmController.dispose();
    _controller.dispose();
    _oldPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return BlocProvider(
      create: (context) => RegistrationCubit(
          authBloc: context.read<AuthBloc>(), editProfile: true),
      child: Builder(builder: (context) {
        return Scaffold(
            appBar: ProfileAppBar(
              overrideLeftButton: _wasEdited
                  ? IconButton(
                      onPressed: () {
                        context.read<BottomBarBloc>().pop();
                      },
                      color: Colors.white,
                      icon: Icon(Icons.cancel_rounded))
                  : null,
              rightmostButton: _wasEdited
                  ? IconButton(
                      onPressed: () async {
                        if (_confirmController.text == _controller.text &&
                            _confirmController.text.isNotEmpty) {
                          final a = await context
                              .read<RegistrationCubit>()
                              .updatePassword(
                                  (context.read<AuthBloc>().state as Authorized)
                                      .token,
                                  newPassword: _controller.text,
                                  oldPassword: _oldPasswordController.text,
                                  newPasswordConfirmation:
                                      _confirmController.text);
                          if (a) {
                            showEdgeAlertNotification(
                                okText: 'Пароль обновлен',
                                errorText: '',
                                context: context,
                                result: a);
                          }
                        } else {
                          showEdgeAlertNotification(
                              okText: '',
                              errorText: (_controller.text.isEmpty)
                                  ? 'Введите корректный пароль'
                                  : 'Пароли не совпадают',
                              context: context,
                              result: false);
                        }
                      },
                      icon: Icon(Icons.verified),
                      color: Colors.white,
                    )
                  : null,
              title: Text(
                'Редактирование профиля',
                style: appBarTextStyle,
              ),
            ),
            backgroundColor: Colors.transparent,
            body: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 20, top: 20, bottom: 20),
                    child: Text(
                      'Смена пароля',
                      style: h2TextStyle.copyWith(
                          fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ),
                  InputContainer(
                    controller: _oldPasswordController,
                    caption: 'Старый пароль',
                    suffix: IconButton(
                      icon: ImageIcon(AssetImage(_isVisible
                          ? 'images/icons/hide.png'
                          : 'images/icons/visible.png')),
                      onPressed: () {
                        setState(() {
                          _isVisible = !_isVisible;
                        });
                      },
                      color: Colors.grey,
                    ),
                    type: _isVisible
                        ? TextInputType.emailAddress
                        : TextInputType.visiblePassword,
                    rightPadding: width / 10,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  InputContainer(
                    controller: _controller,
                    caption: 'Новый пароль',
                    type: TextInputType.visiblePassword,
                    rightPadding: width / 10,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  InputContainer(
                    caption: 'Повторите \n пароль',
                    type: TextInputType.visiblePassword,
                    controller: _confirmController,
                    rightPadding: width / 10,
                  ),
                ],
              ),
            ));
      }),
    );
  }
}
