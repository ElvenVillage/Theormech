import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/profile/chat/ui/chat_screen/delete_message_dialog.dart';
import 'package:teormech/profile/profile/bloc/portfolio_bloc.dart';
import 'package:teormech/profile/profile/bloc/show_portfolio_bloc.dart';
import 'package:teormech/profile/profile/model/portfolio_state.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';

import 'portfolio_pages/first_portfolio_page.dart';
import 'portfolio_pages/second_portfolio_page.dart';
import 'portfolio_pages/third_portfolio_page.dart';

class PortfolioEditScreen extends StatefulWidget {
  const PortfolioEditScreen({
    Key? key,
    required this.showPortfolioCubit,
  }) : super(key: key);

  final ShowPortfolioCubit showPortfolioCubit;

  @override
  _PortfolioEditScreenState createState() => _PortfolioEditScreenState();
}

class _PortfolioEditScreenState extends State<PortfolioEditScreen> {
  var _selectedPage = 0;
  var _showPopup = false;

  final _controller = PageController();

  late final PortfolioBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = PortfolioBloc(afterSyncCallback: () {
      widget.showPortfolioCubit.load();
    });
    _controller.addListener(() {
      setState(() {
        _selectedPage = _controller.page?.floor() ?? 0;
      });
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _bloc.close();
    super.dispose();
  }

  Widget _pageIndexPoint(int idx) {
    final cond = _selectedPage == idx;
    return Padding(
      padding: const EdgeInsets.only(left: 2.0, right: 2.0),
      child: Container(
          width: !cond ? 10 : 12,
          height: !cond ? 10 : 12,
          decoration: BoxDecoration(
              color: cond ? Colors.lightBlueAccent : Colors.grey,
              borderRadius: BorderRadius.circular(128))),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (_showPopup) {
          setState(() {
            _showPopup = false;
          });
          return false;
        }
        if (_selectedPage == 0) return true;
        setState(() {
          _selectedPage--;
        });
        return false;
      },
      child: BlocProvider<PortfolioBloc>.value(
        value: _bloc,
        child: Builder(builder: (context) {
          return BlocBuilder<PortfolioBloc, PortfolioState>(
            builder: (context, state) {
              return SafeArea(
                  child: Scaffold(
                      appBar: AppBar(
                        toolbarHeight: 64,
                        backgroundColor: Color(0xFF0a2192),
                        title: Row(
                          children: [
                            Text(
                              'Портфолио',
                              style: appBarTextStyle,
                            ),
                            Spacer(),
                            for (var i = 0; i < 3; i++) _pageIndexPoint(i),
                            if (state.syncing)
                              SizedBox(
                                height: 45,
                                width: 45,
                                child: Padding(
                                    child: CircularProgressIndicator(),
                                    padding: const EdgeInsets.all(5)),
                              )
                            else
                              SizedBox(
                                height: 45,
                                width: 45,
                                child: IconButton(
                                    icon: (_selectedPage != 2)
                                        ? const Icon(Icons.arrow_forward,
                                            color: Colors.white)
                                        : const ImageIcon(
                                            AssetImage('images/icons/ok.png'),
                                            color: Colors.white,
                                          ),
                                    onPressed: () {
                                      if (_selectedPage != 2) {
                                        setState(() {
                                          _selectedPage++;
                                        });
                                      } else
                                        context.read<PortfolioBloc>().sync();
                                    }),
                              )
                          ],
                        ),
                      ),
                      body: Material(
                        child: Stack(
                          children: [
                            Navigator(
                              pages: [
                                if (_selectedPage == 0)
                                  MaterialPage(
                                      child: FirstPortfolioPage(),
                                      key: ValueKey('FirstPage')),
                                if (_selectedPage == 1)
                                  MaterialPage(
                                      key: ValueKey('SecondPage'),
                                      child: SecondPortfolioPage()),
                                if (_selectedPage == 2)
                                  MaterialPage(
                                      key: ValueKey('ThirdPage'),
                                      child: ThirdPortfolioPage())
                              ],
                              onPopPage: (a, b) {
                                if (!a.didPop(b)) {
                                  return false;
                                }
                                setState(() {
                                  _selectedPage--;
                                });
                                return true;
                              },
                            ),
                            Positioned.fill(
                              child: Offstage(
                                offstage: !_showPopup,
                                child: GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  onTap: () {
                                    setState(() {
                                      _showPopup = false;
                                    });
                                  },
                                  child: Container(
                                    color:
                                        GradientColors.updatingIndicatorColor,
                                  ),
                                ),
                              ),
                            ),
                            DeleteDialog(
                              isDeleting: !_showPopup,
                              text: 'Изменения сохранены.',
                              yesCallback: (_) {
                                setState(() {
                                  _showPopup = false;
                                  _selectedPage = 0;
                                });
                              },
                              noCallback: (_) {
                                setState(() {
                                  _showPopup = false;
                                });
                              },
                            ),
                          ],
                        ),
                      )));
            },
          );
        }),
      ),
    );
  }
}
