import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/generated/l10n.dart';
import 'package:teormech/profile/profile/bloc/portfolio_bloc.dart';
import 'package:teormech/profile/profile/model/portfolio_state.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/widgets/multilne_text_field.dart';

class SecondPortfolioPage extends StatefulWidget {
  const SecondPortfolioPage({Key? key}) : super(key: key);

  @override
  _SecondPortfolioPageState createState() => _SecondPortfolioPageState();
}

class _SecondPortfolioPageState extends State<SecondPortfolioPage> {
  var _professionalSkillsCollapsed = false;

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width / 11;
    final greyStyle = h2DrawerGreyText.copyWith(fontSize: 14);
    final blackStyle = h2TextStyle.copyWith(fontWeight: FontWeight.bold);

    return SingleChildScrollView(
      child: BlocBuilder<PortfolioBloc, PortfolioState>(
        builder: (context, state) {
          return GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () => FocusScope.of(context).unfocus(),
            child: Column(
              children: [
                Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(32),
                    ),
                    child: ExpansionPanelList(
                        dividerColor: Colors.transparent,
                        expansionCallback: (idx, isExpanded) {
                          setState(() {
                            _professionalSkillsCollapsed =
                                !_professionalSkillsCollapsed;
                          });
                        },
                        elevation: 0,
                        expandedHeaderPadding: EdgeInsets.zero,
                        children: [
                          ExpansionPanel(
                            canTapOnHeader: true,
                            isExpanded: !_professionalSkillsCollapsed,
                            body: Column(
                              children: [
                                Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: width),
                                  child: Text(
                                      'Укажите навыки ' +
                                          'которыми владеете, ' +
                                          'и задачи, которые приходилось решать',
                                      style: greyStyle),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.width /
                                          11,
                                      right: MediaQuery.of(context).size.width /
                                          11),
                                  child: MultilineTextField(
                                      controller:
                                          state.professionalSkillsController),
                                ),
                              ],
                            ),
                            headerBuilder:
                                (BuildContext context, bool isExpanded) => Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(left: width),
                                  child: Text(S.of(context).professional_skills,
                                      style: blackStyle),
                                ),
                              ],
                            ),
                          )
                        ])),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: width),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(S.of(context).work_experience, style: blackStyle),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                          'Укажите опыт работы в компании/практики/стажировки/' +
                              'проектной деятельности',
                          style: greyStyle),
                    ],
                  ),
                ),
                for (var i = 0;
                    i < (state.workExperienceControllers?.length ?? 0);
                    i++)
                  Row(
                    children: [
                      Expanded(
                          flex: 7,
                          child: Container(
                            margin: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width / 11),
                            child: MultilineTextField(
                                controller: state.workExperienceControllers?[i],
                                inARow: true),
                          )),
                      Expanded(
                        child: IconButton(
                          icon: Icon(Icons.remove, color: Colors.black),
                          onPressed: () {
                            context
                                .read<PortfolioBloc>()
                                .deleteWorkExperience(i);
                          },
                        ),
                      )
                    ],
                  ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: width),
                  child: Row(
                    children: [
                      Text(S.of(context).add_work_experience,
                          style: blackStyle),
                      IconButton(
                          onPressed: () {
                            context.read<PortfolioBloc>().addWorkExperience();
                          },
                          icon: Icon(Icons.add_rounded))
                    ],
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
