import 'package:dotted_border/dotted_border.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/ui/widgets/gradient_button.dart';
import 'package:teormech/generated/l10n.dart';
import 'package:teormech/profile/profile/bloc/portfolio_bloc.dart';
import 'package:teormech/profile/profile/model/portfolio_state.dart';
import 'package:teormech/profile/profile/rep/user_portfolio_rep.dart';
import 'package:teormech/profile/profile/ui/widgets/tags_container.dart';
import 'package:teormech/profile/ui/widgets/tag.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';

class FirstPortfolioPage extends StatefulWidget {
  const FirstPortfolioPage({Key? key}) : super(key: key);

  @override
  _FirstPortfolioPageState createState() => _FirstPortfolioPageState();
}

class _FirstPortfolioPageState extends State<FirstPortfolioPage> {
  final _tagController = TextEditingController();
  final _expandableController = ExpandableController(initialExpanded: false);

  var _appendableTag = '';
  var _appendableTagLevel = ProfessionalLevel.Initial;
  var _appendableUserTag = false;

  var _enableButtons = true;

  final _dropdownKey = GlobalKey<DropdownSearchState>();

  void addTag({bool userTag = false}) {
    if (_tagController.text.trim().isEmpty) {
      return;
    }
    setState(() {
      _appendableTag = _tagController.text;
      _appendableUserTag = userTag;
    });

    _tagController.clear();
    _appendableTagLevel = ProfessionalLevel.Initial;
    _expandableController.toggle();
  }

  Widget _selectTagBar(List<PortfolioTag> tags) {
    return Container(
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage('images/roles.png'), fit: BoxFit.cover)),
      child: Padding(
        padding: const EdgeInsets.only(left: 15.0, bottom: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(S.of(context).specializations_and_skills,
                style: appBarTextStyle),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(S.of(context).input_tag,
                      style: appBarTextStyle.copyWith(
                          fontWeight: FontWeight.normal, fontSize: 14)),
                  Expanded(
                    flex: 4,
                    child: Container(
                      height: 40,
                      padding: const EdgeInsets.only(left: 8),
                      child: DropdownSearch<String>(
                        key: _dropdownKey,
                        mode: Mode.BOTTOM_SHEET,
                        onChanged: (str) {
                          _tagController.text = str;
                          if (_expandableController.expanded)
                            setState(() {
                              _appendableTag = str;
                            });
                        },
                        isFilteredOnline: true,
                        onFind: (mask) => UserPortfolioRepository().filterTags(
                            mask: mask,
                            tags: tags.map((PortfolioTag e) => e.tag).toList()),
                        showSearchBox: true,
                        showAsSuffixIcons: true,
                        popupItemBuilder: (context, a, b) => Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            if (a == '\n' && _tagController.text.isNotEmpty)
                              _emptyBuilder()
                            else
                              Container(
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(
                                              color: Colors.grey.shade200))),
                                  padding: const EdgeInsets.only(
                                      left: 20, bottom: 10, top: 10),
                                  child:
                                      Text(a, style: TextStyle(fontSize: 22))),
                          ],
                        ),
                        autoFocusSearchBox: true,
                        selectedItem: _tagController.text,
                        dropdownSearchDecoration: InputDecoration(
                          isDense: true,
                          contentPadding:
                              const EdgeInsets.fromLTRB(20, 0, 0, 0),
                          filled: true,
                          fillColor: Colors.white,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(32.0),
                          ),
                        ),
                        searchBoxController: _tagController,
                        enabled: true,
                        dropdownBuilder: (context, a, b) => Text(
                          b,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: (!_expandableController.expanded &&
                              _tagController.text.trim().isNotEmpty)
                          ? IconButton(
                              padding: const EdgeInsets.only(left: 8.0),
                              onPressed: () => addTag(),
                              color: Colors.white,
                              icon: ImageIcon(AssetImage('images/icons/ok.png'),
                                  color: Colors.white),
                            )
                          : SizedBox.shrink(),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text(
                'Введите навыки и программы, которыми вы владеете',
                style: appBarTextStyle.copyWith(
                    fontWeight: FontWeight.normal, fontSize: 14),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _professionalLevelButton(ProfessionalLevel level) {
    var text = '';

    switch (level) {
      case ProfessionalLevel.Beginner:
        text = 'Изучал';
        break;
      case ProfessionalLevel.Advanced:
        text = 'Есть проекты';
        break;
      case ProfessionalLevel.Professional:
        text = 'Работаю';
        break;
      case ProfessionalLevel.Initial:
      case ProfessionalLevel.Service:
      case ProfessionalLevel.Newsfeed:
      case ProfessionalLevel.Caption:
    }
    return MaterialButton(
      onPressed: () {
        setState(() {
          _appendableTagLevel = level;
        });

        Future.delayed(Duration(milliseconds: 500), () {
          if (_enableButtons) {
            _enableButtons = false;
            _expandableController.toggle();
            context.read<PortfolioBloc>().addTag(
                tag: _appendableTag, level: level, userTag: _appendableUserTag);
          }

          Future.delayed(Duration(milliseconds: 300), () {
            _appendableTag = '';
            _appendableTagLevel = ProfessionalLevel.Initial;
            _appendableUserTag = false;
            _dropdownKey.currentState?.changeSelectedItem(null);
            _enableButtons = true;
          });
        });
      },
      child: Text(text, style: h2DrawerGreyText),
    );
  }

  Widget _selectLevelBar() {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Spacer(),
          Flexible(
            flex: 7,
            child: Tag(
              tag:
                  PortfolioTag(tag: _appendableTag, level: _appendableTagLevel),
              isPrimary: true,
            ),
          ),
          Spacer(),
          Expanded(
            flex: 7,
            child: DottedBorder(
              color: Colors.grey,
              dashPattern: [5, 5],
              strokeWidth: 1,
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _professionalLevelButton(ProfessionalLevel.Beginner),
                    _professionalLevelButton(ProfessionalLevel.Advanced),
                    _professionalLevelButton(ProfessionalLevel.Professional)
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            width: 15,
          )
        ],
      ),
    );
  }

  Widget _languageBlock(TextEditingController controller, int idx) {
    return Padding(
        padding: const EdgeInsets.only(top: 5.0),
        child: Row(
          children: [
            Expanded(
              flex: 3,
              child: TextField(
                controller: controller,
                decoration: InputDecoration(
                  isDense: true,
                  isCollapsed: true,
                  contentPadding:
                      const EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                  filled: true,
                  fillColor: Colors.white,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(32.0),
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 15,
            ),
            IconButton(
                icon: Icon(Icons.remove),
                color: Colors.black,
                onPressed: () {
                  context.read<PortfolioBloc>().deleteLanguage(idx);
                }),
            Spacer()
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width / 11;
    return SingleChildScrollView(
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Column(
          children: [
            BlocBuilder<PortfolioBloc, PortfolioState>(
                builder: (context, state) => ExpandablePanel(
                      controller: _expandableController,
                      theme: ExpandableThemeData(
                        animationDuration: !_expandableController.expanded
                            ? Duration(milliseconds: 300)
                            : null,
                        hasIcon: false,
                      ),
                      collapsed: _selectTagBar(state.tags.tags),
                      expanded: Column(
                        children: [
                          _selectTagBar(state.tags.tags),
                          _selectLevelBar()
                        ],
                      ),
                    )),
            BlocBuilder<PortfolioBloc, PortfolioState>(
              builder: (context, state) {
                return Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: width, top: 16),
                      child: Row(
                        children: [
                          Text(
                            'Выбранные теги',
                            style: h2TextStyle.copyWith(
                                fontWeight: FontWeight.bold),
                          ),
                          Spacer(),
                          Text('${state.tags.tags.length}/15',
                              style: h2DrawerGreyText),
                          Spacer()
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 16, horizontal: 8),
                      child: TagsContainer(
                        tags: state.tags.tags,
                        tapCallback: (tag) {
                          context.read<PortfolioBloc>().deleteTag(tag: tag);
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: width, top: 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(S.of(context).language_skills,
                              style: h2TextStyle.copyWith(
                                  fontWeight: FontWeight.bold)),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: Text(S.of(context).enter_language_skills,
                                style: h2DrawerGreyText),
                          ),
                          for (var i = 0;
                              i < (state.languagesControllers?.length ?? 0);
                              i++)
                            _languageBlock(
                                state.languagesControllers!.elementAt(i), i),
                          MaterialButton(
                            padding: EdgeInsets.zero,
                            onPressed: () {
                              context.read<PortfolioBloc>().addLanguage();
                            },
                            child: Row(
                              children: [
                                Text(S.of(context).add_language_skill),
                                Icon(Icons.add),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                );
              },
            )
          ],
        ),
      ),
    );
  }

  _emptyBuilder() {
    return Center(
      child: Padding(
        padding: const EdgeInsets.only(top: 15.0),
        child: Column(
          children: [
            Text(S.of(context).tag_is_not_created_yet),
            Container(
                height: 64,
                child: GradientMaterialButton(
                  padding: 3,
                  style: const TextStyle(
                    color: Colors.white,
                  ),
                  text: 'Добавить тег',
                  from: GradientColors.leftColorLeftButton,
                  to: GradientColors.leftColorRightButton,
                  onTapCallback: () {
                    addTag(userTag: true);
                    Navigator.pop(context);
                  },
                ),
                padding: const EdgeInsets.symmetric(vertical: 15.0))
          ],
        ),
      ),
    );
  }
}
