import 'dart:io';
import 'package:expandable/expandable.dart';
import 'package:flutter/foundation.dart';
import 'package:path/path.dart' as path;
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:open_file/open_file.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/auth/bloc/auth_bloc.dart';
import 'package:teormech/generated/l10n.dart';
import 'package:teormech/profile/chat/ui/chat_screen/delete_message_dialog.dart';
import 'package:teormech/profile/profile/bloc/portfolio_bloc.dart';
import 'package:teormech/profile/profile/model/portfolio_state.dart';
import 'package:teormech/profile/profile/ui/portfolio_preview_pages/portfolio_preview_screen.dart';
import 'package:teormech/rep/native_downloader.dart'
    if (kIsWeb) 'package:teormech/rep/web_downloader';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';

class AppendableBloc extends StatefulWidget {
  AppendableBloc({
    Key? key,
    required this.achievement,
    bool this.isFirst = false,
  }) : super(key: key);

  final PortfolioAchievement achievement;
  final bool isFirst;

  @override
  _AppendableBlocState createState() => _AppendableBlocState();
}

class _AppendableBlocState extends State<AppendableBloc> {
  final _controller = ExpandableController(initialExpanded: true);

  var downloadProgress = 0.0;

  Future<void> _checkIfDownloaded() async {
    final dir = await NativeDownloader.getDownloadDirectory();
    if (dir == null) return;

    if (widget.achievement.submittedFile.isNotEmpty) {
      final exists =
          await File(dir.path + widget.achievement.submittedFile).exists();
      if (exists)
        setState(() {
          downloadProgress = 1;
        });
    }
  }

  @override
  void initState() {
    _checkIfDownloaded();
    super.initState();
  }

  final _decoration = InputDecoration(
    isDense: true,
    isCollapsed: true,
    contentPadding: const EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
    filled: true,
    hintStyle: TextStyle(fontSize: 14),
    fillColor: Colors.white,
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(32.0),
    ),
  );

  Widget _fileRow(String filePath, VoidCallback deleteFunction,
      {bool submitted = false, required isPublication}) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Container(
        padding: const EdgeInsets.only(left: 10, right: 10),
        decoration: BoxDecoration(
            color: Color(0xFFe6f7ff),
            border: Border.all(width: 1, color: Colors.transparent),
            borderRadius: BorderRadius.circular(8)),
        child: Row(
          children: [
            if (downloadProgress == 0 || downloadProgress == 1)
              SizedBox(
                width: 30,
                height: 30,
                child: IconButton(
                  icon: Icon(Icons.download),
                  onPressed: () async {
                    if (!submitted) OpenFile.open(filePath);
                    if (submitted) {
                      final dir = await NativeDownloader.getDownloadDirectory();
                      if (downloadProgress == 0) {
                        final url = isPublication
                            ? PortfolioApi.publicationFiles + filePath
                            : PortfolioApi.achievementFiles + filePath;

                        await download(
                            url: url,
                            fileName: '$filePath',
                            onReceiveProgress: (a, b) {
                              setState(() {
                                downloadProgress = a / b;
                              });
                            });
                      }
                      if (downloadProgress == 1)
                        OpenFile.open(path.join(dir?.path ?? '', filePath));
                    }
                  },
                  padding: EdgeInsets.zero,
                ),
              )
            else
              SizedBox(
                  width: 30,
                  height: 30,
                  child: Center(
                      child: Padding(
                          padding: const EdgeInsets.all(3),
                          child: CircularProgressIndicator(
                            value: downloadProgress,
                          )))),
            Expanded(
              child: Text(
                Uri.parse(filePath).pathSegments.last,
                maxLines: 1,
              ),
            ),
            IconButton(
              onPressed: deleteFunction,
              icon: Icon(Icons.remove, color: Colors.black),
              padding: EdgeInsets.zero,
            )
          ],
        ),
      ),
    );
  }

  Widget _captionRow(TextEditingController controller,
      {bool collapsed = false}) {
    return Padding(
      padding: collapsed
          ? const EdgeInsets.only(bottom: 8, left: 12)
          : const EdgeInsets.only(top: 8),
      child: Row(
        children: [
          if (!collapsed)
            Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: Text(S.of(context).achievement_title,
                  style: h2TextStyle.copyWith(fontSize: 15)),
            ),
          Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.only(left: 5.0, top: 8),
                child: TextField(
                  controller: controller,
                  decoration: _decoration,
                  readOnly: widget.achievement.isApproved,
                ),
              )),
          IconButton(
            onPressed: () {
              _controller.toggle();
            },
            icon: Icon(Icons.arrow_drop_down),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 16, right: 16),
      child: PhysicalModel(
        color: Colors.white,
        elevation: 2,
        borderRadius: BorderRadius.circular(12),
        child: ExpandablePanel(
          collapsed: Padding(
              padding: const EdgeInsets.only(left: 12, right: 12),
              child: _captionRow(widget.achievement.captionController,
                  collapsed: true)),
          expanded: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 12.0, top: 12, bottom: 4),
                child: Row(
                  children: [
                    Material(
                      child: InkWell(
                        onTap: () {
                          context
                              .read<PortfolioBloc>()
                              .deleteAchievement(widget.achievement);
                        },
                        child: const Text(
                          'Удалить достижение',
                          style: TextStyle(
                            color: Colors.grey,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              _captionRow(widget.achievement.captionController,
                  collapsed: false),
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 12, left: 12, right: 12, top: 8),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Flexible(
                            child: Text(
                                widget.achievement.isPublication
                                    ? S.of(context).date_of_publication
                                    : S.of(context).date_of_achievement,
                                style: h2TextStyle.copyWith(fontSize: 15))),
                        Expanded(
                            flex: 1,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 5.0),
                              child: TextField(
                                  onTap: () async {
                                    if (widget.achievement.isApproved) return;
                                    FocusScope.of(context)
                                        .requestFocus(FocusNode());

                                    var date = await showDatePicker(
                                        context: context,
                                        firstDate: DateTime(1945),
                                        lastDate: DateTime(2022),
                                        initialEntryMode:
                                            DatePickerEntryMode.input,
                                        initialDate: DateTime.now());
                                    widget.achievement.dateController.text =
                                        date.toString().substring(0, 10);
                                  },
                                  controller: widget.achievement.dateController,
                                  readOnly: widget.achievement.isApproved,
                                  decoration: _decoration),
                            ))
                      ],
                    ),
                    if (widget.achievement.file.isEmpty &&
                        widget.achievement.submittedFile.isEmpty)
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Row(
                          children: [
                            Text(
                              S.of(context).achievement_link,
                            ),
                            Expanded(
                                flex: 3,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 10.0),
                                  child: TextField(
                                    controller:
                                        widget.achievement.linkController,
                                    decoration: _decoration,
                                  ),
                                )),
                            Text(
                              '   /',
                              style: const TextStyle(fontSize: 16),
                            ),
                            Center(
                              child: IconButton(
                                icon: ImageIcon(
                                  AssetImage('images/icons/skr.png'),
                                  size: 22,
                                ),
                                onPressed: () async {
                                  if (widget.achievement.isApproved) return;
                                  final file =
                                      await FilePicker.platform.pickFiles();
                                  if (file != null && file.paths.isNotEmpty) {
                                    context.read<PortfolioBloc>().addFile(
                                        achievement: widget.achievement,
                                        file: file.files.first.path!);
                                  }
                                },
                              ),
                            )
                          ],
                        ),
                      )
                    else
                      _fileRow(
                          widget.achievement.file.isEmpty
                              ? widget.achievement.submittedFile
                              : widget.achievement.file, () {
                        if (widget.achievement.isApproved) return;
                        context
                            .read<PortfolioBloc>()
                            .clearFile(achievement: widget.achievement);
                      },
                          submitted:
                              widget.achievement.submittedFile.isNotEmpty,
                          isPublication: widget.achievement.isPublication),
                  ],
                ),
              ),
            ],
          ),
          controller: _controller,
        ),
      ),
    );
  }
}

class ThirdPortfolioPage extends StatefulWidget {
  const ThirdPortfolioPage({Key? key}) : super(key: key);

  @override
  State<ThirdPortfolioPage> createState() => _ThirdPortfolioPageState();
}

class _ThirdPortfolioPageState extends State<ThirdPortfolioPage> {
  var _loading = false;
  var _showPopup = false;

  void _showSample(BuildContext context, {String? url}) {
    final credentials = (context.read<AuthBloc>().state as Authorized).token;
    final portfolioBloc = context.read<PortfolioBloc>();
    if (kIsWeb) {
      download(
          url: PortfolioApi.getPdfResume +
              '?session=${credentials.session}&token=${credentials.token}');
      //WebDownloader.download();
    } else {
      Navigator.of(context, rootNavigator: true).push(
        MaterialPageRoute(
          builder: (context) => MultiBlocProvider(
            providers: [
              BlocProvider<PortfolioBloc>.value(
                value: portfolioBloc,
              ),
            ],
            child: PortfolioPreviewScreen(
              url: url,
            ),
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width / 11;
    final greyStyle = h2DrawerGreyText.copyWith(fontSize: 14);
    final blackStyle = h2TextStyle.copyWith(fontWeight: FontWeight.bold);
    return BlocBuilder<PortfolioBloc, PortfolioState>(
        builder: (context, state) {
      final achievements = state.achievementsOnly;
      final publications = state.publicationsOnly;
      return Stack(
        children: [
          SingleChildScrollView(
            child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  FocusScope.of(context).unfocus();
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding:
                          EdgeInsets.only(top: 20.0, left: width, right: width),
                      child: Text(
                        'Подтвердите данные документом или ссылкой, ' +
                            'и они будут проверены администрацией. ' +
                            'Это может послужить основанием для получения ' +
                            'повышенной стипендии',
                        style: greyStyle,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(top: 12.0, left: width, right: width),
                      child: Text(
                        'Достижения и награды',
                        style: blackStyle,
                      ),
                    ),
                    for (var i = 0; i < achievements.length; i++)
                      AppendableBloc(
                        achievement: achievements[i],
                        isFirst: i == 0,
                      ),
                    Padding(
                      padding: EdgeInsets.only(left: width),
                      child: Row(
                        children: [
                          Text(S.of(context).add_achievement),
                          IconButton(
                              onPressed: () {
                                context.read<PortfolioBloc>().addAchievement();
                              },
                              icon: Icon(Icons.add_rounded))
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(top: 12.0, left: width, right: width),
                      child: Text(
                        'Публикации',
                        style: blackStyle,
                      ),
                    ),
                    for (var i = 0; i < publications.length; i++)
                      AppendableBloc(
                        achievement: publications[i],
                        isFirst: i == 0,
                      ),
                    Padding(
                      padding: EdgeInsets.only(left: width),
                      child: Row(
                        children: [
                          Text(S.of(context).add_publication),
                          IconButton(
                              onPressed: () {
                                context.read<PortfolioBloc>().addPublication();
                              },
                              icon: Icon(Icons.add_rounded))
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: width,
                        vertical: 8,
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border(
                                top: BorderSide(color: Colors.grey.shade400))),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: width),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: Text('Генерация резюме', style: blackStyle),
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: Text(
                                  'Получите pdf-версию Вашего резюме на основании введенных данных',
                                  style:
                                      h2DrawerGreyText.copyWith(fontSize: 13),
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  children: [
                                    Container(
                                      height: 30,
                                      decoration: BoxDecoration(
                                        color: GradientColors.pink,
                                        borderRadius:
                                            BorderRadius.circular(30.0),
                                      ),
                                      child: MaterialButton(
                                        padding: const EdgeInsets.fromLTRB(
                                            20, 0, 20, 0),
                                        height: 20,
                                        onPressed: () {
                                          if (state.dirty) {
                                            setState(() {
                                              _showPopup = true;
                                            });
                                          } else {
                                            _showSample(context,
                                                url: PortfolioApi.getPdfResume);
                                          }
                                        },
                                        child: Text('Сгенерировать',
                                            textAlign: TextAlign.center,
                                            style: h2DrawerGreyText.copyWith(
                                                color: Colors.white,
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold)),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 35,
                                      child: TextButton(
                                          onPressed: () {
                                            if (!kIsWeb) {
                                              _showSample(context, url: null);
                                            }
                                          },
                                          child: const Text('Посмотреть шаблон',
                                              style: TextStyle(
                                                  decoration:
                                                      TextDecoration.underline,
                                                  fontSize: 10))),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 50,
                    )
                  ],
                )),
          ),
          Positioned.fill(
            child: Offstage(
              offstage: !(_showPopup || _loading),
              child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  if (!_loading)
                    setState(() {
                      _showPopup = false;
                    });
                },
                child: Container(
                  color: Colors.grey.withAlpha(200),
                  child: _loading
                      ? Center(
                          child: SizedBox(
                              height: 100,
                              width: 100,
                              child: CircularProgressIndicator()))
                      : null,
                ),
              ),
            ),
          ),
          DeleteDialog(
            isDeleting: !(_showPopup || _loading),
            text:
                'У вас есть несохраненные изменения. Сохранить их перед созданием резюме?',
            yesCallback: (_) async {
              if (_loading) return;
              setState(() {
                _showPopup = false;
                _loading = true;
              });
              context.read<PortfolioBloc>().sync(callback: () {
                setState(() {
                  _loading = false;
                });

                _showSample(context,
                    url: state.isEmpty ? null : PortfolioApi.getPdfResume);
              });
            },
            noCallback: (_) {
              setState(() {
                _showPopup = false;
              });
            },
          ),
        ],
      );
    });
  }
}
