import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/profile/bloc/show_portfolio_bloc.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/clipped_avatar.dart';
import 'package:teormech/profile/profile/ui/edit_profile_pages/edit_profile.dart';
import 'package:teormech/profile/profile/ui/edit_profile_pages/portfolio_screen.dart';
import 'package:teormech/profile/profile/ui/edit_profile_pages/privacy_settings.dart';
import 'package:teormech/profile/profile/ui/edit_profile_pages/security_settings.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/empty_avatar.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/styles/transitions/slide_fade.dart';

class EditProfileBaseScreen extends StatelessWidget {
  const EditProfileBaseScreen(
      {Key? key,
      required this.userProfileBloc,
      required this.showPortfolioCubit})
      : super(key: key);

  final UserProfileBloc userProfileBloc;
  final ShowPortfolioCubit showPortfolioCubit;

  static PageRouteBuilder route(
      {required UserProfileBloc userProfileBloc,
      required ShowPortfolioCubit showPortfolioCubit}) {
    return PageRouteBuilder(
        transitionsBuilder: slideFadeTransition,
        pageBuilder: (context, f1, f2) => EditProfileBaseScreen(
            userProfileBloc: userProfileBloc,
            showPortfolioCubit: showPortfolioCubit));
  }

  Widget _button(String text, Function callback) {
    return MaterialButton(
        padding: const EdgeInsets.symmetric(vertical: 12.0),
        onPressed: () => callback(),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
        child: Text(
          text,
          style: TextStyle(fontWeight: FontWeight.normal, fontSize: 16),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ProfileAppBar(
        title: Text(
          'Редактирование профиля',
          style: appBarTextStyle,
        ),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 20),
              BlocBuilder<UserProfileBloc, UserProfileState>(
                builder: (context, state) => state.profile.avatar != null
                    ? UserAvatar.fromNetwork(
                        size: 150,
                        ignoreFrame: true,
                        avatarUrl:
                            ProfileApi.profileAvatar + state.profile.avatar!)
                    : EmptyAvatar(
                        size: 150,
                        ignoreOnline: true,
                        groupChat: false,
                        online: false,
                      ),
                bloc: userProfileBloc,
              ),
              _button('Личная информация', () {
                context.read<BottomBarBloc>().go(MaterialPageRoute(
                    builder: (context) =>
                        EditProfileScreen(userProfileBloc: userProfileBloc)));
              }),
              _button('Портфолио', () {
                Navigator.of(context, rootNavigator: true)
                    .push(MaterialPageRoute(
                        builder: (context) => PortfolioEditScreen(
                              showPortfolioCubit: showPortfolioCubit,
                            )));
              }),
              _button('Уведомления', () {}),
              _button('Приватность', () {
                context.read<BottomBarBloc>().go(MaterialPageRoute(
                    builder: (context) =>
                        PrivacySettingsScreen(bloc: userProfileBloc)));
              }),
              _button('Безопасность', () {
                context.read<BottomBarBloc>().go(MaterialPageRoute(
                    builder: (context) => SecuritySettings()));
              }),
            ],
          ),
        ),
      ),
    );
  }
}
