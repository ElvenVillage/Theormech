import 'package:edge_alert/edge_alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/auth/bloc/auth_bloc.dart';
import 'package:teormech/auth/bloc/reg_cubit.dart';
import 'package:teormech/auth/model/reg_model.dart';
import 'package:teormech/generated/l10n.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/subscription/model/followers_state.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/widgets/additional_user_info.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/clipped_avatar.dart';
import 'package:teormech/widgets/primary_user_info.dart';

class EditProfileScreen extends StatelessWidget {
  final UserProfileBloc userProfileBloc;
  EditProfileScreen({required this.userProfileBloc});
  @override
  Widget build(BuildContext context) {
    return BlocProvider<UserProfileBloc>.value(
      value: userProfileBloc,
      child: Builder(builder: (context) {
        return MultiBlocProvider(providers: [
          BlocProvider<RegistrationCubit>(
              create: (context) => RegistrationCubit(
                  authBloc: context.read<AuthBloc>(), editProfile: true)
                ..readOnly(false)),
          BlocProvider<RegistrationDataCubit>(create: (context) {
            return RegistrationDataCubit(editProfile: true)
              ..subscribe(context.read<UserProfileBloc>())
              ..attachListeners();
          })
        ], child: EditProfileScreenWrapper());
      }),
    );
  }
}

class EditProfileScreenWrapper extends StatefulWidget {
  EditProfileScreenWrapper();

  @override
  _EditProfileScreenWrapperState createState() =>
      _EditProfileScreenWrapperState();
}

class _EditProfileScreenWrapperState extends State<EditProfileScreenWrapper> {
  void onTapCallback({bool photo = false}) async {
    final pickedFile =
        await UserAvatar.selectFile(fromCamera: photo, context: context);
    if (pickedFile != null) {
      context.read<RegistrationDataCubit>().setAvatar(pickedFile);
      context
          .read<RegistrationCubit>()
          .uploadAvatar((context.read<AuthBloc>().state as Authorized).token,
              avatar: pickedFile)
          .then((a) {
        context.read<UserProfileBloc>().fetchFromServer(
              loadFromHive: false,
            );
      });
    }
  }

  bool isExpanded = false;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return BlocConsumer<RegistrationCubit, RegState>(
        listener: (context, regState) async {
      if (regState.regStatus == RegStatus.Success) {
        await context.read<UserProfileBloc>().fetchFromServer(
              loadFromHive: false,
            );
      }
      if (regState.regStatus == RegStatus.Error) {
        EdgeAlert.show(context,
            icon: Icons.error,
            title: 'Ошибка',
            description: 'Не удалось изменить данные',
            duration: 1,
            gravity: EdgeAlert.BOTTOM,
            backgroundColor: Colors.red);
        await context.read<UserProfileBloc>().fetchFromServer(
              loadFromHive: false,
            );
      }
    }, builder: (context, regState) {
      return BlocBuilder<RegistrationDataCubit, RegistrationData>(
          builder: (context, state) {
        return GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: BlocBuilder<UserProfileBloc, UserProfileState>(
              builder: (context, profileState) {
            return Scaffold(
              appBar: ProfileAppBar(
                rightmostButton: IconButton(
                  icon: ImageIcon(
                    AssetImage('images/icons/ok.png'),
                    color: Colors.white,
                  ),
                  onPressed: () async {
                    await context
                        .read<RegistrationCubit>()
                        .uploadAdditionalData(
                            (context.read<AuthBloc>().state as Authorized)
                                .token,
                            data: state,
                            profileState: profileState);

                    await context.read<UserProfileBloc>().fetchFromServer(
                        loadFromHive: false, forceReload: true);
                    context
                        .read<BottomBarBloc>()
                        .setTab(BottomBars.Home, pop: true);
                  },
                ),
                title: Text(S.of(context).from_profile_to_edit_profile,
                    style: appBarTextStyle),
              ),
              backgroundColor: Colors.transparent,
              body: Stack(
                children: [
                  SingleChildScrollView(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 20, top: 10),
                              child: Text(
                                'Фотография профиля',
                                style: h1TextStyle.copyWith(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Stack(children: [
                              ((profileState.profile.avatar != '') &&
                                      (profileState.profile.avatar != null) &&
                                      (state.avatar == null))
                                  ? UserAvatar.fromNetwork(
                                      size: size.width / 3,
                                      avatarUrl: ProfileApi.profileAvatar +
                                          profileState.profile.avatar!,
                                      showEditButton: true,
                                      ignoreFrame: true,
                                      onTapCallback: onTapCallback,
                                    )
                                  : UserAvatar.fromFile(
                                      size: size.width / 3,
                                      avatarFile: state.avatar,
                                      ignoreFrame: true,
                                      showEditButton: true,
                                      onTapCallback: onTapCallback,
                                    ),
                              if ((regState.progress != null) &&
                                  (regState.progress != 1))
                                Positioned(
                                  left: size.width / 32,
                                  top: size.width / 32,
                                  child: SizedBox(
                                    height: size.width / 4,
                                    width: size.width / 4,
                                    child: CircularProgressIndicator(
                                      value: regState.progress,
                                    ),
                                  ),
                                )
                            ]),
                            Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Material(
                                color: Colors.white,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: InkWell(
                                          child: Text(
                                              S.of(context).select_new_photo,
                                              style: const TextStyle(
                                                  color: Colors.black)),
                                          onTap: onTapCallback),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: InkWell(
                                          child: Text(
                                              S.of(context).make_new_photo,
                                              style: const TextStyle(
                                                  color: Colors.black)),
                                          onTap: () {
                                            onTapCallback(photo: true);
                                          }),
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                        Theme(
                          data: Theme.of(context)
                              .copyWith(cardColor: Colors.white),
                          child: ExpansionPanelList(
                            elevation: 0,
                            expandedHeaderPadding: const EdgeInsets.all(0),
                            expansionCallback: (int idx, bool a) {
                              setState(() {
                                isExpanded = !a;
                              });
                            },
                            children: [
                              ExpansionPanel(
                                  isExpanded: isExpanded,
                                  canTapOnHeader: true,
                                  headerBuilder: (context, open) {
                                    return Container(
                                      padding: const EdgeInsets.only(
                                          left: 20, top: 10),
                                      color: Colors.transparent,
                                      child: Text(
                                        'Основная информация',
                                        style: h2TextStyle.copyWith(
                                            fontWeight: FontWeight.bold),
                                      ),
                                    );
                                  },
                                  body: Column(
                                    children: [
                                      RichText(
                                        text: TextSpan(
                                            text: 'Чтобы изменить эти данные, ',
                                            style: h2TextStyle.copyWith(
                                                color: Colors.black,
                                                fontSize: 14),
                                            children: [
                                              TextSpan(
                                                  text: 'отправьте запрос',
                                                  style: h2TextStyle.copyWith(
                                                      color: Colors.blueGrey,
                                                      fontSize: 14))
                                            ]),
                                      ),
                                      PrimaryUserInfo(
                                        state: state,
                                        editProfile: true,
                                        needControl: false,
                                        needValidateEmail: false,
                                        needStringValidation: false,
                                        overrideReadOnly: true,
                                      )
                                    ],
                                  ))
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: Text(
                                'Дополнительная информация',
                                style: h2TextStyle.copyWith(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                              ),
                            ),
                          ],
                        ),
                        BlocBuilder<UserProfileBloc, UserProfileState>(
                            builder: (context, userProfile) =>
                                AdditionalUserInfo(
                                  size: size,
                                  needControl: false,
                                  editProfile: true,
                                  regState: regState,
                                  userProfile: userProfile.profile,
                                )),
                      ],
                    ),
                  ),
                  if (regState.status == FetchingStatus.InProgress)
                    Positioned.fill(
                        child: Container(
                      color: GradientColors.updatingIndicatorColor,
                    )),
                  if (regState.status == FetchingStatus.InProgress)
                    Positioned.fromRect(
                        rect: Rect.fromLTWH(size.width / 3, size.width / 3,
                            size.width / 3, size.width / 3),
                        child: CircularProgressIndicator()),
                ],
              ),
            );
          }),
        );
      });
    });
  }
}
