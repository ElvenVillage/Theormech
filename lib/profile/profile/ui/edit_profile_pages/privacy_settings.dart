import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/profile/model/user_profile.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/styles/text.dart';

class PrivacySettingsScreen extends StatefulWidget {
  const PrivacySettingsScreen({Key? key, required this.bloc}) : super(key: key);
  final UserProfileBloc bloc;

  @override
  _PrivacySettingsScreenState createState() => _PrivacySettingsScreenState();
}

class _PrivacySettingsScreenState extends State<PrivacySettingsScreen> {
  final _limitsController = TextEditingController();
  final _expandableController = ExpandableController(initialExpanded: false);
  bool _enableLimits = false;

  @override
  void dispose() {
    _limitsController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    final bloc = context.read<UserProfileBloc>();
    bloc.getLimits();
    _limitsController.addListener(() {
      if (_limitsController.text.isNotEmpty &&
          _limitsController.text != bloc.state.limits.toString())
        context
            .read<UserProfileBloc>()
            .setLimits(int.parse(_limitsController.text));
    });
    super.initState();
  }

  MaterialButton _privacyButton(String key, UserProfileState state) {
    final field = state.profile.privacySettings[key] ?? Privacy.Public;
    var color = Colors.lightBlue;
    final enabled = field == Privacy.Private ||
        field == Privacy.Public ||
        field == Privacy.Subscribers;
    var text = '';
    switch (field) {
      case Privacy.Private:
      case Privacy.PrivateUpdating:
        text = 'Только мне';
        color = Colors.grey;
        break;
      case Privacy.Subscribers:
      case Privacy.SubscribersUpdating:
        text = 'Моим подпискам';
        color = Colors.grey;
        break;

      case Privacy.Public:
      case Privacy.PublicUpdating:
        text = 'Всем';
        break;

      // text = 'Ошибка';
      // color = Colors.red;
    }

    return MaterialButton(
        onPressed: enabled
            ? () {
                widget.bloc.updatePrivacySettings(key: key);
              }
            : null,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 10),
          child: Text(text,
              style: TextStyle(color: enabled ? color : Colors.grey)),
          decoration: BoxDecoration(
              border: Border.all(color: enabled ? color : Colors.grey),
              borderRadius: BorderRadius.circular(12)),
        ));
  }

  Row _privacyRow(String key, String title, UserProfileState state) {
    return Row(
      children: [
        Expanded(child: Text(title), flex: 1),
        Expanded(child: _privacyButton(key, state), flex: 2),
        Spacer()
      ],
    );
  }

  Widget _limitsTextField(TextEditingController? controller) {
    return Padding(
        padding: const EdgeInsets.only(left: 5.0, right: 5.0),
        child: SizedBox(
            width: 30,
            height: 20,
            child: TextField(
                controller: controller,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    isDense: true,
                    isCollapsed: true,
                    filled: true,
                    contentPadding: const EdgeInsets.only(left: 10),
                    fillColor: Colors.white,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(32.0),
                    )))));
  }

  Widget _header({required bool wasFetched}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Преподавателю',
                style: h2TextStyle.copyWith(fontWeight: FontWeight.bold)),
            SizedBox(
              width: 35,
              height: 35,
              child: wasFetched
                  ? Checkbox(
                      value: _enableLimits,
                      onChanged: (val) {
                        if (val ?? false)
                          context.read<UserProfileBloc>().setLimits(3);
                        else
                          context.read<UserProfileBloc>().setLimits(0);
                      })
                  : Center(
                      child: SizedBox(
                          width: 20,
                          height: 20,
                          child: CircularProgressIndicator()),
                    ),
            )
          ],
        ),
      ],
    );
  }

  Widget _editableBloc() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Ограничение для студентов по отправке',
            style: h2TextStyle.copyWith(fontWeight: FontWeight.bold)),
        Wrap(
          children: [
            Text('не более',
                style: h2TextStyle.copyWith(fontWeight: FontWeight.bold)),
            _limitsTextField(_limitsController),
            Text('сообщений в неделю',
                style: h2TextStyle.copyWith(fontWeight: FontWeight.bold))
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10.0, bottom: 20.0),
          child: Text(
              'Накладывается на всех, кроме старост. Отменить ограничение для конкретных студентов можно в чате.',
              style: h2DrawerGreyText),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: ProfileAppBar(
          title: Text(
            'Приватность',
            style: appBarTextStyle,
          ),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
              child: Column(
                children: [
                  BlocConsumer<UserProfileBloc, UserProfileState>(
                      listener: (_, state) {
                    _limitsController.text = state.limits.toString();
                    if (state.limits == 0)
                      setState(() {
                        _enableLimits = false;
                      });
                    else
                      setState(() {
                        _enableLimits = true;
                      });
                    if (state.limits == 0 && _expandableController.expanded)
                      _expandableController.toggle();
                    if (state.limits != 0 && !_expandableController.expanded)
                      _expandableController.toggle();
                  }, builder: (context, state) {
                    if (state.profile.isTeacher)
                      return ExpandablePanel(
                        controller: _expandableController,
                        collapsed: _header(wasFetched: state.wasFetched),
                        expanded: Column(
                          children: [
                            _header(wasFetched: state.wasFetched),
                            _editableBloc()
                          ],
                        ),
                      );
                    else
                      return SizedBox.shrink();
                  }),
                  Row(
                    children: [
                      Text('Видимость',
                          style: h2TextStyle.copyWith(
                              fontWeight: FontWeight.bold)),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child:
                            ImageIcon(AssetImage('images/icons/visible.png')),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Text(
                      'Выберите, каким группам пользователей будет доступна Ваша информация',
                      style: h2TextStyle,
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text('Личная информация',
                  style: h2TextStyle.copyWith(
                      fontWeight: FontWeight.bold, color: Color(0xFF0d1092))),
            ),
            BlocBuilder<UserProfileBloc, UserProfileState>(
              builder: (context, state) {
                return Padding(
                  padding: const EdgeInsets.only(left: 24.0),
                  child: Column(
                    children: [
                      _privacyRow(
                        'phone',
                        'Номер телефона',
                        state,
                      ),
                      _privacyRow(
                        'email',
                        'Личная почта',
                        state,
                      ),
                      _privacyRow(
                        'contacts',
                        'Соц. сети',
                        state,
                      ),
                    ],
                  ),
                );
              },
              bloc: widget.bloc,
            )
          ],
        ),
      ),
    );
  }
}
