import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/generated/l10n.dart';
import 'package:background_app_bar/background_app_bar.dart';
import 'package:teormech/auth/model/contacts.dart';
import 'package:teormech/profile/newsfeed/bloc/newsfeed_cubit.dart';
import 'package:teormech/profile/newsfeed/ui/newsfeed_screen.dart';
import 'package:teormech/profile/profile/bloc/show_portfolio_bloc.dart';
import 'package:teormech/profile/profile/ui/edit_profile_pages/edit_profile_base_screen.dart';
import 'package:teormech/profile/profile/ui/notifications/notifications_screen.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/empty_avatar.dart';
import 'package:teormech/widgets/bullet_pointer_label.dart';
import 'package:teormech/widgets/profile_tab_bar.dart';
import 'user_profile_pages/profile_screens.dart';
import 'package:teormech/auth/ui/widgets/gradient_button.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/bloc/drawer_bloc.dart';
import 'package:teormech/profile/subscription/bloc/followers_bloc.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/subscription/model/followers_state.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';
import 'package:teormech/profile/chat/ui/pages/chat_screen.dart';
import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/clipped_avatar.dart';
import 'package:teormech/api_constants.dart';
import 'package:url_launcher/url_launcher.dart';
import 'widgets/profile_row.dart';

class ProfileScreen extends StatelessWidget {
  final bool myProfile;
  final double expandedHeight;

  final ScrollController controller;
  final PageController pageController;

  final VoidCallback newsfeedCallback;
  final VoidCallback portfolioCallback;
  final VoidCallback projectsCallback;

  final ProfilePage profilePage;

  final String lid;
  ProfileScreen(
      {required this.lid,
      required this.myProfile,
      required this.expandedHeight,
      required this.controller,
      required this.pageController,
      required this.profilePage,
      required this.newsfeedCallback,
      required this.portfolioCallback,
      required this.projectsCallback});

  Widget _border(BuildContext context) {
    return Container(
      width: 1,
      margin: EdgeInsets.only(left: 10, right: 10, bottom: 5),
      height: 40,
      decoration: BoxDecoration(border: Border.all(color: Colors.white)),
    );
  }

  SliverAppBar _expandableUserInfoAppBar(
      BuildContext context, UserProfileState state) {
    final avatar =
        ((state.profile.avatar != '') && (state.profile.avatar != null))
            ? UserAvatar.fromNetwork(
                size: kToolbarHeight * 1.9,
                online: myProfile || state.online,
                avatarUrl: ProfileApi.profileAvatar + state.profile.avatar!,
                key: Key(state.profile.avatar!))
            : EmptyAvatar(
                size: kToolbarHeight * 1.9,
                online: myProfile || state.online,
                groupChat: false);
    String? headerStatus;
    if (state.profile.isStudent) {
      headerStatus =
          S.of(context).student_of_a_group + (state.profile.group ?? '');
    }
    if (state.profile.isTeacher) {
      headerStatus = state.profile.teacherStatus;
    }
    if (state.profile.isEmployee) {
      headerStatus = state.profile.employeeStatus;
    }

    return SliverAppBar(
        automaticallyImplyLeading: false,
        expandedHeight: expandedHeight,
        toolbarHeight: kToolbarHeight * 2.5,
        collapsedHeight: kToolbarHeight * 2.8,
        title: Column(
          children: [
            if (headerStatus != null)
              Padding(
                padding: EdgeInsets.only(bottom: 10),
                child: Center(
                  child: Text(
                    headerStatus,
                    style: profileTextStyle,
                    overflow: TextOverflow.ellipsis,
                    softWrap: true,
                    textWidthBasis: TextWidthBasis.longestLine,
                  ),
                ),
              ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                avatar,
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(16, 8, 16, 2),
                        child: Text(
                          (myProfile || state.online)
                              ? S.of(context).online
                              : state.onlineString,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: profileTextStyle,
                          textWidthBasis: TextWidthBasis.longestLine,
                        ),
                      ),
                      SizedBox(
                        height: 40,
                        child: Row(
                          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            if (!myProfile)
                              Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: GradientIconButton(
                                  text: S.of(context).from_profile_to_dialog,
                                  from: GradientColors.leftColorRightButton,
                                  to: GradientColors.rightColorRightButton,
                                  imageIcon: ImageIcon(
                                    AssetImage('images/icons/comm.png'),
                                    color: Colors.white,
                                    size: 16,
                                  ),
                                  onTapCallback: () {
                                    if (state.wasFetched) {
                                      context.read<BottomBarBloc>().go(
                                          ChatScreenWrapper.route(
                                              user: Follower(
                                                  id: state.profile.lid,
                                                  avatar: state.profile.avatar,
                                                  name: state.profile.name,
                                                  surname:
                                                      state.profile.surname,
                                                  lastOnline: state
                                                          .profile.lastOnline ??
                                                      '',
                                                  sex: state.profile.sex)),
                                          hideBottombar: true);
                                    }
                                  },
                                ),
                              )
                            else
                              Padding(
                                padding: const EdgeInsets.only(left: 12.0),
                                child: GradientIconButton(
                                    text: S
                                        .of(context)
                                        .from_profile_to_edit_profile,
                                    from: GradientColors.leftColorRightButton,
                                    to: GradientColors.rightColorRightButton,
                                    large: true,
                                    icon: Icon(
                                      Icons.edit,
                                      color: Colors.white,
                                      size: 16,
                                    ),
                                    onTapCallback: () {
                                      // ignore: close_sinks
                                      final profileBloc =
                                          context.read<UserProfileBloc>();
                                      final portfolioCubit =
                                          context.read<ShowPortfolioCubit>();
                                      context.read<BottomBarBloc>().go(
                                            EditProfileBaseScreen.route(
                                                showPortfolioCubit:
                                                    portfolioCubit,
                                                userProfileBloc: profileBloc),
                                          );
                                    }),
                              ),
                            SizedBox(
                              width: 10,
                            ),
                            if (!myProfile)
                              BlocBuilder<FollowersCubit, FollowersState>(
                                  builder: (context, followersState) {
                                final cond = (followersState.isInFollowings(
                                    uid: state.lid));
                                final loading = (followersState.status ==
                                    FetchingStatus.InProgress);
                                return Stack(
                                  children: [
                                    GradientIconButton(
                                      text: (cond)
                                          ? S.of(context).unfollow_from_profile
                                          : S.of(context).follow_from_profile,
                                      from: GradientColors.leftColorLeftButton,
                                      to: GradientColors.rightColorLeftButton,
                                      imageIcon: const ImageIcon(
                                          AssetImage('images/icons/people.png'),
                                          color: Colors.white,
                                          size: 16),
                                      onTapCallback: () async {
                                        if (myProfile ||
                                            followersState.status !=
                                                FetchingStatus.Loaded) return;

                                        await context
                                            .read<FollowersCubit>()
                                            .follow(id: lid, unsub: cond);
                                        context
                                            .read<UserProfileBloc>()
                                            .fetchFromServer(
                                                loadFromHive: false,
                                                forceReload: true);
                                      },
                                    ),
                                    if (loading)
                                      Positioned.fill(
                                        child: Container(
                                          width: 120,
                                          decoration: BoxDecoration(
                                              color: GradientColors
                                                  .updatingIndicatorColor,
                                              borderRadius:
                                                  BorderRadius.circular(12)),
                                        ),
                                      )
                                  ],
                                );
                              }),
                          ],
                        ),
                      ),
                      Padding(
                          padding: EdgeInsets.only(left: 16, top: 6),
                          child: ConstrainedBox(
                            constraints: const BoxConstraints(
                                maxHeight: 40,
                                minHeight: 40,
                                maxWidth: 300,
                                minWidth: 220),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ProfileColumn(
                                    caption: S.of(context).user_follows_count,
                                    value:
                                        state.profile.followings?.toString() ??
                                            '0'),
                                _border(context),
                                ProfileColumn(
                                    caption: S.of(context).user_followers_count,
                                    value:
                                        state.profile.followers?.toString() ??
                                            '0'),
                                _border(context),
                                ProfileColumn(
                                    caption: S.of(context).likes_count,
                                    value:
                                        state.profile.likes?.toString() ?? '0')
                              ],
                            ),
                          ))
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
        backgroundColor: Colors.transparent,
        flexibleSpace: BackgroundFlexibleSpaceBar(
          collapseMode: CollapseMode.none,
          background: ClipRect(
            child: Container(
              child: Padding(
                  padding: EdgeInsets.only(top: kToolbarHeight * 2.8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (state.profile.email != '')
                        ProfileDataRow(
                          caption: '${S.of(context).user_profile_email}: ',
                          value: state.profile.email,
                          link: 'mailto://${state.profile.email}',
                        ),
                      if ((state.profile.phone != null) &&
                          (state.profile.phone != ''))
                        ProfileDataRow(
                          caption:
                              '${S.of(context).user_profile_phone_number}: ',
                          value: state.profile.phone!,
                          link: 'tel://${state.profile.phone!}',
                        ),
                      if (state.profile.birth != null)
                        ProfileDataRow(
                            caption:
                                '${S.of(context).user_profile_birthdate}: ',
                            value: state.profile.birth!),
                      if ((state.profile.hometown != null) &&
                          (state.profile.hometown != ''))
                        ProfileDataRow(
                          caption: '${S.of(context).user_profile_hometown}: ',
                          value: state.profile.hometown!,
                        ),
                      if (state.education.isNotEmpty)
                        ProfileDataRow(
                          caption: '${S.of(context).user_profile_education}: ',
                          value: state.educationString ?? '',
                        ),
                      if ((state.profile.about != null) &&
                          (state.profile.about != ''))
                        Container(
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.only(left: 16, top: 8, bottom: 8),
                          child: Text(
                              '${S.of(context).user_profile_interests}: ',
                              style: h1TextStyle.copyWith(
                                  fontSize: 14, color: Colors.white)),
                        ),
                      if ((state.profile.about != null) &&
                          (state.profile.about != ''))
                        ConstrainedBox(
                            constraints: const BoxConstraints(maxWidth: 500),
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 16, right: 16),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  state.profile.about!,
                                  style:
                                      profileTextStyle.copyWith(fontSize: 16),
                                ),
                              ),
                            )),
                      ConstrainedBox(
                        constraints: const BoxConstraints(maxWidth: 500),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 16, top: 8, bottom: 8),
                          child: Row(
                            children: [
                              if (state.contacts.isNotEmpty &&
                                  state.contacts.any((element) =>
                                      element.network != SocialNetwork.Empty &&
                                      element.url != ''))
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text('Контакты:',
                                      style: h1TextStyle.copyWith(
                                          fontSize: 14, color: Colors.white)),
                                ),
                              Spacer(),
                              Row(
                                children: state.contacts
                                    .where((e) =>
                                        e.network != SocialNetwork.Empty &&
                                        e.url != '')
                                    .map((contact) => MaterialButton(
                                        minWidth: 22,
                                        padding: EdgeInsets.only(right: 16),
                                        child: Image(
                                            image: AssetImage(contact.res),
                                            width: 22,
                                            height: 22),
                                        onPressed: () async {
                                          if (await canLaunch(contact.fullUrl))
                                            await launch(contact.fullUrl);
                                        }))
                                    .toList(),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  )),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(
                        'images/avtor2.png',
                      ),
                      fit: BoxFit.fitWidth,
                      alignment: Alignment.topCenter)),
            ),
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<UserProfileBloc, UserProfileState>(
        listener: (context, state) {
      if (myProfile)
        context
            .read<DrawerBloc>()
            .setTitle(state.profile.name + '\n' + state.profile.surname);

      if (state.error?.isNotEmpty ?? false) {
        context
            .read<BottomBarBloc>()
            .showError(S.of(context).check_your_network_connection);
      }
    }, builder: (context, state) {
      final tStyle = TextStyle(
          color: state.wasFetched ? Colors.white : Colors.grey,
          fontSize: 18,
          fontWeight: FontWeight.bold);

      return Scaffold(
        appBar: ProfileAppBar(
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Text(
                  state.profile.surname,
                  style: tStyle,
                )
              ]),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    state.profile.name + ' ' + state.profile.patronymic,
                    style: tStyle.copyWith(fontSize: 16),
                  )
                ],
              )
            ],
          ),
          rightmostButton: myProfile
              ? Stack(
                  children: [
                    IconButton(
                      icon: Icon(Icons.notifications_active),
                      color: Colors.white,
                      onPressed: () {
                        context.read<BottomBarBloc>().go(
                            NotificationScreen.route(
                                context.read<UserProfileBloc>()));
                      },
                    ),
                    if (state.notifications?.isNotEmpty ?? false)
                      BulletPointerLabel(
                          label: '${state.notifications!.length}')
                  ],
                )
              : null,
        ),
        body: RefreshIndicator(
            notificationPredicate: (predicate) => predicate.depth == 2,
            displacement: 0,
            onRefresh: () async {
              await Future.wait([
                context
                    .read<UserProfileBloc>()
                    .fetchFromServer(loadFromHive: false, forceReload: true),
                context.read<ShowPortfolioCubit>().load()
              ]);
            },
            child: NestedScrollView(
                controller: controller,
                headerSliverBuilder: (context, _) => [
                      _expandableUserInfoAppBar(context, state),
                      SliverAppBar(
                          automaticallyImplyLeading: false,
                          backgroundColor: Colors.white,
                          pinned: true,
                          flexibleSpace: ProfileTabBar(
                            chosen: profilePage,
                            newsfeedCallback: newsfeedCallback,
                            portfolioCallback: portfolioCallback,
                            projectsCallback: projectsCallback,
                          )),
                    ],
                body: Container(
                  decoration: BoxDecoration(
                      border:
                          Border(top: BorderSide(color: Colors.grey.shade300))),
                  child: PageView.builder(
                    controller: pageController,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, idx) {
                      switch (idx) {
                        case 0:
                          return ProfileNewsScreen();
                        case 1:
                          return ProfilePortfolioScreen(
                            isMyProfile: myProfile,
                          );

                        case 2:
                          if (myProfile)
                            context.read<NewsfeedCubit>().init(
                                NewsfeedPageEnum.Projects,
                                forceReload: true);
                          return ProfileProjectsScreen(
                            uid: lid,
                            myProfile: myProfile,
                          );
                        default:
                          return Container();
                      }
                    },
                    itemCount: 3,
                  ),
                ))),
      );
    });
  }
}

class ProfileColumn extends StatelessWidget {
  final String caption;
  final String value;

  ProfileColumn({required this.caption, required this.value});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          value,
          style: profileTextStyle.copyWith(
              fontSize: 18, fontWeight: FontWeight.bold),
        ),
        Text(
          caption,
          style: profileTextStyle.copyWith(fontSize: 10),
        )
      ],
    );
  }
}
