part of 'notifications.dart';

class EventNotification extends StatelessWidget {
  const EventNotification({Key? key, required this.event}) : super(key: key);
  final EventModel event;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
      child: Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(12)),
        child: ScheduleElement(
            withoutTimeColumn: true,
            isNotification: true,
            rightIcon: SizedBox(
              width: 25,
              height: 25,
              child: Image(
                image: AssetImage('images/icons/uvedom.png'),
              ),
            ),
            event: CalendarEvent(
                id: 1,
                title: event.title,
                type: CalendarEventType.Note,
                body: event.description,
                date: event.date.toLocal().toString(),
                startTime: '15:50',
                object: event.type,
                place: event.location)),
      ),
    );
  }
}
