part of 'notifications.dart';

class ProjectBellNotificationWidget extends StatelessWidget {
  const ProjectBellNotificationWidget(
      {Key? key, this.joinLeaveProject, this.request})
      : super(key: key);

  final ProjectBellNotification? joinLeaveProject;
  final ProjectRequestBellNotification? request;

  @override
  Widget build(BuildContext context) {
    String uid;
    String caption;
    int pid;
    String time;
    if (joinLeaveProject != null) {
      uid = joinLeaveProject!.uid;
      pid = joinLeaveProject!.pid;
      caption = joinLeaveProject!.caption;
      time = joinLeaveProject!.time.format(context);
    } else {
      uid = request!.uid;
      pid = request!.pid;
      time = request!.time.format(context);
      caption = (request!.name ?? '') + ' ' + (request!.surname ?? '');
    }

    return InkWell(
      onTap: () {
        if (joinLeaveProject != null)
          context.read<BottomBarBloc>().go(PageRouteBuilder(
              transitionsBuilder: slideTransition,
              pageBuilder: (context, f2, f3) =>
                  ProfileScreenWrapper.fromLid(lid: joinLeaveProject!.uid)));
        else
          context
              .read<BottomBarBloc>()
              .go(ProjectPage.pidRoute(pid: request!.pid));
      },
      child: BlocProvider<UserProfileBloc>(
        create: (context) =>
            UserProfileBloc.fromLid(lid: uid, loadFromHive: false),
        child: Builder(builder: (context) {
          return BlocBuilder<UserProfileBloc, UserProfileState>(
              builder: (context, state) {
            return Padding(
              padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
              child: Container(
                padding: const EdgeInsets.only(top: 10, bottom: 10),
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: Row(
                  children: [
                    Container(
                      width: 3,
                      height: 60,
                      color: Colors.grey,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: state.profile.avatar?.isEmpty ?? true
                          ? UserAvatar.fromString(
                              avatarString: 'images/logo.png',
                              online: state.online,
                              size: 40,
                            )
                          : UserAvatar.fromNetwork(
                              size: 40,
                              avatarUrl: ProfileApi.profileAvatar +
                                  state.profile.avatar!),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: BlocBuilder<NewsfeedCubit, NewsfeedState>(
                            builder: (context, newsfeedState) {
                          final project = newsfeedState.projects
                              ?.firstWhere((e) => e.id == pid);

                          return RichText(
                            text: TextSpan(
                                text: caption + ' \n',
                                style: TextStyle(color: Colors.black),
                                children: [
                                  TextSpan(
                                    text: joinLeaveProject != null
                                        ? (joinLeaveProject?.leave ?? false)
                                            ? (state.profile.sex == '1')
                                                ? 'покинул проект '
                                                : 'покинула проект '
                                            : (state.profile.sex == '1')
                                                ? 'присоединился к проекту '
                                                : 'присоединилась к проекту '
                                        : (state.profile.sex == '1')
                                            ? 'пригласил Вас в проект '
                                            : 'пригласила Вас в проект ',
                                    style: TextStyle(color: Colors.black),
                                  ),
                                  TextSpan(
                                      text: project?.title,
                                      style: TextStyle(
                                          color: GradientColors.backColor)),
                                  TextSpan(
                                      text: ' в ${time}',
                                      style: TextStyle(
                                          color: Colors.grey, height: 1.5))
                                ]),
                          );
                        }),
                      ),
                    ),
                  ],
                ),
              ),
            );
          });
        }),
      ),
    );
  }
}
