part of 'notifications.dart';

class JobNotification extends StatefulWidget {
  JobNotification({Key? key, required this.job}) : super(key: key);

  final InterestVacancyNotification job;

  @override
  State<JobNotification> createState() => _JobNotificationState();
}

class _JobNotificationState extends State<JobNotification> {
  List<PortfolioTag>? tags;
  final style = h2DrawerGreyText.copyWith(
      color: Colors.black, fontSize: 15, fontWeight: FontWeight.w600);

  @override
  void initState() {
    super.initState();
    fetchTags();
  }

  void fetchTags() async {
    final fetchedTags = await NewsfeedRepository()
        .fetchNewsfeedTags(id: widget.job.job.id, mode: RequestMode.Job);
    if (fetchedTags.isNotEmpty)
      setState(() {
        tags = fetchedTags;
      });
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        context
            .read<BottomBarBloc>()
            .go(JobScreen.route(widget.job.job..tags = tags ?? []));
      },
      child: Padding(
        padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
        child: Container(
          padding:
              const EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 10),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(12)),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Padding(
              padding: const EdgeInsets.all(2.0),
              child: Text(
                'Добавлена новая вакансия:',
                style: style.copyWith(fontWeight: FontWeight.normal),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 2.0, top: 4.0),
              child: Text(
                widget.job.job.position,
                style: style.copyWith(fontWeight: FontWeight.normal),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 2.0, top: 4.0),
              child: Text(widget.job.job.type, style: h2DrawerGreyText),
            ),
            if (tags?.isNotEmpty ?? false)
              TagsContainer(
                tags: [
                  PortfolioTag(tag: '#теги', level: ProfessionalLevel.Caption),
                  ...tags!
                ],
                isCompact: true,
              )
          ]),
        ),
      ),
    );
  }
}
