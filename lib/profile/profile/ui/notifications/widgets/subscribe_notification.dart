part of 'notifications.dart';

class SubscribeNotification extends StatelessWidget {
  const SubscribeNotification({Key? key, required this.notification})
      : super(key: key);

  final FollowerNotification notification;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        context.read<BottomBarBloc>().go(PageRouteBuilder(
            transitionsBuilder: slideTransition,
            pageBuilder: (context, f2, f3) =>
                ProfileScreenWrapper.fromLid(lid: notification.uid)));
      },
      child: BlocProvider<UserProfileBloc>(
        create: (context) =>
            UserProfileBloc.fromLid(lid: notification.uid, loadFromHive: false),
        child: Builder(builder: (context) {
          return BlocBuilder<UserProfileBloc, UserProfileState>(
              builder: (context, state) {
            return Padding(
              padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
              child: Container(
                padding: const EdgeInsets.only(top: 10, bottom: 10),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12)),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: notification.photo.isEmpty
                          ? UserAvatar.fromString(
                              avatarString: 'images/logo.png',
                              online: state.online,
                              size: 40,
                            )
                          : UserAvatar.fromNetwork(
                              size: 40,
                              avatarUrl: ProfileApi.profileAvatar +
                                  notification.photo),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: RichText(
                        text: TextSpan(
                            text: notification.caption + ' \n',
                            style: TextStyle(color: Colors.black),
                            children: [
                              TextSpan(
                                  text: state.profile.sex == '1'
                                      ? 'подписался на вас '
                                      : 'подписалась на вас ',
                                  style: TextStyle(color: Colors.black)),
                              TextSpan(
                                  text:
                                      'в ${notification.time.format(context)}',
                                  style: TextStyle(
                                      color: Colors.grey, height: 1.5))
                            ]),
                      ),
                    ),
                    Spacer(),
                    SizedBox(
                      width: 25,
                      height: 25,
                      child: Padding(
                        padding: const EdgeInsets.only(right: 5.0),
                        child: Image(
                          image: AssetImage('images/icons/dobavit.png'),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            );
          });
        }),
      ),
    );
  }
}
