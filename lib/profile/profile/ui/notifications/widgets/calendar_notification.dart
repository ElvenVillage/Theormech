part of 'notifications.dart';

class CalendarNotificaton extends StatelessWidget {
  const CalendarNotificaton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
      child: Container(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(12)),
        child: ScheduleElement(
            withoutTimeColumn: true,
            rightIcon: SizedBox(
              width: 25,
              height: 25,
              child: Image(
                image: AssetImage('images/icons/uvedom.png'),
              ),
            ),
            event: CalendarEvent(
              id: 1,
              title: 'Дедлайн по сопромату 18:00',
              body: 'Закинуть расчетку на диск',
              type: CalendarEventType.Note,
              date: '2021-05-11',
            )),
      ),
    );
  }
}
