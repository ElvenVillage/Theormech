import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/profile/newsfeed/bloc/newsfeed_cubit.dart';
import 'package:teormech/profile/newsfeed/ui/newsfeed_screen.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/profile/model/bell_model.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';
import 'package:teormech/profile/profile/ui/notifications/widgets/notifications.dart';

import 'package:teormech/profile/ui/widgets/profile_app.bar.dart';
import 'package:teormech/styles/text.dart';
import 'package:teormech/styles/transitions/slide_fade.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({Key? key, required this.bloc}) : super(key: key);

  final UserProfileBloc bloc;

  static PageRouteBuilder route(UserProfileBloc bloc) => PageRouteBuilder(
      pageBuilder: (context, f1, f2) => NotificationScreen(bloc: bloc),
      transitionsBuilder: slideTransition);

  @override
  State<NotificationScreen> createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  @override
  void initState() {
    context
        .read<NewsfeedCubit>()
        .init(NewsfeedPageEnum.Projects, forceReload: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserProfileBloc, UserProfileState>(
        bloc: widget.bloc,
        builder: (context, state) {
          return Scaffold(
              backgroundColor: Color(0xFFf9f9f9),
              appBar: ProfileAppBar(
                leftAlignedTitle: true,
                title: Text('Уведомления', style: appBarTextStyle),
                rightButton: MaterialButton(
                    child: Text(
                      'Прочитать все',
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {}),
              ),
              body: ListView.builder(
                  itemCount: state.notifications?.length ?? 0,
                  itemBuilder: (context, idx) {
                    bool needToShowSpacer = false;
                    final notification = state.notifications![idx];
                    if (idx == 0 || idx == state.notifications!.length - 1)
                      needToShowSpacer = true;

                    if (idx > 0) {
                      final prev = state.notifications![idx - 1];
                      if (notification.date.day != prev.date.day)
                        needToShowSpacer = true;
                    }

                    Widget? child;

                    if (notification is FollowerNotification) {
                      child = SubscribeNotification(
                        notification: notification,
                        key: ValueKey(notification.id),
                      );
                    }
                    if (notification is InterestEventNotification) {
                      child = EventNotification(
                        event: notification.event,
                        key: ValueKey(notification.id),
                      );
                    }
                    if (notification is ProjectRequestBellNotification) {
                      child = ProjectBellNotificationWidget(
                        request: notification,
                      );
                    }
                    if (notification is InterestVacancyNotification) {
                      child = JobNotification(
                        job: notification,
                        key: ValueKey(notification.id),
                      );
                    }
                    if (notification is ProjectBellNotification) {
                      child = ProjectBellNotificationWidget(
                        joinLeaveProject: notification,
                        key: ValueKey(notification.id),
                      );
                    }
                    if (child == null) {
                      return Padding(
                        padding: EdgeInsets.only(left: 25.0, top: 15),
                        child: Text(
                          notification.dateDayFormatted,
                          style: h2DrawerGreyText,
                        ),
                      );
                    }
                    if (needToShowSpacer) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(left: 25.0, top: 15),
                            child: Text(
                              notification.dateDayFormatted,
                              style: h2DrawerGreyText,
                            ),
                          ),
                          child
                        ],
                      );
                    } else {
                      return child;
                    }
                  }));
        });
  }
}
