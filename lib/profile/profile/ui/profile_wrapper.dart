import 'dart:async';
import 'package:flutter/material.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/profile/bloc/show_portfolio_bloc.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/profile/model/user_profile.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';
import 'package:teormech/profile/profile/ui/profile_screen.dart';

class ProfileScreenWrapper extends StatefulWidget {
  final String lid;
  final bool myProfile;
  final ProfilePage startPage;

  ProfileScreenWrapper.myProfile()
      : this.lid = UserProfile.myProfileId,
        startPage = ProfilePage.Newsfeed,
        myProfile = true;
  ProfileScreenWrapper.fromLid(
      {required this.lid, this.startPage = ProfilePage.Newsfeed})
      : this.myProfile = false;

  @override
  _ProfileScreenWrapperState createState() => _ProfileScreenWrapperState();
}

class _ProfileScreenWrapperState extends State<ProfileScreenWrapper>
    with AutomaticKeepAliveClientMixin {
  late UserProfileBloc profileBloc;
  late ShowPortfolioCubit portfolioCubit;

  var scrollController =
      ScrollController(initialScrollOffset: kToolbarHeight * 2.9);
  late final PageController pageController;

  late ProfilePage profilePage;

  var expandedHeight = 60.0;
  var maxOffset = 120.0;
  var wasScrolled = false;
  var wasPortfolioLoaded = false;
  var wasExtendedHeightCalculated = false;

  StreamSubscription? _scrollHeightSubscription;

  @override
  void initState() {
    super.initState();

    if (widget.myProfile) {
      profileBloc = context.read<UserProfileBloc>();
      portfolioCubit = context.read<ShowPortfolioCubit>();
    } else {
      profileBloc = UserProfileBloc.fromLid(
          lid: widget.lid, loadFromHive: true, forceReload: true);
      portfolioCubit =
          ShowPortfolioCubit(uid: widget.myProfile ? null : widget.lid)..load();
    }

    pageController = PageController(initialPage: widget.startPage.index);
    profilePage = widget.startPage;

    _scrollHeightSubscription = profileBloc.stream.listen((state) {
      var m = 60.0;
      if ((state.profile.hometown != null) && (state.profile.hometown != ''))
        m += 7;
      if ((state.profile.education != null) && (state.profile.education != ''))
        m += 7;
      if ((state.profile.about != null) && (state.profile.about != '')) m += 20;
      if (state.profile.phone != '') m += 5;

      if (state.contacts.isNotEmpty) m += 10;

      if (expandedHeight != m * 4)
        setState(() {
          expandedHeight = m * 4;
        });
      wasExtendedHeightCalculated = true;

      if (!wasScrolled && wasExtendedHeightCalculated) {
        WidgetsBinding.instance?.addPostFrameCallback((value) async {
          scrollController.jumpTo(expandedHeight - kToolbarHeight * 2.9);
        });

        wasScrolled = true;
      }
    });

    if (widget.myProfile) {
      profileBloc.fetchFromServer(
        loadFromHive: true,
      );
    }

    pageController.addListener(() {
      final idx = pageController.page?.round() ?? 0;
      final prevIdx = profilePage.index;
      if (prevIdx != idx) {
        var page = ProfilePage.Newsfeed;
        switch (idx) {
          case 1:
            page = ProfilePage.Portfolio;
            break;
          case 2:
            page = ProfilePage.Projects;
            break;
        }
        setState(() {
          profilePage = page;
        });
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    if (widget.myProfile) profileBloc.close();
    _scrollHeightSubscription?.cancel();
    scrollController.dispose();
    pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocListener<BottomBarBloc, BottomBarState>(
      listenWhen: (stateA, stateB) {
        return (stateA.needToUpdateProfile != stateB.needToUpdateProfile);
      },
      listener: (context, state) {
        if (state.needToUpdateProfile) {
          context
              .read<UserProfileBloc>()
              .fetchFromServer(loadFromHive: false, forceReload: true);
          context
              .read<BottomBarBloc>()
              .toggleUpdateProfile(!state.needToUpdateProfile);
        }
        //updateKeepAlive();
      },
      child: MultiBlocProvider(
        providers: [
          BlocProvider<UserProfileBloc>.value(value: profileBloc),
          BlocProvider<ShowPortfolioCubit>.value(
            value: portfolioCubit,
          )
        ],
        child: ProfileScreen(
          lid: widget.lid,
          myProfile: widget.myProfile,
          expandedHeight: expandedHeight,
          controller: scrollController,
          pageController: pageController,
          profilePage: profilePage,
          newsfeedCallback: () {
            setState(() {
              profilePage = ProfilePage.Newsfeed;
            });
            pageController.jumpToPage(0);
          },
          portfolioCallback: () {
            setState(() {
              profilePage = ProfilePage.Portfolio;
            });
            pageController.jumpToPage(1);
            if (!wasPortfolioLoaded) {
              portfolioCubit.load();
              wasPortfolioLoaded = true;
            }
          },
          projectsCallback: () {
            setState(() {
              profilePage = ProfilePage.Newsfeed;
            });
            pageController.jumpToPage(2);
          },
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
