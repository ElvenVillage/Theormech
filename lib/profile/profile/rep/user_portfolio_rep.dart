import 'dart:convert';
import 'package:teormech/profile/profile/model/portfolio_state.dart';
import 'package:dio/dio.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/profile/profile/model/user_profile.dart';
import 'package:teormech/profile/profile/rep/user_profile_rep.dart';

class PortfolioResponse {
  PortfolioState? state;
  final bool isEnded;

  PortfolioResponse({required this.state, required this.isEnded});
}

class UserPortfolioRepository {
  static UserPortfolioRepository? _instance;
  late final Dio _dio;

  Map<String, List<PortfolioTag>> _memCache = {};

  void clearMem() {
    _memCache = {};
  }

  UserPortfolioRepository._internal() {
    _dio = Dio()..options.contentType = Headers.formUrlEncodedContentType;
  }

  factory UserPortfolioRepository() {
    if (_instance == null) {
      _instance = UserPortfolioRepository._internal();
    }
    return _instance!;
  }

  Future<List<String>> filterTags(
      {required String mask, required List<String> tags}) async {
    final data = UserProfileRepository().credentials.toJson();
    if (mask.isNotEmpty) data['name'] = mask;
    final response = <String>[];

    final tagsResponse = await _dio.post(
        mask.isEmpty ? PortfolioApi.allTags : PortfolioApi.searchTag,
        data: data,
        options: Options(responseType: ResponseType.plain));
    final tagsString = tagsResponse.data.toString();
    if (tagsString.contains('false'))
      return const ['\n'];
    else {
      final allTags = jsonDecode(tagsResponse.data.toString());
      for (var tag in allTags) if (!tags.contains(tag)) response.add(tag);
      //response.add('\n');

      return response;
    }
  }

  Stream<PortfolioResponse> fetchUserPortfolio(
      {String? uid, bool forceReload = false}) async* {
    final data = UserProfileRepository().credentials.toJson();
    if (uid != null) data['uid'] = uid;

    final portfolioJson = (await _dio.post(PortfolioApi.showPortfolio,
            data: data, options: Options(responseType: ResponseType.plain)))
        .data
        .toString();

    if (portfolioJson != 'false') {
      try {
        final portfolio = jsonDecode(portfolioJson);
        final languages = jsonDecode(portfolio['lang'] ?? []);
        final workExperience = jsonDecode(portfolio['experience'] ?? '[]');
        var portfolioObject = PortfolioState.empty().copyWith(
            languages: <String>[for (var lang in languages) lang],
            professionalSkills: portfolio['professionals'],
            workExperience: <String>[
              for (var exp in workExperience) exp.trim()
            ]);

        yield PortfolioResponse(state: portfolioObject, isEnded: false);

        final achievements = await fetchUserPortfolioAchievements(uid: uid);
        portfolioObject = portfolioObject.copyWith(achievements: achievements);
        yield PortfolioResponse(state: portfolioObject, isEnded: false);

        final tags = await fetchUserTags(uid: uid, forceReload: forceReload);
        portfolioObject = portfolioObject.copyWith(tags: tags);
        yield PortfolioResponse(state: portfolioObject, isEnded: true);
      } catch (e) {
        yield PortfolioResponse(state: null, isEnded: true);
      }
    } else {
      yield PortfolioResponse(state: null, isEnded: true);
    }
  }

  Future<List<PortfolioTag>> fetchUserTags(
      {String? uid, bool forceReload = false}) async {
    final id = uid ?? UserProfile.myProfileId;
    if (_memCache[id] != null && !forceReload) return _memCache[id]!;

    final tags = <PortfolioTag>[];

    final data = UserProfileRepository().credentials.toJson();
    if (uid != null) data['uid'] = uid;

    try {
      final response = await _dio.post(PortfolioApi.showResumeTags,
          data: data, options: Options(responseType: ResponseType.plain));

      final responseString = response.data.toString();
      if (responseString != 'false') {
        final list = jsonDecode(responseString);

        for (var tag in list) {
          final level = int.parse(tag['level']) - 1;
          tags.add(PortfolioTag(
              tag: tag['name'],
              level: ProfessionalLevel.values[level == -1 ? 1 : level]));
        }
      }

      final userTagsResponse = await _dio.post(PortfolioApi.showResumeUserTags,
          data: data, options: Options(responseType: ResponseType.plain));

      final userTagsResponseString = userTagsResponse.data.toString();
      if (userTagsResponseString != 'false') {
        final userTagsList = jsonDecode(userTagsResponseString);

        for (var tag in userTagsList) {
          final level = int.parse(tag['level']) - 1;
          tags.add(PortfolioTag(
              tag: tag['name'],
              level: ProfessionalLevel.values[level == -1 ? 1 : level],
              userTag: true));
        }
      }
    } catch (e) {
      return const [];
    }
    _memCache[id] = tags;
    return tags;
  }

  Future<List<PortfolioAchievement>> _fetchPortflioAchievementsParts(
      {required bool fetchPublications,
      required Map<String, String> data}) async {
    final publications = <PortfolioAchievement>[];
    final fetchedPublications = await _dio.post(
        fetchPublications
            ? PortfolioApi.showPortfolioPublications
            : PortfolioApi.showPortfolioAchievements,
        data: data,
        options: Options(responseType: ResponseType.plain));

    if (fetchedPublications.data.toString() != 'false') {
      final fetchedPublicationsJson =
          jsonDecode(fetchedPublications.data.toString());
      for (var publication in fetchedPublicationsJson) {
        publications.add(PortfolioAchievement(
            id: int.tryParse(publication['id']) ?? 0,
            isPublication: fetchPublications,
            caption: publication['body'],
            date: (publication['year'] ?? '1970-01-01').substring(0, 10),
            link: publication['link'] ?? '',
            file: '',
            isApproved: publication['approved'] == '1',
            submittedFile: publication['file'] ?? ''));
      }
    }
    return publications;
  }

  Future<List<PortfolioAchievement>> fetchUserPortfolioAchievements(
      {String? uid}) async {
    final data = UserProfileRepository().credentials.toJson();
    data['all'] = 'true';
    if (uid != null) data['uid'] = uid;
    final achievements = await _fetchPortflioAchievementsParts(
        fetchPublications: false, data: data);
    final publications = await _fetchPortflioAchievementsParts(
        fetchPublications: true, data: data);
    return [...achievements, ...publications];
  }

  Future<bool> addResumeInfo({required String key, required String val}) async {
    final data = UserProfileRepository().credentials.toJson();
    data['key'] = key;
    data['val'] = val;

    try {
      final response = await _dio.post(PortfolioApi.addResumeInfo,
          data: data, options: Options(responseType: ResponseType.plain));
      return !response.data.toString().contains('false');
    } catch (e) {
      return false;
    }
  }

  Future<bool> pinTag({required PortfolioTag tag, required bool unpin}) async {
    final data = UserProfileRepository().credentials.toJson();
    data['name'] = tag.tag;
    if (!unpin) data['level'] = (tag.level.index + 1).toString();

    try {
      final response = await _dio.post(
          unpin
              ? tag.userTag
                  ? PortfolioApi.removeUserTag
                  : PortfolioApi.unpinTag
              : tag.userTag
                  ? PortfolioApi.addUserTag
                  : PortfolioApi.pinTag,
          data: data,
          options: Options(responseType: ResponseType.plain));
      return !response.data.toString().contains('false');
    } catch (_) {
      return false;
    }
  }

  Future<bool> deleteAchievement(
    PortfolioAchievement achievement,
  ) async {
    if (achievement.id == null) return false;
    final data = UserProfileRepository().credentials.toJson();
    if (achievement.isPublication)
      data['pid'] = achievement.id.toString();
    else
      data['aid'] = achievement.id.toString();

    final response = await _dio.post(
        achievement.isPublication
            ? PortfolioApi.removePortfolioPublication
            : PortfolioApi.removePortfolioAchievement,
        data: data,
        options: Options(responseType: ResponseType.plain));

    return !response.data.toString().contains('false');
  }

  Future<bool> addAchievement(
    PortfolioAchievement achievement,
  ) async {
    final dio = Dio();
    if (achievement.file.isEmpty)
      dio.options.contentType = Headers.formUrlEncodedContentType;
    final credentials = UserProfileRepository().credentials;

    final Map<String, dynamic> data = {
      'session': credentials.session,
      'token': credentials.token,
      'body': achievement.caption,
      'year': achievement.date,
    };
    if (achievement.link.isNotEmpty) data['link'] = achievement.link;
    if (achievement.file.isNotEmpty)
      data['file'] = await MultipartFile.fromFile(achievement.file);

    final response = await dio.post(
        achievement.isPublication
            ? PortfolioApi.addPortfolioPublication
            : PortfolioApi.addPortfolioAchievement,
        data: FormData.fromMap(data),
        options: Options(responseType: ResponseType.plain));

    return response.data.toString().contains('true');
  }
}
