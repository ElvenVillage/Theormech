import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:teormech/api_constants.dart';
import 'package:teormech/auth/model/auth_model.dart';
import 'package:teormech/profile/profile/model/bell_model.dart';
import 'package:teormech/profile/profile/model/user_profile.dart';

class UserProfileResponse {
  final UserProfile? profile;
  final String? error;

  final DateTime time;

  UserProfileResponse({this.profile, this.error, required this.time});

  UserProfileResponse.profile({required this.profile, required this.time})
      : error = null;
  UserProfileResponse.error({required this.error, required this.time})
      : profile = null;
}

class UserProfileRepository {
  static UserProfileRepository? _instance;
  Map<String, Future<UserProfileResponse>> _memCache = {};
  SessionToken credentials;
  late final Dio _dio;

  UserProfileRepository._internal(this.credentials) {
    _dio = Dio()..options.contentType = Headers.formUrlEncodedContentType;
  }

  factory UserProfileRepository({SessionToken? credentials}) {
    if (credentials != null) {
      _instance = UserProfileRepository._internal(credentials);
    }
    return _instance!;
  }

  void clearMem() {
    _memCache = {};
  }

  Future<List<BellNotification>?> fetchNotifications() async {
    final data = credentials.toJson();
    final response = await _dio.post(BellApi.getBell, data: data);
    if (response.statusCode == 200) {
      List<BellNotification> notifications = [];
      if (response.data.toString() == 'false') return null;
      for (var i in response.data) {
        var a = BellNotification.fromJson(i);
        if (a.id != -1) notifications.add(a);
      }
      notifications.sort(
        (a, b) => b.date.compareTo(a.date),
      );
      return notifications;
    } else
      return null;
  }

  Future<UserProfileResponse> fetchUserInfo(
      {required String lid,
      bool extendLifespan = false,
      bool forceReload = false}) async {
    final time = DateTime.now();
    final cachedResponse = _memCache[lid];
    if (cachedResponse != null) {
      try {
        final response = await cachedResponse;

        final timeDiffer = time.difference(response.time).inSeconds;
        if (!forceReload &&
            timeDiffer > 0 &&
            timeDiffer < (extendLifespan ? 3 * 60 : 60)) {
          print('memcached user ${response.profile?.surname ?? lid}');
          return response;
        }
      } catch (_) {
        print('error, reloading user $lid');
      }
    } else {
      print('loading user $lid ');
    }

    final data = credentials.toJson();
    if (lid != UserProfile.myProfileId) {
      data['uid'] = lid;
    }
    try {
      _memCache[lid] =
          _dio.post(ProfileApi.userInfo, data: data).then((response) async {
        try {
          Map<String, dynamic> json = response.data;

          Map<String, Privacy>? privacySettings;
          Map<String, dynamic>? followerJson;

          if (lid == UserProfile.myProfileId) {
            privacySettings = await fetchPrivacySettings();
            followerJson = await fetchUserSubscribers();
          } else {
            followerJson = await fetchUserSubscribers(uid: lid);
          }

          final profile = UserProfile.fromJson(json, lid,
              followersJson: followerJson, privacySettings: privacySettings);

          return UserProfileResponse.profile(profile: profile, time: time);
        } on DioError catch (e) {
          if (e.type == DioErrorType.receiveTimeout)
            return UserProfileResponse.error(
                error: 'Не удалось установить соединение с сервером',
                time: time);
          return UserProfileResponse.error(
              error: 'Ошибка авторизации', time: time);
        }
      });
    } catch (e) {
      return UserProfileResponse.error(error: 'Ошибка сервера', time: time);
    }
    return _memCache[lid]!;
  }

  Future<Map<String, dynamic>> fetchUserSubscribers({String? uid}) async {
    final data = credentials.toJson();
    if (uid != null) {
      data['uid'] = uid;
    }

    try {
      final response = await _dio.post(ProfileApi.subsUser, data: data);
      final subscribers = response.data;

      return subscribers;
    } catch (e) {
      return {"followers": 0, "followings": 0, "likes": 123};
    }
  }

  Future<bool> setLimits({required String limits, String? uid}) async {
    final data = credentials.toJson();
    data['limit'] = limits;
    if (uid != null) data['uid'] = uid;

    try {
      final response = await _dio.post(
          uid == null ? ProfileApi.setLimit : ProfileApi.setCustomLimits,
          data: data,
          options: Options(responseType: ResponseType.plain));
      return !response.data.toString().contains('false');
    } on DioError {
      return false;
    }
  }

  Future<int> getLimits() async {
    final data = credentials.toJson();

    try {
      final response = await _dio.post(ChatApi.getMyLimits,
          data: data, options: Options(responseType: ResponseType.plain));
      final rawString = response.data.toString();
      if (rawString == 'false' || rawString == 'null' || rawString == 'true')
        return 0;

      return int.parse(rawString.replaceAll('"', ''));
    } catch (e) {
      return 0;
    }
  }

  Future<Map<String, Privacy>> fetchPrivacySettings() async {
    try {
      final response = await _dio.post(ProfileApi.fetchPrivacy,
          data: {'session': credentials.session, 'token': credentials.token},
          options: Options(responseType: ResponseType.plain));
      final responseJson = response.data.toString();
      if (responseJson.trim() == 'false') {
        return {};
      }
      final json = jsonDecode(responseJson);

      var privacy = <String, Privacy>{};

      for (final entry in json.entries) {
        privacy[entry.key] = (entry.value == '0')
            ? Privacy.Private
            : entry.value == '1'
                ? Privacy.Subscribers
                : Privacy.Public;
      }
      return privacy;
    } on DioError {
      return {};
    } catch (e) {
      return {};
    }
  }

  Future<bool> updatePrivacySettings(
      {required String key, required Privacy value}) async {
    try {
      var privacy = 0;
      if (value == Privacy.Subscribers) privacy = 1;
      if (value == Privacy.Public) privacy = 2;
      final data = credentials.toJson();
      data['key'] = key;
      data['val'] = privacy.toString();
      final response = await _dio.post(ProfileApi.updatePrivacy,
          data: data, options: Options(responseType: ResponseType.plain));
      return response.statusCode == 200;
    } catch (e) {
      return false;
    }
  }

  Future<bool> followUser({required String uid, bool unsub = false}) async {
    try {
      final data = credentials.toJson();
      data['fid'] = uid;
      if (unsub) {
        data['unsub'] = 'true';
      }

      final response = await _dio.post(ProfileApi.followUser,
          data: data, options: Options(responseType: ResponseType.plain));
      return response.statusCode == 200;
    } on DioError {
      return false;
    } catch (e) {
      return false;
    }
  }
}
