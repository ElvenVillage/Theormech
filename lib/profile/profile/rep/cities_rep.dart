import 'dart:convert';

import 'package:dio/dio.dart';

class CitiesRep {
  static const _apiUrl =
      'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address';

  static const _apiToken = '62b63c932c75bc9e4e02b334ac733c54fa56ce22';

  static Future<List<String>> findCityByMask({required String filter}) async {
    var data = {
      "query": filter,
      "from_bound": {"value": "city"},
      "to_bound": {"value": "city"},
      "locations": [
        {"country": "Беларусь"},
        {"country": "Россия"},
        {"country": "Казахстан"}
      ]
    };
    try {
      var response = await Dio().post(_apiUrl,
          options: Options(
            contentType: 'application/json;charset=UTF-8',
            responseType: ResponseType.plain,
            headers: {
              'Authorization': 'Token ' + _apiToken,
              'Charset': 'utf-8'
            },
          ),
          data: jsonEncode(data));
      var apiString = response.data.toString();
      var apiData = jsonDecode(apiString);

      var suggestions = apiData['suggestions'];
      var values = <String>[];

      for (var i in suggestions) {
        values.add(i['value']);
      }
      return values;
    } catch (e) {
      return [];
    }
  }
}
