import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/ui/widgets/choice_container.dart';
import 'package:teormech/generated/l10n.dart';
import 'package:teormech/profile/subscription/bloc/followers_bloc.dart';
import 'package:teormech/profile/subscription/model/followers_state.dart';
import 'package:teormech/styles/text.dart';

class HeadmanAppBar extends StatelessWidget {
  const HeadmanAppBar({
    Key? key,
    required this.controller,
    required this.sendCallback,
    required this.attachFilesCallback,
  }) : super(key: key);

  final TextEditingController controller;
  final VoidCallback sendCallback;

  final VoidCallback attachFilesCallback;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      decoration: const BoxDecoration(
          border: BorderDirectional(
              bottom: BorderSide(width: 1, color: Colors.grey))),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Stack(
          children: [
            TextField(
              controller: controller,
              maxLines: null,
              decoration: InputDecoration(
                  hintText: 'Сообщение',
                  hintStyle: h2DrawerGreyText,
                  fillColor: Colors.white,
                  contentPadding:
                      EdgeInsets.only(left: 40.0, right: 70, top: 25),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12))),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: IconButton(
                icon: Icon(Icons.arrow_forward_rounded),
                color: Colors.black,
                onPressed: sendCallback,
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: IconButton(
                icon: ImageIcon(AssetImage('images/icons/skr.png')),
                color: Colors.grey,
                onPressed: attachFilesCallback,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class SearchAppBar extends StatelessWidget {
  const SearchAppBar({
    Key? key,
    required this.controller,
    required this.button,
  }) : super(key: key);

  final TextEditingController controller;

  final Widget button;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(9),
      decoration: const BoxDecoration(
          border: BorderDirectional(
              bottom: BorderSide(width: 1, color: Colors.grey))),
      child: Row(
        children: [
          Expanded(
            flex: 3,
            child: TextField(
              controller: controller,
              decoration: InputDecoration(
                  contentPadding:
                      const EdgeInsets.fromLTRB(20.0, 20, 20.0, 0.0),
                  hintText: 'Поиск',
                  hintStyle: h2DrawerGreyText,
                  suffixIcon: const Icon(Icons.search),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(32.0)),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(32.0))),
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          Expanded(flex: 1, child: button),
        ],
      ),
    );
  }
}

class SelectAppBar extends StatelessWidget {
  const SelectAppBar({
    Key? key,
    required this.status,
    required this.controller,
  }) : super(key: key);

  final FollowerScreenState status;

  final PageController controller;

  static final style = ChoiceContainerStyle(
    elevation: 5,
    selectedColor: Colors.black,
    weight: FontWeight.normal,
  );

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 4, right: 4, top: 10),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        ChoiceContainer(
          text: S.of(context).user_follows_count,
          chosen: status == FollowerScreenState.Followings,
          style: style,
          onClickCallback: () {
            BlocProvider.of<FollowersCubit>(context).selectFollowings();
            controller.jumpToPage(0);
          },
        ),
        ChoiceContainer(
          text: S.of(context).followings_list,
          chosen: status == FollowerScreenState.Followers,
          style: style,
          onClickCallback: () {
            BlocProvider.of<FollowersCubit>(context).selectFollowers();
            controller.jumpToPage(1);
          },
        ),
        ChoiceContainer(
          text: S.of(context).recommendations,
          chosen: status == FollowerScreenState.Recommendations,
          style: style,
          onClickCallback: () {
            BlocProvider.of<FollowersCubit>(context).selectRecommendations();
            controller.jumpToPage(2);
          },
        )
      ]),
    );
  }
}
