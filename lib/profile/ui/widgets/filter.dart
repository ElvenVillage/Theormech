import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/subscription/bloc/followers_bloc.dart';
import 'package:teormech/profile/subscription/model/followers_state.dart';
import 'package:teormech/styles/text.dart';

class FilterWidget extends StatelessWidget {
  FilterWidget();

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        var controller = context
            .read<BottomBarBloc>()
            .scaffold
            .currentState
            ?.showBottomSheet(
                (context) => Container(
                      decoration: BoxDecoration(
                          color: Colors.grey.shade100,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(40),
                              topRight: Radius.circular(40))),
                      height: MediaQuery.of(context).size.height / 6,
                      child: BlocBuilder<FollowersCubit, FollowersState>(
                          builder: (context, state) {
                        return GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () {
                            context.read<BottomBarBloc>().nullController();
                          },
                          child: Column(
                            children: [
                              Container(
                                height: 20,
                                alignment: Alignment.centerRight,
                                padding: EdgeInsets.only(right: 15, top: 5),
                                child: Icon(Icons.close),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: 16,
                                ),
                                child: Row(
                                  children: [
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width *
                                          2 /
                                          3,
                                      child: RichText(
                                        text: TextSpan(
                                            text: 'Только пользователи ',
                                            style: h2DrawerGreyText,
                                            children: [
                                              TextSpan(
                                                  text: 'в сети',
                                                  style:
                                                      h2DrawerGreyText.copyWith(
                                                          color: Colors.blue))
                                            ]),
                                      ),
                                    ),
                                    Checkbox(
                                        value: state.filterByOnline,
                                        onChanged: (val) {
                                          context
                                              .read<FollowersCubit>()
                                              .filterByOnline(val ?? false);
                                        }),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 16),
                                child: Row(
                                  children: [
                                    Container(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                    2 /
                                                    3 -
                                                75,
                                        child: Text(
                                          'Пол',
                                          style: h2DrawerGreyText,
                                        )),
                                    Row(
                                      children: [
                                        Text(
                                          'М',
                                          style: h2DrawerGreyText,
                                        ),
                                        Checkbox(
                                            value:
                                                state.filteredGender == '1' &&
                                                    state.filterByGender,
                                            onChanged: (val) {
                                              context
                                                  .read<FollowersCubit>()
                                                  .filterByGender('1');
                                            }),
                                        Text(
                                          'Ж',
                                          style: h2DrawerGreyText,
                                        ),
                                        Checkbox(
                                            value:
                                                state.filteredGender == '0' &&
                                                    state.filterByGender,
                                            onChanged: (val) {
                                              context
                                                  .read<FollowersCubit>()
                                                  .filterByGender('0');
                                            }),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        );
                      }),
                    ),
                backgroundColor: Colors.transparent);
        context.read<BottomBarBloc>().setController(controller);
      },
      child: Row(
        children: [
          Container(
            padding: const EdgeInsets.only(left: 1, right: 8),
            height: 50,
            child: Center(
              child: Text(
                'Фильтр',
                style: h2DrawerGreyText,
              ),
            ),
          ),
          ImageIcon(
            AssetImage('images/icons/filter.png'),
            color: Colors.grey,
          )
        ],
      ),
    );
  }
}
