import 'package:flutter/material.dart';
import 'package:teormech/styles/text.dart';

class DrawerListPoint extends StatelessWidget {
  final String label;
  final String? image;
  final VoidCallback? callback;
  DrawerListPoint({required this.label, this.image, this.callback});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, top: 10),
      child: InkWell(
        onTap: callback,
        child: Row(
          children: [
            if (image != null)
              ImageIcon(
                AssetImage(image!),
                size: 32,
                color: Colors.grey.shade600,
              ),
            Padding(
              padding: EdgeInsets.only(left: 15),
              child: Text(label, style: h2DrawerGreyText),
            )
          ],
        ),
      ),
    );
  }
}
