import 'package:flutter/material.dart';
import 'package:teormech/styles/colors.dart';

class LikeFooter extends StatelessWidget {
  const LikeFooter(
      {Key? key,
      this.likes,
      this.likeCallback,
      required this.liked,
      this.repostCallback})
      : super(key: key);

  final int? likes;
  final bool liked;
  final VoidCallback? likeCallback;
  final VoidCallback? repostCallback;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 9.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text('${likes ?? "21"}',
              style: TextStyle(
                  color: Colors.grey.shade600, fontWeight: FontWeight.w500)),
          IconButton(
              icon: ImageIcon(AssetImage(liked
                  ? 'images/icons/full_like.png'
                  : 'images/icons/like.png')),
              color: GradientColors.pink,
              onPressed: likeCallback),
          Text('3',
              style: TextStyle(
                  color: Colors.grey.shade600, fontWeight: FontWeight.w500)),
          IconButton(
              icon: ImageIcon(AssetImage('images/icons/comments.png')),
              onPressed: () {}),
          Text('1',
              style: TextStyle(
                  color: Colors.grey.shade600, fontWeight: FontWeight.w500)),
          IconButton(
              icon: ImageIcon(AssetImage('images/icons/share.png')),
              onPressed: repostCallback),
        ],
      ),
    );
  }
}
