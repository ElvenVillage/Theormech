import 'package:flutter/material.dart';
import 'package:teormech/profile/profile/model/portfolio_state.dart';
import 'package:teormech/styles/text.dart';

typedef TagTapCallback = void Function(String tag);

class Tag extends StatelessWidget {
  final Key? key;
  final PortfolioTag tag;
  final bool isPrimary;
  final bool isCompact;
  final TagTapCallback? tapCallback;

  static final _beginnerColor = Color(0xFF05aeff);
  static final _advancedColor = Color(0xFF0b239a);
  static final _professionalColor = Color(0xFF001354);

  static final Color _initialGreyColor = Color(0xFFdedede);

  Color get borderColor {
    switch (tag.level) {
      case ProfessionalLevel.Newsfeed:
        return Colors.grey;
      default:
        return Colors.transparent;
    }
  }

  static Color mapColor(ProfessionalLevel level) {
    switch (level) {
      case ProfessionalLevel.Beginner:
        return _beginnerColor;

      case ProfessionalLevel.Advanced:
        return _advancedColor;
      case ProfessionalLevel.Professional:
        return _professionalColor;

      case ProfessionalLevel.Initial:
        return _initialGreyColor;
      case ProfessionalLevel.Service:
      case ProfessionalLevel.Caption:
      case ProfessionalLevel.Newsfeed:
        return Colors.transparent;
    }
  }

  Color get color => mapColor(tag.level);
  Tag(
      {required this.tag,
      this.key,
      this.isPrimary = false,
      this.isCompact = false,
      this.tapCallback})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      height: isPrimary
          ? null
          : isCompact
              ? 30
              : 40,
      padding: tag.level == ProfessionalLevel.Caption
          ? const EdgeInsets.fromLTRB(0, 6, 5, 6)
          : const EdgeInsets.symmetric(horizontal: 16, vertical: 6),
      child: isPrimary
          ? Text(tag.tag,
              style: h2TextStyle.copyWith(
                  fontWeight: FontWeight.bold,
                  fontSize: 28,
                  color: tag.level == ProfessionalLevel.Initial
                      ? Colors.black
                      : Colors.white))
          : Padding(
              padding: const EdgeInsets.only(top: 1.0),
              child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  if (tapCallback != null) tapCallback!(tag.tag);
                },
                child: Text(
                  tag.tag,
                  softWrap: false,
                  style: tag.level == ProfessionalLevel.Caption
                      ? TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.grey)
                      : color == Colors.transparent
                          ? !isCompact
                              ? h2TextStyle
                              : h2TextStyle.copyWith(fontSize: 10.5)
                          : isCompact
                              ? h2TextStyle.copyWith(
                                  color: Colors.white, fontSize: 10.5)
                              : h2TextStyle.copyWith(color: Colors.white),
                ),
              ),
            ),
      decoration: BoxDecoration(
          color: color,
          border: Border.all(color: borderColor),
          borderRadius: BorderRadius.circular(24)),
    );
  }
}
