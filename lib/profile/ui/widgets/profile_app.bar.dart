import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';

class ProfileAppBar extends StatelessWidget implements PreferredSizeWidget {
  final Widget title;
  final Widget? overrideLeftButton;

  final Widget? rightmostButton;
  final Widget? rightButton;

  final bool leftAlignedTitle;

  static const height = 64.0;
  ProfileAppBar(
      {required this.title,
      this.overrideLeftButton,
      this.rightmostButton,
      this.leftAlignedTitle = false,
      this.rightButton});
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final collapsed = rightmostButton == null && rightButton == null;
    return BlocBuilder<BottomBarBloc, BottomBarState>(
        builder: (context, bottomBarState) {
      return Stack(children: [
        Image(
            image: AssetImage('images/avtor1.png'),
            width: double.infinity,
            height: double.infinity,
            fit: BoxFit.fill),
        Row(
          mainAxisAlignment: collapsed
              ? MainAxisAlignment.start
              : MainAxisAlignment.spaceAround,
          children: [
            Flexible(
              child: (overrideLeftButton != null)
                  ? overrideLeftButton!
                  : bottomBarState.canPop
                      ? IconButton(
                          icon: Icon(Icons.arrow_back, color: Colors.white),
                          onPressed: () {
                            BlocProvider.of<BottomBarBloc>(context).pop();
                          })
                      : IconButton(
                          icon: Icon(
                            Icons.menu,
                            size: 32,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            BlocProvider.of<BottomBarBloc>(context)
                                .scaffold
                                .currentState
                                ?.openDrawer();
                          },
                        ),
              flex: 1,
            ),
            if (!leftAlignedTitle) Spacer(),
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Align(child: title, alignment: Alignment.centerLeft),
            ),
            Spacer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                if (rightButton != null)
                  SizedBox(
                    width: width / 10,
                    child: rightButton,
                  ),
                if (rightmostButton != null)
                  SizedBox(
                    width: width / 10,
                    child: rightmostButton,
                  )
              ],
            )
          ],
        ),
      ]);
    });
  }

  @override
  Size get preferredSize => Size.fromHeight(height);
}
