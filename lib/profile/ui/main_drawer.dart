import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/bloc/auth_bloc.dart';
import 'package:teormech/generated/l10n.dart';
import 'package:teormech/profile/about/about_screen.dart';
import 'package:teormech/profile/about/bloc/cache_manager_bloc.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/bloc/drawer_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_list_page_cubit.dart';
import 'package:teormech/profile/profile/bloc/show_portfolio_bloc.dart';
import 'package:teormech/profile/profile/ui/edit_profile_pages/portfolio_screen.dart';
import 'package:teormech/styles/text.dart';
import 'widgets/drawer_list_point.dart';

class MainDrawer extends Drawer {
  final ShowPortfolioCubit showPortfolioCubit;

  MainDrawer({required this.showPortfolioCubit});

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: Column(
      children: [
        SizedBox(
          height: 100,
          child: DrawerHeader(
            margin: EdgeInsets.zero,
            padding: EdgeInsets.zero,
            child: Stack(
              children: [
                Image(
                  image: AssetImage('images/roles.png'),
                  width: double.infinity,
                  height: 100,
                  fit: BoxFit.fitWidth,
                ),
                Positioned.fill(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      IconButton(
                        icon: Icon(
                          Icons.menu,
                          color: Colors.white,
                          size: 32,
                        ),
                        onPressed: () {
                          Scaffold.of(context).openEndDrawer();
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16.0),
                        child: BlocBuilder<DrawerBloc, String>(
                            builder: (context, state) {
                          return Text(
                            state,
                            style: h1TextStyle.copyWith(color: Colors.white),
                          );
                        }),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        DrawerListPoint(
          label: S.of(context).profile,
          image: 'images/icons/drawer/profile.png',
          callback: () {
            context.read<BottomBarBloc>().setTab(
                  BottomBars.Home,
                );
            Scaffold.of(context).openEndDrawer();
          },
        ),
        DrawerListPoint(
          label: S.of(context).portfolio,
          image: 'images/icons/drawer/portfolio.png',
          callback: () {
            Scaffold.of(context).openEndDrawer();
            Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(
                builder: (context) => PortfolioEditScreen(
                      showPortfolioCubit: showPortfolioCubit,
                    )));
          },
        ),
        DrawerListPoint(
          label: S.of(context).subscriptions,
          image: 'images/icons/drawer/subscribes.png',
          callback: () {
            context.read<BottomBarBloc>().setTab(
                  BottomBars.Subscriptions,
                );
            Scaffold.of(context).openEndDrawer();
          },
        ),
        DrawerListPoint(
          label: S.of(context).chats,
          image: 'images/icons/drawer/chats.png',
          callback: () {
            context.read<BottomBarBloc>().setTab(BottomBars.Chat);
            try {
              context
                  .read<ChatListPageCubit>()
                  .selectPage(ChatListPage.Chats, jumpToPage: true);
            } catch (_) {}

            Scaffold.of(context).openEndDrawer();
          },
        ),
        DrawerListPoint(
          label: S.of(context).schedule,
          image: 'images/icons/schedule.png',
          callback: () {
            context.read<BottomBarBloc>().setTab(
                  BottomBars.Schedule,
                );

            Scaffold.of(context).openEndDrawer();
          },
        ),
        SizedBox(
          height: 10,
        ),
        Spacer(),
        Container(
          decoration:
              BoxDecoration(border: Border.all(width: 0.5, color: Colors.grey)),
        ),
        SizedBox(
          height: 10,
        ),
        DrawerListPoint(
          label: S.of(context).about_screen_title,
          callback: () {
            context.read<BottomBarBloc>().go(
                  MaterialPageRoute(builder: (context) => AboutScreen()),
                );
            Scaffold.of(context).openEndDrawer();
          },
        ),
        SizedBox(
          height: 10,
        ),
        DrawerListPoint(
          label: S.of(context).logout,
          callback: () {
            context.read<AuthBloc>().add(LogoutEvent());
            context.read<CacheManagerBloc>().clearAll();
          },
        ),
        SizedBox(height: 30)
      ],
    ));
  }
}
