import 'package:bloc/bloc.dart';

class DrawerBloc extends Cubit<String> {
  DrawerBloc(String initialState) : super(initialState);

  void setTitle(String title) {
    emit(title);
  }
}
