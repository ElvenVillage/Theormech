import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

enum BottomBars { Subscriptions, Schedule, Home, Chat, Newsfeed }

class BottomBarState {
  final BottomBars tab;
  final List<GlobalKey<NavigatorState>> keys;
  final List<List<bool>> bottomBarsVisibilityList;
  final PageController controller;
  final String error;

  final bool needToUpdateProfile;

  bool get canPop => keys[tab.index].currentState?.canPop() ?? false;

  bool get isBottomBarVisible {
    if (bottomBarsVisibilityList[tab.index].isEmpty) {
      bottomBarsVisibilityList[tab.index].add(true);
    }
    return bottomBarsVisibilityList[tab.index].last;
  }

  BottomBarState({
    required this.tab,
    required this.keys,
    required this.controller,
    required this.bottomBarsVisibilityList,
    this.needToUpdateProfile = false,
    this.error = '',
  });

  BottomBarState copyWith(
      {BottomBars? tab,
      bool? bottomSheetOpen,
      String? error,
      bool? needToUpdateProfile}) {
    return BottomBarState(
      tab: tab ?? this.tab,
      keys: keys,
      error: error ?? '',
      needToUpdateProfile: needToUpdateProfile ?? false,
      controller: controller,
      bottomBarsVisibilityList: bottomBarsVisibilityList,
    );
  }
}

class BottomBarBloc extends Cubit<BottomBarState> {
  final GlobalKey<ScaffoldState> scaffold = GlobalKey();
  BottomBarBloc(BottomBarState initialState) : super(initialState);

  void showError(String error) {
    emit(state.copyWith(error: error));
    emit(state.copyWith(error: ''));
  }

  void toggleUpdateProfile(bool val) {
    emit(state.copyWith(needToUpdateProfile: val));
  }

  void setTab(BottomBars tab, {bool pop = false}) {
    nullController();
    if (tab == state.tab || pop) {
      state.keys[tab.index].currentState?.popUntil((route) => route.isFirst);
      emit(state.copyWith());
      state.bottomBarsVisibilityList[tab.index] = [true];
    } else {
      state.controller.jumpToPage(tab.index);

      emit(state.copyWith(
        tab: tab,
      ));
    }
  }

  PersistentBottomSheetController? _controller;
  void setController(PersistentBottomSheetController? c) {
    _controller = c;
  }

  void go(Route route,
      {bool? hideBottombar, BottomBars? tab, bool replace = false}) {
    if (tab != null) setTab(tab);
    nullController();
    if (!replace) {
      state.keys[state.tab.index].currentState?.push(route);
      state.bottomBarsVisibilityList[state.tab.index]
          .add(!(hideBottombar ?? false));
    } else {
      state.keys[state.tab.index].currentState?.pushReplacement(route);
      state.bottomBarsVisibilityList[state.tab.index].last =
          (!(hideBottombar ?? false));
    }
    emit(state.copyWith(
      tab: state.tab,
    ));
  }

  void nullController() {
    if (_controller != null) {
      _controller!.close();
      _controller = null;
    }
  }

  bool pop() {
    nullController();
    if (state.keys[state.tab.index].currentState?.canPop() ?? false) {
      state.keys[state.tab.index].currentState?.pop();
      state.bottomBarsVisibilityList[state.tab.index].removeLast();
      //var cond = state.keys[state.tab.index].currentState?.canPop() ?? true;
      emit(state.copyWith());
      return false;
    }
    return true;
  }

  @override
  Future<void> close() async {
    nullController();

    return super.close();
  }
}
