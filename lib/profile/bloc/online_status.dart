abstract class OnlineStatus {
  static String _dd(int val) {
    if (val < 10)
      return '0' + val.toString();
    else
      return val.toString();
  }

  static String formatDate(DateTime now, String date) {
    final chatDate = DateTime.tryParse(date);
    if (chatDate != null) {
      if (now.day != chatDate.day) {
        return chatDate.hour.toString() + ':' + chatDate.minute.toString();
      } else
        return chatDate.month.toString();
    } else
      return '';
  }

  static String onlineString(
      {required DateTime? lastOnline,
      required String sex,
      required DateTime now}) {
    if (lastOnline == null) {
      if (sex == '1')
        return 'Не был в сети';
      else
        return 'Не была в сети';
    }
    var cond = (sex == '1') ? 'Был в сети ' : 'Была в сети ';
    var diffCommon = lastOnline.difference(now);
    var diffFirst = (-1) * lastOnline.difference(now).inMinutes;
    if (diffFirst < 5) return 'В сети';
    var inDays = (-1) * diffCommon.inDays;

    var res = '';

    if (now.day - lastOnline.day == 1) {
      return cond +
          'вчера в ' +
          _dd(lastOnline.hour) +
          ':' +
          _dd(lastOnline.minute);
    }

    if (inDays >= 1 && inDays <= 6) {
      return cond +
          _dd(lastOnline.day) +
          '.' +
          _dd(lastOnline.month) +
          ' в ' +
          _dd(lastOnline.hour) +
          ':' +
          _dd(lastOnline.minute);
    }

    if (inDays > 6 && inDays <= 28) {
      return cond + 'больше недели назад';
    }

    if (inDays > 28 && inDays <= 31) {
      return cond + 'месяц назад';
    }
    if (inDays > 31 && inDays <= 350) {
      return cond + 'больше месяца назад';
    }
    if (inDays > 350 && inDays <= 375) {
      return cond + 'год назад';
    }
    if (inDays > 375) {
      return cond + 'больше года назад';
    }
    var diff = (-1) * diffCommon.inMinutes;
    if ((diff > 5) && (diff <= 10)) res = '5 минут назад';
    if ((diff > 10) && (diff <= 15)) res = '10 минут назад';
    if ((diff > 15) && (diff <= 20)) res = '15 минут назад';
    if ((diff > 20) && (diff <= 30)) res = '20 минут назад';
    if ((diff > 30) && (diff <= 40)) res = 'полчаса назад';
    if ((diff > 50) && (diff <= 70)) res = 'час назад';
    if (diff > 70) {
      res = 'в ' + _dd(lastOnline.hour) + ':' + _dd(lastOnline.minute);
    }
    return cond + res;
  }
}
