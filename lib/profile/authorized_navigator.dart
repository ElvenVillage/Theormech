import 'dart:async';
import 'dart:convert';
import 'package:edge_alert/edge_alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/bloc/auth_bloc.dart';
import 'package:teormech/auth/model/auth_model.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_list_bloc.dart';
import 'package:teormech/profile/bloc/drawer_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_list_page_cubit.dart';
import 'package:teormech/profile/chat/bloc/saved_messages_bloc.dart';
import 'package:teormech/profile/newsfeed/bloc/newsfeed_cubit.dart';
import 'package:teormech/profile/newsfeed/rep/newsfeed_repository.dart';
import 'package:teormech/profile/newsfeed/ui/pages/events/event_screen.dart';
import 'package:teormech/profile/profile/bloc/show_portfolio_bloc.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/scheduler/bloc/schedule_bloc.dart';
import 'package:teormech/profile/subscription/bloc/followers_bloc.dart';
import 'package:teormech/profile/chat/ui/pages/chats_screen.dart';
import 'package:teormech/profile/ui/main_drawer.dart';
import 'package:teormech/profile/newsfeed/ui/newsfeed_screen.dart';
import 'package:teormech/profile/profile/ui/profile_wrapper.dart';
import 'package:teormech/profile/scheduler/schedule_screen.dart';
import 'package:teormech/profile/subscription/ui/subscriptions_screen.dart';
import 'package:teormech/widgets/persistent_bottom_nav_bar.dart';

class AuthorizedPage extends StatefulWidget {
  AuthorizedPage({Key? key}) : super(key: key);
  @override
  _AuthorizedPageState createState() => _AuthorizedPageState();
}

class _AuthorizedPageState extends State<AuthorizedPage> {
  final _chatListPageCubit = ChatListPageCubit.initial();
  late final BottomBarBloc _bottomBarBloc;

  late final UserProfileBloc _profileBloc;
  late final ShowPortfolioCubit _portfolioCubit;
  late final ChatListBloc _chatListBloc;
  late final NewsfeedCubit _newsfeedCubit;

  late final AuthBloc authBloc;
  late final SessionToken credentials;

  StreamSubscription<AuthState>? _navigatorSubscription;

  @override
  void initState() {
    super.initState();

    authBloc = context.read<AuthBloc>();

    _bottomBarBloc = BottomBarBloc(
      BottomBarState(
        bottomBarsVisibilityList: [
          for (var i = 0; i < 5; i++) [true],
        ],
        tab: BottomBars.Home,
        controller: PageController(initialPage: BottomBars.Home.index),
        keys: [
          GlobalKey<NavigatorState>(),
          GlobalKey<NavigatorState>(),
          GlobalKey<NavigatorState>(),
          GlobalKey<NavigatorState>(),
          GlobalKey<NavigatorState>()
        ],
      ),
    );

    _profileBloc = UserProfileBloc.myProfile(headmanCallback: (bool isHeadman) {
      authBloc.add(HeadmanEvent(isHeadman));
    }, teacherCallback: () {
      authBloc.add(TeacherEvent());
    });

    _profileBloc.fetchFromServer(
      loadFromHive: true,
    );

    _portfolioCubit = ShowPortfolioCubit();

    _chatListBloc = ChatListBloc(
      clearMetadataCallback: () {
        authBloc.add(ClearMetadataEvent());
      },
      pageCubit: _chatListPageCubit,
      bottomBarBloc: _bottomBarBloc,
    )..load(init: true);

    _newsfeedCubit = NewsfeedCubit();

    _navigatorSubscription = authBloc.stream.listen((state) {
      if (state is Authorized) {
        if (state.navigationToken?.type == TokenType.JoinChatToken ||
            state.initialMessage != null) {
          _chatListBloc.authBlocListener(state);
        }

        if (state.navigationToken?.type == TokenType.EventToken) {
          _bottomBarBloc.go(EventScreen.route(
              eventId: int.tryParse(state.navigationToken!.token) ?? 0));
          Future.delayed(
              Duration.zero, () => authBloc.add(ClearMetadataEvent()));
        }

        if (state.navigationToken?.type == TokenType.QRCodeToken) {
          _handleQR(state.navigationToken!);
        }
      }
    });
  }

  Future<void> _handleQR(Token qr) async {
    final confirmation = NewsfeedRepository().confirmQR(qr: qr.token);
    confirmation.then((String response) {
      try {
        if (response != 'false') {
          final body = jsonDecode(response);
          final id = int.tryParse(body['id']);
          final attempt = int.tryParse(body['try']);
          final wasRegisteredBefore = attempt != null && attempt > 0;
          final success = id != null && !wasRegisteredBefore;

          _bottomBarBloc.go(
              EventScreen.route(
                  eventId: id!,
                  registrationSuccess: success,
                  wasRegisteredBefore: wasRegisteredBefore),
              replace: true);
        } else {
          print('b');
        }
      } catch (_) {
        print('a');
      }
    });

    Future.delayed(Duration.zero, () => authBloc.add(ClearMetadataEvent()));
  }

  @override
  void dispose() {
    _chatListPageCubit.close();
    _profileBloc.close();
    _bottomBarBloc.close();
    _chatListBloc.close();
    _newsfeedCubit.close();

    _navigatorSubscription?.cancel();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(builder: (context, authState) {
      if (authState is Authorized) {
        return MultiBlocProvider(
          providers: [
            BlocProvider<NewsfeedCubit>.value(
              value: _newsfeedCubit,
            ),
            BlocProvider<ChatListBloc>.value(
              value: _chatListBloc,
            ),
            BlocProvider<ChatListPageCubit>.value(
              value: _chatListPageCubit,
            ),
            BlocProvider<BottomBarBloc>.value(value: _bottomBarBloc)
          ],
          child: Builder(builder: (context) {
            return MultiBlocProvider(
              providers: [
                BlocProvider<FollowersCubit>(
                    create: (_) =>
                        FollowersCubit(bottomBarBloc: _bottomBarBloc)),
                BlocProvider<SchedulerCubit>(
                  create: (_) => SchedulerCubit(teacher: null),
                ),
                BlocProvider<SavedMessagesBloc>(
                    lazy: false,
                    create: (context) => SavedMessagesBloc(
                          chatListPageCubit: _chatListPageCubit,
                          chatListBloc: context.read<ChatListBloc>(),
                        )),
                BlocProvider<UserProfileBloc>.value(value: _profileBloc),
                BlocProvider<ShowPortfolioCubit>.value(value: _portfolioCubit),
                BlocProvider<DrawerBloc>(create: (context) => DrawerBloc(''))
              ],
              child: BlocListener<BottomBarBloc, BottomBarState>(
                listener: (context, state) {
                  if (state.error.isNotEmpty)
                    EdgeAlert.show(context,
                        title: 'Ошибка',
                        icon: Icons.error_outline_rounded,
                        backgroundColor: Colors.redAccent,
                        duration: EdgeAlert.LENGTH_SHORT,
                        description: state.error,
                        gravity: EdgeAlert.BOTTOM);
                },
                child: PersistentBottomNavigationBar(
                  scaffold: _bottomBarBloc.scaffold,
                  drawer: MainDrawer(showPortfolioCubit: _portfolioCubit),
                  items: [
                    AssetImage('images/icons/people.png'),
                    AssetImage('images/icons/schedule.png'),
                    AssetImage('images/icons/main.png'),
                    AssetImage('images/icons/comm.png'),
                    AssetImage('images/icons/news.png'),
                  ],
                  pages: [
                    SubscriptionsScreen(),
                    ScheduleScreen(teacher: null),
                    ProfileScreenWrapper.myProfile(),
                    ChatsScreen(),
                    NewsfeedScreen(),
                  ],
                ),
              ),
            );
          }),
        );
      } else {
        return const SizedBox();
      }
    });
  }
}
