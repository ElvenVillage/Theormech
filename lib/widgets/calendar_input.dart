import 'package:flutter/material.dart';
import 'package:teormech/auth/ui/widgets/register_text_field.dart';
import 'package:teormech/styles/colors.dart';

typedef CalendarCallback = void Function(DateTime? date);

class CalendarInput extends StatelessWidget {
  const CalendarInput(
      {Key? key, required this.handleDate, required this.controller})
      : super(key: key);

  final CalendarCallback handleDate;
  final TextEditingController controller;

  Future<DateTime?> _handleCalendarClick(BuildContext context) async {
    final date = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2015),
        lastDate: DateTime(2030));
    return date;
  }

  void _onClick(BuildContext context) async {
    final date = await _handleCalendarClick(context);
    handleDate(date);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => _onClick(context),
      child: SizedBox(
        height: 40,
        child: RegisterTextField(
            readOnly: true,
            suffix: IconButton(
              icon: Icon(
                Icons.calendar_today,
                size: 12,
                color: GradientColors.updatingIndicatorColor,
              ),
              onPressed: () => _onClick(context),
            ),
            controller: controller),
      ),
    );
  }
}
