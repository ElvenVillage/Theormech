import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/bloc/reg_cubit.dart';
import 'package:teormech/auth/model/reg_model.dart';
import 'package:teormech/generated/l10n.dart';

class RadioBar extends StatelessWidget {
  final bool overrideReadOnly;
  final bool needControl;
  final RegistrationData state;
  final RegState regState;
  final double rightPadding;

  const RadioBar(
      {Key? key,
      required this.overrideReadOnly,
      required this.needControl,
      required this.state,
      this.rightPadding = 0,
      required this.regState})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Text(S.of(context).select_gender),
        SizedBox(
          child: Row(
            children: [
              SizedBox(
                width: 15,
              ),
              Text(S.of(context).female),
              Radio(
                value: '0',
                groupValue: state.sex,
                onChanged: (val) {
                  if (!((regState.readOnly || overrideReadOnly) &&
                      !needControl))
                    context.read<RegistrationDataCubit>().updateSex('0');
                },
                toggleable: false,
              ),
              Text(S.of(context).male),
              Radio(
                value: '1',
                groupValue: state.sex,
                onChanged: (val) {
                  if (!((regState.readOnly || overrideReadOnly) &&
                      !needControl))
                    context.read<RegistrationDataCubit>().updateSex('1');
                },
                toggleable: false,
              ),
            ],
          ),
        ),
        if (rightPadding != 0)
          SizedBox(
            width: rightPadding,
          )
      ],
    );
  }
}
