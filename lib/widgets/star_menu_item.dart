import 'package:flutter/material.dart';
import 'package:teormech/styles/colors.dart';

Widget first(double width) {
  return DecoratedBox(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12), topRight: Radius.circular(12)),
      color: Colors.white,
    ),
    child: SizedBox(
      height: 10,
      width: width,
    ),
  );
}

Widget last(double width) {
  return DecoratedBox(
    decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(12), bottomRight: Radius.circular(12)),
        color: Colors.white),
    child: SizedBox(
      height: 10,
      width: width,
    ),
  );
}

Widget generateMenuItem(
    {required String title,
    required String icon,
    required double width,
    VoidCallback? onTap}) {
  return Container(
    width: width,
    padding: const EdgeInsets.all(8),
    color: Colors.white,
    child: Row(children: [
      Container(
        margin: const EdgeInsets.symmetric(horizontal: 4),
        width: 20,
        height: 20,
        child: ImageIcon(
          AssetImage(icon),
          color: GradientColors.pink,
        ),
      ),
      Padding(
        padding: const EdgeInsets.only(left: 5.0),
        child: Text(title, style: const TextStyle()),
      ),
    ]),
  );
}
