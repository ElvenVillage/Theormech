import 'package:dropdown_search/dropdown_search.dart';
import 'package:edge_alert/edge_alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/bloc/reg_cubit.dart';
import 'package:teormech/auth/model/reg_model.dart';
import 'package:teormech/auth/ui/regscreens/education_screen.dart';
import 'package:teormech/auth/ui/widgets/calendar_input.dart';
import 'package:teormech/auth/ui/widgets/contacts_container.dart';
import 'package:teormech/auth/ui/widgets/gradient_button.dart';
import 'package:teormech/auth/ui/widgets/input_container.dart';
import 'package:teormech/generated/l10n.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/profile/bloc/user_profile_bloc.dart';
import 'package:teormech/profile/subscription/model/followers_state.dart';
import 'package:teormech/profile/profile/model/user_profile.dart';
import 'package:teormech/profile/profile/rep/cities_rep.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/styles/transitions/slide_fade.dart';
import 'package:teormech/widgets/radio_bar.dart';

class AdditionalUserInfo extends StatelessWidget {
  const AdditionalUserInfo(
      {Key? key,
      required this.size,
      required this.needControl,
      required this.regState,
      required this.editProfile,
      this.prevPage,
      this.userProfile})
      : super(key: key);

  final Size size;
  final VoidCallback? prevPage;
  final UserProfile? userProfile;
  final bool needControl;
  final RegState regState;
  final bool editProfile;

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return BlocBuilder<RegistrationDataCubit, RegistrationData>(
        builder: (context, state) {
      return Stack(
        children: [
          Column(
            children: [
              if (!editProfile)
                SizedBox(
                  height: 20,
                ),
              if (editProfile) ...[
                RadioBar(
                    overrideReadOnly: false,
                    needControl: needControl,
                    state: state,
                    rightPadding: size.width / 12,
                    regState: regState),
                SizedBox(
                  height: 10,
                ),
              ],
              CalendarInput(
                controller: state.dateOfBirth.controller,
                size: size,
                firstDate: DateTime(1970),
                lastDate: DateTime(2022),
                error: state.dateOfBirth.error,
                readOnly: regState.readOnly && !needControl,
              ),
              SizedBox(
                height: 10,
              ),
              InputContainer(
                controller: state.city.controller,
                maxLines: 1,
                caption: 'Родной город',
                rightPadding: size.width / 9,
                error: (state.city.error) ? 'Заполните поле' : null,
                readOnly: regState.readOnly && !needControl,
                dropdownSearch: DropdownSearch<String>(
                    mode: Mode.BOTTOM_SHEET,
                    itemAsString: (str) => ((str.length < 10) ||
                            (str != state.city.controller.text))
                        ? str
                        : '...' + str.substring(str.length - 10),
                    selectedItem: state.city.controller.text,
                    searchBoxController: state.city.controller,
                    onChanged: (city) {
                      state.city.controller.text = city;
                    },
                    showAsSuffixIcons: true,
                    isFilteredOnline: true,
                    emptyBuilder: (ctx, str) =>
                        Center(child: Text(S.of(context).no_data_found)),
                    dropdownSearchDecoration: InputDecoration(
                      isDense: true,
                      isCollapsed: (!state.city.error),
                      contentPadding:
                          const EdgeInsets.fromLTRB(10.0, 12.0, 0, 8.0),
                      filled: true,
                      fillColor: Colors.white,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0),
                      ),
                    ),
                    showSearchBox: true,
                    onFind: (String filter) async {
                      return CitiesRep.findCityByMask(filter: filter);
                    },
                    enabled: !regState.readOnly || needControl),
              ),
              SizedBox(
                height: 10,
              ),
              TextButton(
                  onPressed: () {
                    try {
                      final regDataCubit =
                          context.read<RegistrationDataCubit>();
                      final regCubit = context.read<RegistrationCubit>();
                      context.read<BottomBarBloc>().go(
                            PageRouteBuilder(
                                pageBuilder: (context, f1, f2) =>
                                    EducationScreen(
                                      cubit: regDataCubit,
                                      regCubit: regCubit,
                                      userCubit:
                                          context.read<UserProfileBloc>(),
                                      appBar: !needControl,
                                    ),
                                transitionsBuilder: slideTransition),
                          );
                    } catch (e) {
                      Navigator.of(context, rootNavigator: true)
                          .push(PageRouteBuilder(
                              pageBuilder: (context, f1, f2) => EducationScreen(
                                    cubit:
                                        context.read<RegistrationDataCubit>(),
                                    regCubit: context.read<RegistrationCubit>(),
                                    userCubit: context.read<UserProfileBloc>(),
                                    appBar: !needControl,
                                  ),
                              transitionsBuilder: slideTransition));
                    }
                  },
                  child: Text(S.of(context).user_profile_education)),
              SizedBox(
                height: 10,
              ),
              if (editProfile) ...[
                InputContainer(
                    caption: 'Телефон',
                    type: TextInputType.phone,
                    controller: state.phone.controller,
                    hint: '+7',
                    maxLines: 1,
                    rightPadding: size.width / 9,
                    readOnly: regState.readOnly && !needControl,
                    error: (state.phone.error) ? 'Заполните поле' : null),
                SizedBox(
                  height: 8,
                ),
              ],
              if ((state.isTeacher) && (editProfile)) ...[
                InputContainer(
                  controller: state.teacherStatus.controller,
                  caption: 'Преподаватель',
                  type: TextInputType.text,
                  maxLines: 1,
                  rightPadding: size.width / 9,
                  readOnly: regState.readOnly && !needControl,
                ),
                SizedBox(
                  height: 8,
                )
              ],
              if ((state.isEmployee) && (editProfile)) ...[
                InputContainer(
                  controller: state.employeeStatus.controller,
                  caption: 'Сотрудник',
                  maxLines: 1,
                  type: TextInputType.text,
                  rightPadding: size.width / 9,
                  readOnly: regState.readOnly && !needControl,
                ),
                SizedBox(
                  height: 7,
                )
              ],
              InputContainer(
                controller: state.personalEmail.controller,
                type: TextInputType.emailAddress,
                caption: 'Личная почта',
                maxLines: 1,
                error: (state.personalEmail.error) ? '' : null,
                readOnly: regState.readOnly && !needControl,
                rightPadding: size.width / 9,
              ),
              SizedBox(
                height: 5,
              ),
              ContactsContainer(
                readOnly: regState.readOnly && !needControl,
              ),
              SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(S.of(context).user_profile_interests),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 11 / 18 + 3,
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width / 9,
                    vertical: 10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(32),
                  color: Colors.white,
                ),
                child: TextField(
                    minLines: 5,
                    maxLines: 12,
                    readOnly: regState.readOnly && !needControl,
                    controller: state.about.controller,
                    keyboardType: TextInputType.multiline,
                    decoration: InputDecoration(
                      isCollapsed: true,
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 12.0, 12.0, 10.0),
                      fillColor: Colors.white,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0),
                      ),
                    )),
              ),
              if (needControl)
                SizedBox(
                  height: 10,
                ),
              if (needControl)
                Row(children: [
                  Spacer(),
                  GradientMaterialButton(
                    from: GradientColors.leftColorLeftButton,
                    to: GradientColors.rightColorLeftButton,
                    text: 'Назад',
                    onTapCallback: prevPage,
                  ),
                  Spacer(),
                  GradientMaterialButton(
                    onTapCallback: () {
                      FocusScope.of(context).requestFocus(FocusNode());

                      if (!context
                          .read<RegistrationDataCubit>()
                          .state
                          .password
                          .valid) {
                        EdgeAlert.show(context,
                            title: 'Ошибка',
                            duration: EdgeAlert.LENGTH_SHORT,
                            icon: Icons.error_outline_rounded,
                            backgroundColor: Colors.redAccent,
                            description: 'Проверьте введенные данные',
                            gravity: EdgeAlert.BOTTOM);
                      } else
                        BlocProvider.of<RegistrationCubit>(context).register(
                            context.read<RegistrationDataCubit>().state);
                    },
                    text: "Далее",
                    from: GradientColors.leftColorRightButton,
                    to: GradientColors.rightColorRightButton,
                  ),
                  Spacer()
                ]),
            ],
          ),
          if (regState.registerUploadInfoStatus == FetchingStatus.InProgress)
            Positioned.fromRect(
              child: CircularProgressIndicator(),
              rect: Rect.fromLTWH(width / 3, width / 3, width / 3, width / 3),
            )
        ],
      );
    });
  }
}
