import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:teormech/styles/colors.dart';

typedef SetDateTimeCallback = void Function(DateTime);
typedef SetTimeOfDayCallback = void Function(TimeOfDay);

class CalendarPickerRow extends StatelessWidget {
  const CalendarPickerRow(
      {Key? key,
      required this.dateTimeCallback,
      required this.selectedDateTime,
      required this.timeOfDayCallback,
      required this.selectedTimeOfDay,
      required this.locale})
      : super(key: key);

  final SetDateTimeCallback dateTimeCallback;
  final SetTimeOfDayCallback timeOfDayCallback;

  final DateTime? selectedDateTime;
  final TimeOfDay? selectedTimeOfDay;

  final String? locale;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        MaterialButton(
            padding: EdgeInsets.zero,
            onPressed: () async {
              final date = await showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime(2020),
                  lastDate: DateTime(2040));
              if (date != null) {
                dateTimeCallback(date);
              }
            },
            child: Row(
              children: [
                Icon(
                  Icons.calendar_today,
                  color: GradientColors.pink,
                ),
                Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: selectedDateTime == null
                        ? Text('Дата')
                        : Text(DateFormat.yMMMd(locale)
                            .format(selectedDateTime!))),
              ],
            )),
        MaterialButton(
            onPressed: () async {
              final time = await showTimePicker(
                context: context,
                initialTime: TimeOfDay.now(),
              );
              if (time != null) {
                timeOfDayCallback(time);
              }
            },
            child: Row(
              children: [
                Icon(
                  Icons.timer,
                  color: GradientColors.pink,
                ),
                Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: selectedTimeOfDay == null
                        ? Text('Время')
                        : Text(selectedTimeOfDay!.format(context)))
              ],
            )),
      ],
    );
  }
}
