import 'package:edge_alert/edge_alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/bloc/reg_cubit.dart';
import 'package:teormech/auth/model/reg_model.dart';
import 'package:teormech/auth/ui/widgets/gradient_button.dart';
import 'package:teormech/auth/ui/widgets/input_container.dart';
import 'package:teormech/styles/colors.dart';

class PasswordUserInfo extends StatelessWidget {
  const PasswordUserInfo({
    Key? key,
    this.nextPage,
    this.prevPage,
    required this.state,
    required this.needControl,
    this.overrideReadOnly = false,
  }) : super(key: key);

  final Function? nextPage;
  final Function? prevPage;
  final RegistrationData state;
  final bool overrideReadOnly;
  final bool needControl;

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return BlocBuilder<RegistrationCubit, RegState>(
        builder: (context, regState) => Column(
              children: [
                if (needControl)
                  SizedBox(
                    height: 20,
                  ),
                if (!needControl) ...[
                  InputContainer(
                    controller: state.oldPassword.controller,
                    caption: 'Пароль',
                    type: TextInputType.visiblePassword,
                    rightPadding: width / 10,
                    readOnly: overrideReadOnly && !needControl,
                  ),
                  SizedBox(
                    height: 10,
                  )
                ],
                InputContainer(
                  caption: 'Новый пароль',
                  type: TextInputType.visiblePassword,
                  controller: state.password.controller,
                  rightPadding: width / 10,
                  readOnly: overrideReadOnly && !needControl,
                  error: (state.password.error) ? 'Заполните поле' : null,
                ),
                SizedBox(
                  height: 10,
                ),
                InputContainer(
                  caption: 'Повторите пароль',
                  type: TextInputType.visiblePassword,
                  rightPadding: width / 10,
                  controller: state.confirmPassword.controller,
                  readOnly: overrideReadOnly && !needControl,
                  error: (state.password.controller.text !=
                          state.confirmPassword.controller.text)
                      ? 'Заполните поле'
                      : null,
                ),
                if (needControl)
                  SizedBox(
                    height: 100,
                  ),
                if (needControl)
                  Row(children: [
                    Spacer(),
                    GradientMaterialButton(
                      onTapCallback: () {
                        FocusScope.of(context).requestFocus(FocusNode());

                        if (nextPage != null) prevPage!();
                      },
                      text: "Назад",
                      from: GradientColors.leftColorLeftButton,
                      to: GradientColors.rightColorLeftButton,
                    ),
                    Spacer(),
                    GradientMaterialButton(
                      onTapCallback: () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        context.read<RegistrationDataCubit>().notifyListeners();

                        if (!state.password.valid) {
                          EdgeAlert.show(context,
                              title: 'Ошибка',
                              duration: EdgeAlert.LENGTH_SHORT,
                              icon: Icons.error_outline_rounded,
                              backgroundColor: Colors.redAccent,
                              description: 'Проверьте введенные данные',
                              gravity: EdgeAlert.BOTTOM);
                        } else if (nextPage != null) nextPage!();
                      },
                      text: "Далее",
                      from: GradientColors.leftColorRightButton,
                      to: GradientColors.rightColorRightButton,
                    ),
                    Spacer()
                  ]),
              ],
            ));
  }
}
