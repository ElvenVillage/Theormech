import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_list_bloc.dart';
import 'package:teormech/profile/chat/bloc/saved_messages_bloc.dart';
import 'package:teormech/profile/chat/model/chat_list_state.dart';
import 'package:teormech/widgets/bullet_pointer_label.dart';

class PersistentBottomNavigationBar extends StatelessWidget {
  final List<AssetImage> items;
  final Drawer? drawer;
  final int count;
  final List<Widget> pages;
  final GlobalKey<ScaffoldState> scaffold;
  final int idOfChatsIcon;

  static const SELECTED_ICON_SIZE = 55.0;
  static const ICON_SIZE = 40.0;

  static const ICON_OPACITY = 0.85;

  PersistentBottomNavigationBar(
      {required this.items,
      this.drawer,
      required this.pages,
      this.idOfChatsIcon = 3,
      required this.scaffold})
      : count = items.length;

  Widget _buildBottomNavigationBar(BuildContext context, {bool? isVisible}) {
    final size = MediaQuery.of(context).size.width;
    return Offstage(
      offstage: !(isVisible ?? true),
      child: Container(
        decoration: BoxDecoration(color: Color(0xFF0a239a)),
        child: BlocBuilder<BottomBarBloc, BottomBarState>(
            builder: (context, state) {
          return Row(
              children: items
                  .asMap()
                  .map((int i, AssetImage item) {
                    final selected = state.tab.index == i;
                    return MapEntry(
                        i,
                        GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          child: Stack(
                            children: [
                              Opacity(
                                opacity: selected ? 1.0 : ICON_OPACITY,
                                child: Container(
                                  width: size / 5,
                                  child: ImageIcon(
                                    item,
                                    color: Colors.white,
                                    size: selected
                                        ? SELECTED_ICON_SIZE
                                        : ICON_SIZE,
                                  ),
                                ),
                              ),
                              if (i == idOfChatsIcon)
                                Positioned(
                                  right: 5,
                                  top: 5,
                                  child: BlocBuilder<SavedMessagesBloc,
                                      SavedMessagesState>(
                                    builder: (context, savedChatsState) {
                                      return BlocBuilder<ChatListBloc,
                                              ChatListState>(
                                          builder: (context, chatListState) {
                                        final count = chatListState
                                                .countOfUnreadMessages +
                                            savedChatsState.unreadMailings;
                                        if (count != 0) {
                                          final label = count < 10
                                              ? ' ' + count.toString() + ' '
                                              : count.toString();
                                          return BulletPointerLabel(
                                              label: label);
                                        } else
                                          return SizedBox.shrink();
                                      });
                                    },
                                  ),
                                ),
                            ],
                          ),
                          onTap: () {
                            context
                                .read<BottomBarBloc>()
                                .setTab(BottomBars.values[i]);
                          },
                        ));
                  })
                  .values
                  .toList());
        }),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(child:
        BlocBuilder<BottomBarBloc, BottomBarState>(builder: (context, state) {
      return WillPopScope(
        onWillPop: () async {
          return context.read<BottomBarBloc>().pop();
        },
        child: Scaffold(
            bottomNavigationBar: _buildBottomNavigationBar(context,
                isVisible: state.isBottomBarVisible),
            drawer: drawer,
            backgroundColor: Colors.transparent,
            key: scaffold,
            body: Stack(
              children: [
                OverflowBox(
                  minHeight: 600,
                  child: Image(
                    image: AssetImage('images/roles2.png'),
                    height: double.infinity,
                    fit: BoxFit.fill,
                  ),
                ),
                PageView(
                    controller: state.controller,
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      for (var i = 0; i < count; i++)
                        Navigator(
                          key: state.keys[i],
                          onGenerateRoute: (s) => CupertinoPageRoute(
                              builder: (context) => pages[i]),
                        ),
                    ]),
              ],
            )

            //_buildBody(state)
            ),
      );
    }));
  }
}
