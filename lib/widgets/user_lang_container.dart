import 'package:flutter/material.dart';

class LanguageContainer extends StatelessWidget {
  const LanguageContainer(this.lang, {Key? key}) : super(key: key);

  final String lang;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        ImageIcon(
          AssetImage('images/icons/punson.png'),
          color: Colors.pinkAccent,
          size: 20,
        ),
        Expanded(
            child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 3.0),
          child: Text(lang),
        ))
      ],
    );
  }
}
