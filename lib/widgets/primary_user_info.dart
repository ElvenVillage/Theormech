import 'package:edge_alert/edge_alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/bloc/reg_cubit.dart';
import 'package:teormech/auth/model/reg_model.dart';
import 'package:teormech/auth/ui/widgets/gradient_button.dart';
import 'package:teormech/auth/ui/widgets/input_container.dart';
import 'package:teormech/styles/colors.dart';
import 'package:teormech/widgets/radio_bar.dart';

class PrimaryUserInfo extends StatelessWidget {
  const PrimaryUserInfo(
      {Key? key,
      this.nextPage,
      required this.state,
      required this.editProfile,
      required this.needControl,
      this.needStringValidation = true,
      this.overrideReadOnly = false,
      this.needValidateEmail = true})
      : super(key: key);

  final Function? nextPage;
  final RegistrationData state;
  final bool needControl;
  final bool overrideReadOnly;
  final bool needStringValidation;
  final bool needValidateEmail;
  final bool editProfile;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<RegistrationCubit, RegState>(
        listener: (context, regState) {
      if (regState.regStatus == RegStatus.Error) {
        EdgeAlert.show(context,
            title: 'Ошибка',
            duration: EdgeAlert.LENGTH_SHORT,
            icon: Icons.error_outline_rounded,
            backgroundColor: Colors.redAccent,
            description: 'Пользователь уже зарегистрирован',
            gravity: EdgeAlert.BOTTOM);
      }
    }, builder: (context, regState) {
      return Column(
        children: [
          SizedBox(
            height: 20,
          ),
          InputContainer(
              caption: 'Фамилия',
              type: TextInputType.name,
              controller: state.surname.controller,
              readOnly: regState.readOnly || overrideReadOnly,
              error: (needStringValidation
                  ? (state.surname.error)
                      ? 'Заполните поле'
                      : null
                  : null)),
          SizedBox(
            height: 10,
          ),
          InputContainer(
              caption: 'Имя',
              type: TextInputType.name,
              controller: state.firstName.controller,
              maxLines: 1,
              readOnly: regState.readOnly || overrideReadOnly,
              error: (needStringValidation
                  ? (state.firstName.error)
                      ? 'Заполните поле'
                      : null
                  : null)),
          SizedBox(
            height: 10,
          ),
          if (!editProfile) ...[
            RadioBar(
                overrideReadOnly: overrideReadOnly,
                needControl: needControl,
                state: state,
                regState: regState),
            SizedBox(
              height: 10,
            ),
          ],
          InputContainer(
              caption: 'Отчество',
              type: TextInputType.name,
              controller: state.patronymic.controller,
              maxLines: 1,
              readOnly: regState.readOnly || overrideReadOnly,
              error: (needStringValidation
                  ? (state.patronymic.error)
                      ? 'Заполните поле'
                      : null
                  : null)),
          SizedBox(
            height: 10,
          ),
          if (state.isStudent) ...[
            InputContainer(
                caption: 'Номер группы',
                controller: state.groupID.controller,
                type: TextInputType.datetime,
                maxLines: 1,
                hint: '363010X/XXXXX',
                readOnly: regState.readOnly || overrideReadOnly,
                error: (state.groupID.error) ? 'Заполните поле' : null),
            SizedBox(
              height: 10,
            )
          ],
          if ((state.isTeacher) && (!editProfile)) ...[
            InputContainer(
              controller: state.teacherStatus.controller,
              caption: 'Преподаватель',
              maxLines: 1,
              type: TextInputType.text,
              readOnly: (regState.readOnly || overrideReadOnly) && !needControl,
            ),
            SizedBox(
              height: 10,
            )
          ],
          if ((state.isEmployee) && (!editProfile)) ...[
            InputContainer(
              controller: state.employeeStatus.controller,
              caption: 'Сотрудник',
              maxLines: 1,
              type: TextInputType.text,
              readOnly: (regState.readOnly || overrideReadOnly) && !needControl,
            ),
            SizedBox(
              height: 10,
            )
          ],
          InputContainer(
              caption: 'Эл. почта',
              controller: state.email.controller,
              type: TextInputType.emailAddress,
              maxLines: 1,
              hint: 'examp.le@edu.spbstu.ru',
              readOnly: regState.readOnly || overrideReadOnly,
              error: needValidateEmail
                  ? (state.email.error)
                      ? 'Заполните поле'
                      : null
                  : null),
          SizedBox(
            height: 10,
          ),
          if (!editProfile)
            InputContainer(
                caption: 'Телефон',
                type: TextInputType.phone,
                controller: state.phone.controller,
                hint: '+7',
                readOnly:
                    (regState.readOnly || overrideReadOnly) && !needControl,
                error: (state.phone.error) ? 'Заполните поле' : null),
          SizedBox(
            height: 25,
          ),
          if (needControl)
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                  text: 'Все поля ',
                  style: TextStyle(color: Colors.grey),
                  children: [
                    TextSpan(
                        text: 'обязательны ',
                        style: TextStyle(
                          decoration: TextDecoration.underline,
                        )),
                    TextSpan(text: 'для заполнения')
                  ]),
            ),
          if (needControl)
            SizedBox(
              height: 35,
            ),
          if (needControl)
            Row(children: [
              Spacer(),
              GradientMaterialButton(
                onTapCallback: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                  context.read<RegistrationDataCubit>().notifyListeners();

                  if (!state.firstPageAccepted) {
                    EdgeAlert.show(context,
                        title: 'Ошибка',
                        duration: EdgeAlert.LENGTH_SHORT,
                        icon: Icons.error_outline_rounded,
                        backgroundColor: Colors.redAccent,
                        description: 'Проверьте введенные данные',
                        gravity: EdgeAlert.BOTTOM);
                  } else if (nextPage != null) nextPage!();
                },
                text: "Далее",
                from: GradientColors.leftColorRightButton,
                to: GradientColors.rightColorRightButton,
              ),
              Spacer()
            ]),
        ],
      );
    });
  }
}
