import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/profile/chat/bloc/chat_bloc.dart';
import 'package:teormech/profile/chat/model/chats_state.dart';

class LoadingProgressIndicator extends StatelessWidget {
  const LoadingProgressIndicator(
      {Key? key, required this.needToShow, required this.listenToChatBloc})
      : super(key: key);

  final bool needToShow;
  final bool listenToChatBloc;

  Widget _buildListener(double? progress) {
    return Center(
      child: SizedBox(
          child: CircularProgressIndicator(
            value: progress,
          ),
          width: 100,
          height: 100),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (!listenToChatBloc)
      return Offstage(offstage: !needToShow, child: _buildListener(null));
    else
      return Offstage(
          offstage: !needToShow,
          child: BlocBuilder<ChatBloc, ChatState>(
            builder: (context, state) => _buildListener(state.loadProgress),
          ));
  }
}
