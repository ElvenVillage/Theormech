import 'dart:io';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:teormech/profile/bloc/bottom_bar_bloc.dart';
import 'package:teormech/profile/chat/ui/widgets/chat_gallery.dart';
import 'package:teormech/styles/colors.dart';

typedef DeleteCallback = void Function(int idx);

class ImagesStaggeredGridView extends StatelessWidget {
  const ImagesStaggeredGridView(
      {Key? key,
      this.enableDeleting = false,
      this.deleteCallback,
      required this.fileImages,
      required this.images})
      : super(key: key);

  final bool enableDeleting;
  final DeleteCallback? deleteCallback;
  final List<String> images;
  final bool fileImages;

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(
          maxHeight: images.length == 3
              ? 480
              : images.length == 4
                  ? 480
                  : images.length >= 5
                      ? 480
                      : 240,
          maxWidth: MediaQuery.of(context).size.width * 0.9),
      child: StaggeredGridView.countBuilder(
          crossAxisCount: 12,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: images.length < 5 ? images.length : 5,
          itemBuilder: (context, idx) => Builder(builder: (context) {
                return Stack(
                  key: ValueKey(idx),
                  children: [
                    Positioned.fill(
                      child: GestureDetector(
                        onTap: () {
                          if (!enableDeleting) {
                            context.read<BottomBarBloc>().go(
                                MaterialPageRoute(
                                    builder: (context) => ChatGallery(
                                          files: images,
                                          idx: idx,
                                          mode: PhotoGallery.Post,
                                          isFromChat: false,
                                        )),
                                hideBottombar: true);
                          }
                        },
                        child: ClipRect(
                          clipBehavior: Clip.hardEdge,
                          child: FittedBox(
                            fit: BoxFit.cover,
                            child: SizedBox(
                              child: fileImages
                                  ? Image(image: FileImage(File(images[idx])))
                                  : CachedNetworkImage(imageUrl: images[idx]),
                            ),
                          ),
                        ),
                      ),
                    ),
                    if (enableDeleting)
                      Positioned(
                          right: -5,
                          top: -5,
                          child: IconButton(
                            icon: ImageIcon(AssetImage(
                                'images/icons/selection_bar/cross.png')),
                            color: GradientColors.pink,
                            onPressed: () {
                              if (deleteCallback != null) deleteCallback!(idx);
                            },
                          ))
                  ],
                );
              }),
          staggeredTileBuilder: (idx) {
            final length = images.length;
            if (length == 1) return StaggeredTile.count(12, 8);
            if (length == 2 || length == 4) {
              return StaggeredTile.count(6, 8);
            }
            if (length == 3) {
              if (idx == 0)
                return StaggeredTile.count(12, 8);
              else
                return StaggeredTile.count(6, 8);
            }
            if (length > 4) {
              if (idx < 3) return StaggeredTile.count(6, 8);
              if (idx == 3) return StaggeredTile.count(6, 4);
              return StaggeredTile.count(6, 4);
            }
            return null;
          }),
    );
  }
}
