import 'package:flutter/material.dart';
import 'package:teormech/styles/colors.dart';

class BulletPointerLabel extends StatelessWidget {
  const BulletPointerLabel({Key? key, required this.label}) : super(key: key);

  final String label;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration:
          BoxDecoration(shape: BoxShape.circle, color: GradientColors.pink),
      padding: EdgeInsets.all(4),
      child: Text(
        label,
        style: TextStyle(
            fontSize: 16, color: Colors.white, fontWeight: FontWeight.bold),
      ),
    );
  }
}
