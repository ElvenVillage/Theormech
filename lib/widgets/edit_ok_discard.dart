import 'package:flutter/material.dart';

typedef OkDiscardCallback = Future<bool> Function();

class OkDiscard extends StatefulWidget {
  static const int editing = 1;

  final OkDiscardCallback okCallback;
  final OkDiscardCallback cancelCallback;
  final OkDiscardCallback editCallback;

  OkDiscard(
      {required this.okCallback,
      required this.editCallback,
      required this.cancelCallback});

  @override
  _OkDiscardState createState() => _OkDiscardState();
}

class _OkDiscardState extends State<OkDiscard> {
  int _state = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 5),
      child: (_state == 0)
          ? IconButton(
              icon: Icon(
                Icons.edit,
              ),
              onPressed: () async {
                if (await widget.editCallback())
                  setState(() {
                    _state = OkDiscard.editing;
                  });
              })
          : Row(
              children: [
                IconButton(
                  icon: Icon(Icons.how_to_reg),
                  onPressed: () async {
                    if (await widget.okCallback())
                      setState(() {
                        _state = 0;
                      });
                  },
                ),
                IconButton(
                  icon: Icon(Icons.cancel_outlined),
                  onPressed: () async {
                    if (await widget.cancelCallback())
                      setState(() {
                        _state = 0;
                      });
                  },
                ),
              ],
            ),
    );
  }
}
