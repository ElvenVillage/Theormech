import 'package:flutter/material.dart';

class MultilineTextField extends StatelessWidget {
  const MultilineTextField(
      {Key? key, this.inARow = false, this.controller, this.collapsed = false})
      : super(key: key);

  final TextEditingController? controller;
  final bool inARow;

  final bool collapsed;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10, bottom: 10),
      child: TextField(
          minLines: collapsed ? 3 : 5,
          maxLines: collapsed ? 3 : 12,
          controller: controller,
          keyboardType: TextInputType.multiline,
          decoration: InputDecoration(
            isCollapsed: true,
            filled: true,
            contentPadding: EdgeInsets.fromLTRB(20.0, 12.0, 12.0, 10.0),
            fillColor: Colors.white,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(16.0),
            ),
          )),
    );
  }
}
