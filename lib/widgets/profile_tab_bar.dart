import 'package:flutter/material.dart';
import 'package:teormech/auth/ui/widgets/choice_container.dart';
import 'package:teormech/generated/l10n.dart';
import 'package:teormech/profile/profile/model/user_profile_state.dart';

class ProfileTabBar extends StatelessWidget {
  const ProfileTabBar({
    Key? key,
    required this.newsfeedCallback,
    required this.portfolioCallback,
    required this.projectsCallback,
    required this.chosen,
  }) : super(key: key);

  final VoidCallback newsfeedCallback;
  final VoidCallback portfolioCallback;
  final VoidCallback projectsCallback;

  final ProfilePage chosen;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(left: 4, right: 4, top: 10),
        child: Row(children: [
          Flexible(
            flex: 1,
            child: ChoiceContainer(
              text: S.of(context).newsfeed,
              chosen: chosen == ProfilePage.Newsfeed,
              style: ChoiceContainerStyle.defaultStyle(),
              onClickCallback: newsfeedCallback,
            ),
          ),
          Flexible(
            flex: 1,
            child: ChoiceContainer(
              text: S.of(context).portfolio,
              chosen: chosen == ProfilePage.Portfolio,
              style: ChoiceContainerStyle.defaultStyle(),
              onClickCallback: portfolioCallback,
            ),
          ),
          Flexible(
            flex: 1,
            child: ChoiceContainer(
              text: S.of(context).projects,
              chosen: chosen == ProfilePage.Projects,
              style: ChoiceContainerStyle.defaultStyle(),
              onClickCallback: projectsCallback,
            ),
          )
        ]));
  }
}
