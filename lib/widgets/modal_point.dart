import 'package:flutter/material.dart';

class ModalPoint extends StatelessWidget {
  final String? image;
  final String text;
  final VoidCallback callback;
  const ModalPoint({this.image, required this.text, required this.callback});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        callback();
      },
      child: Container(
        padding: const EdgeInsets.fromLTRB(30, 20, 20, 0),
        child: Row(
          children: [
            if (image != null)
              Image(
                image: AssetImage(image!),
                height: 32,
                width: 32,
              ),
            SizedBox(
              width: 20,
            ),
            Text(text)
          ],
        ),
      ),
    );
  }
}
