import 'dart:html' as html;

import 'package:teormech/rep/downloader.dart';

Future<void> download(
    {required String url,
    String? fileName,
    ProgressCallback? onReceiveProgress}) async {
  html.document.createElement('a') as html.AnchorElement
    ..href = url
    ..target = '_blank'
    ..dispatchEvent(html.Event.eventType('MouseEvent', 'click'));
}
