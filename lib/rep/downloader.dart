typedef ProgressCallback = void Function(int count, int total);

abstract class Downloader {
  static const DOWNLOAD_SEND_PORT = 'download_send_port';
}
