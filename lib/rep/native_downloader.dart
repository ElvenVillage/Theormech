import 'dart:io';
import 'dart:ui';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:path/path.dart' as path;
import 'package:dio/dio.dart' as diol;
import 'package:downloads_path_provider_28/downloads_path_provider_28.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:teormech/rep/downloader.dart';

Future<void> download(
    {required String url,
    String? fileName,
    ProgressCallback? onReceiveProgress}) {
  return NativeDownloader.download(
      url: url, fileName: fileName, onReceiveProgress: onReceiveProgress);
}

abstract class NativeDownloader extends Downloader {
  static Future<bool> checkPermission() async {
    final status = await Permission.storage.status;
    if (status != PermissionStatus.granted) {
      final result = await Permission.storage.request();
      if (result == PermissionStatus.granted) {
        return true;
      }
    } else {
      return true;
    }
    return false;
  }

  static void downloadCallback(
      String id, DownloadTaskStatus status, int progress) {
    final send =
        IsolateNameServer.lookupPortByName(Downloader.DOWNLOAD_SEND_PORT);
    send?.send([id, status, progress]);
  }

  static Future<void> _startDownload(
      {required diol.Dio dio,
      required String savePath,
      required bool preserveFileName,
      required String url,
      ProgressCallback? onReceiveProgress,
      Directory? dir}) async {
    await dio.download(
        url,
        !preserveFileName
            ? savePath
            : (diol.Headers headers) {
                return path.join(
                    dir?.path ?? '',
                    headers
                        .value('Content-Disposition')
                        ?.substring(22)
                        .replaceAll('"', ''));
              },
        onReceiveProgress: onReceiveProgress);
  }

  static Future<Directory?> getDownloadDirectory() async {
    if (Platform.isAndroid) {
      return await DownloadsPathProvider.downloadsDirectory;
    }

    return await getApplicationDocumentsDirectory();
  }

  static Future<void> download(
      {required String url,
      String? fileName,
      ProgressCallback? onReceiveProgress}) async {
    final dio = diol.Dio();
    final dir = await getDownloadDirectory();
    final isPermissionStatusGranted = await _requestPermissions();

    if (isPermissionStatusGranted) {
      final savePath = path.join(dir?.path ?? '', fileName);
      await _startDownload(
          dio: dio,
          savePath: savePath,
          preserveFileName: fileName == null,
          dir: dir,
          url: url,
          onReceiveProgress: onReceiveProgress);
    } else {
      // handle the scenario when user declines the permissions
    }
  }

  static Future<bool> _requestPermissions() async {
    var permission = await Permission.storage.isGranted;

    if (!permission) {
      await Permission.storage.request();
      permission = await Permission.storage.isGranted;
    }

    return permission;
  }
}
