import 'package:flutter/cupertino.dart';

Widget slideFadeTransition(BuildContext context, Animation<double> f1,
    Animation<double> f2, Widget child) {
  var opacityTween = Tween(begin: 0.0, end: 1.0);
  var opacityAnimation = opacityTween.animate(f1);

  return FadeTransition(
    opacity: opacityAnimation,
    child: child,
  );
}

Widget slideTransition(BuildContext context, Animation<double> f1,
    Animation<double> f2, Widget child) {
  var tween = Tween(begin: Offset(1, 0), end: Offset.zero);
  var offsetAnimation = tween.animate(f1);
  return SlideTransition(
    position: offsetAnimation,
    child: child,
  );
}
