import 'package:edge_alert/edge_alert.dart';
import 'package:flutter/material.dart';

showEdgeAlertNotification(
    {required String okText,
    required String errorText,
    required BuildContext context,
    required bool result}) {
  if (result) {
    EdgeAlert.show(context,
        title: 'Ok',
        description: okText,
        duration: EdgeAlert.LENGTH_SHORT,
        gravity: EdgeAlert.BOTTOM,
        backgroundColor: Colors.green,
        icon: Icons.supervisor_account);
  } else {
    EdgeAlert.show(context,
        title: 'Ошибка',
        duration: EdgeAlert.LENGTH_SHORT,
        icon: Icons.error_outline_rounded,
        backgroundColor: Colors.redAccent,
        description: errorText,
        gravity: EdgeAlert.BOTTOM);
  }
}
