import 'package:flutter/material.dart';
//import 'package:google_fonts/google_fonts.dart';

final h1TextStyle = TextStyle(fontFamily: 'Trebuchet', fontSize: 20.0);
//final h1TextStyle = GoogleFonts.;
final h2TextStyle = TextStyle(fontFamily: 'Trebuchet', fontSize: 16.0);

final h2DrawerGreyText = h2TextStyle.copyWith(color: Colors.grey.shade600);

final tStyle =
    TextStyle(decoration: TextDecoration.underline, color: Colors.white);

const profileTextStyle =
    TextStyle(color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.w400);

final appBarTextStyle = h1TextStyle.copyWith(color: Colors.white, fontSize: 18);
