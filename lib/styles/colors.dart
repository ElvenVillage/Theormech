import 'dart:ui';

class GradientColors {
  static const leftColorLeftButton = Color(0xFF001355);
  static const rightColorLeftButton = Color(0xFF042284);

  static const backWhite = Color(0xFFfcfcfc);

  static const documentsBackground = Color(0xFFe6f7ff);

  static const leftColorRightButton = Color(0xFF0164c7);
  static const rightColorRightButton = Color(0xFF029ee8);

  static const backColor = Color(0xFF123cdc);

  static const updatingIndicatorColor = Color(0x66000000);

  static const pink = Color(0xFFFF6666);
}
