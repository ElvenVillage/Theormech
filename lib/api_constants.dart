const releasePrefix = 'https://tm.newpage.xyz';

class AuthApi {
  static const auth = releasePrefix + '/api/auth/';
  static const logout = releasePrefix + '/api/logout/';
  static const reg = releasePrefix + '/api/reg/';
  static const checkEmail = releasePrefix + '/api/check_email/';
  static const fetchPreparedData = releasePrefix + '/api/postreg_info/';
  static const postreg = releasePrefix + '/api/confirm_postreg/';
}

class ProfileApi {
  static const userInfo = releasePrefix + '/api/user_info/';
  static const uploadAvatar = releasePrefix + '/api/upload_profile_photo/';
  static const profileAvatar = releasePrefix + '/profile_files/';
  static const profileAvatarMin = releasePrefix + '/profile_files_min/';
  static const editProfile = releasePrefix + '/api/update_profile/';
  static const fetchPrivacy = releasePrefix + '/api/show_priv/';
  static const updatePrivacy = releasePrefix + '/api/update_priv/';
  static const followUser = releasePrefix + '/api/add_following/';
  static const subsUser = releasePrefix + '/api/sub_num/';
  static const profileFilesmin = releasePrefix + '/profile_files_min/';
  static const forgotPassword = releasePrefix + '/api/forgot_password/';
  static const forgotPasswordInfo =
      releasePrefix + '/api/forgot_password_token_info';
  static const changeForgotPassword =
      releasePrefix + '/api/change_forgot_password/';

  static const setLimit = releasePrefix + '/api/set_limits/';
  static const setCustomLimits = releasePrefix + '/api/set_custom_limits/';
}

class SubscriptionsApi {
  static const userFollowers = releasePrefix + '/api/get_followers/';
  static const userFollowings = releasePrefix + '/api/get_followings/';
  static const searchUser = releasePrefix + '/api/search_profile/';
}

class ChatApi {
  static const showChats = releasePrefix + '/api/show_chats/';
  static const chatMessages = releasePrefix + '/api/chat_messages/';
  static const createMessage = releasePrefix + '/api/create_message/';
  static const showAlbum = releasePrefix + '/api/show_album/';
  static const showAttachments = releasePrefix + '/api/show_files/';
  static const chatPhotosMin = releasePrefix + '/chat_photos_min/';
  static const chatPhotosMax = releasePrefix + '/chat_photos/';
  static const editMessage = releasePrefix + '/api/edit_message/';
  static const forwardMessage = releasePrefix + '/api/forward_chat_message/';
  static const chatUnreadMessages =
      releasePrefix + '/api/count_chat_unread_message';
  static const readChatMessages = releasePrefix + '/api/read_chat_message/';
  static const saveMessage = releasePrefix + '/api/save_message/';
  static const unsaveMessage = releasePrefix + '/api/unsave_message/';
  static const deleteMessage = releasePrefix + '/api/delete_message/';
  static const mySaves = releasePrefix + '/api/my_saves/';
  static const mailings = releasePrefix + '/api/show_warnings/';
  static const searchChats = releasePrefix + '/api/search_chat/';
  static const banUser = releasePrefix + '/api/ban_user/';
  static const showBan = releasePrefix + '/api/show_ban/';
  static const setLimits = releasePrefix + '/api/set_custom_limits';
  static const getMyLimits = releasePrefix + '/api/get_my_limits/';

  static const allChatPhotos = releasePrefix + '/api/chat_photos/';
  static const allChatAttachmets = releasePrefix + '/api/chat_attachments/';

  static const chatFiles = releasePrefix + '/chat_files/';

  static const sendMailing = releasePrefix + '/api/create_mailing/';

  static const getChatLink =
      releasePrefix + '/api/create_group_chat_join_link/';
  static const joinChatFromLink =
      releasePrefix + '/api/join_by_link_to_group_chat/';

  static const countWarnings = releasePrefix + '/api/count_warnings/';
  static const readWarnings = releasePrefix + '/api/read_warnings/';
}

class GroupChatsApi {
  static const createChat = releasePrefix + '/api/create_group_chat/';
  static const showGroupChats = releasePrefix + '/api/show_group_chats/';
  static const addToGroupChat = releasePrefix + '/api/add_to_group_chat';
  static const allChatPhotos = releasePrefix + '/api/group_chat_photos/';
  static const chatMessages = releasePrefix + '/api/group_chat_messages/';
  static const sendChatMessage = releasePrefix + '/api/create_group_message/';
  static const leaveGroupChat = releasePrefix + '/api/leave_group_chat/';
  static const groupChatMember = releasePrefix + '/api/group_chat_members/';
  static const groupChatAdmins = releasePrefix + '/api/group_chat_admins/';
  static const editGroupChatMessage =
      releasePrefix + '/api/edit_group_message/';
  static const saveGroupMessage = releasePrefix + '/api/save_group_message/';
  static const groupChatPhotos = releasePrefix + '/api/group_chat_photos/';
  static const groupChatAttachments =
      releasePrefix + '/api/group_chat_attachments';
  static const countGroupChatUnread =
      releasePrefix + '/api/count_group_chat_unread_message/';
  static const readGroupChat = releasePrefix + '/api/read_group_chat_message/';

  static const groupChatLogoMin = releasePrefix + '/group_chat_logo_min/';
  static const groupChatLogoMax = releasePrefix + '/group_chat_logo/';

  static const uploadGroupChatPhoto =
      releasePrefix + '/api/upload_group_photo/';
  static const adminRemover =
      releasePrefix + '/api/admin_remover_in_group_chat/';
}

class PortfolioApi {
  static const showPortfolio = releasePrefix + '/api/show_resume/';
  static const addResumeInfo = releasePrefix + '/api/add_resume_info/';

  static const showPortfolioPublications =
      releasePrefix + '/api/show_resume_publications/';
  static const showPortfolioAchievements =
      releasePrefix + '/api/show_resume_achievement/';

  static const addPortfolioAchievement =
      releasePrefix + '/api/add_achievement/';
  static const addPortfolioPublication =
      releasePrefix + '/api/add_publication/';
  static const removePortfolioAchievement =
      releasePrefix + '/api/delete_achievement/';
  static const removePortfolioPublication =
      releasePrefix + '/api/delete_publication/';

  static const allTags = releasePrefix + '/api/show_all_tags/';
  static const allUserTags = releasePrefix + '/api/show_all_user_tags/';
  static const searchTag = releasePrefix + '/api/search_tag/';

  static const pinTag = releasePrefix + '/api/pin_tag/';
  static const unpinTag = releasePrefix + '/api/unpin_tag/';

  static const showResumeTags = releasePrefix + '/api/show_resume_tags/';
  static const showResumeUserTags =
      releasePrefix + '/api/show_resume_user_tags/';

  static const addUserTag = releasePrefix + '/api/add_user_tag/';
  static const removeUserTag = releasePrefix + '/api/delete_user_tag/';

  static const getPdfResume = releasePrefix + '/api/get_pdf_resume/';

  static const searchResume = releasePrefix + '/api/search_resume/';

  static const achievementFiles = releasePrefix + '/achievement_files/';
  static const publicationFiles = releasePrefix + '/publication_files/';
}

class SchedulerApi {
  static const getSchedule = releasePrefix + '/api/get_schedule/';
  static const addNote = releasePrefix + '/api/add_note/';
  static const deleteNote = releasePrefix + '/api/delete_note/';
}

class EventsApi {
  static const getEvents = releasePrefix + '/api/get_events/';
  static const showEvent = releasePrefix + '/api/show_event/';
  static const eventAvatar = releasePrefix + '/event_photo/';
  static const eventTags = releasePrefix + '/api/show_event_tags/';
  static const confirmQR = releasePrefix + '/api/confirm_qr/';
  static const likeEvent = releasePrefix + '/api/like_event/';
  static const eventPhoto = releasePrefix + '/event_photo/';
  static const sendEvent = releasePrefix + '/api/send_event/';
  static const showLikers = releasePrefix + '/api/show_likers/';

  static const showAllEventTags = releasePrefix + '/api/show_all_event_tags/';
  static const searchEventTags = releasePrefix + '/api/search_event_tags/';

  static const showInterestTags = releasePrefix + '/api/show_interest_tags';
  static const removeInterestTag = releasePrefix + '/api/remove_interest_tag';
  static const pinInterestTag = releasePrefix + '/api/add_interest_tag';
}

class VacanciesApi {
  static const getVacancy = releasePrefix + '/api/get_vacancy/';
  static const getVacancies = releasePrefix + '/api/get_vacancies/';
  static const vacancyResponse = releasePrefix + '/api/vacancy_response/';
  static const createVacancy = releasePrefix + '/api/create_vacancy/';
  static const sendVacancyToChat = releasePrefix + '/api/send_vacancy_to_chat/';

  static const addVacancyTag = releasePrefix + '/api/add_vacancy_tag/';
  static const deleteVacancyTag = releasePrefix + '/api/delete_vacancy_tag/';
  static const getVacancyTags = releasePrefix + '/api/show_vacancy_tags/';
}

class ProjectsApi {
  static const getProjects = releasePrefix + '/api/show_all_projects/';
  static const getProject = releasePrefix + '/api/show_project/';

  static const getProjectTags = releasePrefix + '/api/show_project_tags/';
  static const pinProjectTag = releasePrefix + '/api/pin_project_tag/';
  static const unpinProjectTag = releasePrefix + '/api/unpin_project_tag/';

  static const getProjectMembers = releasePrefix + '/api/show_project_members/';

  static const getProjectFeed = releasePrefix + '/api/show_project_feed/';
  static const getProjectList = releasePrefix + '/api/show_my_projects/';

  static const publicateProjectPost =
      releasePrefix + '/api/publicate_project_feed/';
  static const createProject = releasePrefix + '/api/create_project/';
  static const editProject = releasePrefix + '/api/edit_project/';

  static const getProjectPhoto = releasePrefix + '/project_photo/';
  static const getFeedPhotos = releasePrefix + '/api/get_feed_photos/';
  static const getFeedFiles = releasePrefix + '/api/get_feed_files/';
  static const showFeedPhotos = releasePrefix + '/project_feed_photos/';
  static const showFeedFiles = releasePrefix + '/project_feed_files/';

  static const getProejctFeedPhotosList =
      releasePrefix + '/api/get_project_photos/';
  static const getProjectFeedFilesList =
      releasePrefix + '/api/get_project_files/';

  static const inviteProjectMember = releasePrefix + '/api/invite_to_project/';
  static const deleteProjectMember =
      releasePrefix + '/api/delete_project_member/';

  static const uploadProjectPhoto =
      releasePrefix + '/api/upload_project_photo/';

  static const editStatus = releasePrefix + '/api/edit_members/';

  static const sendProject = releasePrefix + '/api/send_project/';
  static const leaveProject = releasePrefix + '/api/leave_project/';

  static const inviteToProject = releasePrefix + '/api/invite_to_project/';
  static const showMyInvites = releasePrefix + '/api/show_my_invites/';
  static const requestToProject = releasePrefix + '/api/request_to_project/';
  static const showProjectInvites =
      releasePrefix + '/api/show_project_invites/';

  static const approveMyRequestToProject =
      releasePrefix + '/api/approve_my_request_to_project/';
  static const dismissMyRequestToProject =
      releasePrefix + '/api/dismiss_my_request_to_project/';
  static const approveRequestToProject =
      releasePrefix + '/api/approve_request_to_project/';
  static const dismissRequestToProject =
      releasePrefix + '/api/dismiss_request_to_project/';
}

class BellApi {
  static const getBell = releasePrefix + '/api/get_bell/';
  static const readBell = releasePrefix + '/api/read_bell/';
}
