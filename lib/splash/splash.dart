import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teormech/auth/bloc/auth_bloc.dart';
import 'package:teormech/auth/ui/login.dart';
import 'package:teormech/profile/profile/ui/widgets/avatar/clipped_avatar.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:wave/config.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:wave/wave.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key? key}) : super(key: key);
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  late final AnimationController controller;
  late final AnimationController cloudOpacityController;

  late final Animation<double> cloudOpacityAnimation;

  @override
  void initState() {
    super.initState();
    UserAvatarComponentsUtils.init();
    controller =
        AnimationController(duration: Duration(seconds: 2), vsync: this);

    cloudOpacityController =
        AnimationController(duration: Duration(seconds: 2), vsync: this);

    cloudOpacityAnimation = Tween<double>(begin: 0, end: 100).animate(
        CurvedAnimation(parent: cloudOpacityController, curve: Curves.easeIn))
      ..addListener(() {
        setState(() {});
      });
  }

  @override
  void dispose() {
    controller.dispose();
    cloudOpacityController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            body: GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      behavior: HitTestBehavior.translucent,
      child: SingleChildScrollView(
        child: ConstrainedBox(
          constraints: BoxConstraints(
              maxHeight: MediaQuery.of(context).size.height -
                  MediaQuery.of(context).padding.top),
          child: Stack(children: [
            Positioned.fill(
              child: Image(
                image: AssetImage('images/avtor.png'),
                fit: BoxFit.cover,
                height: MediaQuery.of(context).size.height,
              ),
            ),
            Column(children: [
              Expanded(
                  flex: 1,
                  child: _StaggerAnimation(
                    controller: controller,
                  )),
              Expanded(
                  flex: 2,
                  child: Navigator(
                    onGenerateRoute: (settings) {
                      if (settings.name == '/login') {
                        return PageRouteBuilder(
                            fullscreenDialog: false,
                            transitionDuration: Duration(seconds: 2),
                            pageBuilder: (context, a1, a2) => LoginPage(),
                            transitionsBuilder: (context, a1, a2, child) {
                              final opacityTween = Tween(begin: 0.0, end: 1.0);
                              final opacityAnimation = opacityTween.animate(a1);
                              return FadeTransition(
                                opacity: opacityAnimation,
                                child: child,
                              );
                            });
                      }
                      if (settings.name == '/') {
                        return MaterialPageRoute(
                            fullscreenDialog: false,
                            builder: (context) =>
                                BlocListener<AuthBloc, AuthState>(
                                    listener: (context, state) {
                                      if (state is Unathorized) {
                                        controller.forward();
                                        Navigator.of(context)
                                            .pushReplacementNamed('/login');
                                        cloudOpacityController.forward();
                                      }
                                    },
                                    child: Column(children: [
                                      Spacer(),
                                      LayoutBuilder(
                                          builder: (context, constraints) =>
                                              Transform(
                                                  transform: Transform.translate(
                                                          offset: Offset(
                                                              0,
                                                              cloudOpacityAnimation
                                                                  .value))
                                                      .transform,
                                                  child: WaveWidget(
                                                    config: CustomConfig(
                                                      gradients: [
                                                        [
                                                          Colors.black,
                                                          Colors.white38
                                                        ],
                                                        [
                                                          Colors.blueGrey,
                                                          Colors.white
                                                        ],
                                                        [
                                                          Colors.white60,
                                                          Colors.blueGrey
                                                        ],
                                                        [
                                                          Colors.white70,
                                                          Colors.white
                                                        ]
                                                      ],
                                                      durations: [
                                                        35000,
                                                        19440,
                                                        10800,
                                                        6000
                                                      ],
                                                      heightPercentages: [
                                                        0.20,
                                                        0.23,
                                                        0.25,
                                                        0.30
                                                      ],
                                                      blur: MaskFilter.blur(
                                                          BlurStyle.solid, 10),
                                                      gradientBegin:
                                                          Alignment.bottomLeft,
                                                      gradientEnd:
                                                          Alignment.topRight,
                                                    ),
                                                    waveAmplitude: 0,
                                                    heightPercentange: 0.25,
                                                    size: Size(
                                                        constraints.maxWidth,
                                                        100),
                                                  )))
                                    ])));
                      }
                      return MaterialPageRoute(
                          builder: (context) => Scaffold());
                    },
                    initialRoute: '/',
                  ))
            ])
          ]),
        ),
      ),
    )));
  }
}

class _StaggerAnimation extends StatelessWidget {
  _StaggerAnimation({Key? key, required this.controller})
      : transitionMain = Tween<double>(begin: 80, end: 50)
            .animate(CurvedAnimation(parent: controller, curve: Curves.easeIn)),
        super(key: key);

  final AnimationController controller;

  final Animation<double> transitionMain;

  Widget _buildAnimation(BuildContext context, Widget? child) {
    return Column(
      children: [
        Spacer(
          flex: 3,
        ),
        Transform(
          transform:
              Transform.translate(offset: Offset(0, transitionMain.value))
                  .transform,
          child: Hero(
              tag: 'main-logo-splash',
              child: Image(
                image: AssetImage('images/logo.png'),
                //width: MediaQuery.of(context).size.width / 2,
                height: MediaQuery.of(context).size.height / 4,
                //height: 200,
              )),
        ),
        //Spacer(flex: 1),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(animation: controller, builder: _buildAnimation);
  }
}
